<?php

/**
 * ArrayHelper helps yourself 
 * 
 */

class ArrayHelper {

	/**
     * When acc Challenge leaderboard must be calculated here it is set the attribtue to compare from ranks
     * @var String $key
     */
    protected $key;

    /**
     * @internal 
     */
    public function __construct( $key=null ) {
        if( $key )
            $this->key = $key;

        //parent::__construct();
    }

    public function cmp($a, $b) {
        $key = $this->key;
            

            //print_r( $a );
            //print_r( $b );
            //die;
        if ($a->{$key} == $b->{$key}) {
            return 0;
        }
        return ( $a->{$key} < $b->{$key} ) ? -1 : 1;
    }

	/**
	 * Helping yoou to remove stuff from Array of objects
	 * 
	 * @param  [String] $elem  	[The attribute value to match]
	 * @param  [Array] $array 	[The dirty array]
	 * @param  [String] $name  	[The name of the attribute to match by the object]
	 * @return [Array]       	[Your clean array]
	 */
	public static function deleteFromArray($elem, $array, $name){
        $new = array();
        foreach ($array as $value) {
            if( $value->{$name} == $elem ) { }
            
            else 
                $new[]=$value;
        }
        return $new;
    }

	/**
	 * Helping you to remove stuff from Array of objects
	 * 
	 * @param  [String] $elem  	[The attribute value to match]
	 * @param  [Array] $array 	[The dirty array]
	 * @param  [String] $name  	[The name of the attribute to match by the object]
	 * @return [Array]       	[Your clean array]
	 */
    public static function deleteIdFromArray($id, $array){
        $new = array();
        foreach ($array as $value) {
            if( $value == $id ) { }
            
            else 
                $new[]=$value;
        }
        return $new;
    }

    /**
	 * Helping you to remove stuff from Array of objects
	 * 
	 * @param  [String] $elem  	[The attribute value to match]
	 * @param  [Array] $array 	[The dirty array]
	 * @param  [String] $name  	[The name of the attribute to match by the object]
	 * @return [Array]       	[Your clean array]
	 */
    public static function sumArrayAttribute($attr, $array){
        $sum=0;
        //print_r($array[0]); die;
        if( isset($array[0]->{$attr}) )
	        foreach ($array as $value) {
	        	$sum = $sum + $value->{$attr};
	        }

        return $sum;
    }



}

?>