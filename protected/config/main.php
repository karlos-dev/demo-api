<?php

/**
 *  @abstract DEV NOTES
 * 
 *     Cache server must be installed or set cache-config as false
 *     The log is not active for prod
 *     The error must be active for prod
 * 
 *     No notice for missing db config. 
 *     The application will lauch a mongo connection error 
 * 
 *     Before deployment:
 *     Ensure cache ip and password
 * 
 */

$dbName =               !empty( getenv('API_MONGO_DB_NAME') ) ? getenv('API_MONGO_DB_NAME') : 'dbname';
$dbConnectionString =   !empty( getenv('API_MONGO_CONNECTION_STRING') ) ? getenv('API_MONGO_CONNECTION_STRING') : 'connection_string';
$dbReplica =            !empty( getenv('API_MONGO_REPLICA') ) ? getenv('API_MONGO_REPLICA') : 'rs-id_replica';
$maintenance =          !empty( getenv('API_MAINTENANCE') ) ? getenv('API_MAINTENANCE') : 0;

/**
 * Modified for demo purposes
 * @var $pushAppId
 */
$pushAppId = !empty( getenv('PUSH_APP_ID') ) ? getenv('PUSH_APP_ID') : 'test_application_id';
if( !$pushAppId ) {
  echo 'No PUSH_APP_ID set in user environment.';
  exit;
}
/**
 * Modified for demo purposes
 * @var $pushAccountId
 */
$pushAccountId = !empty( getenv('PUSH_ACC_ID') ) ? getenv('PUSH_ACC_ID') : 'test_account_id';
if( !$pushAccountId ) {
  echo 'No PUSH_ACC_ID set in user environment.';
  exit;
}

/**
 * Modified for demo purposes
 * @var $pushAppSecret
 * @var $pushAppMasterSecret
 * @var $pushAccountSecret
 * @var $pushAccountMasterSecret
 */

$pushAppSecret = !empty( getenv('PUSH_APP_SECRET') ) ? getenv('PUSH_APP_SECRET') : 'string_secret';
$pushAppMasterSecret = !empty( getenv('PUSH_APP_MASTER_SECRET') ) ? getenv('PUSH_APP_MASTER_SECRET') : 'string_secret';

$pushAccountSecret = !empty( getenv('PUSH_ACC_SECRET') ) ? getenv('PUSH_ACC_SECRET') : 'string_secret';
$pushAccountMasterSecret = !empty( getenv('PUSH_ACC_MASTER_SECRET') ) ? getenv('PUSH_ACC_MASTER_SECRET') : 'string_secret';

/**
 * This configuration helps the emails sent by the api have consistency for mail servers.
 * Modified for demo purposes
 * @var $returnMailUrl
 */
$returnMailUrl = !empty( getenv('MAIL_RETURN_ADDR') ) ? getenv('MAIL_RETURN_ADDR') : 'mail_return_address';

date_default_timezone_set('GMT');


if( !$dbName ) {
  echo 'No API_MONGO_DB_NAME set in user environment.';
  exit;
}

if( !$dbConnectionString ) {
  echo 'No API_MONGO_CONNECTION_STRING set in user environment.';
  exit;
}

if( !$returnMailUrl ) {
  echo 'No MAIL_RETURN_ADDR set in user environment.';
  exit;
}

if( !$pushAppId || !$pushAppSecret || !$pushAppMasterSecret ) {
  echo 'No PUSH_APP config set in user environment.';
  exit;
}

//define("PUSH_APP_ID",               '53f72ea7d535cf631e00002f');
//define("PUSH_APP_SECRET",           'f48d819cd6c39e487db9aa0d56d187ea');
//define("PUSH_APP_MASTER_SECRET",    'df36eee2054e432c45485c26dfa5d84f');

return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'WOO Sports REST API',
    //'version'=>$version,
	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
        'application.modules.oauth2.*',
        'application.modules.oauth2.controllers.*',
        'application.modules.aws.controllers.*',
        'application.modules.statistics.*',
        'application.modules.oauth2.models.*',
		'application.components.*',
        'application.extensions.phpmailer.*',
        'application.helpers.*',
        'application.extensions.mongoyii.*',
        'application.extensions.mongoyii.validators.*',
        'application.extensions.mongoyii.behaviors.*',
        'application.extensions.mongoyii.util.*',
        'application.extensions.redis.*',
        'application.extensions.redis.commands.*',
        'application.controllers.RedisController',
        'ext.s3.*',
	),

	'modules'=>array(
        'oauth2'=>array(
            //'class'=>
        ),
        'statistics'=>array(
            //'class'=>
        ),
	),

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
            //'class'=>'WebUser',
		),

        /**
         * CACHING CONFIG
         * Catching is a very important part of this API, 
         * The infrastructre has a separated server for caching which is accessed using private IP 
         * on top of the internal network interface of the server instance.
         * 
         * A Php Extension to work with Redis is required (see extensions folder)
         * 
         * @var $returnMailUrl
         */
        "redis" => array(
            "class" => "application.extensions.redis.ARedisConnection",
            "hostname" => "redis_host_ip",
            "port" => 6379,
            "database" => 1,
            "password" =>  "password_hash_redis",
            "prefix" => "WOO."
        ),
        
		'urlManager'=>array(            
			'urlFormat'=>'path',
            'showScriptName'=>false,
			'rules'=>array(          

                // REST patterns
                // array("api", 'pattern'=>'api/<model:\w+>', 'verb'=>'GET'),
                
                'status'=>'oauth2/com/status',
                'site/<action:\w+>'=>'site/<action>',
                'site/<action:\w+>/<id:\w+>'=>'site/<action>',
                
                'statistics'=>'statistics/manager',
                'statistics/<action:\w+>'=>'statistics/<action>',
                'statistics/<action:\w+>/<id:\w+>'=>'statistics/<action>/<id>',
                'statistics/<action:\w+>/<id:\w+>/<key:\w+>'=>'statistics/<action>/<id>/',
                
                '/token'=>'oauth2/oauth2/access_token',
                '/authorize'=>'oauth2/oauth2/authorize',
                '/register'=>'oauth2/appuser/register',
                '/authorize/<param:\w+>/<clien_id:\w+>/'=>'oauth2/oauth2/authorize',
                
                'user/<action:(create|profile)>'=>'oauth2/appuser/<action>',
                'user/<action:(update|following|followers|isfollowed|profile|health)>/<id:\w+>'=>'oauth2/appuser/<action>',
                'user/<action:(account)>/<account:\w+>'=>'oauth2/appuser/<action>',
                'user/<action:(notifications)>/<command:\w+>'=>'oauth2/appuser/<action>',

                'friend/<action>'=>'oauth2/friend/<action>',
                'friend/<action>/<id:\w+>'=>'oauth2/friend/<action>',

                'user/<action>'=>'oauth2/appuser/<action>',
                
                'session/<action>'=>'oauth2/session/<action>',
                'session/<action>/<id:\w+>'=>'oauth2/session/<action>',
                'session/<id:\w+>'=>'oauth2/session/index',
                
                'session'=>'oauth2/session',
                
                //'spot/<action:(create|detail)>'=>'oauth2/spot/<action>',
                //'spot/<action>/<id:\w+>'=>'oauth2/spot/<action>',
                'spot/<action:\w+>/<id:\w+>'=>'oauth2/spot/<action>',
                'spot/<action:\w+>'=>'oauth2/spot/<action>',    
                'spot/'=>'oauth2/spot/index',                
                'spot/<id:\w+>'=>'oauth2/spot/index',            

                'sensor'=>'oauth2/sensor/index',                
                'sensor/<action:[a-zA-Z-]+>'=>'oauth2/sensor/<action>',
                'sensor/<action:[a-zA-Z-]+>/<id:[\s\S]+>'=>'oauth2/sensor/<action>',
                'sensor/<id:\w+>'=>'oauth2/sensor/index',
                
                'comment/<id:\w+>'=>'oauth2/comment/index',
                'like/<id:\w+>'=>'oauth2/like/index',
                'user/<id:\w+>'=>'oauth2/appuser/view',

                'image/<action>/<id:\w+>/<_type>'=>'oauth2/image/<action>',
                'image/<action>/<id:\w+>/<_type>/<_id>'=>'oauth2/image/<action>',
                'image/<action>'=>'oauth2/image/<action>',
                'image/<id:\w+>/<_type>'=>'oauth2/image/index',
                'continent'=>'oauth2/continent/index',                
                'continent/<action>'=>'oauth2/continent/<action>',                
                'continent/<action>/<name:[a-zA-Z-]+>'=>'oauth2/continent/<action>',

                //'leaderboard'=>'oauth2/continent/index',                
                'leaderboard/<action>'=>'oauth2/leaderboard/<action>',                
                'leaderboard/<action>/<id:\w+>'=>'oauth2/leaderboard/<action>',                
                'firmware/<action>'=>'oauth2/firmware/<action>',                

                'challenge/<action>'=>'oauth2/challenge/<action>',            
                'challenge/<action>/<id:\w+>'=>'oauth2/challenge/<action>',                    
                //'continent/<action>/<name:[a-zA-Z-]+>'=>'oauth2/continent/<action>',

                'friend/<action:\w+>'=>'oauth2/friend/<action>',
                '<controller:\w+>/<action:\w+>'=>'oauth2/<controller>/<action>',
                '<controller:\w+>/<action:\w+>/<id:\w+>'=>'oauth2/<controller>/<action>',
                
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',  
                //'<controller:\w+>/<action:\w+>/<feature:\w+>'=>'<controller>/<action>/<feature>',
                '<controller:\w+>/<action:\w+>/<id:\w+>'=>'<controller>/<action>',
                '<controller:\w+>/<action:\w+>/<id:\w+>/<pos:\w+>'=>'<controller>/<action>',
                '<controller:\w+>/<action:\w+>/<id:\w+>/<ini:\w+>/<end:\w+>'=>'<controller>/<action>'
			),
		),

        'mongodb' => array(
            'class' => 'EMongoClient',
            'server' => $dbConnectionString,
            'db' => $dbName,
            'options' => array(
                'replicaSet' => $dbReplica
            ),
            'RP' => array('RP_PRIMARY_PREFERRED', array())
        ),
        
        
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'oauth2/com/error',
		),
        
       
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error',
				),                
			),
		),

        'push'=>array(
            'class'=>'Notification',
            'pushId'=>$pushAppId,
            'pushIdSecret'=>$pushAppSecret,
            'pushIdMasterSecret'=>$pushAppMasterSecret,

            'pushAccountId'=>$pushAccountId,
            'pushAccountSecret'=>$pushAccountSecret,
            'pushAccountMasterSecret'=>$pushAccountMasterSecret,
        ),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
     
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'karlos.alvan@gmail.com',
        /**
         * Returns a message to client so they display that servers are currently under maintenance mode
         * @var $params['maintenance']
         */
        'maintenance'=>$maintenance,
        /**
         * Mobile versions supported for this API
         * This is a key feature for scaling and system evolution, as the communications evolve, 
         * API endpoints may modify the response, so old versions might stop working.
         * This must be handled by the web service.
         * 
         * It is used to keep backwards compatibility and control which API server serves
         * which versions of the application
         * 
         * @var $params['supportedVersions']
         */
        'supportedVersions' => array('1.7','1.8','1.9'),
        'tcversion' => '1.1',
        /**
         * Enable | disable Caching
         * Modified for demo purposes
         * 
         * @var $params['redis_enabled']
         */
        'redis_enabled' => false,
        /**
         *  Mayor changes are tracked in this version array
         *  @var string
         */
        'version_check' => '1.8.1',
        'roles' => array('user','dev','spotmanager','admin'),
        'version' => '1.4.3',
        'pushurl' => 'https://www.pushtech.com/api/v1/',
        'mailreturn'=>$returnMailUrl,
        'dbn' => $dbName,
        'dbc' => $dbConnectionString,
        'kotalink'=>false,
	),
);
