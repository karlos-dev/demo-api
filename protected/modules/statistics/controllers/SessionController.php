<?php

/**
 *  Session Statistics Controller
 *  
 *  @author Karlos Álvarez
 *  @module Statistics
 *  
 *  @abstract Implements statistic functions on Session Collection
 *  
 */

class SessionController extends Controller {

    public function actionActivity(){

    }

    public function actionIndex(){
        echo "I'm {$this->id}Controller";
        Yii::app()->end();
    }

}

?>