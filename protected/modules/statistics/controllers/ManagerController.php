<?php

/**
 *  Manager Statistics Controller
 *  
 *  @author Karlos Álvarez
 *  @module Statistics
 *  
 *  @abstract Manager for statistic functions 
 *  
 */

class ManagerController extends Controller {

    public function actionIndex(){
        //echo "I'm {$this->id}Controller";
        $this->render('index');
        Yii::app()->end();
    }

}

?>