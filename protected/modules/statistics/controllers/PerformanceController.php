<?php

/**
 *  Performance Statistics Controller
 *  
 *  @author Karlos Álvarez
 *  @module Performance Statistics and Status
 *  
 *  @abstract Performance functions 
 *  
 */

// Include test module for tests
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . "..".DIRECTORY_SEPARATOR. "..".DIRECTORY_SEPARATOR. "test".DIRECTORY_SEPARATOR."Parallel.php";
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . "..".DIRECTORY_SEPARATOR. "..".DIRECTORY_SEPARATOR. "test".DIRECTORY_SEPARATOR."ParallelNonBlockingCurl.php";

class PerformanceController extends ComController {

    public $magic;
    public $results = array( 'max'=> 0, 'min' => 99999 );
    //private $token = '9fd6abf171b5f2835e02cd473365aca8';

    /*
    static private $instance = false;

    public function instance(){
        if (PerformanceController::$instance === false) {
            PerformanceController::$instance = new PerformanceController(1);
        }
        return PerformanceController::$instance;
    }
    */
   
    /**
     * I define the callback for Parallel Curl Tests
     * @return [type] [description]
     */
    public function init(){
        $this->magic = function($result, $value) {
            /*
            print_r( $result );
            echo "<br>";
            print_r( $value );
            echo "<br>";
            */
           
            //$pc = $pc->instance();
            //$array = Yii::app()->params['performance_results'];
            
            //Yii::app()->params['performance_results']['max'] = ( Yii::app()->params['performance_results']['max'] < $value['total_time'] ) ? $value['total_time'] : Yii::app()->params['performance_results']['max'];
            //Yii::app()->params['performance_results']['min'] = ( Yii::app()->params['performance_results']['min'] > $value['total_time'] ) ? $value['total_time'] : Yii::app()->params['performance_results']['min'];
            
            //$value = json_decode( $requestInfo );
            //$max = ( $max > $value['total_time'] ) ? $max : $value['total_time'];
            //$min = ( $min < $value['total_time'] ) ? $min : $value['total_time'];
            $class = ( $value['http_code'] == 200 || $value['http_code'] == 201 ) ? 'ok' : 'fail';
                    
            echo "<div class=\"result-list {$class}\"><div>StatusCode {$value['http_code']}&nbsp</div><div>Time {$value['total_time']}&nbsp</div><div>Request {$value['url']}&nbsp</div><div>bytes {$value['size_download']}&nbsp</div></div>";
            if( $class == 'fail')
                echo "<div class=\"result-list {$class}\"><div>Result: {$result}</div></div></br>";
            
        };
    }

    public function actionIndex(){
        $this->render('index');
        Yii::app()->end();
    }

    private function parseResultArray($hugeArray){
        $min = 99999;
        $max = 0;
        $sum = 0;
        $total = 0;
        foreach ($hugeArray as $str) {
                //$utilArray[] = 
                $keys = array_keys( get_object_vars($str) );
                foreach ($keys as $key) {
                    $obj = $str->{$key};

                    //print_r($obj); die;
                    
                    $results = json_decode($obj->result, true);
                    echo "<div class=\"result-pad\">";
                    foreach ($results as $key => $value) {
                        $max = ( $max > $value['total_time'] ) ? $max : $value['total_time'];
                        $min = ( $min < $value['total_time'] ) ? $min : $value['total_time'];
                        $class = ( $value['response_code'] == 200 || $value['response_code'] == 201 ) ? 'ok' : 'fail';
                        
                        echo "<div class=\"result-list {$class}\"><div>StatusCode {$value['response_code']}&nbsp</div><div>Time {$value['total_time']}&nbsp</div><div>Request {$value['url']}&nbsp</div><div>bytes {$value['bytes_transfered']}&nbsp</div></div>";
                        $total++;
                        $sum += $value['total_time'];
                    }
                    echo "</div>";
                }
            }
        return array( 'max' => $max, 'min' => $min, 'mean' => ($sum/$total) );
    }

    

    public function actionFuntest() {  

        $token = ( isset($_POST['tkn'] )) ? $_POST['tkn'] : null;
        $threads = ( isset($_POST['threads'] )) ? $_POST['threads'] : 1;
        
        $this->magic = function($result, $value) {
            echo '<div class="result-pad">' . $result . '</div>';
        };
        
        $n = $threads;
        $start = microtime(true);
        ob_start();

        $layoutFile=$this->getLayoutFile('//layouts/main') ;
        echo '<style>   
                        .result-pad { padding: 10px; }
                        .result-list div { display: inline-block; } 
                        .result-list.ok { color: green; } 
                        .result-list.fail { color: red; } 

            </style>';

        try {

            $total = $n*18;
            echo "<div class=\"result-pad\"><div>Test application for {$n} User.  </div>";
            echo "<div>Send 3 parallel requests generating sub-parallel request for testing every controller action. </div>";
            echo "<div> Session [7 requests], User [5 requests], Continent [6 requests]  = 18 parallel threads per User </div>";
            echo "<div> Total: {$total} Parallel Request Performed </div></div>";
            
            $serverPath = $this->getServerPath();

            $spot = '53440727632095c30d8b456d';
            $session = '53860beb63209500168b4567'; // 53860c2e632095b2158b4567, 53860ca663209546148b4567
            $controller = 'continent';
            $hugeArray = array();
            
            $ch = curl_init( $serverPath . '/statistics/performance/sessiontest?token='.$token );
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");            
            $response = curl_exec($ch); 

            //print_r( $response ); die;
            echo $response;
            /*
            // n is the number of parallel users
            while( $n > 0 ) {
                $sessionMethods = array( 
                    array( 
                        'endpoint' => 'statistics/performance/sessiontest',
                        'method' => 'GET',
                        'params' => array() , 
                    ),    
                    array( 
                        'endpoint' => 'statistics/performance/usertest',
                        'method' => 'GET',
                        'params' => array() , 
                    ),                          
                );

                $methodArray = array();
                foreach( $sessionMethods as $method ){
                    $obj['postFields'] = $method['params'];
                    $obj['url'] = $serverPath . $method['endpoint'] . '?token=' . $token;
                    $obj['type'] = $method['method'];
                    $urlArray[] = $obj; 
                }

                rolling_curl( $urlArray, $this->magic );
                $n--;
            }
            */
            //$utilArray = array();

            //$times = $this->parseResultArray($hugeArray);            

            $total = microtime(true)-$start;
            echo "<div class=\"result-pad\">Elapsed: {$total} s.</div>";

            /*
            echo "<div class=\"result-pad\">
                    <div>Max Request time: " . Yii::app()->params['performance_results']['max'] . "</div>
                    <div>min Request time: " . Yii::app()->params['performance_results']['min'] . "</div>            
                </div>";
            */
           
            $all = ob_get_contents();
            ob_end_clean();

            echo $this->renderFile($layoutFile,array('content'=> $all  ),true);
            
            Yii::app()->end();

        } catch (Exception $e){
            //$this->sendExceptionInfo($e, $this->id, $this->action->id);
            print_r($e);
        }

    }

/*
    public function actionRuntest($key=1) {
        $n = $key;
        $start = microtime(true);
        $token = $this->token;
        ob_start();

        $layoutFile=$this->getLayoutFile('//layouts/main') ;
        echo '<style>   
                        .result-pad { padding: 10px; }
                        .result-list div { display: inline-block; } 
                        .result-list.ok { color: green; } 
                        .result-list.fail { color: red; } 

            </style>';
        try {
            $total = $n*18;
            echo "<div class=\"result-pad\"><div>Test application for {$n} User.  </div>";
            echo "<div>Send 3 parallel requests generating sub-parallel request for testing every controller action. </div>";
            echo "<div> Session [7 requests], User [5 requests], Continent [6 requests]  = 18 parallel threads per User </div>";
            echo "<div> Total: {$total} Parallel Request Performed </div></div>";
            
            $serverPath = $this->getServerPath();

            $spot = '53440727632095c30d8b456d';
            $session = '53860beb63209500168b4567'; // 53860c2e632095b2158b4567, 53860ca663209546148b4567
            $controller = 'continent';
            $hugeArray = array();

            while( $n > 0 ) {
                $sessionMethods = array( 
                    array( 
                        'endpoint' => 'session/test/1',
                        'method' => 'GET',
                        'params' => array() , 
                    ),
                    /*array( 
                        'endpoint' => 'user/test',
                        'method' => 'GET',
                        'params' => array() , 
                    ),
                    array( 
                        'endpoint' => 'continent/test',
                        'method' => 'GET',
                        'params' => array() , 
                    ), */                               
            /*
                );

                $methodArray = array();
                foreach( $sessionMethods as $method ){
                    $obj['postFields'] = $method['params'];
                    $obj['url'] = $serverPath . $method['endpoint'] . '?token=' . $token;
                    $obj['type'] = $method['method'];

                    $urlArray[$obj['url']] = $obj; 
                }

                $hugeArray[] = json_decode( curlMultiRequest($urlArray, null) );
                $n--;
            }

            $utilArray = array();

            $times = $this->parseResultArray($hugeArray);            
            echo "<div class=\"result-pad\">
                    <div>Max Request time: " . $times['max'] . "</div>
                    <div>min Request time: " . $times['min'] . "</div>
                    <div>mean Request time: " . $times['mean'] . "</div>
                </div>";

            $total = microtime(true)-$start;
            echo "<div class=\"result-pad\">Elapsed: {$total} s.</div>";
            $all = ob_get_contents();
            ob_end_clean();

            echo $this->renderFile($layoutFile,array('content'=> $all  ),true);
            
            Yii::app()->end();

        } catch (Exception $e){
            //$this->sendExceptionInfo($e, $this->id, $this->action->id);
            print_r($e);
        }
    }
*/
    /**
     * Tests execution time and validates the endpoints for session controller
     * @return Array Report with every endpoint tested with total time, status and received response
     */
    public function actionSessiontest( $id=1 ){
        try {            
            $oauth = YiiOAuth2::instance();
            $token = $oauth->verifyToken();
            $user = $oauth->loadUser($token->oauth_token);
            $this->setJsonHeaders();
            $trace = false;
            $serverPath = $this->getServerPath();

            $session = Session::model()->findOne();
            $user = Oauth2User::model()->findOne();
            $spot = Spot::model()->findOne();

            $userId = (string) $user->_id;
            $spotId = (string) $spot->_id;
            $sessionId = (string) $session->_id; // 53860c2e632095b2158b4567, 53860ca663209546148b4567
            $controller = 'session';

            //echo $sessionId; die;

            $sessionMethods = array( 
                array( 
                    'endpoint' => 'activity',
                    'method' => 'POST',
                    'params' => array( "target" => 'community'),
                    ), 
                /*
                array( 
                    'endpoint' => "byspot/{$spotId}",
                    'method' => 'GET',
                    'params' => array(),
                    ), 
                */
                array( 
                    'endpoint' => 'create',
                    'method' => 'POST',
                    'params' => array(
                            'spotId'=>$spotId,
                            '_airs'=>'[{ "height" : 6.78, "airtime": 3.3, "gforce": 1, "timestamp":123456789, "rotation": 180 }]',
                            'numberOfAirs'=>1,
                            'name' => 'performanceTest_' . date( 'dmY_His', time()),
                        ),
                    ),                
                array( 
                    'endpoint' => "useractivity/{$userId}",
                    'method' => 'GET',
                    'params' => array(),
                    ) , 
                array( 
                    'endpoint' => "detail/{$sessionId}",
                    'method' => 'GET',
                    'params' => array(),
                    ) ,                                                     
                array( 
                    'endpoint' => 'comment',
                    'method' => 'POST',
                    'params' => array( 
                                    "sessionId" => (string)$sessionId,
                                    "text" => "Comment generated by parallel tests",
                                ),
                    ),
                /*
                array( 
                    'endpoint' => "delete/{$sessionId}",
                    'method' => 'DELETE',
                    'params' => array(),
                    ), 
                */
                
            );

            $urlArray = array();
            foreach( $sessionMethods as $method ){
                $obj['postFields'] = $method['params'];
                $obj['url'] = $serverPath . $controller . '/' . $method['endpoint'] . '?token=' . (string) $token->oauth_token;
                $obj['type'] = $method['method'];
                if($trace)
                    print_r($obj);
                $urlArray[] = $obj; 
            }
            if( $id >= 1 ) {
                if(!$trace)
                    rolling_curl($urlArray, $this->magic );
            } else {
                //$results = curlMultiRequest($urlArray, null);
                //echo $results ;
            }

        } catch (Exception $e){
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }

    /**
     * Tests execution time and validates the endpoints for appuser controller
     * 
     * @return Array Report with every endpoint tested with total time, status and received response
     */
    public function actionUsertest(){
        try {

            $oauth = YiiOAuth2::instance();
            $token = $oauth->verifyToken();
            $user = $oauth->loadUser($token->oauth_token);
            $this->setJsonHeaders();

            $serverPath = $this->getServerPath();

            $user = 'b.cortes@ingravitymedia.com';
            $pass = '9b1daa940e84689fa9001d569fe58352c48670d2878f897fba413b406765ead922baa219ce283d0fffb54c7989fa6587aaac91a87fa75ac3a0a494c0187f70c6';
            $uid = '536b98da6320956b0e8b4569'; // 53860c2e632095b2158b4567, 53860ca663209546148b4567
            $controller = 'user';

            $sessionMethods = array( 
                array( 
                    'endpoint' => 'login',
                    'method' => 'POST',
                    'params' => array( 
                        "email" => $user,
                        "pass" => $pass,
                        ),
                    ) , 
                array( 
                    'endpoint' => "update",
                    'method' => 'POST',
                    'params' => array(
                            'lastname' => 'updated',
                            'country' => 'Spain',
                            'weight' => '65',
                        ),
                    ) , 
                array( 
                    'endpoint' => 'profile',
                    'method' => 'GET',
                    'params' => array(),
                    ) ,
                array( 
                    'endpoint' => "profile/{$uid}",
                    'method' => 'GET',
                    'params' => array(),
                    ) , 
                /*
                array( 
                    'endpoint' => 'logout',
                    'method' => 'GET',
                    'params' => array(),
                    ) ,                                                     
                    */
                array( 
                    'endpoint' => 'fblogin',
                    'method' => 'POST',
                    'params' => array( 
                                "email" => 'andreu.santaren@gmail.com',
                                "token" => 'CAAMkZB9xZCQIYBAOzlKS4C8311ECQfnKXDwGdLaFwxPZCdMFYoHCp89ZADIP2NXrOWgW42B3aAuF0dlcNTGfX2ArHCOGuKb7Kl7ZA6HbqaDJFgCnAr2ZAKhZCVVyefBUJzZAjIkEwC03hTUUukkS5eQIUPMPaGDXH8UlpuwMNwzderRdRVgHxDIoxUTpHClYD6P3pr2IztCHlMkDkpMHO0bH',
                                "client_id" => '82886a948a1c63e861be8cc4855d9c515f3a352d096dd25fe015f85cd9123de5df36a11ec260b47d9500898e4cde5c19feac79c3e74683f1879f8560fd496d17',
                                "client_secret" => "8119cd9da2faf75a5cdd0c0ae7ea3b41ae4c31ab54335029e03ff070c7d0fb7f2daac55bb32575e1b51a33480cd8ca10eb42b3d559801d9a8a7329d40395d5d6",
                                "redirect_uri"=> '/user/profile',
                                ),
                    ),
                
            );

            $methodArray = array();
            foreach( $sessionMethods as $method ){
                $obj['postFields'] = $method['params'];
                if( ! in_array( $method['endpoint'], array('login','fblogin') ) )
                    $obj['url'] = $serverPath . $controller . '/' . $method['endpoint'] . '?token=' . (string) $token->oauth_token;
                else
                    $obj['url'] = $serverPath . $controller . '/' . $method['endpoint'];

                $obj['type'] = $method['method'];

                $urlArray[$obj['url']] = $obj; 
            }

            $results = curlMultiRequest($urlArray, null);
            echo $results ;

        } catch (Exception $e){
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }


    /**
     * Tests execution time and validates the endpoints for continent controller
     * 
     * @return Array Report with every endpoint tested with total time, status and received response
     */
    public function actionContinentTest(){
        try {

            $oauth = YiiOAuth2::instance();
            $token = $oauth->verifyToken();
            $user = $oauth->loadUser($token->oauth_token);
            $this->setJsonHeaders();

            $serverPath = $this->getServerPath();

            $spot = '53440727632095c30d8b456d';
            $session = '53860beb63209500168b4567'; // 53860c2e632095b2158b4567, 53860ca663209546148b4567
            $controller = 'continent';

            $sessionMethods = array( 
                array( 
                    'endpoint' => 'activity/all',
                    'method' => 'POST',
                    'params' => array( 
                            'offset'=>0,
                            'pageSize'=>100,),
                    ) , 
                array( 
                    'endpoint' => 'leaderboard/all',
                    'method' => 'POST',
                    'params' => array(
                            'target'=>'community',
                            'offset'=>0,
                            'pageSize'=>100,
                            'feature' => 'airtime',
                        ),
                    ) ,
                array( 
                    'endpoint' => 'leaderboard/europe',
                    'method' => 'POST',
                    'params' => array(
                            'target'=>'following',
                            'offset'=>0,
                            'pageSize'=>100,
                            'feature' => 'airtime',
                        ),
                    ) ,
                array( 
                    'endpoint' => 'leaderboard/north-america',
                    'method' => 'POST',
                    'params' => array(
                            'target'=>'me',
                            'offset'=>0,
                            'pageSize'=>100,
                            'feature' => 'airtime',
                        ),
                    ) ,
                 array( 
                    'endpoint' => 'leaderboard/africa',
                    'method' => 'POST',
                    'params' => array(
                            'target'=>'community',
                            'offset'=>0,
                            'pageSize'=>100,
                            'feature' => 'height',
                        ),
                    ) ,
                array( 
                    'endpoint' => 'leaderboard/oceania',
                    'method' => 'POST',
                    'params' => array(
                            'target'=>'following',
                            'offset'=>0,
                            'pageSize'=>100,
                            'feature' => 'height',
                        ),
                    ) ,
            );

            $methodArray = array();
            foreach( $sessionMethods as $method ){
                $obj['postFields'] = $method['params'];
                $obj['url'] = $serverPath . $controller . '/' . $method['endpoint'] . '?token=' . (string) $token->oauth_token;
                $obj['type'] = $method['method'];

                $urlArray[$obj['url']] = $obj; 
            }

            $results = curlMultiRequest($urlArray, null);
            echo $results ;

        } catch (Exception $e){
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }

    }

}

?>