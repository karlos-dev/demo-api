<?php

/**
 *  Spot Statistics Controller
 *  
 *  @author Karlos Álvarez
 *  @module Statistics
 *  
 *  @abstract Implements statistic functions on Spot Collection
 *  
 */

class SpotController extends Controller {

    public function actionKiters(){
        $spots = Spot::model()->findAll();
        $res = array( array('Spot Name', 'Number of Kiters' ) );
        foreach ($spots as $spot) {
            $res[] = array( $spot->name, $spot->totalKiters );
        }

        $this->render('index', array(
                'res'=>$res,
                'title'=>'Number of Kiters per Spot'
            ));
    }

    public function actionActivity(){
        $spots = Spot::model()->findAll();
        $c = new EMongoCriteria();
        //$c->_spot('in', array( $spots[0]->_id, $spots[1]->_id ));

        $sessions = Session::model()->findAll($c);
        //print_r($sessions); die;
        $freq = Yii::app()->request->getQuery('key');

        switch ($freq) {
            case 'year':
                $dateString = 'Y';
                break;
            case 'daily':
                $dateString = 'dmY';
                break;
            case 'montly':
                $dateString = 'mY';
                break;
            default:
                $dateString = 'dmY';
                break;
        }

        $sDay = array();
        $sDay['00002014'] =  array( 'Day' );
        foreach ($spots as $spot ) 
            $sDay['00002014'][] = $spot->name;

        //$sDay['00002014'] = array( 'Day', $spots[0]->name, $spots[1]->name);
        
        foreach ($sessions as $session) {

            $spotpos = ( $session->_spot == $spots[0]->_id ) ? 0 : 1;
            $i=0;
            foreach ($spots as $spot ) 
                if ( $session->_spot == $spot->_id ) {
                    $spotpos = $i;
                    break;
                } else $i++;
            
            if( isset( $sDay[ date( $dateString, $session->created ) ] ) ){
                $elem = $sDay[ date($dateString, $session->created ) ];
                //print_r($elem); die;
                $elem[ $spotpos + 1 ]++;
                $sDay[ date($dateString, $session->created ) ] = $elem;

            } else {
                $sDay[ date($dateString, $session->created ) ] = array( date($dateString, $session->created) );
                foreach ($spots as $spot ) 
                    if ( $session->_spot == $spot->_id )
                        $sDay[ date($dateString, $session->created ) ][] = 1;
                    else 
                        $sDay[ date($dateString, $session->created ) ][] = 0;
            }

        }

        //print_r($sDay); die;
        $days = array_keys( $sDay );
        $total = count( $sDay );


        $res = array();
        foreach ($sDay as $key => $day) {
            $res[] = $day;
        }

        $this->render('activity', array(
                'res'=>$res,
                'title'=> "Session Creation between {$days[1]} and {$days[$total-1]}",
            ));
    }

    public function actionIndex(){
        //echo "I'm {$this->id}Controller";
        
        $spots = Spot::model()->findAll();
        $res = array( array('Spot Name', 'Number of sessions' ) );
        foreach ($spots as $spot) {
            $res[] = array( $spot->name, $spot->totalSessions );
        }

        $this->render('index', array(
                'res'=>$res,
                'title'=>'Number of Sessions per Spot'
            ));

        Yii::app()->end();
    }

    public function actionSessions(){
        $spots = Spot::model()->findAll();
        // Data for column chart 
        $res = array( array('Spot Name', 'Number of Sessions' ) );
        foreach ($spots as $spot) {
            $res[] = array( $spot->name, $spot->totalSessions );
        }

        $this->render('index', array(
                'res'=>$res,
                'title'=>'Number of Sessions per Spot'
            ));
    }

}

?>