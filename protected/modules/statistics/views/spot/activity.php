<?php
/* @var $this SpotController */
/* @var $res */
/* @var $title */

$action = $this->action->id;

?>
<h1>Woo <?php echo $action; ?></h1>

<?php
  if( Yii::app()->user->isGuest ) {
?>
  <h3>Please, log in if you want to do anything...</h3>

<?php

    echo "You are connecting from: <br><br>"; 
    echo "IPv4 address <b>{$_SERVER['REMOTE_ADDR']}</b><br>";   
    echo "Using: <b>{$_SERVER['HTTP_USER_AGENT']}<br><br>";
    echo "This visit will not be logged";
   // echo "<a href='/site/createspots'>create spots</a>";
    

} else { 

  
  $json = json_encode($res);
/*
  $reflection = new ReflectionClass('Spot'); 
  $methods = $this->getMethods();
  $json = json_encode($methods);
*/
  $jsactions = array('activity','kiters','ranking','sessions');
  //echo "<div>{$json}</div>"; 
  foreach ($jsactions as $action) {
        echo CHtml::button( $action, array(
            //'submit' => 
            'name' => 'btnBack',
            'class' => 'uibutton loading confirm',
            'style' => 'width:150px;',
            'onclick' => "js:document.location.href='/statistics/spot/{$action}'",
                )
        );
  }
  echo CHtml::button('Back', array(
            'name' => 'backbutton',
            'class' => 'uibutton loading confirm',
            'style' => 'width:150px;',
            'onclick' => "history.go(-1)",
                )
        );

  echo '<br>';
  $jsactions = array('daily','montly','year');
  
  foreach ($jsactions as $action) {
        echo CHtml::button( $action, array(
            //'submit' => 
            'name' => 'btnBack',
            'class' => 'uibutton loading confirm',
            'style' => 'width:150px;',
            'onclick' => "js:document.location.href='/statistics/spot/activity/{$action}'",
                )
        );
  }

?>

<div>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var array = JSON.parse( '<?php echo $json; ?>');
        var data = google.visualization.arrayToDataTable( array );
        /*
        var data = google.visualization.arrayToDataTable([
          ['Year', 'Sales', 'Expenses'],
          ['2004',  1000,      400],
          ['2005',  1170,      460],
          ['2006',  660,       1120],
          ['2007',  1030,      540]
        ]);
  */
        var options = {
          title: '<?php echo $title; ?>',
          hAxis: {title: 'Year', titleTextStyle: {color: 'red'}}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>
    <div id="chart_div" style="width: 900px; height: 800px;"></div>
    
</div>

<?php   
}

?>