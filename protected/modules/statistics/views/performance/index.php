<?php
/* @var $this SpotController */
/* @var $res */
/* @var $title */

$action = $this->action->id;

?>
<style>
    .launchtest input   { display: block; }
    .launchtest i       { color: #858585; }
</style>

<h1>Woo <?php echo $action; ?></h1>

<?php
  if( Yii::app()->user->isGuest ) {
?>
  <h3>Please, log in if you want to do anything...</h3>

<?php

    echo "You are connecting from: <br><br>"; 
    echo "IPv4 address <b>{$_SERVER['REMOTE_ADDR']}</b><br>";   
    echo "Using: <b>{$_SERVER['HTTP_USER_AGENT']}<br><br>";
    echo "This visit will not be logged";

} else { 
    
        $jsactions = array('runtest','funtest');
        echo "<h3>Performance Sections:</h3>"; 
        echo "<p>This is a curl parallel test suite.<br>Have in mind is designed to test general server working, and can be customized to simulate a number of parallel request.<br>
        This is an experimental feature, just informative that gives general overview of the API health status.</p>"; 

       
        echo '<form class="launchtest" action="performance/funtest" enctype="application/x-www-form-urlencoded" method="post">
        <label for="tkn">Valid access token. <i>(You can get it using postman or any other REST helper)</i></label>
        <input name="tkn" type="text" placeholder="Your access token" />
        <label for="threads">Parallel threads</label>
        <input name="threads" type="text" placeholder="Number of parallel theads" /><br>
        <input type="submit" placeholder="Number of parallel theads" value="Run test" />
        </form>';
  
        //echo CHtml::link("Parallel Curl",array("performance/runtest/1"));
        //echo '<br>';
        //echo CHtml::link("Non Blocking Parallel Curl",array("performance/funtest/1"));
        //echo '<br>';

?>

<div>
    
    
</div>
<?php   
}

?>