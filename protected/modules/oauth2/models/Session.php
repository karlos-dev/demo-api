<?php
/**
 * Kinematiq project
 * 
 * Session Model
 * Makes use of the mongoDb extension to handle persistence on noSql environment
 *
 * 
 * @since 0.8.4
 * 
 * Added support for rotations in jump ODM
 * Extended session overview with the longest rotation in degrees
 * 
 * @since 1.3.0
 *  Added airs replacement and recalculations
 *  Added geolocation
 * 
 */

class Session extends EMongoDocument {

    /** @virtual */
    // public $_id;

    /**
     * @var Timestamp Session system creation time
     */
    public $created;

    /**
     * @var Timestamp Session recording time
     */
    public $time;

    /**
     * @var String  Session name
     */
    public $name;          
    
    /**
     * @var Integer  Session score
     */
    // public $points;

    /**
     * @var Integer  Actual number of airs during the session record
     */
    public $numberOfAirs;

    /**
     * @var Integer  Maximun amount of time, in second sof the air duration
     */
    public $maxAirTime;

    /**
     * @var Integer  Maximun amount of time, in second sof the air duration
     */
    public $totalAirTime;

    /**
     * @var Integer  Maximun amount of time, in second sof the air duration
     */
    public $totalHeight;

    /**
     * @var Integer Maximun Height
     */
    public $maxHeight;

    /**
     * @var Integer Maximun gforce at jump start
     */
    public $maxPop;

    /**
     * @var Integer 
     */
    public $maxCrashPower;    

    /**
     * @var Integer  Maximun amount of gravitational force taken during the session
     */
    public $maxCrashVelocity;    

    /**
     * @var Integer  Maximun amount of gravitational force taken during the session
     */
    public $maxHpower;         

    /**
     * @var Integer  Maximun amount of gravitational force taken during the session
     * @deprecated
     */
    public $maxGforce;    

    /**
     * @var Integer  The longest rotation in angular degrees, i.e.360
     * @deprecated
     */
    public $longestRotation;

    /**
     * @var Integer
     *
     * @to be deprecated
     * 
     */
    public $highestAir;    

    /**
     * @var Object
     * 
     * Highest jump object
     * 
     */
    public $detailsObject;     

    /**
     * @var mongoId The id relative to the user uploading the session
     */
    public $_user;     

    /**
     * @var mongoId The id of the Spot where the session was recorded
     */
    public $_spot;  
    
    /**
     * @var array Array of ids of every air recorded in the session
     * @since beta
     */
    public $_airs;

    /**
     * @var Integer Total number of comments in session
     * @since beta
     */
    public $totalcomments; 

    /**
     * @var Integer Session duration in seconds
     * @since 1.0.5
     */
    public $duration;     

    /**
     * @var Integer Total number of likes in session
     * @since beta extended
     */
    public $totallikes;

    /**
     * @var array Array of session pictures
     */ 
    public $_pictures;

    /**
     * @var String The firmware version in the sensor recording the session
     * @since 0.8.10
     */
    public $woo_fwv;

    /**
     * @var string The id of the sensor which recorded the session data
     * @since 0.8.10
     */ 
    public $woo_id;

    /**
     * WKN-1554
     */

    /**
     * The user latitude in the session upload moment
     * @var Float
     */
    public $user_lat;

    /**
     * The user latitude in the session upload moment
     * @var Float
     */
    public $user_lng;
    
    /**
     * @internal 
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * @internal 
     */   
    public function collectionName() {
        return 'session';
    }   

    /**
     * @internal 
     */
    public function behaviors() {
        return array(
            /*
            'embeddedArrays' => array(
                'class'=>'ext.yiimongodbsuite.extra.EEmbeddedArraysBehavior',
                'arrayPropertyName'=>'_airs',       // name of property, that will be used as an array
                'arrayDocClassName'=>'Air'          // class name of embedded documents in array
            ),
            */
        );
    }

    /**
     *  @internal 
     */
    public static function model($classname=__CLASS__) {
        return parent::model($classname);
    }
    
    /**
     * @internal 
     */
    public function rules() {
        return array(
            //array('points, numberOfAirs, maxAirTime, highestAir, endTime, startTime', 'numerical', 'integerOnly' => false),            
            array('numberOfAirs, maxAirTime, highestAir, time', 'numerical', 'integerOnly' => false), 
            // array('_spot, created, _user, numberOfAirs, maxAirTime, highestAir, maxPop,  name, _id', 'safe' ),                  //
        );
    }   

    /**
     *  @version 0.8.2
     *           Session images array is defined to have only one image
     * 
     *  @version 1.4.3
     *  @return  boolean The result of the model update(save) operation
     * 
     *  @internal 
     */
    public function addImage( $img ){
        $data = array(
            'type' => $img->type,
            'url' => $img->url,
        );
        
        //if (!isset($this->_pictures))
        $this->_pictures = array();

        array_push($this->_pictures, $data);
        
        //$this->update(array('_pictures'), true);
        return $this->save();
    }

    /**
     * Decrements the liked count of the session
     * @return boolean
     *
     * @version 1.4.2
     *          - ADD return boolean value
     */
    public function dislike() {
        if( $this->totallikes == 0 )
            $this->totallikes = 0;
        else {
            $this->totallikes--;
        }
        return $this->update(array('totallikes'));
    }

    /**
     * Increments the liked count of the session
     * @return boolean
     *
     * @version 1.4.2
     *          - ADD return boolean value
     */
    public function like() {
        if( $this->totallikes == 0 )
            $this->totallikes = 1;
        else {
            $this->totallikes++;
        }
        return $this->update(array('totallikes'));
    }

    /**
     * Validates the sensor id 
     * Is valid if
     * - the user has currently registered the sensor which recorded the session 
     * - the user has been owner of the sensor recording the session
     *
     * 
     * @param  Array $sid An array containing wooids
     * @return Boolean True if the id is in the array so valid, false otherwise
     *
     * @deprecated 1.0.2
     * 
     */
    public function validateSensor( $user ) {    

        //$c = new EMongoCriteria;
        //$c->compare( 'woo', $sid );

        $sensor = Sensor::model()->findAllByPk($user->sensors);
        foreach ($sensor as $s) {
            if( $s->wooid == $this->woo_id )
                return true;
        }

        $c= new EMongoCriteria();
        $c->addCondition('wooid', $this->woo_id );
        $s = Sensor::model()->findOne($c);
        if( !$s )
            return false;

        return $s->hasBeenOwner( (string) $user->_id);

    }

    /**
     *  Adds the session data to the Rank collection so available in the Leaderboards
     *  
     */
    public function rankSession() {
        /**
         *  $status : contains the maximun values of the session
         */

        $status['feature']['highestAir'] =          $this['highestAir'];
        $status['feature']['maxAirTime'] =          $this['maxAirTime'];

        $status['feature']['maxPop'] =              $this['maxPop'];
        $status['feature']['maxHpower'] =           $this['maxHpower'];
        $status['feature']['maxCrashPower'] =       $this['maxCrashPower'];
        $status['feature']['maxCrashVelocity'] =    $this['maxCrashVelocity'];

        /**
         *  NAVIGATE 
         */
        foreach ($status['feature'] as $key => $value) {

            $c = new EMongoCriteria();
            $c->addCondition( 'feature', $key );
            $c->addCondition( '_user',  $this->_id );
            $c->addCondition( '_spot', $this->_spot );

            //$c->_user = $this->_id;
            //$c->_spot = $session->_spot;

            $item = Rank::model()->findOne($c);

            if( !$item ) {
                $r = new Rank();
                $r->_session = $this->_id;
                $r->_spot = $this->_spot;
                $r->_user = $this->_user;
                $r->feature = $key;
                $r->score = $value;
                $r->created = $this->created;
                $r->save();

            } else {
                //update record only if the feature is higher
                if( $item->score < $this->{$key} ){
                    $item->score = $value;
                    $item->_session = $this->_id;
                    $item->created = $this->created;
                    //$item->update( array('score','_session','created'), true );
                    $item->save();
                }
            }
        }
    }


    /**
     * This method updates every single entity within the Woo application that must hold data about recorded sessions. 
     * It parses the input POST, validates its content, 
     *
     * 1 mongodb read operation
     * 
     * 1 mongodb write operation
     * 
     * O(n) complexity parsing airs array
     * 
     * @version 1.4.1
     *  - update $filters
     *  - duration only counts valid jumps
     *  
     */
    public function validateParams(){

        $filters = array(
            
            "name" =>           array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/[\s\S]{1,128}/") ),  
            "numberOfAirs" =>   array("filter" => FILTER_VALIDATE_INT),
            "duration" =>       array("filter" => FILTER_VALIDATE_INT),
            "time" =>           array("filter" => FILTER_VALIDATE_INT),
            "user_lat" =>       array("filter" => FILTER_VALIDATE_FLOAT),
            "user_lng" =>       array("filter" => FILTER_VALIDATE_FLOAT),
            "spotId" =>         array("filter" => FILTER_VALIDATE_REGEXP, "options"=>array("regexp"=>"/[a-z0-9\s]{24}/") ),  
            "_airs" =>          array(),
            "wooid" =>          array("filter" => FILTER_VALIDATE_REGEXP, "options"=>array("regexp"=>"/(woo)-[a-z0-9\s]{4}-[a-z0-9\s]{4}/i") ),
            "fwv" =>            array("filter" => FILTER_VALIDATE_REGEXP, "options"=>array("regexp"=>"/[0-9]{1,4}\.[0-9]{1,4}\.[0-9]{1,4}/i")  ),
        );
        
        $input = filter_input_array(INPUT_POST, $filters);    
        $oneHour = 60 * 60;
        $version = Yii::app()->controller->getModule("oauth2")->getClientVersion();

        if( empty( $input['time']) || $input['time'] > ( time() + $oneHour ) )
            throw new OAuth2Exception( array('code' => NO_TIME_SET, 'message' => "The session seems to be recorded in the future." ));

        if( 
            !empty( $input['fwv'] ) &&
            !empty( $input['spotId'] ) &&            
            !empty( $input['name'] ) &&
             isset( $input['_airs'] ) 
        ) {

            // Check the spot is available in db
            // $mongoId = new MongoId( $input['spotId'] );
            $spot = Spot::model()->findByPk( $input['spotId'] );
            if(!$spot)
                throw new OAuth2Exception( array('code' => BAD_SPOT_ID, 'message' => "No available Spot having the id provided." ));
            
            $this->_spot = $spot->_id;
            $this->name = (string) $input['name'];

            $this->created = time();
            $this->time = $input['time'];
            $this->_airs = array();

            /**********************************************************************************************************
            * These vars store the max values for every Air feature
            **********************************************************************************************************/
            
            $maxAirTime = 0;
            $highestair = 0;
            $totalTime = 0;
            $totalAirTime = 0;
            $totalHeight = 0;

            // @deprecated
            $maxGforce = 0;
            $longestRotation = 0;

            // New IOS 1.6
            $max_hpower = 0;
            $max_pop = 0;
            $max_crash_power = 0;
            $max_crash_velocity = 0;

            // WKN-1244
            $highestAirObject = 0;
            $totaljumps=0;
            
            $airArray = json_decode( $input['_airs'] );
            if( !$airArray || !is_array($airArray) )
                throw new OAuth2Exception( array('code' => BAD_POST_DATA, 'message' => "Not a valid json string" ));

            $currentJumpIndex =0;

            foreach ($airArray as $air) {
                /**
                 *  WKN-1527
                 */
                $singleAir = new Air();
                $singleAir->validateParams( $air );

                if ( version_compare($version, '1.6') >= 0 ) {
                    
                    /**
                     * WKN-1244
                     * WKN-1527
                     */
                    if ( $singleAir->tag === "valid" ) { 

                        if( $max_pop < $air->pop )
                            $max_pop = $air->pop;

                        if( $max_hpower < $air->h_power )
                            $max_hpower = $air->h_power;
                        
                        if( $max_crash_power < $air->crash_power )
                            $max_crash_power = $air->crash_power;
                        
                        if( $max_crash_velocity < $air->crash_velocity )
                            $max_crash_velocity = $air->crash_velocity;

                        /**
                         * We compute the session data with only valid jumps
                         */
                        
                        // Session duration is independent of jump quality
                        $totalTime = ($totalTime < $air->timestamp) ? $air->timestamp : $totalTime;
                        
                        $maxAirTime = ($maxAirTime < $air->airtime ) ? $air->airtime : $maxAirTime;   
                        $totalAirTime = (float) $totalAirTime + (float) $air->airtime;
                        $totalHeight = (float) $totalHeight + (float) $air->height;
                        $totaljumps++;

                        if ( $highestair < $air->height ) { 
                            $highestAirObject = $currentJumpIndex;
                            $highestair = $air->height;
                        }
                    }    
                } else {
                    
                    // Session duration is dependent of jump quality
                    $totalTime = ($totalTime < $air->timestamp) ? $air->timestamp : $totalTime;
                    
                    $maxAirTime = ($maxAirTime < $air->airtime ) ? $air->airtime : $maxAirTime;   
                    $totalAirTime = (float) $totalAirTime + (float) $air->airtime;
                    $totalHeight = (float) $totalHeight + (float) $air->height;
                    $totaljumps++;
                    
                    if ( $highestair < $air->height ) { 
                        $highestAirObject = $currentJumpIndex;
                        $highestair = $air->height;
                    }
                }   

                $this->_airs[] = $singleAir;
                $currentJumpIndex++;
            } 

            // if( isset( $input['duration'] ) )
            //    $this->duration = $input['duration'];
            //else

            // DEV_CHALLENGE
            // We don't rely on IOS info to set duration
            $this->duration = $totalTime;

            if( isset( $input['user_lat'] ) && ( isset( $input['user_lng'] ) ) ) { 
                $this->user_lat = $input['user_lat'];
                $this->user_lng = $input['user_lng'];

            } else {
                $this->user_lat = 0;
                $this->user_lng = 0;
            }

            /**
             *  FIX IOS 1.5 WKN-1661
             */
            if( empty($input['wooid']) )
                $this->woo_id = strtolower( "WOO-30C3-1C5B" );
            else
                $this->woo_id = strtolower( $input['wooid'] );

            $this->woo_fwv = $input['fwv'];
           
            // Maximun session data
            
            $this->maxPop = $max_pop;
            $this->maxCrashPower = $max_crash_power;
            $this->maxCrashVelocity = $max_crash_velocity;
            $this->maxHpower = $max_hpower;
            $this->maxAirTime = (float) $maxAirTime;
            $this->maxHeight = (float) $highestair;

            // Kept for compatibility with IOS < 1.6
            $this->highestAir = (float) $this->maxHeight;

            // $this->maxGforce = (float) $highestAirObject->gforce;
            // $this->longestRotation = (integer) $highestAirObject->rotation;

            // Accomulative session data
            $this->totalAirTime = (float) $totalAirTime;
            $this->totalHeight = (float) $totalHeight;
            $this->numberOfAirs = $totaljumps;

            //Now the data for session details
            $this->detailsObject = $highestAirObject;
            
            // Modify the Spot Document
            // Rollabck if fail
            $spot->registerNewSessionData( $this );                  

            $this->_pictures = array();
            $this->totalcomments = 0;
            $this->totallikes = 0;

            return true;

        } else return false;
    }

    /**
     *  Check if max values in the session are correct
     *  Parses the Airs array of the session and matches the session maximum scores
     *  
     *  @since 1.3.0
     * 
     *  @return boolean If the calculation matches the current session values
     * 
     */
    public function check(){
        $session_total_jumps = $this->numberOfAirs;
        $session_total_height = $this->totalHeight;

        if( isset($this->totalAirTime) )
            $session_total_airtime = $this->totalAirTime;
        else
            $session_total_airtime = 0;

        $session_max_height = $this->highestAir;
        $session_max_airtime = $this->maxAirTime;
        $session_duration = isset( $this->duration ) ? $this->duration : 0;

        $total_airtime = 0;
        $total_height = 0;
        $total_jumps = 0;

        $maxPop = 0;
        $maxHpower = 0;
        $maxCrashVelocity = 0;
        $maxCrashPower = 0;

        $max_height = 0;
        $max_airtime = 0;
        $duration = 0;

        $unstable = false;

        $sessionNew = false;
        foreach ($this->_airs as  $air) {
            $air = (object) $air;

            if( $air->tag == "valid" ) {
                if( $air->height > $max_height )
                     $max_height = $air->height;
                if( $air->airtime > $max_airtime )
                     $max_airtime = $air->airtime;
                if( $air->created > $duration )
                    $duration = $air->created;

                if( isset($air->pop) ) {
                    if($air->pop > $maxPop )
                         $maxPop = $air->pop;

                    if( $air->h_power > $maxHpower )
                         $maxHpower = $air->h_power;

                    if( $air->crash_power > $maxCrashPower )
                         $maxCrashPower = $air->crash_power;

                    if( $air->crash_velocity > $maxCrashVelocity )
                         $maxCrashVelocity = $air->crash_velocity;
                }

                $total_height += $air->height;
                $total_airtime += $air->airtime;
                $total_jumps += 1;

            }  
        }

        if( 
            $session_total_jumps    == $total_jumps &&
            $session_total_airtime  == $total_airtime &&
            $session_total_height   == $total_height &&
            $session_max_height     == $max_height &&
            $session_max_airtime    == $max_airtime &&
            $session_duration       == $duration
        ) {
            if( isset($this->maxPop) &&
                isset($this->maxHpower) && 
                isset($this->maxCrashPower) && 
                isset($this->maxCrashVelocity) ) { 

                if( $this->maxPop == $maxPop && 
                    $this->maxHpower == $maxHpower && 
                    $this->maxCrashPower == $maxCrashPower && 
                    $this->maxCrashVelocity == $maxCrashVelocity 
                )
                    return true;
                else 
                    return false;

            }
        } else return false; 
    }

    /**
     *  Parses the input airs array
     *  Recalculates Session maximum values based on the input Array valid jumps
     *  
     *  Updates current Session Object
     *  @since 1.3.0
     * 
     */
    public function pushAirs( $airs ){

        if( isset($this->totalAirTime) )
            $session_total_airtime = $this->totalAirTime;
        else
            $session_total_airtime = 0;

        $session_max_height = $this->highestAir;
        $session_max_airtime = $this->maxAirTime;
        $session_duration = isset( $this->duration ) ? $this->duration : 0;
        $session_total_jumps = $this->numberOfAirs;
        $session_total_height = $this->totalHeight;

        $total_airtime = 0;
        $total_height = 0;
        $total_jumps = 0;

        $maxPop = 0;
        $maxHpower = 0;
        $maxCrashVelocity = 0;
        $maxCrashPower = 0;

        $max_height = 0;
        $max_airtime = 0;
        $duration = 0;

        foreach ($airs as  $air) {
            $air = (object) $air;

            if( $air->tag == "valid" ) {
                if( $air->height > $max_height )
                     $max_height = $air->height;
                if( $air->airtime > $max_airtime )
                     $max_airtime = $air->airtime;
                if( $air->created > $duration )
                    $duration = $air->created;

                if( isset($air->pop) ) {
                    if($air->pop > $maxPop )
                         $maxPop = $air->pop;

                    if( $air->h_power > $maxHpower )
                         $maxHpower = $air->h_power;

                    if( $air->crash_power > $maxCrashPower )
                         $maxCrashPower = $air->crash_power;

                    if( $air->crash_velocity > $maxCrashVelocity )
                         $maxCrashVelocity = $air->crash_velocity;
                }

                $total_height += $air->height;
                $total_airtime += $air->airtime;
                $total_jumps += 1;

            }  
        }

        #echo "T1 " . $max_height . " ". $max_airtime . " ". $duration . " ". $maxPop . " ". $maxHpower . " ". $maxCrashPower . " ". $maxCrashVelocity . "\n";
        #echo "T0 " . $session_max_height . " ". $session_max_airtime . " ". $session_duration . " ". $maxPop . " ". $maxHpower . " ". $maxCrashPower . " ". $maxCrashVelocity . "\n";

        $this->highestAir = $max_height;
        $this->maxHeight = $max_height;
        $this->maxAirTime = $max_airtime;
        $this->duration = $duration;
        $this->maxPop = $maxPop;
        $this->maxHpower = $maxHpower;
        $this->maxCrashPower = $maxCrashPower;
        $this->maxCrashVelocity = $maxCrashVelocity;
        $this->_airs = $airs;
        $this->totalHeight = $total_height;
        $this->totalAirTime = $total_airtime;
        $this->numberOfAirs = $total_jumps;

        #return $this;
    }

    /**
     *  Checks if current session has a dupplicate in the db
     *  Uses the following session attributes
     *      - highestAir
     *      - maxAirTime
     *      - totalHeight
     *      - totalAirtime
     *      - numberOfAirs
     *      - time
     *
     *  @since 1.4.1
     *  
     *  @return boolean If a equal session has been found
     */
    public function isRepeated() {
        
        $c = new EMongoCriteria();
        $c->addCondition("highestAir",      $this->highestAir );
        $c->addCondition("maxAirTime",      $this->maxAirTime );
        $c->addCondition("totalHeight",     $this->totalHeight );
        $c->addCondition("totalAirtime",    $this->totalAirtime );
        $c->addCondition("numberOfAirs",    $this->numberOfAirs );
        $c->addCondition("time",            $this->time );

        $it = Session::model()->find($c);
        if( $it->count() > 1)
            return true;

        else return false;
    }
    
}

?>
