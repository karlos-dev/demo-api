<?php
/**
 * Kinematiq API
 * Oauth2User MongoDocument
 * 
 * @author      Karlos Álvarez <karlos.alvan@gmail.com>
 * @abstract    Model storing all user related data in mongoDb
 * @uses        YiiMongoDbSuite ActiveRecord EMongoDocument declaration for Yii to be used with mongoDb
 */

class Oauth2User extends EMongoDocument {
    
    /** @virtual */
    public $_id;

    public $email;
    public $username;

    public $created;
    public $fbid;
    public $enable_notifications;
    public $role;
    
    public $active;
    
    /**
     * @var $trialEnd Timestamp 
     *      When a profile is new, the time in which the App trial ends
     *      From this moment, the account must be activated.
     * 
     * @version 1.3.2
     */
    public $trialEnd;

    /**
     * @var $tcversion String 
     *      The current approved version of terms and conditions
     * 
     * @version 1.3.3
     */
    public $tc_version;

    /**
     * @var $tcdate String 
     *      The date the user signed the current version of tc
     * 
     * @version 1.3.3
     */
    public $tc_acceptdate;

    public $language;
    public $details;
    public $challengedetails;
    public $_followed;
    public $_followedBy;
    public $userAgent;
    /**
     * @var $appVersion string 
     *      Track which version is used by this user
     * 
     * @version 1.3.2
     * 
     */
    public $appVersion;
    public $password;  
    public $device_id;  
    public $hash;

    //public $woo_id;    
    public $sensors;

    public $client_id;
    public $client_secret;
    public $redirect_uri;
    
    /** @deprecated */
    private $_friends;
    
    /** @virtual */

    public static $privateAttributes_1_7_16 =  array(  'email', 'fbid', 'role', 'created', 'enable_notifications', 'username',
                                                'redirect_uri', 'client_id', 'client_secret', 'sensors','password',
                                                'active', 'language', '_followed','_followedBy', 'hash', 'userAgent', 'device_id',  
                                                'appVersion','trialEnd','tc_version','tc_acceptdate'
                                                //'details','challengedetails'
                                                );

    public static $privateAttributes =  array(  'email', 'fbid', 'role', 'created', 'enable_notifications', 
                                                'redirect_uri', 'client_id', 'client_secret', 'sensors','password',
                                                'active', 'language', 'hash', 'userAgent', 'device_id', 
                                                'appVersion','trialEnd','tc_version','tc_acceptdate'
                                                //'details','challengedetails'
                                                );
    /*
    
    public static $publicAttributes =  array(  '_id', '_pictures', 'role', 'created', 'enable_notifications', 
                                                'redirect_uri', 'client_id', 'client_secret', 'sensors','password',
                                                'active', 'language', '_followed','_followedBy', 'hash', 'userAgent', 'device_id',  
                                                //'details','challengedetails'
                                                );
    */

    public static $publicAttributes_1_8_1 =     array('_id', 'email', 'details');

    public static $publicAttributes =           array('_id', 'email', 'details', '_followed', '_followedBy');

    

    /** @internal Internal ODM management */
    public function collectionName() {
        return 'oauth2Users';
    }    
   
    /** 
     * @internal Internal ODM management
     */
    public function tableName() {
        return self::collectionName();
    }

    /**
     * Returns the url belonging to the user pofile
     */
    public function getProfileImage($object=false){

        $details = (Object) $this->details;
        if( is_array($details->_pictures) )
            foreach ($details->_pictures as $img) {
                if( $img['type'] == "user")
                    if( $object )
                        return $img;
                    else
                        return $img['url'];
            }
        return "";
    }

    /**
     * Get user role
     * @return [type] [description]
     */
    public function getRole(){
        return ( isset($this->role) && $this->role !== null ) ? $this->role : 'user';
    }
    
    
    /** @internal Internal ODM management */
    public function rules() {
        return array(
            array('details', 'subdocument', 'type' => 'one', 'class' => 'UserDetails'),
            array('challengedetails', 'subdocument', 'type' => 'one', 'class' => 'ChallengeDetails'),
            array('email', 'EMongoUniqueValidator', 'className' => 'Oauth2User', 'attributeName' => 'email'),
        );
    }

    public function reset(){
        $details = (object) $this->details;

        $details->highestAir = 0;
        $details->maxAirTime = 0;
        $details->maxGforce = 0;
        $details->numberOfAirs = 0;
        $details->numberOfSessions = 0;
        $details->longestRotation = 0;

        $this->woo_id = array();
        $this->active=1;
        $this->device_id="";
        
        $this->details = $details;
        $this->update();
    }
    

    public static function listUsers(){

        $time = microtime(true);
        $timeStart = $time*1000;
        
        $elems = Oauth2User::model()->findAll();
        
        $time = microtime(true);
        $timeEnd = $time*1000;
        
        $response = array(
            'status'=>'ok',
            'totalItems'=>'ok',
            'currentSize'=>'',
            //'time'=>round($timeEnd-$timeStart).'ms',
            'time'=>round($timeEnd-$timeStart).'ms',
            'items' => $elems,
        );
        
        return $response;
    }
    
    
    public function friendshipStatus( $id ) {
        foreach ($this->_friends as $fobj) {
            if( $fobj->friendId == $id ) { 
                return $fobj->status();
            }
        }
        return 'nofriendship';
    }
    
    
    public function getFriendship( $id ) {
        foreach ($this->_friends as $fobj) {
            if( $fobj->friendId == $id ) { 
                return $fobj;
            }
        }
        return null;
    }
    
    
    public function isFollowing( $id ) {
        if( !$this->_followed )
            return false;
        
        foreach ($this->_followed as $uid) {
            if( $uid == $id ) { 
                return true;
            }
        }
        return false;
    }
    
    
    public function isFollowedBy( $id ) {
        if( !$this->_followedBy )
            return false;
        foreach ($this->_followedBy as $uid) {
            if( $uid == $id ) { 
                return true;
            }
        }
        return false;
    }
    
    /**
     * Adds an image to the embedded images array of the user profile
     * 
     * @since 0.8.2
     * User array only contains one image of each type
     *
     * @since 0.8.5
     * @updated to MongoClient
     */
    public function addImage( $img, $type ){
        $data = array(
            'type' => $type,
            'url' => $img->url,
        );

        $details = new UserDetails();
        $details = (object) $this->details;
        //print_r( $details );
        /**
         * @abstract
         * Since 0.8.2 Images array in user contains only the last uploaded images, one for the user profile 
         * and other for the cover.
         */
        if (!isset( $details->_pictures )) {
            $details->_pictures = array();

        } else {
            $pics = array();
            foreach ($details->_pictures as $pic ) {
                //print_r( $pic ); die;
                if( $pic['type'] !== $type )
                    $pics[] = $pic;
            }

            $pics[] = $data;
            $details->_pictures = $pics;
        }
        $this->details = $details;
        //print_r($this->details); die;
        $this->update();
    }

    /**
     * Reduce the data to be sent out of the service
     * Secure access information is always removed
     */
    public function compress(){
        
        unset( $this->password );
        unset( $this->redirect_uri ); 
        unset( $this->userAgent );  
        unset( $this->device_id ); 
        unset( $this->_friends );
        unset( $this->client_id );
        unset( $this->client_secret );
        unset( $this->created );
        unset( $this->hash );
        unset( $this->trialEnd );

        /**
         *  @abstract Version Compatibility
         *  @hack
         * 
         */ 
        $version = Yii::app()->controller->getModule("oauth2")->getClientVersion();

        if( version_compare( $version, Yii::app()->params['version_check'] ) >= 0 ) { 
            $vr = (array) $this;
            $vr['following'] = count( $this->_followed );
            $vr['followers'] = count( $this->_followedBy );

            unset( $vr['_followed']     );
            unset( $vr['_followedBy']   );

            return (object) $vr;

        } else {
            return $this;
        }
    }

    /**
     *  Reduces the user data to be sent out to other user
     * 
     *  Secure access information is always removed
     */
    public function publish(){

        $version = Yii::app()->controller->getModule("oauth2")->getClientVersion();

        /**
         *  @abstract Version Compatibility
         *  @hack
         * 
         *  Reducing the data we send about the user profile
         *  Some versions do always need _following users
         *  
         *  We removed it due to performance issues
         */

        if( version_compare( $version, Yii::app()->params['version_check'] ) >= 0 ) { 
            $profilePrivate = self::$privateAttributes_1_7_16;
        } else {
            $profilePrivate = self::$privateAttributes;
        }

        $details = (object) $this->details;
        $newdetails = new UserDetails();

        $followwing = count( $this->_followed );
        $followers = count( $this->_followedBy );

        foreach ( $profilePrivate as $key ) {
            unset( $this->{$key} );
        }

        foreach ( UserDetails::$privateDetails as $key ) {
            unset( $details->{$key} );
        }

        if( version_compare( $version, Yii::app()->params['version_check'] ) >= 0 ) { 
            $var = (array) $this;
            $var['following'] = $followwing;
            $var['followers'] = $followers;
            $var = (object) $var; 

        } else {
            $var = (object) $this;
        }

        $var->details = (object) $details;

        return $var;
    }

    /**
     *  add T&C version and approval ts
     *  
     *  @param  String $version T&C version to be approved
     */
    public function acceptTC( $version ) {
        $this->tc_version = $version;
        $this->tc_acceptdate = time();
    }

    /**
     * Updates the oauth2User model
     * Adds a wooid to the device array if its not present
     * 
     * @param  String $wooid The lower case sensor id to be added
     * @throws OAuth2Exception When any error updating the mongo document occurs
     * @deprecated
     */
    public function addWooDevice( $wooid ){
        echo "@deprecated"; return;
        if( is_array( $this->woo_id ) && !in_array($wooid, $this->woo_id ) )
            $this->woo_id[] =  $wooid;

        else if ( in_array($wooid, $this->woo_id) ) {
            throw new OAuth2Exception( array('code' => HACKING_ACTIVITY, 'message' => "You already linked this device to your profile") );
        } else {
            $this->woo_id = array( $wooid );
        }
        if( !$this->update( array('woo_id') ))
            throw new OAuth2Exception( array('code' => UNKNOWN_DB_ERROR, 'message' => "Error adding wooid for this user") );
        
        return true;
    }
    

    /**
     * Updates the oauth2User model by enabling/disabling the notification system
     * 
     * @param  String $wooid The lower case sensor id to be removed
     * @throws OAuth2Exception When any error updating the mongo document occurs
     * @deprecated
     */
    public function removeWooDevice( $wooid ){
        echo "@deprecated"; return;
        if( !is_array( $this->woo_id ) ) {
            $this->sendOk("Warning, there's no wooid to remove in profile");
            return false;
        } else {
            $array=array();
            foreach ($this->woo_id as $woo_id) {
                if( $woo_id !== $wooid )
                    $array[]=$woo_id;
            }
            $this->woo_id = $array;
            if( !$this->update( array('woo_id') ))
                throw new OAuth2Exception( array('code' => UNKNOWN_DB_ERROR, 'message' => "Error adding wooid for this user") );

            return true;
        }
    }


    public function deleteAllTokens(){
        $c = new EMongoCriteria;
        $c->addCondition( 'user_id', (string) $this->_id ) ;         
        $result = Oauth2Token::model()->deleteAll($c); 

        if($result && $result['ok']==1) 
            return true;
        else return false;
    }


    /**
     * Updates the oauth2User model
     * Adds a wooid to the device array if its not present
     *
     * @since 0.10.2 
     * @param  String $wooid The lower case sensor id to be added
     * @throws OAuth2Exception When any error updating the mongo document occurs
     */
    public function addSensor( $id ){
        if(isset($this->sensors) && in_array($id, $this->sensors ) ) {
            //$this->sendOk("You already have this device");
            return true;
        } else {
            $this->sensors[]=$id;         
            //$this->woo_id=$wooid;
            if( !$this->update( array('sensors') ))
                throw new OAuth2Exception( array('code' => UNKNOWN_DB_ERROR, 'message' => "Error adding wooid for this user") );

            return true;
        }
    }

    /**
     * Removes a sensor from the user profile
     *
     *
     * @param  mongoId $sensorId The mongoId of the sensor document to link
     * @throws OAuth2Exception When any error updating the mongo document occurs
     */
    public function removeSensor( $id ){
        if(isset($this->sensors) && !in_array($id, $this->sensors ) ) {
            //$this->sendOk("You dont have this device");
            return false;
        } else {
            $array = array();
            foreach ( $this->sensors as $sid ) {
                if( $sid !== $id )
                    $array[]=$sid;
            }
            // in case we need it we will use the last added sensor in case we have more
            // this->woo_id=null;

            $this->sensors = $array;
            if( !$this->update( array('sensors') ))
                throw new OAuth2Exception( array('code' => UNKNOWN_DB_ERROR, 'message' => "Error adding sensor for this user") );

            return true;
        }
    }    

    /**
     * Updates the oauth2User model by enabling/disabling the notification system
     * 
     * @param  Booolean $status The boolean for enabled or disabled notifications
     * @throws OAuth2Exception When any error updating the mongo document occurs
     */
    public function updateNotifications( $enable ){
        if( $this->enable_notifications !== $enable ){
            $this->enable_notifications = $enable;
            if( !$this->update( array('enable_notifications') ))
                throw new OAuth2Exception( array('code' => UNKNOWN_DB_ERROR, 'message' => "Cannot update nofications for this user") );
        }
    }


    /**
     * Updates mobile phone deviceId when the user connects using a different device 
     * 
     * @param  String $newDeviceId The push server related deviceId of the current mobile device
     * @throws OAuth2Exception When any error updating the mongo document occurs
     */
    public function updateDeviceid( $newDeviceId ){
        if( $this->device_id !== $newDeviceId ) {
            $this->device_id = $newDeviceId;
            if( !$this->update( array('device_id') ))
                throw new OAuth2Exception( array('code' => UNKNOWN_DB_ERROR, 'message' => "Cannot update deviceId for this user") );
        }
    }
    
    /**
     * Updates user profile 
     * $session is already saved in db
     * 
     * Evaluates profile maxvalues to create a new Rank
     * 
     * Updates Rank::collection
     *     - adding new rank OR
     *     - updating an existing one
     * 
     * @fine method 09/04/2014
     * 
     * @param   Session $session    The session data to insert
     * @throws  OAuth2Exception     If any update Error
     * 
     * @version 1.0.6
     *
     * @version 1.4.1
     *          ADD     Redis Management
     *          ADD     Delete saved session on user profile update Error
     *          UPDATE  Redis LB when updating Rank::collection
     *          
     *
     */
    public function insertNewSessionData( $session ) {
        if( isset($session) ) {

            $redis = new RedisController(0);
            $status = array();

            /**
             * UPDATE USER PROFILE
             *
             * Collect records for current user profile
             * Perform user profile update
             */
            
            $details=(Object) $this->details;

            //if( !isset($details->maxPop) ) {
            $newDetails = new UserDetails();

            foreach ( get_object_vars($details) as $key => $value ) {
                $newDetails->$key = $value;
            }
            $details = $newDetails;
            //}

            $details->numberOfAirs = $details->numberOfAirs + $session['numberOfAirs'];

            /*
                if ($details->maxAirTime > $session['maxAirTime'] ) {
                    //do nothing
                } else {
                    $details->maxAirTime = $session['maxAirTime'];
                }

                if ($details->highestAir > $session['highestAir'] ) {
                    //do nothing
                } else {
                    $details->highestAir = $session['highestAir'];
                }

                if ($details->maxPop > $session['maxPop'] ) {
                    //do nothing
                } else {
                    $details->maxPop = $session['maxPop'];
                }

                if ($details->maxHpower > $session['maxHpower'] ) {
                    //do nothing
                } else {
                    $details->maxHpower = $session['maxHpower'];
                }

                if ($details->maxCrashPower > $session['maxCrashPower'] ) {
                    //do nothing
                } else {
                    $details->maxCrashPower = $session['maxCrashPower'];
                }

                if ($details->maxCrashVelocity > $session['maxCrashVelocity'] ) {
                    //do nothing
                } else {
                    $details->maxCrashVelocity = $session['maxCrashVelocity'];
                }
            */

            /* DEPRECATED FIELDS

                if ($details->maxGforce > $session['maxGforce'] ) {
                    //do nothing
                } else {
                    $details->maxGforce = $session['maxGforce'];
                }

                if ($details->longestRotation > $session['longestRotation'] ) {
                    //do nothing
                } else {
                    $details->longestRotation = $session['longestRotation'];
                }

            */
            $details->maxGforce = 0;
            $details->longestRotation = 0;

            //Update information about user maximun scores
            $details->maxAirTime = ( $details->maxAirTime > $session['maxAirTime'] ) ? $details->maxAirTime : $session['maxAirTime'];
            $details->highestAir = ( $details->highestAir > $session['highestAir'] ) ? $details->highestAir : $session['highestAir'];

            $details->maxPop =              ( $details->maxPop > $session['maxPop'] ) ? $details->maxPop : $session['maxPop'];
            $details->maxHpower =           ( $details->maxHpower > $session['maxHpower'] ) ? $details->maxHpower : $session['maxHpower'];
            $details->maxCrashPower =       ( $details->maxCrashPower > $session['maxCrashPower'] ) ? $details->maxCrashPower : $session['maxCrashPower'];
            $details->maxCrashVelocity =    ( $details->maxCrashVelocity > $session['maxCrashVelocity'] ) ? $details->maxCrashVelocity : $session['maxCrashVelocity'];

            $details->maxGforce = 0;
            $details->longestRotation = 0;

            $details->totalTimeInWater +=   (float) $session['duration'];
            $details->totalHeight +=        (float) $session['totalHeight'];
            $details->totalAirTime +=       (float) $session['totalAirTime'];
            
            $details->numberOfSessions = $details->numberOfSessions+1;
            
            $this->details = $details;

            if( !$this->update() ) {
                // Here There's an Error, cannot update profile, session not being saved
                $session->delete();
                throw new OAuth2Exception( array('code' => UNKNOWN_DB_ERROR, 'message' => "Cannot update this user") );
            }

            //$status['feature']['maxGforce'] = $session['maxGforce'];

            /**
             * 	@var $status
             *  Contains the maximun values of the session
             */

            $status['feature']['highestAir'] =          $session['highestAir'];
            $status['feature']['maxAirTime'] =          $session['maxAirTime'];

            $status['feature']['maxPop'] =              $session['maxPop'];
            $status['feature']['maxHpower'] =           $session['maxHpower'];
            $status['feature']['maxCrashPower'] =       $session['maxCrashPower'];
            $status['feature']['maxCrashVelocity'] =    $session['maxCrashVelocity'];

            /**
             *  NAVIGATE & UPDATE
             */
            foreach ($status['feature'] as $key => $value) {

                $c = new EMongoCriteria();
                $c->addCondition( 'feature', $key );
                $c->addCondition( '_user',  $this->_id );
                $c->addCondition( '_spot', $session->_spot );

                $item = Rank::model()->findOne($c);
                $updateLb = false;

                if( !$item ) {
                    $item = new Rank();
                    $item->_session = $session->_id;
                    $item->_spot = $session->_spot;
                    $item->_user = $session->_user;
                    $item->feature = $key;
                    $item->score = $value;
                    $item->created = $session->created;

                    // Added 1.4.1
                    // Update LB Only if Rank::save() operation success 
                    if( $item->save() )
                        $updateLb = true;

                } else {
                    // Update record only if the feature is higher
                    if( $item->score < $session->{$key} ){
                        // echo "Updating item $item->score < $session->{$key}\n";
                        $item->score = $value;
                        $item->_session = $session->_id;
                        $item->created = $session->created;
                        //$item->update( array('score','_session','created'), true );
                        
                        // Added 1.4.1
                        // Update LB Only if Rank::save() operation success 
                        if( $item->save() )
                            $updateLb = true;
                    }
                }
                // Added 1.4.1
                if( $updateLb ) {
                    if( !$redis->add( $key, $item ) ) {
                        Yii::log("Error adding session to lb cache $key", "info");
                    	//echo "ERROR!\n"; die;
                    } else {
                        Yii::log("Added session to lb cache $key", "info");
                    }
                }
            }

        }
    }


    /**
     *  Parses user sessions and sets the profile maxiun data from the list of stored sessions
     *  @return Boolean  If the profile was sucessfully repaired
     *  
     *  @version 1.3.5
     */
    public function repair() {
        
        $user = $this;
        $details = (object) $user->details;

            $newDetails = new UserDetails();
            foreach ( get_object_vars($details) as $key => $value ) {
                $newDetails->$key = $value;
            }

        $details = $newDetails;

        $c= new EMongoCriteria();
        $c->addCondition('_user', $user->_id );
        $any = iterator_to_array(  Session::model()->findAll($c), false) ;
        $n = count($any);

        $maxHeight=0;
        $highestAir=0;
        $maxAirTime=0;

        $maxPop=0;
        $maxHpower=0;
        $maxCrashPower=0;
        $maxCrashVelocity=0;
        $maxrotations=0;
        $numberofjumnps = 0;
        $totalHeight = 0;
        $totalAirTime = 0;
        $timeInWater = 0;

        foreach ($any as $key => $session ) {
            $maxHeight = ($maxHeight > $session->highestAir) ? $maxHeight : $session->highestAir;
            $highestAir = ($highestAir > $session->highestAir) ? $highestAir : $session->highestAir;
            $maxAirTime = ($maxAirTime > $session->maxAirTime) ? $maxAirTime : $session->maxAirTime;
            
            $maxPop = ($maxPop > $session->maxPop) ? $maxPop : $session->maxPop;
            $maxHpower = ($maxHpower > $session->maxHpower) ? $maxHpower : $session->maxHpower;
            $maxCrashPower = ($maxCrashPower > $session->maxCrashPower) ? $maxCrashPower : $session->maxCrashPower;                    
            $maxCrashVelocity = ($maxCrashVelocity > $session->maxCrashVelocity) ? $maxCrashVelocity : $session->maxCrashVelocity;

            $maxrotations = ($maxrotations > $session->rotations) ? $maxrotations : $session->rotations;

            $numberofjumnps += $session->numberOfAirs;
            $totalHeight += $session->totalHeight;
            $totalAirTime += $session->totalAirTime;
            $timeInWater += $session->duration;
        }

        if ( 
            // $details->maxHeight == $maxHeight &&  

            $details->highestAir == $highestAir &&  
            $details->maxAirTime == $maxAirTime &&  
            $details->maxPop == $maxPop &&  
            $details->maxHpower == $maxHpower &&  
            $details->maxCrashPower == $maxCrashPower &&  
            $details->maxCrashVelocity == $maxCrashVelocity &&

            $details->totalHeight == $totalHeight &&
            $details->totalAirTime == $totalAirTime &&
            $details->totalTimeInWater == $timeInWater &&

            $details->numberOfSessions == $n

        ) { 
            
            return true;

        } else {

            $details->maxHeight = $maxHeight;
            $details->highestAir = $highestAir;
            $details->maxAirTime = $maxAirTime;
            $details->maxPop = $maxPop;
            $details->maxHpower = $maxHpower;
            $details->maxCrashPower = $maxCrashPower;
            $details->maxCrashVelocity = $maxCrashVelocity;

            $details->rotations = $maxrotations;
            $details->numberOfAirs = $numberofjumnps;
            $details->numberOfSessions = count($any);

            $details->totalHeight = $totalHeight;
            $details->totalAirTime = $totalAirTime;
            $details->totalTimeInWater = $timeInWater;

            $user->details = $details;

            //echo "User ".$details->name." has inconsistent profile\n";
            //$fail++;

            return $user->update();
        }
    }


    /**
     * Updates user profile
     * $session has been DELETED, 
     * 
     * Updates leaderboards context
     *
     * @param Session $session The session object to be deleted
     *
     * @version 1.4.1
     *          ADD     Redis management
     *          ADD     Check profile maximum values are above 0
     *          
     */
    public function removeSessionData( $session ){

        $redis =    new RedisController(0);
        $details =  new UserDetails();
        $details =  (object) $this->details;
        $report =   array();
        $debug =    false;

        /**
         *  Check User profile records
         *  In case the session was a profile record, we need to look for a new one
         */

        $features = array('highestAir','maxAirTime','maxPop','maxHpower','maxCrashPower','maxCrashVelocity');
        foreach ($features as $feature) {
            $report[$feature] = false;
            if( isset($details->{$feature}) ) {
                if( isset($session->{$feature}) ) {
                    if( $details->{$feature} == $session->{$feature} )
                        $report[$feature] = true;
                }
                else 
                    $session->{$feature} = 0;
            }
            //else 
            //    $details->{$feature} = 0;
        }

        //if( $details->maxGforce == $session->maxGforce )
        //    $report['maxGforce'] = true;
        //if( $details->longestRotation == $session->longestRotation )
        //    $report['longestRotation'] = true;

        $h = 0;
        $t = 0;

        $p = 0;
        $hp = 0;
        $cp = 0;
        $cv = 0;

        //$g = 0;
        //$r = 0;

        // I want to ONLY update user profile now

        // $c = new EMongoCriteria;
        // $c->addCondition( '_user', $this->_id );
        // $c->addCondition( '_spot', $session->_spot );
        // $c->addCondition( '_session', $session->_id, '<>' );
        // $c->compare('_session','<>'.$session->_id);
        
        // Here we might filter a little bit more when dealing with > 1K sessions per user
        // $sessions = iterator_to_array( Session::model()->find( $c ), false) ;

        $q = array( 
            '_user'=>  $this->_id,
            //'_spot'=> $session->_spot,
            '_id' => array('$ne' => $session->_id)
        );
        $sessions = iterator_to_array( Yii::app()->mongodb->session->find($q), false );

        // print_r(count($sessions)); die;

        if( $debug )
            echo "Borrando: {$session->name} ... {$session->_spot} \n";

        if( isset($sessions) && !empty($sessions) )
            foreach ($sessions as $s ) {
                $s = (object)$s;
                if( (string) $session->_id !== (string) $s->_id) {
                    if( $h < $s->highestAir ){
                        $h = $s->highestAir;
                        $report['highestAir'] = $s;
                        if( $debug )
                            echo "mi mejor session en highestAir es {$s->name}:{$h} \n";
                    }
                    if( $t < $s->maxAirTime ) {
                        $t = $s->maxAirTime;
                        $report['maxAirTime'] = $s;
                        if( $debug )
                            echo "mi mejor session en maxAirTime es {$s->name}:{$t} \n";
                    }

                    /**
                     * New Air features for 1.6
                     */
                    
                    if( $p < $s->maxPop ){
                        $p = $s->maxPop;
                        $report['maxPop'] = $s;
                    }
                    if( $hp < $s->maxHpower ) {
                        $hp = $s->maxHpower;
                        $report['maxHpower'] = $s;
                    }
                    if( $cp < $s->maxCrashPower ){
                        $cp = $s->maxCrashPower;
                        $report['maxCrashPower'] = $s;
                    }
                    if( $cv < $s->maxCrashVelocity ) {
                        $cv = $s->maxCrashVelocity;
                        $report['maxCrashVelocity'] = $s;
                    }

                    /*

                    @deprecated 

                    if( $g < $s->maxGforce ) {
                        $g = $s->maxGforce;
                        $report['maxGforce'] = $s;
                        if( $debug )
                            echo "mi mejor session en maxGforce es {$s->name}:{$g} \n";
                    }
                    if( $r < $s->longestRotation ) {
                        $r = $s->longestRotation;
                        $report['longestRotation'] = $s;
                        if( $debug )
                            echo "mi mejor session en longestRotation es {$s->name}:{$r} \n";
                    }
                    */
                }
            }
        /*else { 

            $c = new EMongoCriteria();
            $c->addCondition( '_user',  $this->_id );
            $c->addCondition( '_spot', $session->_spot );
            $c->addCondition( '_session', $session->_id );
            $rankItem = Rank::model()->deleteAll($c);
        }*/

        // DELETING

        $details->totalTimeInWater -=   (float) $session->duration;
        $details->totalHeight -=        (float) $session->totalHeight;
        $details->totalAirTime -=       (float) $session->totalAirTime;

        $details->numberOfAirs = $details->numberOfAirs - $session->numberOfAirs;
        $details->maxAirTime = $t;
        $details->highestAir = $h;

        $details->maxPop = $p;     
        $details->maxHpower = $hp;  
        $details->maxCrashPower = $cp;
        $details->maxCrashVelocity = $cv;

        $details->maxGforce = 0;
        $details->longestRotation = 0;

        $details->numberOfSessions = $details->numberOfSessions - 1;

        $reset = array_merge( $features, array("totalTimeInWater", "totalHeight", "totalAirTime") );

        // FIX
        // Check no values are below 0
        foreach ($reset as $feature) { 
        	if( $details->{$feature} < 0.0 ) {
        		$details->{$feature} = 0.0;
        	}
        }

        $this->details = $details;

        // update profile records
        if( !$debug )
            $this->update();

        // Update spot records in index. Keep index consistency        
        foreach ($features as $key) {

            $c = new EMongoCriteria();
            $c->addCondition( 'feature', $key );
            $c->addCondition( '_user',  $this->_id );
            $c->addCondition( '_spot', $session->_spot );
            $c->addCondition( '_session', $session->_id );

            $rankItem = Rank::model()->findOne($c);

            if( isset($rankItem) ) {
                if( $debug )
                    echo "Existe elemento en Ranking para {$key} su valor es: {$rankItem->score} en: {} \n";

                // find smaller session in the spot for the feature

                // we must find another session which has a record for the leaderboard, when this session 
                // $c = new EMongoCriteria();   
                // $c->addCondition( '_user',  $this->_id );
                // $c->addCondition( '_spot', $session->_spot );
                // $c->addCondition( '_id', $session->_id, "<>" );
                // $c->compare('_id', '<'.$session->_id);

                $q = array( 
                    '_user'=>  $this->_id,
                    '_spot'=> $session->_spot,
                    '_id' => array('$ne' => $session->_id)
                );
                $c = new EMongoCriteria(); 
                $array = iterator_to_array( Yii::app()->mongodb->session->find($q)->sort( array( $key => -1 ) ), false );

                if( !empty($array) )
                    $lower = (object) $array[0];

                if( !isset($lower) ) {
                    if( $debug ) {
                        echo "No hay más sessiones\n";
                    }
                    else {
                        // echo "deleting...{$rankItem->feature}\n";
                        $redisRemove = $redis->remove( $key, $rankItem );
                        $dbRemove = $rankItem->delete();
                        if( $redisRemove  && $dbRemove ){
                            Yii::log("Redis::Removed rank from redis lb $key", "info");
                        } else {
                            Yii::log("Oauth2User::removeSessionData() ERROR deleting Rank", "error");
                        }
                        $rankItem = null;
                    }
                } else {
                    $redisOk = true;

                    if( !$redis->remove( $key, $rankItem ) ) {
                        $redisOk = false;
                        Yii::log("Redis::Cannot delete before updating", "info");
                    }

                    $lowerValue = $lower->{$key};
                    if( $debug )
                        echo "Existe elemento en Session para sustituir {$session->name}:{$rankItem->score} su nombre es: {$lower->name}:{$lowerValue} \n";

                    $rankItem->score = $lowerValue;
                    $rankItem->_session = $lower->_id;
                    $rankItem->_spot = $lower->_spot;
                    if( !$debug ) {
                        //$rankItem->update( array('score','_session','_spot'), true );
                        $dbSave = $rankItem->save();
                        if( $dbSave && $redisOk ) {
                            $redis->add( $key, $rankItem );
                            Yii::log("Redis::Updated session Rank in lb cache $key", "info");
                        } else {
                            Yii::log("Oauth2User::removeSessionData() ERROR updating Rank", "error");
                        }           
                    }
                    $rankItem = null;
                }
            } else if( $debug )
                        echo "No existe elemento en Ranking para {$key}\n";
        }

        if($debug)
            Yii::app()->end();
    }


    /**
     *  Validates that the session data has been correctly inserted
     */
    public function validate( $session ){
        $details = (object) $this->details;
        $keys = (new UserDetails())->getRecordKeys();
        $valid = true;

        foreach ($keys as $key) {
            if( isset($details->$key) && $key !== 'numberOfSessions' && $key !== 'numberOfAirs' ) {
                if( $details->$key < $session->$key ) {
                    $valid = false;
                }
            }
        }
        return $valid;
    }
    
    
    public static function loadUser( $id ){
        $model = Oauth2User::model()->findByPk( array('_id'=>$id) );
        if( isset($model) )
            return $model;
        return null;
    }


    public static function publicAttributes() { 

        $version = Yii::app()->controller->getModule("oauth2")->getClientVersion();

        if( version_compare( $version, Yii::app()->params['version_check'] ) >= 0 ) {  
            $attr = self::$publicAttributes_1_8_1;
        } else {
            $attr = self::$publicAttributes;      
        }

        return $attr;
    }
    
    
    public static function model($classname=__CLASS__) {
        return parent::model($classname);
    }

    /**
     * Generate fullname with no blank spaces in within 
     * 
     * @return String Formatted user name
     */
    public function generateFullname($name, $lastname){
        return strtolower( str_replace(' ', '-', $name) . "-" . str_replace(' ', '-', $lastname) );
    }

    /**
     *  Generate UNIQUE username with no blank spaces 
     *  Retries till a valid unique username has been generated
     * 
     *  @return String Formatted username
     */
    public function generateUsername($name, $lastname) {
        $exit=false;
        $tries=0;
        while(!$exit && $tries<100){
            if( $tries == 0 )
                $username = strtolower( str_replace(' ', '', $name) . ".". str_replace(' ', '', $lastname) );
            else
                $username = strtolower( str_replace(' ', '', $name) . ".". str_replace(' ', '', $lastname) . $tries );

            $c= new EMongoCriteria();
            $c->addCondition('username', $username);
            $result = Oauth2User::model()->findAll($c);

            if($result->count() == 0)
                $exit=true;

            else $tries++;
        }
        return $username;
    }

    /**
     *  Adds a challenge created to challengedetails
     *  Increases the count of created
     *  Updates the enrolled array with a new $challenge key
     * 
     *  Updates challengedetails Object in user document
     *  Updates $challenge Object
     * 
     *  @param Challenge $challenge     If null the ChallengeDetails Object must be created for this user
     *  @param Boolean $mode            If active the ChallengeDetails Object must be created for this user
     *  @throws OAuth2Exception         In case the user is not invited to current challenge.
     *
     */
    public function addChallenge($challenge=null, $mode=false) {

        if( !isset( $this->challengedetails ) || $mode ) {
            $cd = new ChallengeDetails();
            $cd->accepted=0;
            $cd->inscribed=0;
            $cd->declined=0;
            $cd->winned=0;
            if( $challenge ) {
                $cd->enrolled=array( 
                    (string) $challenge->_id => array()
                );
                $cd->organized=1;
            } else {
                $cd->enrolled=array();
                $cd->organized=0;
            }
            
            $cd->lastCheck=0;
            $cd->created = time();
            $this->challengedetails = $cd;

        } else {
            $cd = (object) $this->challengedetails;
            $cd->organized = $cd->organized+1;
            $cd->enrolled=array( 
                (string) $challenge->_id => array()
            );
            $this->challengedetails = $cd;
        }
        
        return $this->update();
    }

    /**
     *  Accepts a challenge invitation (enroles the user in the $challenge)
     * 
     *  Updates challengedetails Object in user document
     *  Updates $challenge Object
     * 
     *  @param Challenge $challenge
     *  @throws OAuth2Exception In case the user is not invited to current challenge.
     *
     */
    public function enroleInChallenge( $challenge ){
        $cd = (object) $this->challengedetails;

        if( !isset($cd) ) {
            $cd = new ChallengeDetails();
            $cd->init();
        }

        //print_r( $cd );

        if( !$challenge->isInvited( (string) $this->_id )  )
            throw new OAuth2Exception( array('code' => BAD_CHALLENGE_ID, 'message' => "Challenge not available for this user" ) );

        if( $challenge->type == 'public' )
            $cd->inscribed++;
        elseif( $challenge->type == 'private' && $challenge->isInvited( (string) $this->_id ) )
            $cd->accepted++;
        else if( $challenge->_creator == $this->_id )
            return true;

        if( $challenge->isEnrolled( (string) $this->_id) )
            throw new OAuth2Exception( array('code' => BAD_CHALLENGE_ID, 'message' => "The user is already enrolled" ) );

        $challenge->enroll( (string) $this->_id );
        $cd->enrolled[ (string) $challenge->_id ] = array();

        $this->challengedetails = $cd;
        if( $this->update() ) { 
            if( $challenge->update() ) {
                return true;
            } else {
                // Rollback needed here
                // Extend Challenge Model
                return false;
            }
        }
        
        return false;
    }

    /**
     *  Rejects a challenge invitation
     * 
     *  Updates challengedetails Object in user document
     * 
     *  @param Challenge $challenge
     *  @throws OAuth2Exception In case the user is not invited to current challenge.
     *
     */
    public function declineChallenge( $challenge ){ 
        $cd = (object) $this->challengedetails;

        if( ! isset($cd) ) {
            $cd = new ChallengeDetails();
            $cd->init();
        }

        if( !$challenge->isInvited( (string) $this->_id )  )
            throw new OAuth2Exception( array('code' => BAD_CHALLENGE_ID, 'message' => "Challenge not available for this user" ) );

        $cd->declined = $cd->declined+1;
        $this->challengedetails = $cd;
        
        return $this->update();
    }

    /**
     *  Parses the enrolled array inside challengedetails object in the user document
     *  Gets all primary keys from there and requests the belonging $challenge Objects updating the session array within.
     * 
     *  Updates Challenge Object
     * 
     *  @param Challenge $challenge
     *  @throws OAuth2Exception In case the user is not invited to current challenge.
     *
     */
    public function addSessionToChallenges( $session ){ 
        $cd = (object) $this->challengedetails;
        if( !isset($cd) )
            throw new OAuth2Exception( array('code' => BAD_CHALLENGE_ID, 'message' => "User not registered to this challenge." ) );

        if( !empty( $cd->enrolled )) {
            $challenges = array_keys( $cd->enrolled );
            $chArr = iterator_to_array( Challenge::model()->findAllByPk( $challenges ), false );

            foreach ($chArr as $challenge ) {

                if( $challenge->isEnrolled( (string) $this->_id ) )
                    $challenge->addSession( $session );
            }
        }
    }


}

?>
