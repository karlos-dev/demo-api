<?php

/**
 * Like mongoDb Document Class
 * 
 * @author      Karlos Álvarez <karlos.alvan@gmail.com>
 * @abstract    Gives database support to a single Comment
 * @uses        php mongoDb extension to handle nosql ODM 
 */

class Like extends EMongoDocument{
    /**
     * @var Float 
     */
    public $_user; 
    /**
     * @var Float 
     */
    public $_session;     
    /**
     * @var Float 
     */
    public $created;     
    
    
    /**
     * @internal 
     */
    public function collectionName() {
        return 'like';
    }
    
    /**
     * @internal 
     */
    public function rules() {
        return array(
            array('created, _user, _session', 'required'),            
        );
    }
    
    /**
     * @internal 
     */
    public static function model($classname=__CLASS__) {
        return parent::model($classname);
    }

    /**
     * @internal
     * Validates Like POST attributes and prepares the object to be saved
     */
    public function validateParams(){
        $filters = array(        
            "sessionId" => array(
                "filter"=> FILTER_VALIDATE_REGEXP,
                "options"=>array("regexp"=>"/[a-z0-9\s]{24}/") ),          
            /*"userId" => array(
                "filter"=> FILTER_VALIDATE_REGEXP,
                "options"=>array("regexp"=>"/[a-z0-9\s]{24}/") ),      
            */
        );        
        
        $input = filter_input_array(INPUT_POST, $filters);        
        
        
        /**
         * @abstract Time here to parse text agaisnt words or contexts
         */

        if( isset( $input['sessionId'] ) /*&& isset( $input['userId'] ) */ ){
            //$this->_user= new MongoId( $input['userId']);
            $this->_session = new MongoId( $input['sessionId']);
            $this->created = time();
            
            return $input;    
        } 

        return false;
    }

}

?>
