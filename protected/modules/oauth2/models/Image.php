<?php
/**
 * Image mongoDb Document Class
 * 
 * @author      Karlos Álvarez <karlos.alvan@gmail.com>
 * @abstract    Manages and stores uploaded images
 * @uses        php mongoDb extension to handle nosql ODM 
 */

class Image extends EMongoDocument{
    /**
     * @var Timestamp Image system creation time
     */
    public $created;            
    /**
     * @var String  Image filename
     */
    public $filename;               
    /**
     * @var mongoId  User uploading the image
     */
    public $_user;   
    /**
     * @var mongoId  Session the image belongs to
     */
    public $_session;    
    /**
     * @var String if the image is for user profile, or for a recorded session
     *      Values: user | cover | session
     */
    public $type;        
    /**
     * @var String  Mime type for image
     */
    public $mimetype;
    /**
     * @var String  Url for the image
     */
    public $url;    
    /**
     * @var Integer Spot location Latitude coordinate
     */
    public $size;
    
    /**
     * @internal 
     */
    public function collectionName() {
        return 'image';
    }

    /**
     * @throws 
     *     Oauth2Exception
     *     Exception
     *     MongoException
     */
    public function getMostRecientImage($type){

        return "Check this code";

        $oauth = YiiOAuth2::instance();
        $token = $oauth->verifyToken();

        $currentUser = $oauth->loadUser($token->oauth_token);
        $c = new EMongoCriteria();
        $c->_user = (String) $currentUser->_id;
        $c->type = $type;
        $c->limit(1)->sort('created', EMongoCriteria::SORT_DESC);
        $img = Image::model()->find($c);

        return $img;
    }
    
    /**
     * @internal 
     */
    public function rules() {
		return array(
			array('size, created', 'numerical'),            
		);
	}
    
    /**
     * @internal 
     */
    public static function model($classname=__CLASS__) {
        return parent::model($classname);
    }


}

?>
