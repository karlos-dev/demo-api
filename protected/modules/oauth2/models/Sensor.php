<?php

/**
 * Sensor mongoDb Document Class
 * @author      Karlos Álvarez <karlos.alvan@gmail.com>
 * @abstract    Manages and stores sensor data
 * @uses        php mongoDb extension to handle nosql ODM 
 *
 * @since 0.10.2 
 */

define("SADMIN",   '["created", "wooid", "_owner", "currentFwv","isRegistered", "ownerStory", "errors", "fwStory" ]');
define("SDEFAULT", '["created", "wooid", "_owner", "currentFwv","isRegistered" ]');
define("SPARTIAL", '[ "wooid", "_owner", "currentFwv" ]');

class Sensor extends EMongoDocument {
    /** 
     * @virtual 
     */
    public $_id;
    /**
     * @var Timestamp Sensor ODM creation time
     */
    public $created;    
    /**
     * @var String The sensor unique id
     */
    public $wooid;    
    /**
     * @var mongoId Current owner user reference
     */
    public $_owner;   
    /**
     * @var String The firmware version loaded in the sensor
     */
    public $currentFwv; 
    /**
     * @var Array 
     */
    public $errors;
    /**
     * @var Array 
     * { fwversion-timestamp-userid }
     */
    public $fwStory;
    /**
     * @var Array 
     * { (register|unregister) r|u-timestamp-userid }
     */
    public $ownerStory;
    /**
     * @var Boolean The registration status for this sensor
     */
    public $isRegistered; 
    
    /**
     * @internal 
     */
    public function collectionName(){
        return 'sensor';
    }

    /**
     * Add new sensor to the database
     * 
     * @param Array     $data   Key value sensor data
     * @param boolean   $fw     If there must be added a fw update to the sensor
     * 
     * @return Boolean          If the document as succesfully saved
     * 
     * @version 1.0.7
     */
    public function addNewWoo( $data, $fw=false ){
        $this->ownerStory = array();
        if($fw)
            $this->fwStory = array( $data['fwv'] ."-".time()."-". $data['owner'] );
        else 
            $this->fwStory = array();

        $this->errors = array();
        $this->currentFwv = $data['fwv'];
        $this->_owner = $data['owner'];
        $this->wooid = $data['wooid'];
        $this->created = time();

        return $this->save();
    }
    
    /**
     * @internal 
     */
    public function rules() {
        return array(
            // array('created', 'numerical'),            
            // array('wooid, currentFwv', 'required'),            
            // array('wooid', 'unique'), 
        );
    }

    /** 
     * @internal Internal ODM management
     */
    public function tableName() {
        return self::collectionName();
    }
    
    /**
     * @internal 
     */
    public static function model($classname=__CLASS__) {
        return parent::model($classname);
    }

    /**
     * [addToUser description]
     * @param [type] $user [description]
     * 
     * @throws Oauth2Exception, Exception
     */
    public function addToUser( $user ){
        // HERE:
        if( $this->isRegistered )
            throw new OAuth2Exception( array('code' => HACKING_ACTIVITY, 'message' => "This device is already registered") );

        // update owner story
        $this->ownerStory[] = "r-".time()."-".$user;

        /*array(
            'action'=>'register',
            'time'=>time(),
            'owner'=>$user,
        );*/

        // update current owner         
        $this->_owner=$user;

        // update isregistered
        $this->isRegistered=true;
        $this->update( array('ownerStory', '_owner', 'isRegistered') );
    }

    /**
     * Updates the firmware in the sensor
     * @param String $user          The user id
     * @param String $fwversion     The fw version installed
     */
    public function updateFirmware( $user, $fwversion ){

        // update fwStory story
        $this->fwStory[] = $fwversion ."-".time()."-".$user;
        $this->currentFwv=$fwversion;
        $this->update( array('fwStory', 'currentFwv') );
    }    

    /**
     * Adds error to the sensor structure and updates the document
     * @throws MongoException, Exception
     */
    public function addError($id){

        if( is_array($this->errors) )    
            $this->errors[]=$id;
        else {
            $this->errors = array( );
            $this->errors[] =$id;
        }

        return $this->update( array('errors') );
    }

    /**
     * Checks if the input user id is present within the sensor owner log
     * @param  String $userId The 24bytes mongoid in string format
     * @return boolean         Wether the usertr has been found in the log or not
     */
    public function hasBeenOwner( $userId ){

        foreach ($this->ownerStory as $reg) {
            
            $regArray = explode('-', $reg);
            if( isset($regArray[2]) )
                if( $regArray[2] === $userId )
                    return true;
        }
        return false;
    }

    /**
     * [removeFromUser description]
     * @param  [type] $user [description]
     * @return [type]       [description]
     */
    public function removeFromUser( $user ){
        // HERE:
        if( !$this->isRegistered )
            throw new OAuth2Exception( array('code' => HACKING_ACTIVITY, 'message' => "This device is not registered") );
        // update owner story
        $this->ownerStory[] = "u-".time()."-".$user;
        /*$this->ownerStory[] = array(
            'action'=>'unregister',
            'time'=>time(),
            'owner'=>$user,
        );*/

        // update current owner         
        $this->_owner=null;

        // update isregistered
        $this->isRegistered=false;

        $this->update(array('ownerStory', '_owner', 'isRegistered'));
    }

    public static function data( $version=null ){
        $projection = json_decode(SDEFAULT);

        return $projection;
    }
}

?>
