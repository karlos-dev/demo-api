<?php

/**
 * Comment mongoDb Document Class
 * 
 * @author      Karlos Álvarez <karlos.alvan@gmail.com>
 * @abstract    Gives database support to a single Comment
 * @uses        php mongoDb extension to handle nosql ODM 
 */

class Comment extends EMongoDocument{
    /**
     * @var String 
     */
    public $_user; 
    /**
     * @var String 
     */
    public $_session;     
    /**
     * @var Integer 
     */
    public $created;     
    /**
     * @var String  
     */
    public $text;        
    
    /**
     * @internal 
     */
    public function collectionName() {
        return 'comment';
    }
    
    /**
     * @internal 
     */
    public function rules() {
		return array(
			//array('created, text', 'required'),            
		);
	}
    
    /**
     * @internal 
     */
    public static function model($classname=__CLASS__) {
        return parent::model($classname);
    }

    /**
     * @internal
     */
    public function validateParams(){
        $filters = array(        
            "sessionId" => array(
                "filter"=> FILTER_VALIDATE_REGEXP,
                "options"=>array("regexp"=>"/[a-z0-9\s]{24}/") ),          
            "text" => array(
                "filter"=> FILTER_VALIDATE_REGEXP,
                "options"=>array("regexp"=>"/[.*]{0,1200}/") ),
        );        
        
        $input = filter_input_array(INPUT_POST, $filters);        
        
        /**
         * @abstract Time here to parse text agaisnt words or contexts
         */
        if( isset( $input['sessionId'] ) && isset( $input['text'] ) ){
            $this->text= $input['text'];            
            $this->_session = new MongoId( $input['sessionId']);
            $this->created = time();
            
            return $input;    
        } 

        return false;
    }

}

?>
