<?php

/**
 * Kinematiq project
 * 
 * Rank 
 * Stores data to hold leaderboard information based on session and feature
 * 
 */

class Rank extends EMongoDocument {

    /**
     * The feature value
     * @var string $score
     */
    public $score;

    /**
     * The identifier of the spot where session was taken
     * @var MongoId $_spot
     */
    public $_spot;

    /**
     * The identifier of the session
     * @var MongoId $_session
     */
    public $_session;

    /**
     * The identifier of the spot where session was taken
     * @var MongoId $_user
     */
    public $_user;

    /**
     * The feature represented by this document
     * @var string $feature
     */
    public $feature;

    /**
     * The timestamp of the session recorded for this score
     * @var timestamp $created
     */
    public $created;

    private $_indexes = array('airtime', 'height');


    /**
     * @internal
     */
    public function init() {
        $this->ensureIndexes = true;
    }

    /**
     * @internal
     */
    public function collectionName() {
        return 'rank';
    }

    /**
     * @internal
     */ 
    public function indexes() {
        /*
        $indexes = array();

        foreach ($this->_indexes as $name) {
            $indexes[ {$name} ] = array(
                'key' => array(
                        ''
                    )

            )
        }
        */
        return array(
            'score_index'=>array(
                'key'=>array(
                    'score'=>EMongoCriteria::SORT_DESC,
                ),

            ),
        );
    }

    /**
     * @internal
     */
    public function rules() {
        return array();
    }    

    /**
     * @internal
     */
    public static function model($classname=__CLASS__) {
        return parent::model($classname);
    }

}

?>