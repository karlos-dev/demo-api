<?php

/**
 * PassCode mongoDb Document Class
 * 
 * @author      Karlos Álvarez <karlos.alvan@gmail.com>
 * @abstract    Manages and stores temporal security codes for account recovery
 * @uses        php mongoDb extension to handle nosql ODM 
 */

class PassCode extends EMongoDocument {
    /**
     * @var Timestamp Image system creation time
     */
    public $created;    
    /**
     * @var Timestamp Image system creation time
     */
    public $expires;    
    /**
     * @var mongoId  User uploading the image
     */
    public $user;   
    /**
     * @var String  The safe code itself
     */
    public $code;        
    
    /**
     * @internal 
     */
    public function collectionName() {
        return 'passcode';
    }
    
    /**
     * @internal 
     */
    public function rules() {
		return array(
			array('expires, created', 'numerical'),            
		);
	}
    
    /**
     * @internal 
     */
    public static function model($classname=__CLASS__) {
        return parent::model($classname);
    }
}

?>
