<?php

/**
 * WOO API
 * 
 * ChallengeDetails Model
 * @since  1.3.0
 * @version  New challnenge feature added on 1.3.0
 * @author  Karlos Alvarez <karlos.alvan@gmail.com>
 * 
 */


class ChallengeDetails extends EMongoDocument {
    
    /**
     * The timestamp in whihc this register is created.
     * Similar to the time in which the user first enrolled any Challenge
     * @var Unix timestamp
     */
    public $created;

    /**
     * The number of invitations to challenges that the user accepted
     * @var Integer
     */
    public $accepted;
    /**
     * The number of inscriptions to challenges by user initiative
     * @var Integer
     */
    public $inscribed;
    /**
     * The number of challenge invitations the user declined
     * @var Integer
     */
    public $declined;
    /**
     * The number of challenges created by this user
     * @var Integer
     */
    public $organized;

    /** 
     * Contains a resume of the challenges accepted by the user the position obtained and the score of that challenge
     * @var $arrayName = array('$_challengeId' => array( 'position' => 'score' ) );
     */
    public $enrolled;

    /**
     * Number of challenges winned by the current user
     * @var Integer
     */
    public $winned;

    /**
     * Timestamp of the last time the user was informed about challenge invitations
     * @var Integer UNIX timestamp
     */
    public $lastCheck;


     /**
     * @internal
     */    
    public function __construct() {
        parent::__construct();
    }

    public function rules() {
        return array();
    }

    public function init() {
        $this->accepted=0;
        $this->inscribed=0;
        $this->declined=0;
        $this->winned=0;
        $this->enrolled=array();

        $this->organized=0;
        
        $this->lastCheck=0;
        $this->created = time();

        return $this;
    }

    public static function model($className=__CLASS__){
        return parent::model($className);
    }

    /**
     * @internal
     */
    public function validate($attributes = NULL, $clearErrors = true){
        $validator = new EMongoUniqueValidator();
        return $validator->validateAttribute($this, '_user');
    }
}