<?php

/**
 * WOO API
 * 
 * UserDetails Model
 * @author      Karlos Alvarez <karlos.alvan@gmail.com>
 * @since       0.1.0 
 *             
 *             Added new fields maxPop, maxHpower, crash features and rotations
 * 
 *             Added accomulative fields
 * 
 *             Removed Gforce
 * 
 */

class UserDetails extends EMongoDocument {
    
    public $modified;
    public $name;
    public $lastname;
    public $fullname;
    public $country;
    public $gender;
    public $birthday;
    public $weight;

    /**
     * Highest jump of the user
     * Modified user profile with new user profile metrics information about the user activity during session recording
     * 
     * @var highestAir
     */
    public $highestAir;     // float
    /**
     * Longest jump of the user
     * 
     * @var maxAirTime
     */
    public $maxAirTime;     // float

    /**
     * Maximun power at jump start
     * 
     * @var maxPop
     */
    public $maxPop;
    public $maxHpower;
    public $maxCrashPower;
    public $maxCrashVelocity;
    public $rotations;

    public $longestRotation;
    /**
     * Total number of sessions the user has done
     * 
     * @var numberOfSessions
     */ 
    public $numberOfSessions;  
    /**
     * Total number of airs the user has done
     * 
     * @var numberOfAirs
     */  
    public $numberOfAirs;

    public $totalTimeInWater;
    public $totalHeight;
    public $totalAirTime;

    public $_homespot;    
    public $_pictures;

    // to be deprecated
    public $maxGforce;      // float

    /** @virtual */
    public static $privateDetails = array('weight', 'birthday', 'gender');
    
    /**
     * @internal
     */    
    public function __construct() {
        //$this->created = true;
        //$this->modified = true;
        parent::__construct();
    }

    /**
     * @internal
     */
    public function rules() {
        return array(
            array('_pictures', 'subdocument', 'type' => 'many'),
            array('name, lastname, country, gender, birthday, weight, highestAir, numberOfAirs, maxAirTime, numberOfSessions', 'safe', 'on' => 'search'),
        );
    }

    public function reset(){
        $this->highestAir = 0;
        $this->numberOfAirs = 0;
        $this->maxAirTime = 0;
        $this->maxGforce = 0;
        $this->longestRotation = 0;
        $this->numberOfSessions = 0;
    }

    public function getRecordKeys(){
        return array( 'highestAir','maxAirTime','maxPop','maxHpower','maxCrashPower','maxCrashVelocity','numberOfAirs','numberOfSessions' );
    }

    public static function model($className=__CLASS__){
        return parent::model($className);
    }

    /**
     * @internal
     */
    public function validate($attributes = NULL, $clearErrors = true){
        $validator = new EMongoUniqueValidator();
        return $validator->validateAttribute($this, 'client_id');
    }
}

?>
