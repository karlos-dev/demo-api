<?php

/**
 * Woo API Continent ODM
 * 
 * Stores abstract data to represent a continent
 * and Spot data along his part of the world
 */

class Continent extends EMongoDocument {

    /** @virtual */
    public $_id;
    
    /**
     * The name of the continent
     * @var string $name
     */
    public $name;

    /**
     * The list of countries which geographically belong to this continent
     * @var array $_countries
     */
    public $_countries;    
    
    /**
     * The list of spot ids belonging this continent
     * @var array $_spot
     */
    public $_spots;

    /**
     * The overall number of recorded sessions in this Spot
     * @var integer $totalSessions
     */
    public $totalSessions;

    /**
     * Array holding the people with recorded sessions within the continent
     * @var array $_kiters
     */
    public $_kiters;

    /**
     * The overall distinct number of kiters having sessions within theçis Spot
     * @var integer $totalKinters
     */
    public $totalKiters;

    /**
     * The timestamp of the last session recorded in any Spot in the continent
     * @var timestamp $lastSessionTime
     */
    public $lastSessionTime;

    /**
     * [registerNewSessionData description]
     * @param  [type] $session [description]
     * @return [type]          [description]
     */
    public function registerNewSessionData( $session ){ 
        $this->totalSessions++;
        $this->lastSessionTime = $session->created;

        if( !in_array( (string) $session->_user, $this->_kiters ) ) {
            $this->totalKiters++;
            $this->_kiters[] = (string) $session->_user;
        }

        $this->save();
    }

    /**
     * @internal
     */
    public function collectionName() {
        return 'continent';
    } 

    /**
     * @internal
     */
    public function rules() {
		return array();
	}    

    /**
     * [addspot]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function addspot( $id ){
        if( !is_array($this->_spots) )
            $this->_spots = array();

        $this->_spots[] = $id ;
        $this->update( array('_spots') );
    }

    /**
     * [removespot]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function removespot( $id ){
        if( !is_array($this->_spots) )
            $this->_spots = array();

        $this->_spots = ArrayHelper::deleteIdFromArray( $id, $this->_spots);
        $this->update( array('_spots') );
    }

    /**
     * Reduce or extend continent data to be sent 
     * @param  Arrays $rules Array with attribute names without leading underscore character
     * @return Continent filtered continent
     */
    public function filter( $rules ){
        foreach ($rules as $rule => $status) {
            switch ($rule) {
                case 'spots':
                    if( $status == 0)
                        unset( $this->_spots );
                    break;
                case 'kiters':
                    if( $status == 0)
                        unset( $this->_kiters );
                    break;
                 
                 default:
                     break;
            }  
        }
        unset( $this->_countries );
        return $this;        
    }


    /**
     * @internal
     */
    public static function model($classname=__CLASS__) {
        return parent::model($classname);
    }

}

?>