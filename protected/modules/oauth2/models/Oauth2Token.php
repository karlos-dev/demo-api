<?php


/*
 * Kinematiq test project
 * 
 * User Model
 * Makes use of the mongoDb extension to handle persistence on noSql environment
 * 
 */

class Oauth2Token extends EMongoDocument{
    
    // `oauth_token` varchar(40) NOT NULL,
    public $oauth_token; 
    // `token_type` enum('code', 'access', 'refresh') default 'code',
    public $token_type; 
    //`client_id` varchar(20) NOT NULL,
    public $client_id; 
    // `user_id` int(11) unsigned NOT NULL,
    public $user_id; 
    //`expires` int(11) NOT NULL,
    public $expires; 
    //`redirect_uri` varchar(200) NOT NULL default 'oob',
    public $redirect_uri;
    //`scope` varchar(200) DEFAULT NULL,
    public $scope;
    //`created_at` timestamp NOT NULL default CURRENT_TIMESTAMP,
    public $created_at;
    
    public function collectionName() {
        return 'oauth2Tokens';
    }    
    
    public function tableName() {
        return self::getCollectionName();
    }    
    /*
    public function validate($attributes = NULL, $clearErrors = true){
        $validator = new EMongoUniqueValidator();
        $validator->validateAttribute($this, 'oauth_token');
    }
    */
    public function rules(){
        return array(
            //array('oauth_token','unique'),
        );
    }


    public static function model($classname=__CLASS__) {
        return parent::model($classname);
    }
}

?>
