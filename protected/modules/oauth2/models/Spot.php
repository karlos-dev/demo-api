<?php
/**
 * Spot mongoDb Document Class
 * 
 * @author      Karlos Álvarez <karlos.alvan@gmail.com>
 * @abstract    Gives database support to a kite spot
 * @uses        php mongoDb MongoClient library
 *
 * @version 0.8.7
 * @since 0.8.7
 * 
 */

class Spot extends EMongoDocument{
    
    /** @virtual */
    public $_id;

    public $created;            // TS
    public $modified;           // TS
    public $name;               // string
    public $country;            // string
    public $countryCode;        // string
    public $continent;          // string
    public $area;
    public $area_level_1;
    public $lat;                // Integer
    public $lng;                // Integer

    /** 
     * Object for setting the geoLocation for the mongodb internal methods
     * @var Object loc 
     */
    private $loc;

    /**
     * The overall number of recorded sessions in this Spot
     * @var integer $totalSessions
     */
    public $totalSessions;
    /**
     * Array holding the people recording sessions within the spot
     * @var array $_kiters
     */
    public $_kiters;
    /**
     * The overall distinct number of kiters having sessions within theçis Spot
     * @var integer $totalKinters
     */
    public $totalKiters;
    /**
     * The timestamp of the last session recorded in any Spot in the continent
     * @var timestamp $lastSessionTime
     */
    public $lastSessionTime;

    /**
     * Updates the Spot structure upon new session registration
     * @param  Session $session The recient stored session
     *
     * @updated to MongoClient mongo library
     */
    public function registerNewSessionData( $session ) {

        $this->totalSessions++;
        $this->lastSessionTime = $session->created;

        if( !in_array( (string) $session->_user, $this->_kiters ) ) {
            $this->totalKiters++;
            $this->_kiters[] = (string) $session->_user;
        }

        $this->update(array('totalSessions', '_kiters', 'totalKiters', 'lastSessionTime'), true);

        $c = new EMongoCriteria;
        $c->addCondition('name', new MongoRegex('/(' . $this->continent . ')/i') );

        $continent = Continent::model()->findOne($c);

        // Modify the Continent Document
        // Rollback if fail (needed)
        $continent->registerNewSessionData( $session );
    }
    
    /**
     * Updates the spot upon session insert | delete
     * 
     * @param  [integer] $sessionsHere The number of sessions in current spot
     * @param  [mongoId] $user         The user session owner id
     * @param  [Session] $session      Session to be inserted | deleted 
     * 
     * @throws OAuth2Exception
     */
    public function updateSpot($sessionsHere, $user, $session) {
        if( $sessionsHere == 1) {
            $this->totalKiters--;

            $oldKiters = $this->_kiters;
            $this->_kiters = array();
            foreach ($oldKiters as $k ) {
                if( (string) $k !== (string) $user->_id )
                    $this->_kiters[] = $k;
            }
        } 

        if( $this->lastSessionTime == $session->created )
            $this->lastSessionTime = 0;

        $this->totalSessions--;

        // OLD DEPRECATED line
        // if( !$this->update( array('totalKiters', '_kiters', 'totalSessions', 'lastSessionTime'), true))
        
        if( !$this->save() )
            throw new OAuth2Exception( array('code' => UNKNOWN_DB_ERROR, 'message' => "Cannot update Spot") );

        $c = new EMongoCriteria;
        //$c->name = new MongoRegex('/(' . $this->continent . ')/i');
        $c->addCondition('name', new MongoRegex('/(' . $this->continent . ')/i') );

        $sc = new SessionController(0);
        $sessions = $sc->numberOfSessionsInContinent($this->continent, $user->_id );
        $continent = Continent::model()->findOne($c);
        if( !$continent )
            throw new OAuth2Exception( array('code' => UNKNOWN_DB_ERROR, 'message' => "Cannot find continent {$this->continent}") );

        if( $sessions == 1) {
            $continent->totalKiters--;

            $oldKiters = $continent->_kiters;
            $continent->_kiters = array();

            foreach ($oldKiters as $k ) {
                if( (string) $k !== (string) $user->_id )
                    $continent->_kiters[] = $k;
            }

            //$continent->_kiters = (array) array_diff( $continent->_kiters, array( (string) $user->_id ) );
        }

        if( $continent->lastSessionTime == $session->created )
            $continent->lastSessionTime = 0;
        $continent->totalSessions--;

        if( !$continent->update(array('totalKiters', '_kiters', 'totalSessions', 'lastSessionTime'), true))
            throw new OAuth2Exception( array('code' => UNKNOWN_DB_ERROR, 'message' => "Cannot update session") );
    }

    /**
     * @internal
     */
    public function collectionName() {
        return 'spot';
    } 

    /**
     * @internal
     */
    public function encode(){
        foreach ($this->attributes as $key => $value) {
            if( gettype($value) == 'object' ) {
                $spot[ '_id' ]= $value;
            } elseif( gettype($key) == 'array') { 
                $spot[ $key ]= $value;
            } else {
                $spot[ $key ]= $this->{$key};
            }
        }
        return $spot;
    }
    
    /**
     * @internal
     */
    public function rules() {
		return array(
			array( 'lat, lng', 'numerical', 'integerOnly'=>false ),       
            array('_id', 'safe'),
		);
	}    
    /**
     * @internal
     */
    public static function model($classname=__CLASS__) {
        return parent::model($classname);
    }

    /**
     * Spot model validation 
     * 
     * Special method created to add spots for admins
     * @param  Object $data The spot data
     * @throws OAuth2Exception
     * 
     * @return Spot The created spot in case of success
     */
    public function validateParams( $data ){

        $filters = array(
            //"name" =>           array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/[a-zA-Z0-9_ ]{1,128}/") ),  
            "name" =>           array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/[\s\S]{1,128}/") ),  
            "area" =>           array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/[\s\S]{1,128}/") ),  
            "area_level_1" =>   array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/[\s\S]{1,128}/") ),  
            "country" =>        array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/[\s\S]{1,128}/") ),
            "continent" =>      array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/[\s\S]{1,128}/") ),
            "countryCode" =>    array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/[\s\S]{2,3}/") ),
            "lat" =>            array("filter" => FILTER_VALIDATE_FLOAT,  "options"=> array("min_range"=>  -90.0, 
                                                                                            "max_range"=>   90.0 ) ),
            "lng" =>            array("filter" => FILTER_VALIDATE_FLOAT,  "options"=> array("min_range"=> -180.0, 
                                                                                            "max_range"=>  180.0 ) ),  
        );
        
        $input = filter_var_array($data, $filters); 

        if( empty( $input['name'] ) || 
            empty( $input['continent'] ) || 
            empty( $input['lat'] ) || 
            empty( $input['lng'] ) || 
            empty( $input['countryCode'] ) 
        ) { 
            throw new OAuth2Exception( array('code' => BAD_POST_DATA, 'message' => "Missing spot data" ));
        }

        $this->name = $input['name'];
        $this->continent = $input['continent'];
        $this->lat = (float) $input['lat'];
        $this->lng = (float) $input['lng'];

        $this->loc = array(
            "type" => "Point",
            "coordinates" => array($this->lng, $this->lat)
        );

        $this->countryCode = $input['countryCode'];
        $this->country = $input['country'];
        $this->area = empty( $input['area'] ) ? "" : $input['area'];
        $this->area_level_1 = empty( $input['area_level_1'] ) ? "" : $input['area_level_1'];

        $this->totalSessions = (integer) 0;
        $this->_kiters = array();
        $this->totalKiters = (integer) 0;
        $this->lastSessionTime = (integer) 0;

        return $this;
    } 

}

?>
