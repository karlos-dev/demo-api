<?php
/**
 * Air mongoDb Document Class
 * 
 * @author      Karlos Álvarez <karlos.alvan@gmail.com>
 * @abstract    Gives database support to a single kite session Air
 * @uses        php mongoDb extension to handle nosql ODM 
 */

class Air extends EMongoDocument {
    /**
     * @var Timestamp Jump during session creation
     */
    public $created;

    /**
     * The quality of the jump
     * dev, fake or valid
     *
     * Only valid jumps will be sent to client
     * @var String
     */
    public $tag;

    /**
     * @var Float 
     */
    public $height;

    /**
     * @var Float 
     */
    public $airtime;

    /**
     * @var Float 
     *
     * to be deprecated
     */
    public $gforce;

    /**
     * @var Array 
     *
     * [ Rotatios_X, Rotations_Y, Rotatios_Z ]
     */
    public $rotations; 

    /**
     * @var Float 
     */
    public $h_power;

    /**
     * @var Float 
     */
    public $pop;

    /**
     * @var Float 
     */
    public $crash_power;

    /**
     * @var Float 
     */
    public $crash_velocity;


    
    /**
     * @internal
     */    
    public function __construct() {        

        parent::__construct();
    }

    /**
     * @internal 
     */
    public function collectionName() {
        return 'air';
    }
    
    /**
     * @internal 
     */
    public function rules() {
		return array(
            array('height, airtime, created', 'numerical'),            
        );
	}

    /**
     * @internal 
     */
    public function validateParams( $obj ) {

        $this->height = $obj->height;
        $this->airtime = $obj->airtime;        
        $this->created = $obj->timestamp;
        
        $version = Yii::app()->controller->getModule("oauth2")->getClientVersion();

        if( version_compare( $version, '1.6' ) >= 0 ) {
            /* jump is from APP >= 1.6 */

            $tag = filter_var( $obj->tag, FILTER_VALIDATE_REGEXP, array( 'options' => array("regexp"=>"/(fake)|(valid)|(dev)/i") ) );
            if( empty( $tag ) ) { 
                // Not a valid tag
                $this->tag = "dev";
            } else
                $this->tag = $tag;

            /**
             * WKN-1375
             * Validate new IOS 1.6 version data
             */
            if( isset( $obj->rotations ) && 
                isset( $obj->pop ) && 
                isset( $obj->crash_power ) && 
                isset( $obj->crash_velocity ) && 
                isset( $obj->h_power )
            ) {
                $this->rotations =              $obj->rotations;
                $this->pop =            (float) $obj->pop;
                $this->crash_power =    (float) $obj->crash_power;
                $this->crash_velocity = (float) $obj->crash_velocity;
                $this->h_power =        (float) $obj->h_power;

            } else throw new OAuth2Exception( array('code' => BAD_AIR_DATA, 'message' => "Incomplete air data") );

        } else {

            /* FLAG the jump, APP < 1.6

            if( $this->height < (1.1 * ( $this->airtime - 3 ) ) ) {
                $this->tag = 'fake';
            } else if( $this->height > 7 && $this->height > ( 9.81 * $this->airtime * ( $this->airtime-1 ) ) ) {
                $this->tag = 'dev';
            } else { 
                $this->tag = 'valid';
            }

            */
            
            $this->tag =            "valid";
            $this->gforce =         $obj->gforce;
            $this->rotations =      array();
            $this->pop =            (float) 0.0;
            $this->crash_power =    (float) 0.0;
            $this->crash_velocity = (float) 0.0;
            $this->h_power =        (float) 0.0;
        }

        return true;
    }
    
    /**
     * @internal 
     */
    public static function model($classname=__CLASS__) {
        return parent::model($classname);
    }
}

?>
