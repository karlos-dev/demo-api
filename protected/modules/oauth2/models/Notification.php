<?php

/**
 * Notification mongoDb Document Class
 * 
 * @author      Karlos Álvarez <karlos.alvan@gmail.com>
 * @abstract    Gives database support to a single Notification
 * @uses        php mongoDb extension to handle nosql ODM 
 */

define("FOLLOW_TEXT", "is following you");
define("UNFOLLOW_TEXT", "no longer follows you");
define("LIKE_TEXT", "liked your session");
define("DISLIKE_TEXT", "no longer likes your session");
define("COMMENT_TEXT", "commented your session");


class Notification extends EMongoDocument{

    /** @virtual */
    public $_id;
    
    /**
     * @var String Identifier for the sender user
     */
    public $_sender; 

    /**
     * @var String the user name
     */
    public $senderName; 

    /**
     * @var String the user lastname
     */
    public $senderLastname;     

    /**
     * @var String the session name
     */
    public $sessionName;  

    /**
     * @var String Identifier for the sender user
     */
    public $_senderImage;

    /**
     * @var String Identifier for the receiver user
     */
    public $_receiver;

    /**
     * @var String 
     */
    public $item;

    /**
     * @var String 
     */
    public $type;     

    /**
     * @var Integer 
     */
    public $created;   

    /**
     * @var boolean If the notification 
     */
    public $read;  


    /**  @virtual  */
    public $_actions = array('follow','unfollow','like','comment','dislike');


    /**  @virtual  */
    public static $_action_text = array( 
                                'follow'=> FOLLOW_TEXT ,
                                'unfollow'=> UNFOLLOW_TEXT,
                                'like'=> LIKE_TEXT,
                                'dislike'=> DISLIKE_TEXT,
                                'comment'=> COMMENT_TEXT,
                            );

    /**
     * Parametric constructor
     * 
     * @param [String] $type            [The type of the push otification]
     * @param [Oauth2User] $sender      [The origin user object]
     * @param [Oauth2User] $receiver    [The notificated user object]
     * @param [Session] $item           [The session object ]
     */
    function __construct($type=null, $sender=null, $receiver=null, $item=null, $name=null, $lastname=null, $sessionname=null) {
        $this->_sender = isset( $sender ) ? $sender->_id : '';
        $this->_receiver = isset( $receiver ) ? $receiver->_id : '';
        $this->item = isset( $item ) ? $item->_id : null;
        $this->type = isset( $type ) ? $type : '';
        $this->created = time();     
        $this->read = 0;    

        $this->senderName = $name;        
        $this->senderLastname = $lastname;        
        $this->sessionName = $sessionname;        
        $this->_senderImage = isset( $sender ) ? $sender->getProfileImage() : '';

        parent::__construct();
    }

  
    /**
     * @internal 
     */
    public function collectionName() {
        return 'notification';
    }
    
    /**
     * @internal 
     */
    public function rules() {
        return array(
            //array('created, item, type, _sender, _receiver', 'required'),            
            //array('created, item, type, _sender, _receiver', 'safe'),
        );
    }
    
    /**
     * @internal 
     */
    public static function model($classname=__CLASS__) {
        return parent::model($classname);
    }

    /**
     * @internal
     * Validates notification attributes and prepares the object to be saved
     
    public function validateParams($params=null){
        $filters = array(        
            "_sender" => array(
                "filter"=> FILTER_VALIDATE_REGEXP,
                "options"=>array("regexp"=>"/[a-z0-9]+{8,24}/") ),
            "name" => array(
                "filter"=> FILTER_VALIDATE_REGEXP,
                "options"=>array("regexp"=>"/[a-z0-9]+{1,128}/") ),
            "lastname" => array(
                "filter"=> FILTER_VALIDATE_REGEXP,
                "options"=>array("regexp"=>"/[a-z0-9]+{1,128}/") ),            
            "_receiver" => array(
                "filter"=> FILTER_VALIDATE_REGEXP,
                "options"=>array("regexp"=>"/[a-z0-9]+{8,24}/") ),  
            "item" => array(
                "filter"=> FILTER_VALIDATE_REGEXP,
                "options"=>array("regexp"=>"/[a-z0-9]+{8,24}/") ),  
            "type" => array(
                "filter"=> FILTER_VALIDATE_REGEXP,
                "options"=>array("regexp"=>"/(follow)|(unfollow)|(like)|(dislike)|(comment)/") ),            
        );        
        
        $input = filter_var_array( $params, $filters);        
        
        
        if( isset( $input['_sender'] ) && 
            isset( $input['_receiver'] ) &&
            isset( $input['name'] ) && isset( $input['lastname'] ) &&
            isset( $input['type'] )
            ){
            // references as mongoIds
            $this->_sender = new MongoId( $input['_sender']);
            $this->_receiver = new MongoId( $input['_receiver']);
            $this->item = isset(  $input['item'] ) ? new MongoId( $input['item']) : '';
            $this->type = $input['type'];
            $this->created = time();
            
            return true;    
        } 

        return false;
    }
    */

    public function send( $rawdata ){
        
        $ch = curl_init( Yii::app()->params['pushurl'] );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);

        /**
         * @abstract
         * When sending to Push server we had issues dealing with POST body data
         * Currently, the server is accepting json encoded body, instead of http_encoded data
         */
        
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        //'Content-Type: application/x-www-form-urlencoded',
                        //'Content-Length: ' . strlen($rawData),
                        'Accept: application/json',
                        //'Authorization: Token token=' . Yii::app()->push['pushIdMasterSecret']
                        'Authorization: Token token=' . Yii::app()->push['pushAccountMasterSecret']
                    ));        
        
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($rawdata) );
        $response = curl_exec($ch);     

        Yii::log("Notification: {$this->type} at {$this->created} [RAW response: {$response}]");  

        return true;
    }

    /**
     * Parses and formates a feed of notifications
     * @return IOS formated notifiction feed
     */
    public static function generateFeed( $array ){
        $feed = array();
        foreach ( $array as $notification ) {
            $obj['_sender'] = $notification->_sender;
            $obj['_senderImage'] = $notification->_senderImage;
            $obj['created'] = $notification->created;
            # $obj['text'] = self::$_action_text[$notification->type];
            $obj['text'] = self::$_action_text[$notification->type];
            $obj['type'] = $notification->type;
            $obj['item'] = $notification->item;
            $obj['_id'] =  $notification->_id;
            $feed[]=$obj;
        }
        return $feed;
    }

}

?>
