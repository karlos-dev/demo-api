<?php

/**
 * evice error mongoDb Document Class
 * @author      Karlos Álvarez <karlos.alvan@gmail.com>
 * @abstract    Manages and stores errors from sensors
 * @uses        php mongoDb extension to handle nosql ODM 
 * 
 * @since 0.10.2 
 */

class SensorError extends EMongoDocument {
    /**
     * @var Timestamp Sensor ODM creation time
     */
    public $created;  
    /**
     * @var String Fw version installed during error
     */
    public $errorFwv;

    /**
     * @var sensorId the woo id string of the sensor
     */
    public $sensorId;    

    /**
     * @var Array 
     */
    public $watchdog;
    /**
     * @var object
     */
    public $boardlevel;
    /**
     * @var object
     */
    public $rssi;

    /**
     * @var object
     */
    public $selftest;


    /**
     * @internal 
     */
    public function collectionName() {
        return 'sensorerror';
    }
    
    /**
     * @internal 
     */
    public function rules() {
        return array(
            array('created', 'numerical'),            
            array('sensorId, errorFwv', 'required'),            
        );
    }

    /**
     * Complete the data inside the object
     * @param  Sensor $s    The sensor where errors where detected
     * @param  Objet  $data The error description
     * @return [type]       [description]
     */
    public function fill($s, $data ){
        
        $this->errorFwv = $s->currentFwv;
        $this->sensorId = $s->_id;
        $this->watchdog = $data->watchdog;
        $this->boardlevel = $data->boardlevel;
        $this->rssi = $data->rssi;
        $this->selftest = $data->selftest;
        $this->created = time();

        return $this->save();
    }
    
    /**
     * @internal 
     */
    public static function model($classname=__CLASS__) {
        return parent::model($classname);
    }
}

