<?php
define("MAX_NUMBER_OF_WINNERS", 3);

/**
 * Challenge mongoDb Document Class
 * 
 * Gives database support to the challenge feature  
 * 
 * In the 1.0 version:  
 * 
 * public and private challenges<br>   
 * height, airtime, numberofAirs possible features<br>
 * accomulated or maximum as possible calculation logic<br>
 * 
 * @author      Karlos Álvarez <karlos.alvan@gmail.com>
 * @uses        php mongoDb extension to handle +nosql ODM 
 * @version     1.0
 */

class Challenge extends EMongoDocument {

    // NOTIFICACIONES
    // newchallenge, winchallenge, startchallenge, endchallenge, cancellecchallenge

    /** @virtual */
    public $_id;

    /**
     * @var Timestamp Challenge system creation time
     */
    public $created;

    /**
     * @var Timestamp If the challenge was modified after creation, this field would not be 0.
     */
    public $modified;

    /**
     * @var         String
     * @example     public | private   The type determines if anyone can enroll to the challenge or is based on invitation
     */
    public $type;

    /**
     * @var Object Spot MongoId
     */
    public $_creator;

    /**
     * @var Array Image
     */
    public $_pictures;

    /**
     * UNIQUE
     * The name of the challenge
     * @var String 
     */
    public $name;

    /**
     * @var String 
     */
    public $description;

    /**
     * @var Timestamp 
     */
    public $start;

    /**
     * @var Timestamp 
     */
    public $end;    

    /**
     * In case it's a private challenge, the invited user ids array
     * @var Array of String user ids 
     */
    public $_invited;

    /**
     * The user id array of the challenge members
     * @var Array of String user ids 
     */
    public $_enrolled;

    /**
     * The user id array of the challenge members
     * @var Array of String user ids 
     */
    public $_rejected;

    /**
     * @var Object Spot MongoId
     */
    public $_location;

    /**
     * @var Array of String Session ids that counted for the challenge
     */
    public $_sessions;

    /**
     * @var         String Valid Air attributes
     * @example     height | airtime | numberOfAirs
     * @version     1.0
     * 
     */
    public $feature;

    /**
     * @var         String Logic to apply with the collected data
     * @example     max | acc
     * @version     1.0
     * 
     */
    public $logic;

    /**
     * @var         String The status of the challenge.
     * @example     active: the challenge is created and open to inscriptions 
     *              closed: the challenge is finished
     *             
     */
    public $status;

    /**
     * @var Integer 
     */
    public $totalInvited;

    /**
     * @var Integer 
     */
    public $numberOfWinners;

    /**
     * @var Array of String user ids
     */
    public $_winners;

    /**
     * @var Object 
     */
    public $price;

    

    private $fields = array( '_sessions','_invited','_enrolled','_winners','_rejected' );

    /**
     * @internal 
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * @internal 
     */   
    public function collectionName() {
        return 'challenge';
    }  

    /**
     * @internal 
     */ 
    public function rules() {
        return array(
            array('name', 'EMongoUniqueValidator', 'className' => 'Challenge', 'attributeName' => 'name'),        
        );
    }

    /**
     * @internal 
     */
    public static function model($classname=__CLASS__) {
        return parent::model($classname);
    }

    /**
     * Challenge images array is defined to have only one image
     * Adds a image or replaces it into the _pictures array
     */
    public function addImage( $img ){
        $data = array(
            'type' => $img->type,
            'url' => $img->url,
        );
        
        $this->_pictures = array();
        array_push($this->_pictures, $data);
        $this->update();
    }


    public static function fields() {
        return array( '_id','name','description','start','end','feature','logic','status','numberOfWinners', 'price','_pictures' );
    }

    public static function publish( $elem, $role=null ) {

        if( $role )
            switch( $role ) {
                case 'admin': break;
                case 'dev': break;
                case 'user': break;
                default: {
                    return $elem;
                }
            }

        else {
            foreach ( $elem->fields as $key ) {
                unset( $elem->{$key} );
            }
            return $elem;
        }
    }

    /**
     *  Validate the input params for a new Challenge
     *  Reads input data from http $_POST
     *
     *  @return boolean If the parameters are valid enought to create a new profile
     */
    public function validateParams( $update=false ){

        $filters = array(
            "name" =>               array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/[\s\S]{1,128}/") ),  
            "description" =>        array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/[\s\S]{1,1024}/") ), 
            "invited" =>            array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/[\s\S]/") ), 
            "start" =>              array("filter" => FILTER_REQUIRE_SCALAR),
            "end" =>                array("filter" => FILTER_REQUIRE_SCALAR),
            "type"=>                array("filter" => FILTER_VALIDATE_REGEXP, 'options' => array("regexp"=>"/(public)|(private)/i") ),
            "feature"=>             array("filter" => FILTER_VALIDATE_REGEXP, 'options' => array("regexp"=>"/(airtime)|(height)|(numberOfAirs)/i") ),
            "logic"=>               array("filter" => FILTER_VALIDATE_REGEXP, 'options' => array("regexp"=>"/(max)|(acc)/i") ),
            "winners" =>            array("filter" => FILTER_VALIDATE_INT),
            "location" =>           array("filter" => FILTER_VALIDATE_REGEXP, "options"=>array("regexp"=>"/[a-z0-9\s]{24}/") ),
        );
        
        $input = filter_input_array(INPUT_POST, $filters);    

        //print_r($input); die;

        if( $update ) {

            $end = isset( $input["end"] ) ? $input["end"] : $this->end;
            $start = isset( $input["start"] ) ? $input["start"] : $this->start;

            if( isset( $input['logic'] ) )
                switch( $input['logic'] ) {
                    case 'max' : {
                        $logic = "max";
                    } break;
                    case 'acc' : {
                        $logic = "acc";
                    } break;
                }

            $this->modified = time();
            $this->name = isset( $input["name"] ) ? $input["name"] : $this->name;
            $this->description = isset( $input["description"] ) ? $input["description"] : $this->description;
            $this->start = (int) $start;
            $this->end = (int) $end;

            $this->type = isset( $input["type"] ) ? $input["type"] : $this->type;
            $this->_pictures = array();
                
            if( isset( $input['invited'] ) ) {
                $new = json_decode( $input['invited'] );
                foreach ($new as $invitedId ) {
                    if( !in_array( $invitedId, $this->_invited ) )
                        $this->_invited[] = $invitedId;
                }
            }

            $this->feature = isset( $input["feature"] ) ? $input["feature"] : $this->feature;
            $this->logic = isset( $input["logic"] ) ? $input["logic"] : $this->logic;
            //$this->logic = $input["logic"];
            $winners = isset( $input["winners"] ) ? ( $input["winners"] > MAX_NUMBER_OF_WINNERS ) ? MAX_NUMBER_OF_WINNERS : $input['winners'] : $this->numberOfWinners;
            // always MAX of 3 users
            $this->numberOfWinners = $winners;

            $this->_location = isset( $input["location"] ) && !empty( $input['location'] ) ? $input["location"] : $this->_location;

            $this->totalInvited = count( $this->_invited );

            return true;

        } elseif( 
                !empty( $input["name"] ) && !empty( $input["description"] ) && 
                !empty( $input["start"] ) && !empty( $input["end"] ) && 
                !empty( $input["feature"] ) && !empty( $input["logic"] ) && 
                !empty( $input["type"] ) && !empty( $input["winners"] ) 
            )
        {

            $duration = $input["end"] - $input["start"];

            $seg_extra = $duration % 60;
            $min = $duration / 60;
            $min_extra = $min % 60;
            $hours = $min / 60;
            $hours_extra = $hours % 24;
            $days = intval( $hours / 24 );

            $d = $days;

            $ends = $input["end"] - time();
            $seg_extra = $ends % 60;
            $min = $ends / 60;
            $min_extra = $min % 60;
            $hours = $min / 60;
            $hours_extra = $hours % 24;
            $days = intval( $hours / 24 );

            switch( $input['logic'] ) {
                case 'max' : {
                    $logic = "max";
                } break;
                case 'acc' : {
                    $logic = "acc";
                } break;
            }

            //$timestamp = new DateTime('05:00');
            //$diff = $timestamp->diff(new DateTime());
            //echo $diff->format('%h hours, %i minutes');
            
            $this->created = time();
            $this->modified = 0;

            $this->setInvited( array() );
            $this->setEnrolled( array() );            
            $this->setSessions( array() );
            $this->setWinners( array() );
            $this->setRejected( array() );
            
            $this->name = $input["name"];
            $this->description = $input["description"];
            $this->start = (int) $input["start"];
            $this->end = (int) $input["end"];
            $this->type = $input["type"];
            $this->_pictures = array();

            if( $this->type == 'public') {
                $this->status = 'active';
                if( $update )
                    if( isset( $input['invited'] ) )
                        $this->_invited = array_merge($this->_invited, json_decode(  $input['invited'] )  );
                else
                    $this->_invited = array();
            } else {
                $this->status = 'active';
                //echo $input['invited'] ;
                //print_r(json_decode( $input['invited'] ));
                //die;
                if( $update ) {
                    if( isset( $input['invited'] ) )
                        $this->_invited = array_merge($this->_invited, json_decode(  $input['invited'] )  );
                } else {

                    if( isset( $input['invited'] ) )
                        $this->_invited = json_decode( $input['invited'] );
                    else 
                        $this->_invited = array();
                }
                
            }

            $this->feature = $input["feature"];
            $this->logic = $input["logic"];

            //always MAX of 3 users
            $this->numberOfWinners = ( $input["winners"] > MAX_NUMBER_OF_WINNERS ) ? MAX_NUMBER_OF_WINNERS : $input['winners'];

            if( !empty( $input['location']) ) {
                $this->_location = $input['location'];
            } else
                $this->_location = "worldwide";

            $this->totalInvited = count( $this->_invited );

            //echo "Challenge parameters are correct\n";            
            //echo "{$days} days, {$hours_extra} hours, {$min_extra} minutes, {$seg_extra} segundos\n";
            //echo "Challenge is about {$logic} {$input["feature"]}\n";

            $res = array(
                "duration" => $d,
                "ends" => "{$days} days, {$hours_extra} hours, {$min_extra} minutes, {$seg_extra} segundos"
            );

            return true;
        } else {
            // echo "Challenge parameters are missing";            
            return false;
        }
    }

    /**
     *  Inserts session data into the challenge object
     *  Validates the location and the timestamps of session recording (time) and server save ts (created)
     *  
     *  @version 
     *  
     *  - check if session is registered in challenge already
     *
     * @param $session Session ODM to be inserted in the challenge
     */

    public function addSession( $session ) { 
        // The session is recorder in the spot, if not worldwide
        if( $this->_location !== 'worldwide' && (string) $session->_spot == $this->_location ) { 
            // The session time, must be recorded after the start time
            if( !in_array((string) $session->_id, $this->_sessions) && $this->start < $session->time && $this->end > $session->created ) {
                $this->_sessions[] = (string) $session->_id;
                return $this->update();
            }
        }
        return false;
    }

    public function sessions(){
        return $this->_sessions;
    }

    public function invited(){
        return $this->_invited;
    }

    public function enrolled(){
        return $this->_enrolled;
    }

    public function winners(){
        return $this->_winners;
    }

    protected function setSessions( $item ){
        if( is_array($item) )
            $this->_sessions = $item;
    }

    protected function setInvited( $item ){
        if( is_array($item) )
            $this->_invited = $item;
    }

    protected function setEnrolled( $item ){
        if( is_array($item) )
            $this->_enrolled = $item;
    }

    protected function setWinners( $item ){
        if( is_array($item) )
            $this->_winners = $item;
    }

    protected function setRejected( $item ){
        if( is_array($item) )
            $this->_rejected = $item;
    }

    public function isInvited( $id ) { 
        return in_array($id, $this->_invited );
    }

    public function isEnrolled( $id ) { 
        return in_array($id, $this->_enrolled ) || ( $id == (string) $this->_creator ) ;
    }

    public function isOwner( $id ) { 
        return ( $id == $this->_creator );
    }

    public function isFinished() { 
        return ( $this->end < time() );
    }

    public function isStarted() { 
        return ( $this->start < time() );
    }

    public function enroll( $id ) { 
        if( ! in_array($id, $this->_enrolled ) )
            $this->_enrolled[] = $id;
    }

    public function decline( $id ) { 
        //if( in_array($id, $this->_invited ) ) { 
        if (($key = array_search( $id , $this->_invited)) !== false) {
            $this->_rejected[] = $this->_invited[$key];
            
            unset( $this->_invited[$key] );
        }
        
    }

}