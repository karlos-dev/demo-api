<?php
/**
 * Kinematiq API
 * Friendship MongoEmbeddedDocument
 * 
 * Model storing all friendship related attributes
 * 
 * @author      Karlos Álvarez <karlos.alvan@gmail.com>
 * @uses        YiiMongoDbSuite ActiveRecord EMongoEmbeddedDocument declaration for Yii to be used with mongoDb
 */

class Friendship extends EMongoEmbeddedDocument{
    
    public $friendId;
    public $pending;
    public $accepted;
    public $blocked;
    
    //public function __construct( $id = null ) {
    public function __construct( $id = 0, $mode = '' ) {
        if( $id == 0 ) {
            $this->pending = true;
            $this->accepted = ( $mode == 'activo' );
            $this->friendId = $id;
            $this->blocked = false;
        }
        parent::__construct();
    }
    
    public function rules() {
        return array(
            //array('client_id, client_secret', 'required'),
        );
    }
    
    public function block() {
        $this->pending = false;
        $this->accepted = false;        
        $this->blocked = true;
    }
    
    public function accept() {
        $this->pending = false;
        $this->accepted = true;        
        $this->blocked = false;
    }
    
    public function status() {
        if( $this->pending && !$this->accepted )
            return 'pending';
        else if ( !$this->pending && $this->accepted )
            return 'accepted';
        else if ( $this->blocked )
            return 'blocked';
    }
    
    public function collectionName() {
        return 'friendship';
    }  
    
    public static function model($classname=__CLASS__) {
        return parent::model($classname);
    }
}

?>
