<?php
/**
 * Kinematiq API
 * SessionController
 * 
 * Library implementing all sport session related actions 
 * 
 * @author      Karlos Álvarez <karlos.alvan@gmail.com>
 * @uses        OAuth2Exception
 * @uses        MongoException
 * @uses        Exception
 * 
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . "..".DIRECTORY_SEPARATOR. "lib".DIRECTORY_SEPARATOR."OAuth2Exception.inc";


class SessionController extends RedisController {



    public function filters() {
        return array(
            'accessControl',
        );
    }


    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     *
     *  ORDER Deny, Allow
     * 
     * @return array access control rules
     */
    public function accessRules() {

        return array(
            
            
            array('deny', // deny all users
                'actions' => array('health','dupplicates','index'),
                'users' => array('*'),
            ),

            array('allow', // allow all users to perform 'index' and 'view' actions
                'users' => array('*'),
                //'expression' => '',
            )

        );
    }


    /**
     * @internal
     */
    public function actionHealth(){
        try {
            $start = microtime(true);
            $id = Yii::app()->controller->getModule("oauth2")->getUid();
            $user = $this->findByPk('Oauth2User', $id );

            ob_start();
            $this->setJsonHeaders();
            
            if( !$user )
                throw new OAuth2Exception( array('code' => UNKNOWN_OAUTH_TOKEN, 'message' => "Cannot find user for this token.") );
            if( isset( $user->role ) &&  $user->role !== 'admin' )
                throw new OAuth2Exception( array('code' => UNKNOWN_OAUTH_TOKEN, 'message' => "You cannot do this") );
            
            $fix=false;

            if( isset( $_POST["fix"]) ) {
                if( $_POST["fix"] == true ){
                    $fix=true;
                }
            }

            $pageSize = 100;
            if( isset( $_POST["psz"]) ) {
                if( $_POST["psz"] < 100 ){
                    $pageSize = $_POST["psz"];
                }
            }

            $info = ( $fix ) ? 'Enabled' : 'Disabled';
            echo "\nFIX is {$info}\n"; 
            
            // return $fix; die;
            // $session = iterator_to_array( Session::model()->findAll(), false );
            // #$likes = iterator_to_array( Like::model()->findAll(), false );

            $old = 0;
            $new = 0;
            $unconsistent = 0;
            $otherTagsSessionIds = array();
            $offset=0;

            // $pageSize = 100;
            $exit = false;
            
            $invited = array();
            $accepted = array();
            $passed = array();

            $c = new EMongoCriteria;
            $c->setLimit($pageSize)->setSort( array('created'=>'desc'))->setSkip($offset);  

            while( !$exit ) {
                $it = Session::model()->find( $c );

                if( $it->count(true) < $pageSize )
                    $exit = true;
                else echo "\nLoaded this time: " . $it->count(true) ."\n";

                while( $it->hasNext() ) {

                    $s = $it->getNext();

                    $session_total_jumps = $s->numberOfAirs;
                    $session_total_height = $s->totalHeight;

                    if( isset($s->totalAirTime) )
                        $session_total_airtime = $s->totalAirTime;
                    else
                        $session_total_airtime = 0;

                    $session_max_height = $s->highestAir;
                    $session_max_airtime = $s->maxAirTime;

                    $total_airtime = 0;
                    $total_height = 0;
                    $total_jumps = 0;

                    $maxPop = 0;
                    $maxHpower = 0;
                    $maxCrashVelocity = 0;
                    $maxCrashPower = 0;

                    $max_height = 0;
                    $max_airtime = 0;
                    $duration = 0;

                    $unstable = false;

                    $sessionNew = false;
                    foreach ($s->_airs as  $air) {
                        $air = (object) $air;

                        if( $air->tag == "valid" ) {
                            if( $air->height > $max_height )
                                 $max_height = $air->height;
                            if( $air->airtime > $max_airtime )
                                 $max_airtime = $air->airtime;
                            if( $air->created > $duration )
                                $duration = $air->created;

                            if( isset($air->pop) ) {
                                if($air->pop > $maxPop )
                                     $maxPop = $air->pop;

                                if( $air->h_power > $maxHpower )
                                     $maxHpower = $air->h_power;

                                if( $air->crash_power > $maxCrashPower )
                                     $maxCrashPower = $air->crash_power;

                                if( $air->crash_velocity > $maxCrashVelocity )
                                     $maxCrashVelocity = $air->crash_velocity;
                            }

                            $total_height += $air->height;
                            $total_airtime += $air->airtime;
                            $total_jumps += 1;

                        }  else {
                            if( !in_array( (string) $s->_id, $otherTagsSessionIds) )
                                $otherTagsSessionIds[] = (string) $s->_id;
                        }
                    }

                    if( !$fix ) {

                        //echo "Jumps stored {$session_total_jumps}, jumps real {$total_jumps}\n";
                        //echo "Total airtime stored {$session_total_airtime}, total airtime real {$total_airtime}\n";
                        //echo "Total height stored {$session_total_height}, total height real {$total_height}\n";
                        //echo "Max height stored {$session_max_height}, total height real {$max_height}\n";
                        //echo "Max airtime stored {$session_max_airtime}, max airtime real {$max_airtime}\n";
                        //echo "Duration stored {$s->duration}, max airtime real {$duration}\n";
                        // echo "\n";

                    }

                    if( 
                        $session_total_jumps == $total_jumps &&
                        $session_total_airtime == $total_airtime &&
                        $session_total_height == $total_height &&
                        $session_max_height == $max_height &&
                        $session_max_airtime == $max_airtime &&
                        
                        isset($s->duration) && ( $s->duration == $duration )

                    ) {
                        if( isset($s->maxPop) &&
                            isset($s->maxHpower) && 
                            isset($s->maxCrashPower) && 
                            isset($s->maxCrashVelocity) ) {

                            if( $s->maxPop == $maxPop && 
                                $s->maxHpower == $maxHpower && 
                                $s->maxCrashPower == $maxCrashPower && 
                                $s->maxCrashVelocity == $maxCrashVelocity 
                            )
                                echo "Session {$s->name} stable\n";
                            else
                                $unstable=true;

                        } else 
                            echo "Session {$s->name} stable\n";

                    } else 
                        $unstable=true;

                    if( $unstable ) {

                        echo "Session {$s->name} unstable \nDUR ({$s->duration}, {$duration})\nTJ ({$session_total_jumps}, {$total_jumps})\nTA ({$session_total_airtime}, {$total_airtime})\nTH ({$session_total_height}, {$total_height})\nMH ({$session_max_height}, {$max_height})\nMA ({$session_max_airtime}, {$max_airtime})\n";
                        if( $fix ) {
                            
                            $s->numberOfAirs = $total_jumps;
                            $s->totalHeight = $total_height;
                            $s->totalAirTime = $total_airtime;
                            
                            $s->maxHeight = $max_height;
                            $s->highestAir = $max_height;
                            $s->maxAirTime = $max_airtime;
                            $s->duration = $duration;

                            $s->maxPop = $maxPop;
                            $s->maxHpower = $maxHpower;
                            $s->maxCrashPower = $maxCrashPower;
                            $s->maxCrashVelocity = $maxCrashVelocity;

                            if( $s->update() )
                                echo "{$s->name} FIXED\n";
                            else
                                echo "{$s->name} ERROR fixing\n";

                            
                        }
                        $unconsistent++;
                    }

                    ob_flush();
                    flush();
                }

                $offset = $offset + $pageSize;
                $c->setSkip($offset)->setLimit($pageSize)->setSort( array('created'=>'desc'));  
            }
            
            $end = microtime(true) - $start;
            echo "\nUnconsistent: {$unconsistent}\n";
            echo "Sessions with fake or dev tags: ".count($otherTagsSessionIds)."\n";
            echo "Execution time: {$end} seconds\n";

            //echo "Sessions id array: ";
            // echo json_encode($otherTagsSessionIds);
            // echo "\nTotal: " . count($session) . " sessions\n" . "NEW: " . $new ." sessions\nERR: " . $unconsistent . " unconsistent\nOLD: " . $old . " sessions can be update with totalAirTime\n";

        } catch(Exception $e){ 
            // $this->sendExceptionInfo($e, $this->id, $this->action->id);
            echo "Exception\n";
            echo $e->getMessage();
        }
    }
    

    /**
     *  Check if ther are any repeated session in the database
     *  The cost is o(n) thus it has to check one by one if there are results having the same data
     *
     *  Uses the method of session model, isRepeated
     */
    public function actionDupplicates(){
        //return;
        try {
            # ob_start();

            $id = Yii::app()->controller->getModule("oauth2")->getUid();
            #$this->setJsonHeaders();

            $user = $this->findByPk('Oauth2User', $id );
            if( !$user )
                throw new OAuth2Exception( array('code' => UNKNOWN_OAUTH_TOKEN, 'message' => "Cannot find user for this token.") );
            if( isset( $user->role ) &&  $user->role !== 'admin' )
                throw new OAuth2Exception( array('code' => UNKNOWN_OAUTH_TOKEN, 'message' => "You cannot do this") );
            
            $out = 50;
            $count = 0;
            $exit=false;

            $pageSize = 100;
            $offset = 0;
            $affected = array();
            $c = new EMongoCriteria();
            $c->setLimit($pageSize);

            $c->setSkip($offset); 
            $it = Session::model()->find($c);
            $total = $it->count();

            echo "Total {$total} sessions\n";

            while( !$exit ) {
                if( $it->count(true) < $pageSize  ) {
                    $exit = true;
                }
                while( $it->hasNext() ) {
                    $obj = $it->getNext();
                    $count +=1;
                    if( $obj->isRepeated() ) {
                        echo "Session {$obj->name} {$obj->_id} is repeated\n";
                        $affected[] = (string) $obj->_user;
                    } else {
                        // output status each 50 requests
                        $out -= 1;
                        if( $out <= 0) {
                            $out=50;

                            echo "checked ($count/$total)\n";
                        }
                    }

                    ob_flush();
                    flush();
                }
                if( !$exit ) {
                    $offset += $it->count(true);
                    $c->setSkip($offset); 
                    $it = Session::model()->findAll($c);
                }
            }

            echo "Affected users:\n";
            echo json_encode($affected);
            
  
        } catch(Exception $e){ 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }


    /**
     * Creates a sport tracking session.
     * It receives session information from POST, validates data and stores the session
     * 
     */
    public function actionCreate(){
        try {

            $uid = Yii::app()->controller->getModule("oauth2")->getUid();
            $this->setJsonHeaders();
            
            if( !isset($_POST) )
                throw new OAuth2Exception( array('code' => BAD_SESSION_DATA, 'message' => "No valid user id provided.")  );          

            $currentUser = $this->findByPk('Oauth2User', $uid );
            if( !$currentUser ) 
                throw new Exception( "No user" );
            
            $session = new Session();
            $session->created = time();
            $session->_user = $currentUser->_id;

            /*
                This method ensures Atomic operation when creating session 
                Since 1.0.0
            */

            $this->validate( $currentUser, $session );

        } catch (Exception $e) {            
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }


    /**
     *  @internal
     * 
     *  Inserts the session in the system
     *  Called from actionCreate() method
     * 
     *  In case of errors inserting keeps the ATOMICITY of the operation
     *  by rolling back in case of later error.
     * 
     *  @since 1.2.1
     *
     *  @version 1.4.2
     *      
     *          ADD add session to Redis cache
     */
    private function validate( $user, $session ) {

        // User data before insertion
        $previousUserData = $user;
        // Key of the redis Sorted Set to use
        $key = 'activity';

        if( !$session->validateParams() ) 
            throw new OAuth2Exception( array('code' => BAD_SESSION_DATA, 'message' => "No valid session data.") );

        // print_r( $session );
        // Insert session
        
        if( !$session->save() ) {
            // Rollback $spot update
            $spot = $this->findByPk('Spot', $session->_spot );
            
            if( !$spot ) { }
            else {

                $n = $this->numberOfSessionsInSpot($spot->_id, $user->_id);
                $spot->removeSessionData($n, $user, $session );

                throw new OAuth2Exception( array('code' => UNKNOWN_DB_ERROR, 'message' => "Something went wrong when saving session.") );
            }
        } else {
            
            // Session was saved
            // insertNewSessionData() Throws Exception if error 
            $user->insertNewSessionData( $session );

            // User data after insertion
            $uid = Yii::app()->controller->getModule("oauth2")->getUid();
            $newUserData = $this->findByPk('Oauth2User', $uid );

            if( $newUserData->validate($session) ) {
                // Session is now on the Leaderboards
                if( !$this->add( $key, $session ) ) {
                    Yii::log("Redis::Error adding session to lb cache $key", "info");
                    //echo "ERROR!\n"; die;
                } else {
                    Yii::log("Redis::Added session to lb cache $key", "info");
                }

                //Now is safe to add the session to the challenges
                $newUserData->addSessionToChallenges( $session );
                //echo "here2"; die;
                //Everything is now correct
                $this->sendCreated("Session {$session->name} succesfully saved.", (string) $session->_id );
            }
            else
                echo json_encode(array("status"=>"error"));
        }
    }
    

    /**
     * @deny
     * 
     * Retrieves an uncompressed list of sessions for the current user or, if provided any user id, for this known id user
     * 
     * @uses offset and pageSize $_POST params allow pagination control
     * @example {API_URL}/session/{OPTIONAL_known_user_id}/?token={any_authorized_token}
     * @param String $id
     */
    public function actionIndex($id = null){
        try {
            $oauth = YiiOAuth2::instance();
            $token = $oauth->verifyToken();
            $this->setJsonHeaders();

            switch( $_SERVER['REQUEST_METHOD'] ){

                case 'GET': {
                    if( $id !== null ) { 
                       $user = Oauth2User::model()->findByPk( new MongoId($id) );
                       
                    } else {
                        // loading current user's friend set
                        $oauth = YiiOAuth2::instance();
                        $user = $oauth->loadUser($token->oauth_token);
                    }                       
                    
                    // check if user is friend            
                    if( !$user )
                        throw new OAuth2Exception( array('code' => UNKNOWN_OAUTH_TOKEN, 'message' => "No user found for the provided token.") );
                        
                    // default session parameters
                    $offset = 0;
                    $pageSize = 20;
                    
                    // Check if the actual post contains extra parameters
                    $params = $this->validatePagination();
                    if( $params ) {
                        $offset = $params['offset'];
                        $pageSize = $params['pageSize'];
                    }

                    $feed = $this->loadUserSessions($offset, $pageSize, $user->_id);
                    echo json_encode( $this->setPaginatedOutput( $feed, count($feed), $offset)  , JSON_NUMERIC_CHECK ); 
                }
                break;
                
            }
            
        } catch (Exception $e) { 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    } 


    /**
     *  Deletes a session by id
     *  It only accepts Authorized DELETE requests
     * 
     *  @param  String $id The id of the session to Delete
     *
     *  @version 1.4.2
     *           ADD remove session from Redis cache
     *  
     */
    public function actionDelete($id = null){
        try {

            $uid = Yii::app()->controller->getModule("oauth2")->getUid();
            $this->setJsonHeaders(); 

            switch( $_SERVER['REQUEST_METHOD'] ){
                case 'GET': 
                    $this->sendOK( 'succesfully GET received :' . $id);
                break;
                case 'POST': 
                    $this->sendOK( 'succesfully POST received :' . $id);
                break;
                case 'PUT': 
                    $this->sendOK( 'succesfully PUT received :' . $id);
                break;
                case 'PATCH': 
                    $this->sendOK( 'PATCH is for you');
                break;
                case 'DELETE': {

                    // Key of the redis Sorted Set to use
                    $key = 'activity';

                    $user = $this->findByPk('Oauth2User', $uid );
                    if( !$user )
                        throw new OAuth2Exception( array('code' => UNKNOWN_OAUTH_TOKEN, 'message' => "No valid user id provided.") );

                    $role = $user->getRole();
                    $result = $this->findByPk("Session", $id );
                    if( !$result )
                        $result = Session::model()->findByPk( new MongoId($id) );
                    
                    if( !$result )
                        throw new OAuth2Exception( array('code' => BAD_SESSION_ID, 'message' => "Session unexistent or already deleted") );

                    if( (string) $user->_id !== (string) $result->_user && $role === 'user' )
                        throw new OAuth2Exception( array('code' => BAD_USER_ID, 'message' => "This user cannot delete this session") );

                    $spot = Spot::model()->findByPk( new MongoId( $result->_spot ) );
                    $n = $this->numberOfSessionsInSpot($spot->_id, $user->_id);

                    $spot->updateSpot($n, $user, $result );    
                    $user->removeSessionData( $result );
                    
                    //echo "hello"; die;
                    // insert parse the user sessions to redo the index
                    // get best session for airtime, height y gforce
                    // add info to rank structure

                    $result->IsNewRecord = false;
                    $deleteOp = $result->delete();
                    
                    if( $this->remove( $key, $result ) && $deleteOp ) {
                        Yii::log("Redis::Removed rank from redis lb $key", "info");

                    } elseif( !$deleteOp )

                        throw new OAuth2Exception( array('code' => UNKNOWN_DB_ERROR, 'message' => "Cannot delete session") );
                    else {

                        Yii::log("Redis::ERROR rank from redis lb $key", "info");
                    }

                

                    $this->sendOK('succesfully session deleted');
                } break;
            }

        } catch (Exception $e) { 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    } 


    /**
     * Updates partial fields from a session document by id
     * It only accepts Authorized POST requests
     * 
     * @param  String $id The id of the session to Update
     */
    public function actionUpdate(){
        try {            

            //$oauth = YiiOAuth2::instance();
            //$token = $oauth->verifyToken();
            $this->setJsonHeaders();            

            switch( $_SERVER['REQUEST_METHOD'] ){
                case 'POST' : {
                    $name = isset($_POST[ 'name' ]) ? $_POST[ 'name' ] : '';
                    $id = isset($_POST[ 'sessionId']) ? $_POST[ 'sessionId' ] : '';
                    $spot = isset( $_POST[ 'spotId']) ? $_POST[ 'spotId'] : '';
                } break;
                case 'PATCH' : {
                    $name = isset($_POST[ 'name' ]) ? $_POST[ 'name' ] : '';
                    $id = isset($_POST[ 'sessionId']) ? $_POST[ 'sessionId' ] : '';
                    $spot = isset( $_POST[ 'spotId']) ? $_POST[ 'spotId'] : '';
                } break;
                default:
                    throw new OAuth2Exception( array('code' => BAD_REST_METHOD, 'message' => "Unknown method") );
            }

            $session = Session::model()->findByPk( new MongoId($id) );
            if( !$session )
                throw new OAuth2Exception( array('code' => BAD_SESSION_ID, 'message' => "Unknown session") );

            $session->name = ($name == '') ? $session->name : $name;
            //$session->_spot = ($spot == '') ? $session->_spot : new MongoId($spot);
            $session->IsNewRecord = false;

            $session->update();

            $this->sendOk("Session succesfully updated");
            
        } catch (Exception $e) { 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    } 


    /**
     * 
     */
    public function actionReplace( $id ){
        try {

            $uid = Yii::app()->controller->getModule("oauth2")->getUid();
            $user = $this->findByPk('Oauth2User', $uid );
            $this->setJsonHeaders();

            // User authorization
            if( !$user )
                throw new OAuth2Exception( array('code' => UNKNOWN_OAUTH_TOKEN, 'message' => "Cannot find user for this token") );
            if( isset( $user->role ) &&  $user->role !== 'admin' )
                throw new OAuth2Exception( array('code' => UNKNOWN_OAUTH_TOKEN, 'message' => "You cannot do this") );

            // Find the session
            $session = Session::model()->findByPk( new MongoId($id) );
            if( !isset( $session ) )
                throw new Oauth2Exception( array('code' => BAD_SESSION_ID, 'message' => "No session found") );   
            
            // Parse airs input params
            if( !isset($_POST['_airs']) )
                throw new OAuth2Exception( array('code' => BAD_SESSION_DATA, 'message' => "Nothing to do")  );    

            $airs = json_decode( $_POST['_airs'] );
            if( !$airs ) {
                throw new OAuth2Exception( array('code' => BAD_SESSION_DATA, 'message' => "Not a valid Json string." ) );
            }      

             // I need to get the index of the previous non modified session
            $oldSession = clone $session;

            $onlyCalc = isset( $_POST['calc'] ) ? $_POST['calc'] : false;
            $session->pushAirs( $airs );

            if( ! $onlyCalc ) {
                if( !$session->update() )
                    throw new OAuth2Exception( array('code' => UNKNOWN_DB_ERROR, 'message' => "Something went wrong when saving session.") );

                // Forward object updates to Redis Cache
                if( $this->remove( "activity", $oldSession ) ) {

                    $this->add( "activity", $session );
                    
                    if( !$redis->remove( 'highestAir', $item ) || !$redis->remove( 'maxAirTime', $item )  ) {
                        Yii::log("Error adding session to lb cache $key", "info");
                    }
                }

                $c = new EMongoCriteria();
                $c->addCondition( '_user',  $session->_user );
                $c->addCondition( '_session', $session->_id );
                Rank::model()->deleteAll($c);

                // update user data
                $currentUser = $this->findByPk('Oauth2User', $session->_user );
                if( ! $currentUser )
                    throw new OAuth2Exception( array('code' => UNKNOWN_DB_ERROR, 'message' => "No user found to update profile") );

                if( ! $currentUser->repair() )
                    throw new OAuth2Exception( array('code' => UNKNOWN_DB_ERROR, 'message' => "Cannot repair user profile") );

                // updates leaderboard index
                $session->rankSession();
                
                $this->sendOk("Session {$session->_id} airs replaced.");
            }
            else {
                $session = $session->attributes;
                unset( $session['startTime'] );
                unset( $session['endTime'] );

                $this->sendObj( $session );
            }
            
        } catch (Exception $e) {            
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }


    /**
     *  Returns a feed of sessions
     *  Receives query filters in http POST
     *
     *  @version  1.4.2
     *      - ADD redis support
     *      - MODIFY Algorithm
     *      
     *      @abstract Feb 3rd 2016
     *          Since the activity is not returning REST entities, the Redis Caching is not optimal.
     *          Change this when Apps are updated to consume Rest Objects
     *          
     *          - LOAD Redis Cache only in community case 
     * 
     *  @version  1.4.3.
     *       - ADD redis cache ttl
     *       - ADD pagination user cache
     *       - ADD activity 60s cache
     *  
     */
    public function actionActivity(){
        //$start = microtime(true);

        try {   
            ini_set('max_execution_time', 35);

            // Basic user retrieval 
            $uid = Yii::app()->controller->getModule("oauth2")->getUid();
            $this->setJsonHeaders();
            $user = $this->findbyPk('Oauth2User', $uid );

            // MAX session activity pageSize;
            $MAX = 50;
            // Time as a Redis key in seconds
            $CACHED_TIME = 300;
            $CACHED_ACTIVITY = 60;

            if(!$user)
                throw new OAuth2Exception( array('code' => OAUTH2_ERROR_UNAUTHORIZED_CLIENT, 'message' => "Unauthorized token.") );

            $role = $user->getRole();
            $filters = array(
                "target" =>     array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/(following)|(community)|(me)/") ),  
                #"order" =>      array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/(time)/") ),  
                "spotinfo" =>   array("filter" => FILTER_REQUIRE_SCALAR),
                "offset" =>     array("filter" => FILTER_REQUIRE_SCALAR),
                "pageSize" =>   array("filter" => FILTER_REQUIRE_SCALAR),                
            );
            
            $input = filter_input_array(INPUT_POST, $filters);  

            $target =   isset($input['target']) ? $input['target'] : 'following';
            $offset =   isset($input['offset']) ? $input['offset'] : 0;
            $pageSize = ($input['pageSize'] && $input['pageSize'] < $MAX ) ? $input['pageSize'] : $MAX;
            //$order =    isset($input['order']) ? $input['order'] : 'time';
            $order =    'time';
            $filter =   isset($input['filter']) ? $input['filter'] : 1;
            $spotinfo = isset($input['spotinfo']) ? $input['spotinfo'] : 1;

            $hash = 'activity'.$uid.$target."_{$offset}_{$pageSize}";
            $hashactivity = 'activity'.$target;

            if( self::$REDIS ) {
                /**
                 * @abstract
                 *     $offset 0
                 * 
                 *     The INITIAL activity is cached for 1 minute
                 *     During that minute the key if existing will not be refreshed
                 *     This operation leverages server occupation during repeated calculations
                 */
                if( $offset == 0  && $target == 'community' ) {
                    $key = self::$rediscli->get($hashactivity);
                    if($key) {
                        $this->setJsonHeaders();
                        echo $key;
                        Yii::app()->end();
                    }
                }
                $key = self::$rediscli->get($hash);
                if($key) {
                    $this->setJsonHeaders();
                    echo $key;
                    Yii::app()->end();
                }
            }

            $mongo = array();
            $minHeight = 5;
            $userRange = array();

            // Load target users
            switch( $target ) {
                case 'following' : { 
                    
                    // Get users and ip range
                    $users = $this->findAllbyPk('Oauth2User', $user->_followed );  
                    $userRange = array_merge( $user->_followed, array( $uid ) );

                } break;
                case 'community' : { 

                    // Only users with sessions
                    // Only users with highestAir bigger than activity limit
                    
                    $k = new EMongoCriteria();
                    $k->compare( "details.numberOfSessions", '>0');
                    $k->compare( "details.highestAir", ">".$minHeight );

                    $users = Oauth2User::model()->findAll($k, array('_id', 'details.name', 'details.lastname', 'details._pictures') );

                    foreach ($users as $user) { 
                        $userRange[] = (string) $user->_id;
                    }
                    
                    /**
                     *  @abstract
                     *  The filtering is done in activity cache creation time
                     */

                } break;
                case 'me' : { 
                    $users = array( $user );
                    $userRange = array( $uid );

                } break;
            }
           
            $notCached = true;
            $feed = array();
            $spotIds = array();
            $lb = $this->getLb('activity');

            if($lb) { 
                if( $target == 'community' ) {
                    $loads = $lb->getRange($offset, ($offset + $pageSize), -1);
                
                    /*
                        $end = microtime(true) - $start;
                        echo "Execution load ". $end. "\n";
                        $end = microtime(true);
                    */
                    
                    $sinloads = array();
                    $sArr=array();

                    foreach ($loads as $key => $value) {
                        $obj = json_decode($key);
                        if($obj) {
                            if( $obj->highestAir > $minHeight ) {
                                $sinloads[] = new MongoId( $obj->_id->{'$id'} );
                                $sArr[]=(array) $obj;
                            }
                        }
                    }

                    $c = new EMongoCriteria;
                    $c->addCondition(   '_user',    new MongoId($uid) );
                    $c->compare(        '_session', $sinloads );
                    $result = Like::model()->findAll($c, array('_session') );

                    $likedSessions=array();

                    foreach ($result as $r) {
                        $likedSessions[] = (string) $r->_session;
                    }

                    foreach ($sArr as $s) {
                        $usr = $this->findInArray( new MongoId( $s['_user']->{'$id'} ), $users, '_id' );
                        
                        $details = (object) $usr->details;
                        $s['user_name'] =     $details->name;
                        $s['user_lastname'] = $details->lastname;
                        $s['user_pictures'] = $details->_pictures;

                        if( in_array( $s['_id']->{'$id'}, $likedSessions ) ) {
                            $s['liked'] = true;
                        } else
                            $s['liked'] = false;

                        $feed[] = $s;
                    } 

                    $notCached = false;
                } 
            } 

            /**
             * @version 1.4.2
             *
             *     @abstract
             * 
             *     This piece is ejecuted when the request must be reparsed and filtered (following | me)
             *     This case the remote db is faster, just because the RedisSortedSet parsing in memory is slower than the mongodb request itself
             */
            if( $notCached ) {

                /*
                    $end = microtime(true) - $start;
                    echo "Execution load ". $end. "\n";
                    $end = microtime(true);
                */

                $k = new EMongoCriteria();
                foreach( $userRange as $fu )
                    $mongo[] = new mongoId( $fu );

                $k->compare( "_user", $mongo );

                // FILTERING
                // In following or me the filtering is disabled
                // $k->compare( "maxHeight", ">".$minHeight );  
                
                $k->setSort( array("time" => -1) );          
                $it = Session::model()->find($k);

                // Helps to control offset
                $currentInTarget = 0;
                $inserted = 0;

                $c = new EMongoCriteria;
                while( $it->hasNext() ) {
                    $session = $it->getNext();
                    $obj = $session->attributes;                
                    
                    $usr = $this->findInArray( $session->_user, $users, '_id' );

                    if($usr) {
                        if( $currentInTarget >= $offset && $inserted < $pageSize ) {
                            $details = (object) $usr->details;
                            
                            $c->addCondition('_user', $user->_id );
                            $c->addCondition('_session', $session->_id );
                            $likeObj = Like::model()->findOne($c);
                            
                            if(!$likeObj)
                                $obj['liked'] = false;
                            else
                                $obj['liked'] = true;

                            $jumps = $this->filter( $session->_airs, $role );
                            $obj['_airs'] = $jumps;
                            $obj['user_name'] = $details->name;
                            $obj['user_lastname'] = $details->lastname;
                            $obj['user_pictures'] = $details->_pictures;
                        
                            $feed[] = $obj;
                            $inserted += 1;

                        } elseif( $inserted >= $pageSize )
                            break;

                        $currentInTarget+=1;
                    }
                }
            }

            $json = json_encode( $this->setPaginatedOutput($feed, count($feed), $offset), JSON_NUMERIC_CHECK );
            if( self::$REDIS ) {
                if( $offset == 0 && $target == 'community' ) { 
                    self::$rediscli->set($hashactivity, $json, array('nx','ex'=>$CACHED_ACTIVITY));
                }
                self::$rediscli->set($hash, $json, array('nx','ex'=>$CACHED_TIME));
            }
            echo $json;

        } catch (Exception $e) { 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        } 
    }


    /**
     * Retrieves all sessions stored for the user id
     * 
     * @uses        offset and pageSize $_POST params allow pagination control
     * @param       String $id The user id we want to see the timeline
     *
     * @since 0.8.0
     */
    public function actionUseractivity( $id = null ){
        try {            
            $this->setJsonHeaders();

            $pk = Yii::app()->controller->getModule("oauth2")->getUid();
            $user = $this->findByPk('Oauth2User', $pk );
            $role = ( isset($user->role) && $user->role !== null ) ? $user->role : 'user';
            if(!$user)
                throw new OAuth2Exception( array('code' => OAUTH2_ERROR_UNAUTHORIZED_CLIENT, 'message' => "Unauthorized token.") );
            
            if( $id )
                $queryUser = Oauth2User::model()->findByPk( new MongoId($id) );

            if(!$queryUser)
                throw new OAuth2Exception( array('code' => BAD_USER_ID, 'message' => "Not a user") );
            
            // default session parameters
            $offset = 0;
            $pageSize = 20;
            $spotinfo = isset($_POST['spotinfo']) ? $_POST['spotinfo'] : 1;

            // Check if the actual post contains extra parameters
            $params = $this->validatePagination();
            if( $params ) {
                $offset = $params['offset'];
                $pageSize = $params['pageSize'];
            }
            
            $c = new EMongoCriteria;            
            $c->addCondition('_user',$queryUser->_id);
            $c->setLimit($pageSize)->setSort( array('time' => 'desc') )->setSkip($offset);

            $feed = array();
            $spotIds = array();
            $result = $this->findAll('Session',$c);

            if( $spotinfo == 1 ){
                foreach ($result as $session ) { 
                    $spotIds[] = $session->_spot;
                }
                $spots = $this->findAllbyPk('Spot', $spotIds ) ;
            }

            foreach ($result as $session ) {
                $jumps = $this->filter( $session->_airs, $role );
                $obj = $session->attributes;   

                if( $spotinfo == 1 ){
                    $spot = $this->findInArray( $session->_spot, $spots, '_id' );
                    $obj['spot_name'] = $spot->name;
                    $obj['spot_country'] = $spot->country;
                    $obj['continent_name'] = $spot->continent;
                }

                $c = new EMongoCriteria;
                $c->addCondition('_user', $user->_id );
                $c->addCondition('_session', $session->_id );
                $result = Like::model()->findOne($c);
                
                if(!$result)
                    $obj['liked'] = false;
                else
                    $obj['liked'] = true;

                $obj['_airs'] = $jumps;
                $obj['user_name'] = $queryUser->details['name'];
                $obj['user_lastname'] = $queryUser->details['lastname'];
                $obj['user_pictures'] = $queryUser->details['_pictures'];
                $feed[] = $obj;
            }

            echo json_encode( $this->setPaginatedOutput($feed, count($feed), $offset), JSON_NUMERIC_CHECK );
            
        } catch (Exception $e) { 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }       
    }


    /**
     * Retrieves sessions recorded in the spot ONLY for the current user
     * 
     * @uses        offset and pageSize $_POST params allow pagination control
     * @example     {API_URL}/{API_VERSION}/session/byspot/{MANDATORY_known_spot_id}/?token={any_authorized_token} path description
     * @param       String $id The Spot id
     */
    public function actionBySpot( $id ){
        try {            
            $this->setJsonHeaders();

            $pk = Yii::app()->controller->getModule("oauth2")->getUid();
            $user = $this->findByPk('Oauth2User', $pk );
            $role = ( isset($user->role) && $user->role !== null ) ? $user->role : 'user';

            if(!$user)
                throw new OAuth2Exception( array('code' => OAUTH2_ERROR_UNAUTHORIZED_CLIENT, 'message' => "Unauthorized token.") );
            
            if( $id == null || $id=='' ) 
                throw new Exception( array('code' => BAD_SPOT_ID, 'message' => "No valid spot id provided.") );

            // default session parameters
            $offset = 0;
            $pageSize = 20;

            // Check if the actual post contains extra parameters
            $params = $this->validatePagination();
            if( $params ) {
                $offset = $params['offset'];
                $pageSize = $params['pageSize'];
            }

            if( isset( $_POST['target'] ) ) {

                $target = filter_var( $_POST['target'], FILTER_VALIDATE_REGEXP, array( 'options' => array("regexp"=>"/(community)|(following)|(me)/i") ) );
                if( !$target )
                    throw new OAuth2Exception( array('code' => BAD_POST_DATA, 'message' => "Please check your target.") );

            } else {
                $target = 'me';
            }
            
            $c = new EMongoCriteria;
            $c->addCondition( '_spot', new MongoId($id) );

            // print_r($user->_followed); die;

            switch ($target) {
                case 'community':
                    $spot = $this->findByPk("Spot", $id);
                    if( !$spot )
                        throw new OAuth2Exception( array('code' => BAD_SPOT_ID, 'message' => "Not spot found" ));

                    $mongoIds = array();
                    foreach ($spot->_kiters as $id) {
                        $mongoIds[] = new MongoId( $id );
                    }

                    // limit users of community only if have sessions in the spot
                    $c->compare( '_user', $mongoIds );

                    $uc = new EMongoCriteria();
                    $uc->compare("_id", $mongoIds );
                    $users = $this->findAll('Oauth2User', $uc );

                    break;
                case 'following':
                    $mongoIds = array();
                    foreach ($user->_followed as $id) {
                        $mongoIds[] = new MongoId( $id );
                    }
                    $c->compare( '_user', $mongoIds );

                    $uc = new EMongoCriteria();
                    $uc->compare("_id", $mongoIds );
                    $users = $this->findAll('Oauth2User', $uc );

                    break;
                case 'me':
                    $c->addCondition( '_user', $user->_id);
                    $users = array( $user );
                    break;
            }
            
            $c->setLimit($pageSize)->setSort( array('time' => 'desc') )->setSkip($offset);
            
            // $result = iterator_to_array( Session::model()->findAll($c), false );
            $it = Session::model()->findAll($c);
            $result = array();

            foreach ($it as $session) {
                $usr = $this->findInArray( $session->_user, $users, '_id' );
                $details = (object) $usr->details;

                $c = new EMongoCriteria;
                $c->addCondition('_user', $user->_id );
                $c->addCondition('_session', $session->_id );

                $likes = Like::model()->findOne($c);
                $jumps = $this->filter( $session->_airs, $role );
                $obj = $session->attributes; 
                
                if(!$likes)
                    $obj['liked'] = false;
                else
                    $obj['liked'] = true;
                        
                $obj['_airs']=$jumps;   
                $obj['user_name'] = $details->name;
                $obj['user_lastname'] = $details->lastname;
                $obj['user_pictures'] = $details->_pictures;             
                //$session = (object) $it->next();
                $result[]=$obj;
            }
            
            echo json_encode( $this->setPaginatedOutput($result, count($result), $offset), JSON_NUMERIC_CHECK );
            
        } catch (Exception $e) { 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }       
    }


    /**
     * Parametric method
     * This method accepts POST request with pagination configuration
     * @deprecated
     */
    /*
    public function actionFind(){
        try {            
            $this->setJsonHeaders();

            $field = (isset($_POST['query'])) ? $_POST['query'] : "{}";
            //$value = (isset($_POST['value'])) ? $_POST['value'] : null;
            $query = json_decode($field, true);
            print_r($query);

            print_r( iterator_to_array( Yii::app()->mongodb->session->find( $query ), false )  ); die;

            $offset = 0;
            $pageSize = 20;

            // Check if the actual post contains extra parameters
            $params = $this->validatePagination();
            if( $params ) {
                $offset = $params['offset'];
                $pageSize = $params['pageSize'];
            }
            if( !$field || !$value )
                throw new OAuth2Exception( array('code' => BAD_POST_DATA, 'message' => "Dont know what to do") );

            $c = new EMongoCriteria();
            $c->compare( $field, $value );
            $c->setLimit($pageSize)->setSort( array('time' => 'desc') )->setSkip($offset);
            $result = iterator_to_array( Session::model()->findAll($c), false );

            echo json_encode( $this->setPaginatedOutput($result, count($result), $offset), JSON_NUMERIC_CHECK );
            
        } catch (Exception $e) { 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        } 
    }
    */

    /**
     * Retrieves extended session information having the session id
     * @param String $id The session id
     */
    public function actionDetail( $id ){
        try {
            //$oauth = YiiOAuth2::instance();
            //$token = $oauth->verifyToken();
            $this->setJsonHeaders();

            if( !isset($id) )
                throw new Oauth2Exception( array('code' => BAD_SESSION_ID, 'message' => "No valid session id provided.") );          

            $c = new EMongoCriteria();
            $c->addCondition( '_session', new MongoId( $id ) );

            $likes = iterator_to_array( Like::model()->findAll( $c ), false);
            if( !isset( $likes ) )
                $likes = array();

            $session = Session::model()->findByPk( new MongoId($id) );
            if( !isset( $session ) )
                throw new Oauth2Exception( array('code' => BAD_SESSION_ID, 'message' => "No session found") );      

            $session = $session->attributes;
            unset( $session['startTime'] );
            unset( $session['endTime'] );
            // unset( $session['_id'] );
            $session['likes'] = $likes;

            echo json_encode( $session, JSON_NUMERIC_CHECK );
            
        } catch (Exception $e) { 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        } 
    }


    /**
     * Sends the same session object than Activity methos
     * This method sends information formatted for client views and not REST entities
     * @version 1.3.0
     * 
     * @param String $id The session id
     */
    public function actionView( $id ){
        try {
            $this->setJsonHeaders();

            if( !isset($id) )
                throw new Oauth2Exception( array('code' => BAD_SESSION_ID, 'message' => "No valid session id provided.") );          

            $c = new EMongoCriteria();
            $c->addCondition( '_session', new MongoId( $id ) );

            $likes = iterator_to_array( Like::model()->findAll( $c ), false);
            if( !isset( $likes ) )
                $likes = array();

            $session = Session::model()->findByPk( new MongoId($id) );
            if( !isset( $session ) )
                throw new Oauth2Exception( array('code' => BAD_SESSION_ID, 'message' => "No session found") ); 

            $user = Oauth2User::model()->findByPk( $session->_user );
            if( !isset( $user ) )
                throw new Oauth2Exception( array('code' => BAD_USER_ID, 'message' => "No user found") );

            $pk = Yii::app()->controller->getModule("oauth2")->getUid();

            $session = (array) $session->attributes;
            $details = (object) $user->details;

            $session['user_name'] = $details->name;
            $session['user_lastname'] = $details->lastname;
            $session['user_pictures'] = $details->_pictures; 
            $session['liked'] = false;

            foreach ($likes as $value) {
                if( (string) $value->_user == $pk )
                    $session['liked'] = true;
            }


            unset( $session['startTime'] );
            unset( $session['endTime'] );
            $session['likes'] = $likes;

            echo json_encode( $session, JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES );
            
        } catch (Exception $e) { 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        } 
    }


    /**
     *  Adds a comment to the current session.
     *  It receives $_POST data, parses it and upon validation adds the comment data in the database.     
     *  Updates the session object
     *
     *  @version 1.4.2
     *           - ADD redis management
     */
    public function actionComment(){
        try {

            $pk = Yii::app()->controller->getModule("oauth2")->getUid();
            $user = $this->findByPk('Oauth2User', $pk );
            if(!$user)
                throw new OAuth2Exception( array('code' => OAUTH2_ERROR_UNAUTHORIZED_CLIENT, 'message' => "Unauthorized user") );

            $role = ( isset($user->role) && $user->role !== null ) ? $user->role : 'user';
            $this->setJsonHeaders();
           
            $comment = new Comment();
            $comment->_user = $user->_id;
            
            if ( !$comment->validateParams() )
                throw new Exception( "No valid comment data.", BAD_COMMENT_DATA );

            $session = Session::model()->findByPk( $comment->_session ) ;

            if( !($session) ) 
                //throw new Exception( array('code' => BAD_SESSION_ID, 'message' => ) );            
                throw new Exception( "No valid session id provided.", BAD_SESSION_ID );
            
            $oldSession = clone $session;
            if( $comment->save() ) {      
                
                $session->totalcomments++;
                
                if ( $session->update( array('totalcomments'), true ) ) {
            
                // I need to get the index of the previous non modified session
                // if( $session->save() ) {
                    if( $this->remove( "activity", $oldSession ) ) {

                        $this->add( "activity", $session );
                    } //else
                        //throw new Exception( "Cannot remove old session!!!", BAD_SESSION_ID );
                }

                $destUser = Oauth2User::model()->findByPk( $session->_user );
                if( !$destUser )
                    throw new Exception( "No valid session id provided.", BAD_SESSION_ID );
                
                $this->manageNotification('comment', $user, $destUser, $session );
                $this->sendCreated("Comment successfully created", (string) $comment->_id );
            } 


        } catch(Exception $e){
            $this->sendExceptionInfo($e, $this->id, $this->action->id );
        }      
    }


    /**
     * Add a Like to the curren session
     * @param  string $id The mongoId string belonging to the liked session
     *
     * @version 1.4.2
     *           - ADD redis update key value
     *           
     */
    public function actionLike( $id ){
        try {
            $this->setJsonHeaders();
            
            $pk = Yii::app()->controller->getModule("oauth2")->getUid();
            $currentUser = $this->findByPk('Oauth2User', $pk );
            if( !$currentUser )
                throw new OAuth2Exception( array('code' => UNKNOWN_OAUTH_TOKEN, 'message' => "No valid access token error.") );

            $role = ( isset($user->role) && $user->role !== null ) ? $user->role : 'user';
            $session = Session::model()->findByPk( new MongoId($id) );
            
            if( !$session )
                throw new OAuth2Exception( array('code' => BAD_SESSION_ID, 'message' => "No valid session id provided.") );

            $c = new EMongoCriteria();
            $c->addCondition('_user', $currentUser->_id );
            $c->addCondition('_session', $session->_id );

            $liked = Like::model()->findOne($c);
            
            if( $liked )
                throw new OAuth2Exception( array('code' => ALREADY_LIKED, 'message' => "You already like this session") );                

            
            $like = new Like();
            $like->_user = $currentUser->_id;
            $like->_session = $session->_id;
            $like->created = time();
            $like->save();
            
           
            // I need to get the index of the previous non modified session
            $oldSession = clone $session;

            if( $session->like() ) {
                if( $this->remove( "activity", $oldSession ) ) {

                    $this->add( "activity", $session );
                }
            }

            // WKN-569 Avoid to nofify twice the session like
            $c = new EMongoCriteria();
            $c->addCondition('_sender', $currentUser->_id );
            $c->addCondition('_receiver', $session->_user );
            $c->addCondition('type', $session->_id );

            $notified = Notification::model()->findOne($c);
            if( ! $notified ) {
                $destUser = Oauth2User::model()->findByPk( $session->_user );
                $this->manageNotification('like', $currentUser, $destUser, $session );
            }

            //$this->sendOk("Session {$session->name} was liked by {$currentUser->details['name'] }");
            
            echo json_encode( $session, JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES );
        
        } catch (Exception $e) { 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }


    /**
     * Remove a Like to the curren session
     * @param  string $id The mongoId string belonging to the disliked session
     *
     *  @since 0.9.0 The action dislike is not notified
     *
     *  @version 1.4.2
     *           - ADD redis update key value
     */    
    public function actionDislike( $id ){
        try {
            $this->setJsonHeaders();
            
            $pk = Yii::app()->controller->getModule("oauth2")->getUid();
            $currentUser = $this->findByPk('Oauth2User', $pk );
            if( !$currentUser )
                throw new OAuth2Exception( array('code' => UNKNOWN_OAUTH_TOKEN, 'message' => "No valid access token error.") );

            $role = ( isset($user->role) && $user->role !== null ) ? $user->role : 'user';
            $session = Session::model()->findByPk( new MongoId($id) );
            
            if( !$session )
                throw new OAuth2Exception( array('code' => BAD_SESSION_ID, 'message' => "No valid session id provided.") );
            
            $c = new EMongoCriteria();
            $c->addCondition('_user', $currentUser->_id );
            $c->addCondition('_session', $session->_id );
            $liked = Like::model()->findOne($c);

            if( !$liked )
                throw new OAuth2Exception( array('code' => ALREADY_DISLIKED, 'message' => "You didnt like this session") );                

            $liked->delete();

            // I need to get the index of the previous non modified session
            $oldSession = clone $session;
            if( $session->dislike() ) {
                if( $this->remove( "activity", $oldSession ) ) {

                    $this->add( "activity", $session );
                }
            }

            $destUser = Oauth2User::model()->findByPk( $session->_user );

            //$this->manageNotification('dislike', $currentUser, $destUser, $session );

            echo json_encode( $session );

            //$this->sendOk("Removed like from session {$session->name}");
        
        } catch (Exception $e) { 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }


    /**
     *  Load sport sessions having offset and pagesize parameters defined
     *  It's intended to provide support for session pagination
     * 
     *  @param integer $offset       First element to be retrieved
     *  @param integer $pageSize     Actual number of sessions to be retrieved
     *  @param integer $userId       The userId to which retrieved sessions belongs to
     *  @return JSON Array   
     */
    protected function loadUserSessions( $offset, $pageSize, $userId ){
        try {
            //load session 
            $c = new EMongoCriteria;
            $c->addCondition('_user', $userId );

            
            if( $pageSize > 0 )
                // Get the $pageSize sessions starting by $offset
                $c->setLimit($pageSize)->setSort( array('created' => 'desc') )->setSkip($offset);
            else
                // Get all sessions starting by $offset
                $c->setSort( array('created' => 'desc') )->setSkip($offset);
            
            
            $result = iterator_to_array( Session::model()->findAll($c), false);
            return $result;                    
            
        } catch (Exception $e) { 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        } 
    }  


    /**
     * Retrieves how many sessions has recorded the user in the selected spot
     * @param  Object $spotId The mongoId belonging to the spot
     * @param  Object $userId The mongoId belonging to the user
     * @throws Exception
     * @return Integer Number of sessions
     */
    public function numberOfSessionsInSpot( $spotId, $userId ){
        $c = new EMongoCriteria;
        $c->addCondition('_spot', $spotId );
        $c->addCondition('_user', $userId );
        $res = iterator_to_array( Session::model()->findAll($c), false);
        return count($res);
    }


    /**
     * Retrieves how many sessions has recorded the user in the selected continent
     * @param  Object $spotId The mongoId belonging to the spot
     * @param  Object $userId The mongoId belonging to the user
     * @throws Exception
     * @return Integer Number of sessions
     */
    public function numberOfSessionsInContinent( $name, $userId ){
       
        $c = new EMongoCriteria;
        $c->addCondition('name', new MongoRegex('/(' . strtolower( $name ) . ')/i') );
        $continent = Continent::model()->findOne($c);
        $spots = $continent->_spots;

        $c = new EMongoCriteria;
        $c->compare( '_spot', $spots ); 
        //$c->_spot('in', $spots );
        
        $c->addCondition('_user', $userId );
        $res = Session::model()->findAll($c);

        return count($res);
    }   
}
?>
