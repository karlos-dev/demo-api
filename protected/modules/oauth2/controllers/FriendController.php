<?php
/**
 * Kinematiq API
 * FriendController
 * 
 * @author      Karlos Álvarez <karlos.alvan@gmail.com>
 * @abstract    Library implementing all friendship related actions 
 * @uses        OAuth2Exception
 * @uses        MongoException
 * @uses        Exception
 * 
 * @version     03/09/2014
 * @since 0.8.7
 * 
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . "..".DIRECTORY_SEPARATOR. "lib".DIRECTORY_SEPARATOR."OAuth2Exception.inc";



class FriendController extends ComController {
    
    /**
     * Add as Followed friend the user with $id to the current user identified by access token
     * @param  string $id The string version of the mongoId belonging to the followed user     
     */
    public function actionFollow( $id ){
        try {

            $oauth = YiiOAuth2::instance();
            $token = $oauth->verifyToken();
            $this->setJsonHeaders();  
            
            $requestedFriend = Oauth2User::model()->findByPk( new MongoId($id) );
            
            if( !$requestedFriend )
                throw new OAuth2Exception( array('code' => BAD_FRIEND_ID, 'message' => "No valid user id provided.") );
            
            $currentUser = $oauth->loadUser($token->oauth_token);

            if( $currentUser->_id == $requestedFriend->_id )
                throw new OAuth2Exception( array('code' => BAD_FRIEND_ID, 'message' => "You cannot follow yourself") );

            if( !$currentUser )
                throw new OAuth2Exception( array('code' => UNKNOWN_OAUTH_TOKEN, 'message' => "No valid access token error.") );
            if( $currentUser->_followed == null ) {
                $currentUser->_followed = array(  (string)$requestedFriend->_id );
            } else {
                if( $currentUser->isFollowing($id) )
                    throw new Exception( "Current user already follows this user", ALREADY_FOLLOWED );
                
                $currentUser->_followed[] = (string)$requestedFriend->_id;
            }
            
            if( $requestedFriend->_followedBy == null ) {
                $requestedFriend->_followedBy = array( (string) $currentUser->_id );
            } else {
                if( $requestedFriend->isFollowedBy($id) )
                    throw new Exception( "This user is already followed by current user", ALREADY_FOLLOWED );
                $requestedFriend->_followedBy[] = (string) $currentUser->_id;
            }
            
            $currentUser->update( array('_followed') );
            $requestedFriend->update( array('_followedBy') );  


            $this->manageNotification('follow', $currentUser, $requestedFriend );

            $this->sendOk("Added follow from {$currentUser->details['name']} to {$requestedFriend->details['name']}");
            
        } catch (Exception $e) { 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }

    /**
     * Remove as Followed friend the user with $id to the current user identified by access token
     * @param  string $id The string version of the mongoId belonging to the unfollowed user    
     *
     * @since 0.9.0 The action unfollow is not notified
     */    
    public function actionUnfollow( $id ){
        try {
            $oauth = YiiOAuth2::instance();
            $token = $oauth->verifyToken();
            $this->setJsonHeaders();  
            
            $requestedFriend = Oauth2User::model()->findByPk( new MongoId($id) );
            
            if( !$requestedFriend )
                throw new OAuth2Exception( array('code' => BAD_FRIEND_ID, 'message' => "No valid user id provided.") );
            
            $currentUser = $oauth->loadUser($token->oauth_token);
            
            if( !$currentUser )
                throw new OAuth2Exception( array('code' => UNKNOWN_OAUTH_TOKEN, 'message' => "No valid access token error.") );
            if( $currentUser->_followed == null ) {
                throw new Exception( "This user is not followed by current user", NOT_FOLLOWED );
            } else {
                if( !$currentUser->isFollowing($id) )
                    throw new Exception( "Current user already follows this user", ALREADY_FOLLOWED );
                
                $newFollowGroup = $this->removeFromArray($currentUser->_followed, $id);
                $currentUser->_followed = $newFollowGroup;
            }
            
            if( $requestedFriend->_followedBy == null ) {
                throw new Exception( "This user has not following users", NOT_FOLLOWED );
            } else {
                if( !$requestedFriend->isFollowedBy( (string)$currentUser->_id) )
                    throw new Exception( "This user is not being followed by current user", NOT_FOLLOWED );
                
                $newFollowGroup = $this->removeFromArray($requestedFriend->_followedBy, (string)$currentUser->_id);
                $requestedFriend->_followedBy = $newFollowGroup;
            }
            
            $currentUser->update( array('_followed'), true );
            $requestedFriend->update( array('_followedBy'), true );            

            //$this->manageNotification('unfollow', $currentUser, $requestedFriend );
            
            $this->sendOk("Removed follow from {$currentUser->details['name'] } to {$requestedFriend->details['name']}");            
        
        } catch (Exception $e) { 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }


    /**
     * Returns information array with user profiles 
     * Receives the content within the http request POST body: { set:["user_id_0", "user_id_1", "user_id_2"] }
     *
     * @since 0.8.8
     * @updated to MongoClient 
     * 
     */
    public function actionList() {
        try {
            $oauth = YiiOAuth2::instance();
            $token = $oauth->verifyToken();
            $this->setJsonHeaders();            

            if( !$_SERVER['REQUEST_METHOD'] == 'POST' || !isset( $_POST['set'] ))
                throw new OAuth2Exception( array('code' => BAD_POST_DATA, 'message' => "Cannot find valid user set") );

            $filters = array(
                "type" =>     array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/(email)|(fbid)/") ),  
                );
            $input = filter_input_array(INPUT_POST, $filters);
            if(!isset( $input['type'] ))
                throw new OAuth2Exception( array('code' => BAD_POST_DATA, 'message' => "Cannot find valid set type") );

            $ids = json_decode( $_POST['set'], true );
            if( !$ids || !is_array($ids) )
                throw new OAuth2Exception( array('code' => BAD_POST_DATA, 'message' => "Not a valid json string") );     
                       
            $c = new EMongoCriteria();
            $c->compare( $input['type'], $ids );

            // We don't want to output any sensitive data from db
            // 
            // 
            // $result = iterator_to_array( Oauth2User::model()->findAll( $c, Oauth2User::publicAttributes() ), false);
            // 
            $result = iterator_to_array( Oauth2User::model()->findAll( $c ), false);

            if( !$result ) {  
                echo json_encode( array() );
                Yii::app()->end();
            }

            $offset = 0;
            $pageSize = 20;
            
            // Check if the actual post contains extra parameters
            $params = $this->validatePagination();
            if( $params ) {
                $offset = $params['offset'];
                $pageSize = $params['pageSize'];
            }
            $parsed = array();
            foreach ($result as $value) {
                $parsed[] = $value->compress();
            }

            echo json_encode( $this->setPaginatedOutput( $parsed, count($parsed), $offset) );

        } catch( Exception $e){ 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }

    
    private function removeFromArray( $array, $elem ){
        if( $array && $elem ) {
            $reducedArray = array();
            foreach ($array as $value) {
                if( $value !== $elem ) 
                    $reducedArray[] = $value;
            }
            return $reducedArray;
        } return null;
    }
    
}

?>
