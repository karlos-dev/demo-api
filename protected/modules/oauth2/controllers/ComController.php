<?php

/**
 *  Kinetatiq API
 *  ComController
 * 
 * @author      Karlos Álvarez <karlos.alvan@gmail.com>
 * @abstract    Implements functions aimed to correctly establish communication between clients and API ends.
 *              API error codes
 *              REST operation codes
 *              ERROR definition and error messages
 *              Hashing
 */

define("RESOURCE_CREATED", "201 Created");

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . "..".DIRECTORY_SEPARATOR. "lib".DIRECTORY_SEPARATOR."OAuth2.inc";


/**
 * Modified for demo purposes
 * 
 */

define("CLIENT_ID_IOS",         openssl_digest('key_IOS_app', 'sha512') );
define("CLIENT_ID_ANDROID",     openssl_digest('key_ANDROID_app', 'sha512') );
define("CLIENT_ID_WPHONE",      openssl_digest('key_WPHONE_app', 'sha512') );
define("CLIENT_SECRET",         openssl_digest('key_API_v1', 'sha512') );

define("COMPANION_ID",          openssl_digest('key_Companion_app',     'sha512') );
define("COMPANION_SECRET",      openssl_digest('key_Companion_secret',  'sha512') );

/**
 * @abstract    Woo API Error definition list
 */

define("UNSOPPORTED_APP_VERSION", 10);
define("OUTDATED_APP_VERSION", 11);
define("OUTDATED_TC", 12);
define("ACCEPT_TC", 13);

define("BAD_PASS_ERR", 20);
define("BAD_USERNAME_ERR", 21);
define("BAD_MAIL_ERR", 22);

define("BAD_WORDS_NAME_ERROR", 23);
define("BAD_WORDS_LASTNAME_ERROR", 24);
define("BAD_WORDS_COUNTRY_ERROR", 25);
define("BAD_CREDENTIALS", 26);
define("BAD_FRIEND_ID", 27);
define("UNKNOWN_DEVICE_PUSH_TOKEN", 28);
define("FB_LOGIN_EMAIL", 29);

define("BAD_POST_DATA", 30);
define("INCOMPLETE_PROFILE_DATA", 31);

define("UNKNOWN_OAUTH_TOKEN", 32);
define("ALREADY_FRIENDS", 33);
define("ALREADY_SENT", 34);
define("ALREADY_RECEIVED", 35);
define("BLOCKED_FRIEND", 36);
define("ALREADY_FOLLOWED", 37);
define("NOT_FOLLOWED", 38);
define("NOT_FRIENDSHIP", 39);

define("UNKNOWN_DB_ERROR", 40);
define("BAD_SPOT_ID", 41);

define("BAD_SESSION_ID", 51);
define("BAD_SESSION_DATA", 52);
define("BAD_USER_ID", 53);
define("BAD_COMMENT_DATA", 54);


define("UNKNOWN_IMAGE_TYPE", 55);
define("BAD_USER_IDENT", 56);
define("NO_IMAGE", 57);
define("UNCONFIRMED_ACCOUNT", 58);
define("UNCONFIGURED_SOCIAL", 59);

define("BAD_LIKE_DATA", 60);
define("ALREADY_LIKED", 61);
define("ALREADY_DISLIKED", 62);
define("NO_TIME_SET", 63);

define("BAD_REST_METHOD", 64);

define("NO_FILE", 65);
define("BAD_AIR_DATA", 66);
define("INCORRECT_COORDINATE", 67);

define("HACKING_ACTIVITY", 69);

define("BAD_CHALLENGE_ID", 71);
define("CHALLENGE_FINISHED", 72);
define("BAD_CHALLENGE_STATUS", 73);
define("CHALLENGE_LOCKED", 74);


/**
 * @abstract
 * 
 * Woo REST API
 * Error definition for system service notifications
 */
define("INVALID_HTTP_METHOD",  "406 Not Acceptable");
define("APP_UPGRADE_REQUIRED", "426 Upgrade Required");

define("SERVICE_UNAVAILABLE", "503 Service Unavailable");

define("FACEBOOK",'fbid');
define("TWITTER",'twtid');
define("PLAY",'playid');

require_once __DIR__. '/../../../modules/mobile-detect/Mobile_Detect.php';

/**
 * 
 * Helper wrapper to API responses
 * Handles general reponse format and notifications
 */
class ComController extends Controller {


    public function noActive(){
        header("HTTP/1.1 410 Gone");
        header("Content-Type: application/json");
        echo json_encode ( array(
            "status"=> "Unavailable", 
            "message" => "@deprecated method",             
        ) );
    }


    public function actionStatus(){

        if( isset( getallheaders()['AppVersion'] ) ) {

            $version = getallheaders()['AppVersion'];    
            $dirtyArray = explode(".", $version );
            $appVersion = $dirtyArray[0] . "." . $dirtyArray[1];
        }
        
        if( Yii::app()->params['maintenance']) {

            header("HTTP/1.1 503 Server Maintenance");
            header("Content-Type: application/json");
            echo json_encode ( array(
                "status"=> "Unavailable", 
            ) );

        } elseif( isset($appVersion) && !in_array($appVersion, Yii::app()->params['supportedVersions'] )  ) {

            header("HTTP/1.1 426 Upgrade Required");
            header("Content-Type: application/json");
            echo json_encode ( array(
                "status"=> "Outdated client version", 
            ) );

        } else {
            header("HTTP/1.1 200 Ok");
            header("Content-Type: application/json");
            echo json_encode ( array(
                "status"=> "Ok", 
            ) );
        }
    }    


    /**
     *
     * Parses Every single incoming request
     * Each request must be tracked. Some of them trigger different actions like notifications or admin actions
     *
     * Things we have for now:
     * 1. Trackable action
     * 2. System status action [service maintenance]
     * 3. Log admin actions
     * 
     * @return boolean If the incoming action can be executed or not
     *
     * @deprecated
     * 
     */
    //public function beforeAction(){

        // $this->manage3scaleTracking();
        // return parent::beforeAction( Yii::app()->controller->action->id );
    //}


    protected function getNotificationText($action) {

        if( in_array( $action, array('like', 'dislike', 'comment', 'newchallenge') ) ){

            if( $action == 'comment')
                $text="commented your session";

            elseif( $action == 'like')
                $text="liked your session";

            elseif( $action == 'dislike')
                $text="no longer likes your session";

            elseif( $action == 'newchallenge')
                $text="invited you to a new challenge";

            // elseif( $action == 'newchallenge')
            //    $text="created a new challenge";
            
        } else {
            if( $action == 'follow')
                $text="is following you";
        }

        return $text;
    }

    /**
     * Push notification manager
     * 
     * @see  Notification
     * @param  String $action  Action to be notified
     * @param  String $user    The sender user id
     * @param  String $desUser The destination user id
     * @param  Object $obj     Notification data
     */
    protected function manageNotification( $action, $user, $desUser, $obj =null ) {

        // Self notifications are not sent WKN-360
        if( !$user || ($user->_id == $desUser->_id) )
            return;
        
        $details = (Object) $user->details;

        $text = "{$details->name} " . $this->getNotificationText($action);
        $oid = isset( $obj ) ? (string) $obj->_id : ''; 

        /**
         * @since November 10th
         * @version 0.10.2
         * We save the nofification information
         */
        if( $obj )
            $sname = $obj->name;
        else
            $sname = null;

        $notif = new Notification($action, $user, $desUser, $obj, $details->name, $details->lastname, $sname );
        
        if ( !$notif->save() )
            throw new Exception( "Notification was not saved", UNKNOWN_DB_ERROR );


        $q = array( 
            '_receiver'=> $desUser->_id,
            'read' => 0
        );
        $unread = Yii::app()->mongodb->notification->find($q)->count();

        /**
         * Then if notifications are disabled we avoid sending push notifications
         */
        if( !$desUser->enable_notifications )
            return;

        $rawData = array(
            //'app_id' => PUSH_APP_ID,
            //'account_id' => Yii::app()->push['pushAccountId'],
            'app_id' => Yii::app()->push['pushId'],
            'subject' => $action,
            'message_holder' => '{"uid":"%{userId}","nid":"%{notifId}","sid":"%{objId}","created":"%{ts}"}',
            'start_now' => 'true',
            //"start_at" => "",
            "alert" => "{$text}",
            "sound" => "default",
            "badge" => $unread,
            'deliveries' => 
                array(
                    0 => array( 
                        'device_id' => $desUser->device_id,
                        'replacements' => array(
                            'userId' => (string) $user->_id,
                            'notifId' => (string) $user->_id,
                            'objId' =>  (string) $oid,
                            'ts'=> (string)$notif->created,
                        )
                    ),
                ),
            );

        $notif->send($rawData, $text);
        Yii::log("Notification... {$action} from {$details->name} to device-push-token {$desUser->device_id} on user {$desUser->_id} sent.");
    }

    

    /**
     * Send notification method
     * @deprecated
     */
    protected function sendNotification($user, $action, $obj, $destArray ) {
        $notifArray = array();
        
        foreach ($destArray as $value) {
            # code...
            $details = (object) $user->details;
            $dest['_id'] = new MongoId( $value );

            $notif = new Notification($action, $user, (object)$dest, $obj, $details->name, $details->lastname, $obj->name );
            $notifArray[] = $notif;
        }

        foreach ($notifArray as $notif) {
            if ( !$notif->save() )
                throw new Exception( "Notification was not saved", UNKNOWN_DB_ERROR );
        }
    }

    
    /**
     * Default Error Action
     */
    public function actionError() {

        if($error=Yii::app()->errorHandler->error) {

            if( $error['code'] == 404 )
                $err = '404 Not Found';
            else
                $err = OAUTH2_HTTP_BAD_REQUEST;

            header("HTTP/1.1 ". $err);
            header("Content-Type: application/json");
            header('Access-Control-Allow-Origin: *');
            header("Cache-Control: no-store");
            echo json_encode ( array(
                "status"=> "error", 
                "message" => $error['message'], 
                "error"=> $error['type'],
                "file" => $error['file'], 
                "line" => $error['line'],
                ), JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES );
            
            Yii::app()->end();
        }
    }

    /**
     * Sends an email using the paramteres set as input fields throught the configured email server
     * 
     * @param  Object $options The information we need to create the email includes also the code
     * @param  String $subject The subject of the email
     * @param  String $message Html rendered text prepared to be sent by mail
     * 
     * @throws Exception      
     */
    protected function sendMail( $options, $subject, $message ){

        $email = $options->user;       
        
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->IsSMTP();
        $mail->Host = 'smtp.address.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'woo@address.com';
        $mail->Password = 'testest12';
        $mail->SetFrom('woo@address.com', 'WOO Info');
        $mail->Subject = $subject;
        $mail->Body = $message;

        //$mail->AltBody = $message;
        //$mail->MsgHTML($message);
        
        $mail->IsHTML(true);            
        $mail->AddAddress($email);
        
        if( !$mail->Send() ){
            //throw new Exception('There was an error sending the recovery mail.');
        }
    }

    /**
     * Filters the info provided about the user according the scope of current user
     * @see Oauth2User
     * 
     * @todo Set action based in oauth2 current scope
     * 
     * @param Object $result
     * @return Object Filtered user Profile 
     */
    protected function filterProfileData( $result ){

        $attrArray = $result->attributes;

        unset( $attrArray['client_id'] );      
        unset( $attrArray['client_secret'] );      
        unset( $attrArray['redirect_uri'] );      
        unset( $attrArray['password'] );     
        unset( $attrArray['email'] );  
        unset( $attrArray['userAgent'] );      
        unset( $attrArray['device_id'] );
        unset( $attrArray['hash'] );
        unset( $attrArray['_friends'] );

        return $attrArray; 
    }
    
    /**
     * Prepares the response headers as json
     */
    protected function setJsonHeaders() {
        header("Content-Type: application/json");
        header("Cache-Control: no-store");
        header('Access-Control-Allow-Origin: *');
    }
    
    /**
     * Formats the response for pagination results
     * 
     * @param Array $items          The actual element set to be returned
     * @param Integer $size          The actual element set to be returned
     * @param Integer $current      The first element retrieved in the list
     * @return JSONArray
     */ 
    protected function setPaginatedOutput($items, $size, $current ) {
        return array(
            "status"=>"ok",
            "size"=>$size,
            "offset"=>$current,
            "items"=>$items,
            //"total"=>$totalItems,
        );
    }

    /**
     * Formats the response for spot version results
     * 
     * @param Array $added            The set of spots reciently added 
     * @param Array $removed          The set of spots reciently removed 
     * @param Array $updated          The set of spots reciently updated 
     * @return Array
     */ 
    protected function setSpotUpdates($added=array(), $updated=array(), $removed=array(), $nadded=0, $nupdated=0, $ndeleted=0 ) {
        return array(
            "status"=>"ok",
            "totaladded"=> $nadded,
            "totalupdated"=> $nupdated,
            "totaldeleted"=> $ndeleted,
            "added"=>$added,
            "updated"=>$updated,
            "removed"=>$removed,          
        );
    }   

    /**
     * Formats the response for challenge status request
     * 
     * @param Array $invited           
     * @param Array $accepted         
     * @param Array $passed          
     * @return Array
     */ 
    protected function setChallenges( $invited=array(), $accepted=array(), $passed=array() ) {
        return array(
            "status"=>      "ok",
            "invited"=>     $invited,
            "accepted"=>    $accepted,
            "passed"=>      $passed,
        );
    }    

    /**
     * Formats the response for spot version results
     * 
     * @param Array $added            The set of spots reciently added 
     * @param Array $removed          The set of spots reciently removed 
     * @param Array $updated          The set of spots reciently updated 
     * @return Array
     */ 
    protected function setSpotUpdatesInfo($added=0, $updated=0, $deleted=0 ) {
        return array(
            "status"=>"ok",
            "added"=>$added,
            "updated"=>$updated,
            "removed"=>$deleted,          
        );
    } 

    /**
     * Formats the response for paginated leaderboard results
     * 
     * @param Array $items The actual element set to be returned
     * @param Integer $size The number of elements present in the response
     * @param Integer $current The first element retrieved in the list
     * @param Object $current The first element retrieved in the list
     * @return JSONArray
     */ 
    protected function setLeaderboardFeed($items, $rank, $size, $current ) {
        return array(
            "status"=>"ok",
            "size"=>$size,
            "offset"=>$current,
            "rank"=> $rank,
            "items"=>$items,
            //"total"=>$totalItems,
        );
    }

    /**
     * [setKotaFeed description]
     * @param Array $items The actual element set to be returned
     * @param [type] $rank  [description]
     * @param [type] $size  [description]
     * @param [type] $comp  [description]
     * @return JSONArray
     */
    protected function setKotaFeed( $items, $rank, $size, $comp ) {
        return array(
            "status"=>"ok",
            "size"=>$size,
            "rank"=> $rank,
            "competition"=>$comp,
            "items"=>$items,
        );
    }    
    
    /**
     * Formats the response 
     * 
     * @param Array   $items            The actual element set to be returned
     * @param Integer $packed           The actual element set to be returned
     * @return JSONArray
     */ 
    protected function setOutput($items, $size) {
        return array(
            "status" => "ok",
            "items"=>$items,
            "size"=>$size,
        );
    }
    
    protected function sendOk( $message = null){
        $this->setJsonHeaders();
        echo json_encode( array(
            "status" => "ok",
            "message" => ($message !== null) ? $message : "",
        ) );
    }

    /**
     * Sends 201 HTTP Status Code
     * Confirms successful creation of a REST entity
     * 
     * @param string $message   Message to show in the exception
     * @param string $id        Id of the resource already created
     */
    protected function sendCreated( $message = null, $id = null){
        header("HTTP/1.1 ". RESOURCE_CREATED);
        header("Content-Type: application/json");
        header('Access-Control-Allow-Origin: *');
        header("Cache-Control: no-store");
        echo json_encode( array(
            "status" => "ok",
            "id" => ($id !== null) ? $id : "",
            "message" => ($message !== null) ? $message : "",
        ) );
    }
    
    /**
     * Sends 201 HTTP Status Code
     * Confirms successful creation of a REST entity
     * 
     * @param string $message   Message to show in the exception
     * @param string $id        Id of the resource already created
     */
    protected function sendObjCreated( $obj ){
        header("HTTP/1.1 ". RESOURCE_CREATED);
        header("Content-Type: application/json");
        header('Access-Control-Allow-Origin: *');
        header("Cache-Control: no-store");
        echo json_encode($obj, JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES) ;
        //echo json_encode( $obj );
    }

    /**
     * Sends 200 HTTP Status Code
     * Confirms successful operation
     * 
     * @param Object | Array $obj  The object to be transmitted
     */
    protected function sendObj( $obj ){
        $this->setJsonHeaders();
        header("Cache-Control: no-store");
        echo json_encode($obj, JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES) ;
    }


    /**
     * Sends 201 HTTP Status Code
     * Confirms successful creation of a REST image entity and its storage in S3
     * 
     * @param string $message   Message to show in the exception
     * @param string $id        Id of the resource already created
     */
    protected function sendImageCreated( $message = null, $url = null){
        header("HTTP/1.1 ". RESOURCE_CREATED);
        header("Content-Type: application/json");
        header('Access-Control-Allow-Origin: *');
        header("Cache-Control: no-store");
        echo json_encode( array(
            "status" => "ok",
            "url" => ($url !== null) ? $url : "",
            "message" => ($message !== null) ? $message : "",
        ) );
    }
    
    protected function sendExceptionInfo( $e, $c, $a, $error = null  ){
        
        if( !$error )
            $error = OAUTH2_HTTP_BAD_REQUEST;
      
        header("HTTP/1.1 ". $error);
        header("Content-Type: application/json");
        header('Access-Control-Allow-Origin: *');
        header("Cache-Control: no-store");
        $res = array(
                "status"=> "error", 
                "message" => $e->getMessage(), 
                "file" => $e->getFile(), 
                "code"=> $e->getCode(),
                "method" => "{$c}Controller::{$a}()", 
                "line" => $e->getLine(),
            );
                
        echo json_encode($res, JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES);
    }

    /**
     *  In order to accept a new version, this error provides the apps with the varsion to be sent for approval
     *  @param  String   $message    
     *  @param  String   $version    
     *  @param  Integer  $code
     *
     *  @version 1.3.3
     */
    protected function sendAcceptVersion( $message, $version, $code ){
        
        $error = OAUTH2_HTTP_BAD_REQUEST;
      
        header("HTTP/1.1 ". $error);
        header("Content-Type: application/json");
        header('Access-Control-Allow-Origin: *');
        header("Cache-Control: no-store");
        $res = array(
                "status"=> "error", 
                "version" => $version,
                "message" => $message,
                "code"=> $code
            );
                
        echo json_encode($res, JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES);
    }
    
    protected function getServerPath($out=false){

        if( $out ){
            $rurl = Yii::app()->params['mailreturn'];

            if ( isset( $_SERVER['HTTP_X_FORWARDED_PROTO'] ) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' )
                // $serverName = 'https://' . $_SERVER['SERVER_NAME'] . "/";
                $serverName = 'https://' . $rurl . "/";
            else 
                $serverName = 'http://' . $rurl . "/";

            return $serverName;
        }

        if ( isset( $_SERVER['HTTP_X_FORWARDED_PROTO'] ) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' ) 
            $serverName = 'http://' . $_SERVER['SERVER_ADDR'] . ':8080';
        else if( $_SERVER['SERVER_ADDR'] == $_SERVER['REMOTE_ADDR'] ) 
            $serverName = 'http://' . $_SERVER['SERVER_ADDR'] . ':8080';
        else {
            if( !preg_match( '(ec2-([0-9]{2,3})-([0-9]{2,3})-([0-9]{2,3})-([0-9]{2,3})\.(.*)*)', $_SERVER['SERVER_NAME'], $matches) ) {
                $serverName= 'http://' . $_SERVER['SERVER_NAME'];
                
            } else {
                $serverName = 'http://' . $matches[0].':8080';

            }
        }

        return $serverName . '/';
    }

    /**
     * Performs a curl http request operation based on input params
     * 
     * @param  String  $url    The url where the request is to be sent starting by /
     * @param  Array  $data    The actual data to be sent within the POST
     * @param  boolean $get    Whether the request is GET, default is POST
     * @param  boolean $return It the return transfer muist be hold synchronously
     */
    public function woopost($url, $data=null, $get=False, $return=False){

        if ( isset( $_SERVER['HTTP_X_FORWARDED_PROTO'] ) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' ) 
            $serverName = 'http://' . $_SERVER['SERVER_ADDR'] . ':8080';
        else if( $_SERVER['SERVER_PORT'] == '2000')
            $serverName = 'http://' . $_SERVER['HTTP_HOST'];
        //else if( $_SERVER['SERVER_PORT'] == '8080')
        //    $serverName = 'http://' . $_SERVER['HTTP_HOST'];
        else if( $_SERVER['SERVER_ADDR'] == $_SERVER['REMOTE_ADDR']  ) 
            $serverName = 'http://' . $_SERVER['SERVER_ADDR'] . ':' . $_SERVER['SERVER_PORT'] ;
        else
            $serverName = 'http://' . $_SERVER['SERVER_NAME'];

        $curl = curl_init($serverName . $url);

        if(!$get)
            curl_setopt($curl, CURLOPT_POST, true);
        if($data)
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        if($return)
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        return curl_exec($curl);
    }

    /**
     * Validate Hardware tools actions
     * @return Boolean True if Valid action
     */
    protected function validateCompanionAction(){

        if( isset($_POST['client_id']) && isset($_POST['client_secret']) && $_POST['client_id'] == COMPANION_ID && $_POST['client_secret'] == COMPANION_SECRET ) 
            return true;

        throw new OAuth2Exception( array('code' => HACKING_ACTIVITY, 'message' => "No valid credentialss") );   
    }

    /**
     * Retrieves validated pagination parameters which come as a POST message
     * @return mixed boolean | array
     */
    protected function validatePagination(){
        $filters = array(
            "offset" => array("filter" => FILTER_REQUIRE_SCALAR),
            "pageSize" => array("filter" => FILTER_REQUIRE_SCALAR),
        );
        
        $input = filter_input_array(INPUT_POST, $filters);
        
        if( isset($input['offset'] ) && isset( $input['pageSize'] ) )
            return $input;
        else 
            return false;
    }

    protected function findInArray($elem, $array, $name){
        foreach ($array as $value) {
            if( $value->{$name} == $elem )
                return $value;
        }
        return false;
    }

    
}

?>
