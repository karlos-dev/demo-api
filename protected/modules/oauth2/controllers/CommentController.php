<?php

/**
 * Kinematiq API
 * CommentController
 * 
 * @author      Karlos Álvarez <karlos.alvan@gmail.com>
 * @abstract    Library implementing all Comments functionallity
 * @uses        OAuth2Exception
 * @uses        MongoException
 * @uses        Exception
 * 
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . "..".DIRECTORY_SEPARATOR. "lib".DIRECTORY_SEPARATOR."OAuth2Exception.inc";


/**
 * @abstract    Error definition for every single API action
 */


class CommentController extends KController {
    
    
    /**
     * Creates a Comment
     * It receives session information from $_POST params and stores them upon validation inside db
     *
     * @deprecated
     * 
     * @todo Future development
     * @throws OAuth2Exception
     * @throws MongoException
     */

    public function actionCreate(){
        try {
            
            //  check post data 
            //  check name and geolocation for validation
        
        } catch (Exception $e) {
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }
    
    
    /**
     * Retrieves a list of Comments
     * 
     * @param Integer $id the session id
     * @throws OAuth2Exception
     */

    public function actionIndex( $id = null ) {
        try {
            $oauth = YiiOAuth2::instance();
            $token = $oauth->verifyToken();
            $this->setJsonHeaders();

            // here we work with any other session comments
            if( $id == null )
                throw new Exception(array('message' => "No session id set in url. Not sending lists")) ;

            $c = new EMongoCriteria;            
            $c->addCondition( '_session', new MongoId($id) );
            
            // default session parameters
            $offset = 0;
            $pageSize = 20;
            
            // Check if the actual post contains extra parameters
            $params = $this->validatePagination();
            if( $params ) {
                $offset = $params['offset'];
                $pageSize = $params['pageSize'];
            }
            $c->setLimit($pageSize)->setSort( array('created' => 'desc') )->setSkip($offset);

            $comments = $this->findAll('Comment', $c );
            $userIds = array();
            $feed = array();
            foreach ($comments as $comment) {
                $userIds[] = $comment->_user;
            }

            $users = $this->findAllbyPk( 'Oauth2User', $userIds );

            foreach ($comments as $commentFeed ) {

                $usr = $this->findInArray( $commentFeed->_user, $users, '_id' );
                if ( $usr ) {
                    $obj = $commentFeed->attributes;
                    unset( $obj['_session'] );
                    unset( $obj['_id'] );                    
                    $obj['user_name'] = $usr->details['name'];
                    $obj['user_lastname'] = $usr->details['lastname'];
                    $obj['user_pictures'] = $usr->details['_pictures'];
                    $feed[] = $obj;
                }
            }
            echo json_encode( $this->setPaginatedOutput( $feed, count($feed), $offset) );
            
        } catch( Exception $e ) {
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    } 

    
    
    public function actionDelete( $id ){
        try {
            $oauth = YiiOAuth2::instance();
            $token = $oauth->verifyToken();

            $this->setJsonHeaders();

        } catch( Exception $e ) {
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
            
    }
   
}
?>
