<?php

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "lib" . DIRECTORY_SEPARATOR . "OAuth2Exception.inc";

function cmp($a, $b) {
    return strcmp( $a['created'], $b['created']);
}

/**
 * Kinematiq API LeaderboardController
 * 
 * Library implementing actions for leaderboard result computing. Microservice oriented implementation
 * 
 * @author      Karlos Álvarez <karlos.alvan@gmail.com>
 *              
 * @uses        OAuth2Exception
 * @uses        MongoException
 * @uses        Exception
 */

class LeaderboardController extends KController { 


    public function actionSpot( $id ){
        try {            
            $this->setJsonHeaders();

            $spot = $this->findByPk( "Spot", $id );

            if( !$spot ) {
                throw new OAuth2Exception( array('code' => BAD_SPOT_ID, 'message' => "Not spot found" ));
            }

            $id = Yii::app()->controller->getModule("oauth2")->getUid();
            $user = $this->findByPk('Oauth2User', $id );
            if( !$user )
                throw new OAuth2Exception( array('code' => UNKNOWN_OAUTH_TOKEN, 'message' => "Cannot find user for this token.") );


            $filters = array(
                "target" =>     array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/(following)|(community)/") ),  
                "feature" =>    array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/(airtime)|(height)/") ),
                "offset" =>     array("filter" => FILTER_REQUIRE_SCALAR),
                "pageSize" =>   array("filter" => FILTER_REQUIRE_SCALAR),                
            );
            
            $input = filter_input_array(INPUT_POST, $filters);  

            $target =   isset($input['target']) ? $input['target'] : 'following';            
            $offset =   isset($input['offset']) ? $input['offset'] : 0;
            $pageSize = isset($input['pageSize']) ? $input['pageSize'] : 20;
            $feature =  'highestAir';

            if( isset($input['feature']) )
                switch ($input['feature']) {
                    case 'airtime':
                        $feature = 'maxAirTime';
                        break;
                    case 'height':
                        $feature = 'highestAir';
                        break;
                }
            
            if( is_array( $spot->_kiters ) )
                $kiters = $spot->_kiters;
            
            switch ($target) {
                case 'community':
                    $targetUsers = $kiters;
                    break;
                case 'following':
                    // Iclude myself in the leaderboard to compare my rank with my followings
                    if( !$user->_followed )
                        $targetUsers = array( $user->_id );
                    else 
                        $targetUsers = array_intersect( array_merge( $user->_followed, array($user->_id) ), $kiters );
                    break;
                default:
                    $targetUsers = array();
            }  

            $mongoIds = array();
            foreach ($targetUsers as $id) {
                $mongoIds[] = new MongoId( $id );
            }
            
            $users = Oauth2User::model()->findAllByPk( $mongoIds );
            $mySet = array();
            foreach ($users as $u) {
                $details = (object) $u->details;
                $mySet[ (string) $u->_id ] = array( 
                    "_id" => $u->_id,
                    "name" => $details->name,
                    "lastname" => $details->lastname,
                    "country" => $details->country,
                    "_pictures" => $details->_pictures,
                );                
            }

            $c = new EMongoCriteria();
            $c->addCondition('feature', $feature);
            $c->compare('_spot', $spot->_id);
            
            // OLD WAY TO DO THIS
            // $c->_user('in', $mongoIds );
            // $c->_spot('in', $spotIds );
            
            $c->setSort( array('score' => 'desc' ));
            $rankedItems = iterator_to_array( Rank::model()->findAll( $c ), false);

            $pos = 1;
            $done = false;
            $leaderBoard = array();
            $yourPosition = -1;
            $rankedIds = array();

            //print_r( $spotIds ); die;
            //print_r( $rankedItems ); die;
            
            foreach ($rankedItems as $rankedSession) {   
                if( isset($mySet[ (string) $rankedSession->_user ]) &&  !in_array( (string)$rankedSession->_user, $rankedIds)
                ) {
                    $obj = $mySet[ (string) $rankedSession->_user ];
                    $obj['score'] = $rankedSession->score;

                    $leaderBoard[] = $obj;

                    if(!$done && (string) $rankedSession->_user === (string) $user->_id ) {
                        $yourPosition = $pos;
                        $done = true;
                    }
                    $rankedIds[] = (string) $rankedSession->_user;
                    $pos++;
                }
            }

            $max = ( $pageSize > count($leaderBoard) ) ? count($leaderBoard) : $pageSize;
            $feed = array_slice($leaderBoard ,$offset, $max);

            echo json_encode( $this->setLeaderboardFeed($feed, array("total"=>count($leaderBoard), "position"=>$yourPosition ), count($feed), $offset), JSON_NUMERIC_CHECK );

        } catch (Exception $e) { 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        } 
    }

    /**
     * Action created for redbull kota competition
     * 
     */
    public function actionKota(){
        try {            
            $this->setJsonHeaders();

            $c = new EMongoCriteria();
            $c->addCondition('role', 'kota2015');
            $c->setSort( array( "details.name" => 'asc' ) );

            $users = Oauth2User::model()->findAll( $c );

            $mySet = array();
            $steadySet = array();
            $kiterIds = array();

            foreach ($users as $u) {
                $details = (object) $u->details;
                $mySet[ (string) $u->_id ] = array( 
                    "_id" => $u->_id,
                    "email" =>  $u->email,
                    "name" => $details->name,
                    "lastname" => $details->lastname,
                    "country" => $details->country,
                    "_pictures" => $details->_pictures,
                );                
                $steadySet[] = $mySet[ (string) $u->_id ];
                $kiterIds[] = $u->_id;
            }

            $riders = 24;
            $kotaId = '5437e9fb63209597048b4858';
            $kotaFeature = 'highestAir';

            $start =            1423648800;
            $endDayOne =        1423677600;

            $startDayFour =     1423900800;
            $endDayFour =       1423936800;

            $startDayFive =     1424001600;
            $endDayFive =       1424023200;


            $c = new EMongoCriteria();            
            $c->addCondition('_spot', new MongoId($kotaId) );            
            $c->compare('_user', $kiterIds);
            $c->compare('time', '>'.$start );
            $c->setSort( array( $kotaFeature => 'desc' ));

            $kotaSessions = array();

            $tempkotaSessions = iterator_to_array( Session::model()->findAll( $c ), false);

            foreach ($tempkotaSessions as $ks) {
                if( $ks->time < $endDayOne ) {

                    $kotaSessions[] = $ks;

                } 

                elseif ( $ks->time > $startDayFour && $ks->time < $endDayFour  ) {

                    $kotaSessions[] = $ks;                    

                } 
                elseif ( $ks->time > $startDayFive && $ks->time < $endDayFive  ) {

                    $kotaSessions[] = $ks;                    

                }
            }

            /*
             *  If there are no sessions, we feed the kiter list
             */
            if ( !$kotaSessions || empty( $kotaSessions ) ) {

                echo json_encode( $this->setKotaFeed( $steadySet, array("total"=>count($steadySet), "position"=>-1 ), count($steadySet), false), JSON_NUMERIC_CHECK );

            } else { 
            /*
             *  Then feed the kiters with the maximun scores during the competition rounds
             */
                $pos = 1;
                $done = false;
                $leaderBoard = array();
                $yourPosition = -1;
                $rankedIds = array();

                foreach ($kotaSessions as $rankedSession) { 

                    if( isset($mySet[ (string) $rankedSession->_user ]) && 
                        !in_array( (string) $rankedSession->_user, $rankedIds) 
                        // && $rankedSession->time > $start
                    ) {

                        $obj = $mySet[ (string) $rankedSession->_user ];
                        $obj['score'] = $rankedSession->{$kotaFeature};
                        $leaderBoard[] = $obj;
                        $rankedIds[] = (string) $rankedSession->_user;
                        $pos++;
                    } 
                }
                
                /*
                 *  Then get the rest of kiters having no sessions and return them with no score, 
                 *  sorted by name
                 */
                $rest = array_diff($kiterIds, $rankedIds);
                $set = array();

                foreach ($rest as $u) {  
                    $obj = $mySet[ (string) $u ];
                    $leaderBoard[] = $obj;
                }

                echo json_encode( $this->setKotaFeed($leaderBoard, array("total"=>count($leaderBoard), "position"=>$yourPosition ), count($leaderBoard), true), JSON_NUMERIC_CHECK );
            }
        } catch (Exception $e) { 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        } 
    }

    /**
     *  Check LB health
     *  Parses Rank collection finding the session which created the rank,
     *  ranks without linked session, mean the session was deleted so they
     *  must be removed.
     *  
     *  @version 1.3.5
     *  
     */
    public function actionCheck(){
        try {            
            
            
            $start = microtime(true);
            ob_start();
            $this->setJsonHeaders();

            $ill = 0;
            $loopSize = 100;
            $loopoffset = 0;
            $exit = false;
            
            $c = new EMongoCriteria();
            //$c->compare("_spot", $spotIds );
            //$c->addCondition("_id", $rank->_session );
            $c->setLimit( $loopSize );
            $c->setSkip( $loopoffset );
            $it = Rank::model()->find($c);
            //print_r($it); die;
            
            echo "Loaded {$it->count()} ranking items\n";
            ob_flush();
            flush();

            while ( !$exit ) {
                // Nothing else to get
                if( $it->count() < $loopSize )
                    $exit=true;

                while( $it->hasNext() ) {
                    $rank = (object) $it->getNext();
                    $ses = Session::model()->findByPk( $rank->_session );
                    //print_r($ses); die;
                    if( !$ses ) {
                        echo "\nRank {$rank->_session} {$rank->feature} {$rank->score} has no session\n";
                        $ill++;
                    } else {
                        echo ".";
                    }

                    ob_flush();
                    flush();
                }

                // Otherwise we have nothing else to do
                if( !$exit ) {
                    $loopoffset += $loopSize; 

                    $c = new EMongoCriteria();
                    //$c->addCondition("_id", $rank->_session );
                    $c->setLimit( $loopSize );
                    $c->setSkip( $loopoffset );
                    $it = Rank::model()->find($c);
                }

            }

            $end = microtime(true) - $start;

            echo "\n{$ill} Rank entries must be removed\n";
            echo "total {$end} seconds\n";
            ob_flush();
            flush();

            ob_clean();

        } catch (Exception $e) { 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        } 
    }

    /**
     *  Parse spot sessions to check integrity 
     *  It makes use of the MapReduce techniche from Mongodb
     *  @link https://docs.mongodb.org/manual/reference/method/db.collection.mapReduce/#db.collection.mapReduce
     *  
     *  @version 1.3.4
     *  
     */
    public function actionHealth(){
        try {            
            //$oauth = YiiOAuth2::instance();
            //$token = $oauth->verifyToken();
            $start = microtime(true);
            ob_start();
            $this->setJsonHeaders();
            //$users = $this->findAll('Oauth2User');
            $ill_spots = array();
            $loopSize = 100;
            $loopoffset = 0;
            $exit = false;
            
            //$it = $this->findAll('Oauth2User', $criteria);

            /*
            $map = new MongoCode("function() { emit( this._user, this._spot ); }");
            $reduce = new MongoCode("function(user, spots) { return spots }");
            $res = Yii::app()->mongodb->command( array_merge( array(
                    'mapreduce' => 'session',
                    'map' => $map,
                    'reduce' => $reduce,
                    //'finalize' => $finalize,
                    'query' => (object) array(),
                    //'out' => array('inline' => true)
                    'out' => 'results'
            ) ));
            */

            // Only spots with sessions stored haaaa
            $spotIds = Yii::app()->mongodb->session->distinct('_spot');
            echo "done spots " . "\n";
            ob_flush();
            flush();
            //$spotCollection = Spot::model()->findAllByPk( $spots );

            $c = new EMongoCriteria();
            $c->compare("_id", $spotIds );
            //$c->addCondition("_id", $rank->_session );
            $c->setLimit( $loopSize );
            $c->setSkip( $loopoffset );
            $it = Spot::model()->find($c);
            
            $feature = 'highestAir';
            //print_r( $spots[0]->{'$id'} ); die;
            echo "Loaded {$it->count()} spots\n";

            while ( !$exit ) {
                // Nothing else to get
                if( $it->count() < $loopSize )
                    $exit=true;

                while( $it->hasNext() ) {
                    $spot = $it->getNext();
                    //$c = new EMongoCriteria();
                    //$c->compare("_id", $spot );
                    //$spot = Spot::model()->findOne($c);

                    echo "Spot " . $spot->_id . " {$spot->name} \n";
                    ob_flush();
                    flush();

                    //print_r( $spot ); echo "done"; die;
                    $result = Session::model()->aggregate( 
                        array(
                            array('$match' => array("_spot" => $spot->_id ) ) ,
                            array('$group'=>array(
                                '_id'=>'$_spot',
                                //'value' => '$maxHeight',
                                'sessions'=>array('$push'=> 
                                    array( 
                                        '_id' => '$_id', 
                                        'name' => '$name', 
                                        'highestAir' => '$highestAir', 
                                        'maxAirTime' => '$maxAirTime',
                                        'numberOfAirs' => '$numberOfAirs',    
                                        'time' => '$time',
                                        'created' => '$created'
                                    ) ))),
                            //array('$sort' => array( 'value' => -1) ),
                        ));

                    // echo count($result['result']); die;
                    // print_r($result['result']); die;

                    foreach ( $result['result'] as $elem) {
                        //echo $elem['_id'] . "\n";
                        //ob_flush();
                        //flush();

                        $spotid = $elem['_id'];
                        $sessions  = $elem['sessions'];

                        usort($sessions, "cmp");
                        $ranked = null;
                        foreach ($sessions as $obj ) {
                            $s = (object) $obj;
                            $c = new EMongoCriteria();
                            $c->addCondition("_spot", $spotid );
                            $c->addCondition("_session", $s->_id );
                            //$c->addCondition("_user", $uid );
                            $c->addCondition("feature", $feature );

                            $itrank = Rank::model()->find($c);
                            $date = date('Y-m-d H:i:s', $s->created );
                            
                            //echo $date; die;
                            
                            if( $itrank->count() > 0) {
                                if( !$ranked ) {
                                    $ranked = $s;
                                    echo "Session {$s->_id} ranked in {$spot->name} as {$itrank->getNext()->feature} {$s->$feature} {$date}\n";
                                } elseif( $s->{$feature} < $ranked->{$feature} ) {
                                    echo "Session {$s->_id} ranked in {$spot->name} as {$itrank->getNext()->feature} {$s->$feature} {$date}\n";
                                    //echo "EXCESS: Session {$s->_id} probaly not properly ranked in {$spot->name} {$s->$feature} {$date} \n";
                                } elseif( $s->{$feature} >= $ranked->{$feature} ) {
                                    $ranked = $s;
                                    $date = date('Y-m-d H:i:s', $ranked->created);
                                    echo "Session {$s->_id} ranked in {$spot->name} as {$itrank->getNext()->feature} {$s->$feature} {$date}\n";
                                }

                            } elseif($ranked && $s->{$feature} > $ranked->{$feature} ) {
                                //echo $s->{$feature} . "\n";
                                //echo $ranked->feature . "\n";
                                echo "MISSING: Session {$s->_id} probaly not properly ranked in {$spot->name} {$s->$feature} {$date} \n";
                                $ill_spots[] = array( $spot->_id, $spot->name );
                            }
                            
                            ob_flush();
                            flush();
                        }
                        
                        //print_r($sessions);
                        // echo "{$key} visited " . count($value['visited']) . " spots\n";
                    }
                }

                // Otherwise we have nothing else to do
                if( !$exit ) {
                    $loopoffset += $loopSize; 

                    $c = new EMongoCriteria();
                    $c->compare("_spot", $spotIds );
                    //$c->addCondition("_id", $rank->_session );
                    $c->setLimit( $loopSize );
                    $c->setSkip( $loopoffset );
                    $it = Spot::model()->find($c);
                }

            }

            $end = microtime(true) - $start;

            echo "total ". count($ill_spots) . " unstable spots\n";
            echo json_encode( $ill_spots );
            echo "total {$end} seconds\n";
            ob_flush();
            flush();

            ob_clean();


        } catch (Exception $e) { 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        } 
    }



    public function actionCheckdelete(){
        try {            
            //$oauth = YiiOAuth2::instance();
            //$token = $oauth->verifyToken();
            $this->setJsonHeaders();
            $users = $this->findAll('Oauth2User');
            
            echo "Users: ".count($users)."\n";
            
            foreach ($users as $user) {
                $details = (object) $user->details;

                $c = new EMongoCriteria();
                $c->addCondition("_user", $user->_id );
                //$c->addCondition("_session", $session->_id );
                $ranks = iterator_to_array(  Rank::model()->findAll($c), false ); 

                foreach ($ranks as $rank) {
                    $c = new EMongoCriteria();
                    $c->addCondition("_user", $user->_id );
                    $c->addCondition("_id", $rank->_session );
                    $sessions = iterator_to_array(  Session::model()->findAll($c), false );
                    if( !$sessions )
                        echo "Error: user {$details->name} {$details->lastname} deleted a session but the rank {$rank->feature} is present.\n";
                    else
                        foreach ($sessions as $key => $session) {
                            echo "Session {$session->name} is ranked in {$rank->feature}.\n";
                        }
                }

                //$c = new EMongoCriteria();
                //$c->addCondition("_user", $user->_id );
                //$c->addCondition("_user", $user->_id );
                //$sessions = iterator_to_array(  Session::model()->findAll($c), false );

            }
            
            //echo json_encode( array("enable" => Yii::app()->params['kotalink']) );

        } catch (Exception $e) { 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        } 
    }

    /**
     * Dynamic IOS app modifier
     * Decides whether to show or not to show the kota menu within the IOS app 
     * @return boolean 
     */
    public function actionEnablekotamenu(){
        try {            
            $oauth = YiiOAuth2::instance();
            $token = $oauth->verifyToken();
            $this->setJsonHeaders();
            
            echo json_encode( array("enable" => Yii::app()->params['kotalink']) );

        } catch (Exception $e) { 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        } 
    }


}