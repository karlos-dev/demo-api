<?php

/**
 * Kinematiq API
 * FirmwareController
 * 
 * @author      Karlos Álvarez <karlos.alvan@gmail.com>
 * @abstract    Library implementing actions on Firmware builds
 * @uses        OAuth2Exception
 * @uses        MongoException
 * @uses        Exception
 * 
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "lib" . DIRECTORY_SEPARATOR . "OAuth2Exception.inc";


class FirmwareController extends KController { 


    /**
     * Uploads an image into filesystem, validates it, creates the mongodb ODM to handle its information and finally 
     * 
     */
    public function actionUpload(){
        try {
            
            $uid = Yii::app()->controller->getModule("oauth2")->getUid();
            $this->setJsonHeaders();
            $user = $this->findByPk('Oauth2User', $uid );
            if(!$user)
                throw new OAuth2Exception( array('code' => 401, 'message' => OAUTH2_ERROR_UNAUTHORIZED_CLIENT) );

            $role = $user->getRole();
            if(! in_array($role, array('admin','dev') ))
                throw new OAuth2Exception( array('code' => 401, 'message' => OAUTH2_ERROR_INVALID_SCOPE) );
                      
            if( !$_FILES )
                throw new OAuth2Exception( array('code' => NO_FILE, 'message' =>  'No file found') );
            
            $filters = array(
                    "hwversion" =>  array("filter" => FILTER_VALIDATE_REGEXP, "options"=>array("regexp"=>"/[0-9]{1,4}\.[0-9]{1,4}\.[0-9]{1,4}/i")  ),
                    "fwversion" =>  array("filter" => FILTER_VALIDATE_REGEXP, "options"=>array("regexp"=>"/[0-9]{1,4}\.[0-9]{1,4}\.[0-9]{1,4}/i")  ),
                    //"filename" =>  array("filter" => FILTER_VALIDATE_REGEXP, "options"=>array("regexp"=>"/^(woo)-[a-z0-9\s]{4}-[a-z0-9\s]{4}$/i") ),
                );

            $input = filter_input_array(INPUT_POST, $filters);

            if( empty($input["hwversion"]) || empty($input["fwversion"]))
                throw new OAuth2Exception( array('code' => BAD_POST_DATA, 'message' => "Please check your request") );

            $saveDir = '/tmp/';
            $file = $_FILES['fwbuild'];
            // $fname = sha1( time() . substr($file['name'], 0, -4) );                
            // print_r($_FILES); die;

            if (!move_uploaded_file(
                $file['tmp_name'],
                sprintf('%s%s',
                    $saveDir,
                    $file['name']
                    //pathinfo($file['name'], PATHINFO_EXTENSION)
                )
            )) //{
                throw new RuntimeException('Failed to move uploaded file.');

            $sc = new StorageController(0);
            // Create uri:
            // It creates the folder inside the S3 bucket
            $awsUri =  $input["hwversion"]."/".$input["fwversion"]."/".$file['name'];
            
            if( !$sc->addFwToBucket( $saveDir . $file['name'], $awsUri ) )
                throw new Exception('ERROR saving into bucket' );


            //$imageType = filter_var($_POST['type'], FILTER_VALIDATE_REGEXP, array( "options"=> array("regexp"=>"/(user)|(cover)|(session)|(fbshare)/")) ) ;
            //if( !$imageType )
            //    throw new Oauth2Exception( array('code' => UNKNOWN_IMAGE_TYPE, 'message' => "No valid image type"));
            //$savedImage = $this->generateImageFromFile($_FILES['imgUpload'],  $imageType, $user );           
            //$this->sendImageCreated('Image successfully saved', $savedImage->url);  
            
        } catch (OAuth2Exception $e){
            if( $e->getCode() === 401 )
                $this->sendExceptionInfo($e, $this->id, $this->action->id, OAUTH2_HTTP_UNAUTHORIZED);
            else
                $this->sendExceptionInfo($e, $this->id, $this->action->id);
        } catch (Exception $e){
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }

    /**
     * 
     */
    public function actionDownload(){ 
        try {
            
            $uid = Yii::app()->controller->getModule("oauth2")->getUid();
            $this->setJsonHeaders();
            $user = $this->findByPk('Oauth2User', $uid );
            if(!$user)
                throw new OAuth2Exception( array('code' => 401, 'message' => OAUTH2_ERROR_UNAUTHORIZED_CLIENT) );

            $role = $user->getRole();
            if(! in_array($role, array('admin','dev') ))
                throw new OAuth2Exception( array('code' => 401, 'message' => OAUTH2_ERROR_INVALID_SCOPE) );

            $filters = array(
                    "hwversion" =>  array("filter" => FILTER_VALIDATE_REGEXP, "options"=>array("regexp"=>"/[0-9]{1,4}\.[0-9]{1,4}\.[0-9]{1,4}/i")  ),
                    "fwversion" =>  array("filter" => FILTER_VALIDATE_REGEXP, "options"=>array("regexp"=>"/[0-9]{1,4}\.[0-9]{1,4}\.[0-9]{1,4}/i")  ),
                    //"filename" =>  array("filter" => FILTER_VALIDATE_REGEXP, "options"=>array("regexp"=>"/^(woo)-[a-z0-9\s]{4}-[a-z0-9\s]{4}$/i") ),
                );

            $input = filter_input_array(INPUT_POST, $filters);

            if( empty($input["hwversion"]) || empty($input["fwversion"]))
                throw new OAuth2Exception( array('code' => BAD_POST_DATA, 'message' => "Please check your request") );

             $sc = new StorageController(0);
             echo $sc->getLastFirmwareDownload($input["hwversion"], $input["fwversion"] );


        } catch (OAuth2Exception $e){
            if( $e->getCode() === 401 )
                $this->sendExceptionInfo($e, $this->id, $this->action->id, OAUTH2_HTTP_UNAUTHORIZED);
            else
                $this->sendExceptionInfo($e, $this->id, $this->action->id);
        } catch (Exception $e){
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }

}