<?php

/**
 * WOO API
 * ChallengeController
 * 
 * @author      Karlos Álvarez <karlos.alvan@gmail.com>
 * @abstract    Library implementing all Challenge functionallity
 * @uses        OAuth2Exception
 * @uses        MongoException
 * @uses        Exception
 * 
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "lib" . DIRECTORY_SEPARATOR . "OAuth2Exception.inc";


class ChallengeController extends KController {
    

    /**
     * Creates a Challenge
     * It receives Challenge data into http $_POST and stores them upon validation inside db
     * The operation is ATOMIC
     *
     * @return 201 http code Created 
     *         404 http code Not Found
     *         401 http code Unauthorized | Token Expired
     */
    public function actionCreate(){ 
        try {
            
            $this->setJsonHeaders();
            $pk = Yii::app()->controller->getModule("oauth2")->getUid();
            $user = $this->findByPk('Oauth2User', $pk );    

            if(!$user)
                throw new OAuth2Exception( array('code' => 401, 'message' => OAUTH2_ERROR_UNAUTHORIZED_CLIENT ) );

            if( isset( $_POST ) ){ 
                $challenge = new Challenge();

                if( !$challenge->validateParams() )
                    throw new OAuth2Exception( array('code' => BAD_POST_DATA, 'message' => "Missing data") );

                $challenge->_creator = $user->_id;
                
                if( $challenge->save() ) {
                    if( $user->addChallenge($challenge) ) {
                        $this->notifyNewChallenge( $challenge, $user );
                        $this->sendObjCreated( $challenge );
                    } else {
                        $challenge->delete();
                        throw new OAuth2Exception( array('code' => UNKNOWN_DB_ERROR, 'message' => "There was an error creating challenge" ) );
                    }
                } else
                    throw new OAuth2Exception( array('code' => UNKNOWN_DB_ERROR, 'message' => "There was an error creating challenge" ) );
            }

        } catch (Exception $e) { 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        } 
    }

    /**
     * Creates a Challenge
     * It receives Challenge data into http $_POST and stores them upon validation inside db
     * The operation is ATOMIC
     *
     * @return 201 http code Created 
     *         404 http code Not Found
     *         401 http code Unauthorized | Token Expired
     */
    public function actionUpdate($id){ 
        try {
            
            $this->setJsonHeaders();
            $pk = Yii::app()->controller->getModule("oauth2")->getUid();
            $user = $this->findByPk('Oauth2User', $pk );    

            if(!$user)
                throw new OAuth2Exception( array('code' => 401, 'message' => OAUTH2_ERROR_UNAUTHORIZED_CLIENT ) );

            if( isset( $_POST ) ){ 
                //identify the challenge
                $challenge = Challenge::model()->findByPk( $id );
                if( !$challenge ) 
                    throw new OAuth2Exception( array('code' => BAD_CHALLENGE_ID, 'message' => "Not a valid id") );
                
                // Authorization to update challenge
                if( ! $challenge->isOwner($pk) )
                    throw new OAuth2Exception( array('code' => 401, 'message' => "Unauthorized") );  

                // The challenge must be not started and not ended
                if( $challenge->isStarted() || $challenge->isFinished() )
                    throw new OAuth2Exception( array('code' => CHALLENGE_LOCKED, 'message' => "Cannot modify challenge") ); 

                if( !$challenge->validateParams( true ) )
                    throw new OAuth2Exception( array('code' => BAD_POST_DATA, 'message' => "Invalid data") );

                if( ! $challenge->update() ) 
                    throw new OAuth2Exception( array('code' => UNKNOWN_DB_ERROR, 'message' => "There was an error updating challenge" ) );

                $this->sendOk("Chanllenge {$challenge->_id} successfully updated");
            }

        } catch (Exception $e) { 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        } 
    }

    /**
     *  Search for a challenge by name
     *  Finds input query in the POST:query attribute
     *  It performs a paginated search in the challenge database
     * 
     *  @return Json The paginated match of the Input query name
     */
    public function actionSearch(){
        try {
            $this->setJsonHeaders();
            if( !isset( $_POST['query']) )
                throw new OAuth2Exception( array('code' => BAD_POST_DATA, 'message' => "Not search string found") );

            $offset = 0;
            $pageSize = 20;
            
            // Check if the actual post contains extra parameters
            $params = $this->validatePagination();
            if( $params ) {
                $offset = $params['offset'];
                $pageSize = $params['pageSize'];
            }  

            $c = new EMongoCriteria;
            $c->addCondition('name', new MongoRegex('/(' . $_POST['query'] . ')/i') ); 
            $c->setLimit($pageSize)->setSort( array('name'=>'asc'))->setSkip($offset);  

            $it = Challenge::model()->find( $c );
            $arr = iterator_to_array( $it, false );

            echo json_encode( $this->setPaginatedOutput($arr, count($arr), $offset), JSON_NUMERIC_CHECK );

        } catch( Exception $e){ 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }

    /**
     *  
     */
    public function actionActive() {
        $this->searchByStatus('open');
    }


    public function actionFinished() {
        $this->searchByStatus('closed');
    }

    /**
     *  Find challenges in which the current token owner is enrolled
     */
    public function actionPending() {
        try {
            
            $this->setJsonHeaders();
            $pk = Yii::app()->controller->getModule("oauth2")->getUid();
            $user = $this->findByPk('Oauth2User', $pk );
            if(!$user)
                throw new OAuth2Exception( array('code' => 401, 'message' => OAUTH2_ERROR_UNAUTHORIZED_CLIENT ) );

            $challengedetails = (object) $user->challengedetails;
            
            if( isset($challengedetails) ) {

                $user->addChallenge(null, true);
                $challengedetails = (object) $user->challengedetails;
            }

            $offset = 0;
            $pageSize = 50;
            $count = 0;
            $exit=false;

            $c = new EMongoCriteria;
            $c->compare('created' , '>'. $challengedetails->lastCheck );
            $c->setLimit($pageSize)->setSort( array('created'=>'desc'))->setSkip($offset);  

            while( !$exit ) {
                // $it = Challenge::model()->find( $c, Challenge::fields() );
                $it = Challenge::model()->find( $c );

                if( $it->count(true) < $pageSize )
                    $exit = true;

                while( $it->hasNext() ) {
                    $challenge = $it->getNext();
                    //print_r( Challenge::publish( $challenge ) ); die;

                    if( $challenge->isInvited( $pk ) &&  !$challenge->isEnrolled( $pk ) )
                        $count++;
                }

                $offset = $offset + $pageSize;
                $c->setLimit($pageSize)->setSort( array('created'=>'desc'))->setSkip($offset);  
            }

            $challengedetails->lastCheck = time();
            $user->challengedetails = $challengedetails;

            if( !$user->update() )
                throw new OAuth2Exception( array('code' => UNKNOWN_DB_ERROR, 'message' => "There was an error updating challenge details" ) );

            $this->sendObj( array( 'items' => $count ) );

        } catch( Exception $e){ 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }

    /**
     *  Find challenges in which the current token owner is enrolled
     *  Makes difference between:
     *  - invited: received invitation but still not accepted
     *  - accepted: accepted (enroled) challenges still active
     *  - passed: enroled challenges already finished
     * 
     */
    public function actionStatus() {
        try {
            
            $this->setJsonHeaders();
            $pk = Yii::app()->controller->getModule("oauth2")->getUid();
            $user = $this->findByPk('Oauth2User', $pk );
            if(!$user)
                throw new OAuth2Exception( array('code' => 401, 'message' => OAUTH2_ERROR_UNAUTHORIZED_CLIENT ) );

            $c = new EMongoCriteria;

            if( isset( $_POST['type']) ) {
                $type = filter_var( $_POST['type'], FILTER_VALIDATE_REGEXP, array( "options"=> array("regexp"=>"/(public)|(private)/i")) );
                if( !empty($type) )
                    $c->addCondition( 'type', $type );

                //echo $type; die;
            }

            $limit = 6;
            $passedCount = 0;
            $acceptedCount = 0;
            $invitedCount = 0;

            $pageSize = 100;
            $offset = 0;
            $exit = false;
            
            $invited = array();
            $accepted = array();
            $passed = array();

            $c->setLimit($pageSize)->setSort( array('created'=>'desc'))->setSkip($offset);  

            while( !$exit ) {
                // $it = Challenge::model()->find( $c, Challenge::fields() );
                $it = Challenge::model()->find( $c );

                if( $it->count(true) < $pageSize )
                    $exit = true;

                while( $it->hasNext() ) {
                    $challenge = $it->getNext();
                    //print_r( Challenge::publish( $challenge ) ); die;

                    if( $challenge->isInvited( $pk ) || $challenge->isOwner( $pk )  ) {

                        if( $challenge->isEnrolled( $pk ) ){

                            if ( $challenge->end < time() ) { 

                                if( $passedCount < $limit ) {
                                    $passed[] = Challenge::publish( $challenge );
                                    $passedCount++;
                                }
                                
                            } else {

                                if( $acceptedCount < $limit ) {
                                    $accepted[] = Challenge::publish( $challenge );
                                    $acceptedCount++;
                                }

                            } 

                        } else if( $challenge->isInvited( $pk ) && $challenge->end > time() ) {

                            if( $invitedCount < $limit ) {
                                $invited[] = Challenge::publish( $challenge );
                                $invitedCount++;
                            }
                        }
                    }
                }

                $offset = $offset + $pageSize;
                $c->setLimit($pageSize)->setSort( array('created'=>'desc'))->setSkip($offset);  
            }

            $this->sendObj( $this->setChallenges( $invited, $accepted, $passed ) ) ;
            

        } catch( Exception $e){ 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }

    /**
     * Returns a leaderboard set of the session that are actually registered to the challenge
     * @param  String $id The identificator of the challenge
     */
    public function actionLeaderboard($id){
        try {            
            $this->setJsonHeaders();

            //identify the current user
            $pk = Yii::app()->controller->getModule("oauth2")->getUid();
            $user = $this->findByPk('Oauth2User', $pk );
            if(!$user)
                throw new OAuth2Exception( array('code' => OAUTH2_ERROR_UNAUTHORIZED_CLIENT, 'message' => "Unauthorized token.") );

            //identify the challenge
            $challenge = Challenge::model()->findByPk( $id );
            if( !$challenge ) { 
                throw new OAuth2Exception( array('code' => BAD_CHALLENGE_ID, 'message' => "Not a valid id") );
            }
            
            // Authorization to get info from challenge
            if( $challenge->type == 'private' && !$challenge->isEnrolled($pk) )
                throw new OAuth2Exception( array('code' => 401, 'message' => "Not enrolled in challenge") );  

            $filters = array(
                "target" =>     array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/(following)|(community)/") ),  
                "offset" =>     array("filter" => FILTER_REQUIRE_SCALAR),
                "pageSize" =>   array("filter" => FILTER_REQUIRE_SCALAR),                
            );
            
            $input = filter_input_array(INPUT_POST, $filters);  

            $target =   isset($input['target']) ? $input['target'] : 'following';
            $offset =   isset($input['offset']) ? $input['offset'] : 0;
            $pageSize = isset($input['pageSize']) ? $input['pageSize'] : 20;
            
            switch ( $challenge->feature ) {
                case 'airtime':
                    $feature = 'maxAirTime';
                    break;
                case 'height':
                    $feature = 'highestAir';
                    break;
                case 'numberOfAirs':
                    $feature = 'numberOfAirs';
                    break;                    
            }
            
            // The kiters array 
            $kiters = $challenge->enrolled();
            if( $target == 'following' ) {
                $kiters = array_intersect($user->_followed, $kiters );
                $kiters[] = $pk;
            }
            if( empty($kiters) )
                $kiters = array($pk);

            $mongoIds = array();
            foreach ($kiters as $id) {
                $mongoIds[] = new MongoId( $id );
            }

            $sessionMongoIds = array();
            foreach ($challenge->sessions() as $id) {
                $sessionMongoIds[] = new MongoId( $id );
            }

            $users = Oauth2User::model()->findAllByPk( $mongoIds );
            $mySet = array();
            foreach ($users as $u) {
                $details = (object) $u->details;
                $mySet[ (string) $u->_id ] = array( 
                    "_id" => $u->_id,
                    "name" => $details->name,
                    "lastname" => $details->lastname,
                    "country" => $details->country,
                    "_pictures" => $details->_pictures,
                );                
            }

            $c = new EMongoCriteria();
            
            // session by challenge users
            $c->compare('_user', $mongoIds);
            
            // sessions included in challenge users
            $c->compare('_id', $sessionMongoIds);

            // sort by the challenge feature
            $c->setSort( array( $feature => 'desc' ) );

            $pos = 1;
            $done = false;
            $exit = false;
            $leaderBoard = array();
            $tempLb = array();
            $yourPosition = -1;
            $rankedIds = array();
            $tempAcc = array();
            $loopoffset = $offset;
            $loopSize = 100;

            $c->setLimit($loopSize)->setSkip($offset);  
            $it = Session::model()->find( $c );

            while( !$exit ) {

                if( $it->count(true) < $loopSize )
                    $exit = true;

                while( $it->hasNext() ) {
                    $rankedSession = $it->getNext();
                    //echo $rankedSession->_id . " "; echo $rankedSession->_user . " "; echo $rankedSession->name . " "; echo $rankedSession->{$feature} . "\n";
                    
                    if( $challenge->logic == 'max') {
                        if( isset($mySet[ (string) $rankedSession->_user ]) && !in_array( (string) $rankedSession->_user, $rankedIds) ) {
                            $obj = $mySet[ (string) $rankedSession->_user ];
                            $obj['score'] = $rankedSession->{$feature};
                            $leaderBoard[] = $obj;

                            if(!$done && (string) $rankedSession->_user === (string) $user->_id ) {
                                $yourPosition = $pos;
                                $done = true;
                            }

                            $rankedIds[] = (string) $rankedSession->_user;
                            $pos++;
                        }

                    } else if( $challenge->logic == 'acc') {
                        if( isset($tempLb[ (string) $rankedSession->_user ]) )
                            $obj = (array) $tempLb[ (string) $rankedSession->_user ];
                        else
                            $obj = (array) $mySet[ (string) $rankedSession->_user ];

                        /**
                         * @abstract
                         *  Accomulated scores need to be added to the current user object score.
                         *  Session ids are stored to match created timestamps to decide the max value 
                         *  in case of equal LB score.
                         * 
                         * @version 1.3.0
                         */
                        $previousScore = isset($obj['score']) ? $obj['score'] : 0;
                        $obj['score'] = $previousScore + $rankedSession->{$feature};
                        $obj['sids'][] = (string) $rankedSession->_id;

                        $tempLb[ (string) $rankedSession->_user ] = (object) $obj;
                    }
                }

                $loopoffset = $loopoffset + $loopSize;
                $c->setLimit($loopSize)->setSkip($loopoffset);  
                $it = Session::model()->find( $c );
            }       

            $yourScore = 0;

            if( $challenge->logic == 'acc') {
                foreach ($tempLb as $key => $value) {
                    if( $key == $pk ) {
                        $yourScore = $value;
                        $yourPosition = 1;
                    }
                }

                /**
                 * @abstract
                 *  The position of the current user must be calculated parsing again the accomulated 
                 *  results array. 
                 *  In case of equal values response becomes slowe by adding db requests to get the session 
                 *  created timestamps
                 */
                foreach ($tempLb as $key => $value) {
                    if( $key !== $pk && $value->score > $yourScore->score ) {
                        $yourPosition++;
                    } else if( $key !== $pk && $value->score == $yourScore->score ) {

                        /**
                         * @abstract
                         *  In case there's a strike in any accomulated results, the earliest sessions do have 
                         *  more value than latest. This way, the first one recording a good session has more
                         *  chances to have better score.
                         * 
                         *  The Lowest timestamp always beats.
                         * 
                         * @version 1.3.0
                         */

                        $other = iterator_to_array( Session::model()->findAllByPk( $value->sids, array('created') ), false );
                        $other_ts = ArrayHelper::sumArrayAttribute('created', $other);

                        $yours = iterator_to_array( Session::model()->findAllByPk($yourScore->sids, array('created')), false );
                        $yours_ts = ArrayHelper::sumArrayAttribute('created', $yours);

                        if( $yours_ts > $other_ts )
                            $yourPosition++;

                    }
                    $value = (array) $value;
                    unset($value['sids']);
                    $leaderBoard[] = $value;
                }

                // ???
                // usort($leaderBoard, array( (new ArrayHelper( 'score' )) ,  'cmp' ) );
            }
                
            $feed = array_slice($leaderBoard ,$offset, $pageSize);

            echo json_encode( $this->setLeaderboardFeed($feed, array("total"=>count($leaderBoard), "position"=>$yourPosition ), count($feed), $offset), JSON_NUMERIC_CHECK );

        } catch (Exception $e) { 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        } 
    }

    /**
     *  The current user accepts the challenge with identified by $id
     *  @param $id The accepted challenge identificator
     */
    public function actionAccept($id) {
        try {
            
            $this->setJsonHeaders();
            $pk = Yii::app()->controller->getModule("oauth2")->getUid();
            $user = $this->findByPk('Oauth2User', $pk );
            if(!$user)
                throw new OAuth2Exception( array('code' => 401, 'message' => OAUTH2_ERROR_UNAUTHORIZED_CLIENT ) );

            $offset=0;
            $pageSize=20;

            $challenge = Challenge::model()->findByPk( $id );
            if( !$challenge ) 
                throw new OAuth2Exception( array('code' => BAD_CHALLENGE_ID, 'message' => "Not a challenge id" ) );
            
            if( $challenge->isFinished() )
                throw new OAuth2Exception( array('code' => CHALLENGE_FINISHED, 'message' => "Challenge is finished" ) );

            if( !$user->enroleInChallenge( $challenge ) )
                throw new OAuth2Exception( array('code' => UNKNOWN_DB_ERROR, 'message' => "Cannot enrole in challenge" ) );

            $this->sendOk("User {$pk} enroled in challenge {$challenge->_id}");

        } catch( Exception $e){ 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }

    /**
     *  The current user declines the challenge with identified by $id
     *  @param $id The declined challenge identificator
     */
    public function actionDecline($id) {
        try {
            
            $this->setJsonHeaders();
            $pk = Yii::app()->controller->getModule("oauth2")->getUid();
            $user = $this->findByPk('Oauth2User', $pk );
            if(!$user)
                throw new OAuth2Exception( array('code' => 401, 'message' => OAUTH2_ERROR_UNAUTHORIZED_CLIENT ) );

            $challenge = Challenge::model()->findByPk( $id );
            if( !$challenge ) { 
                throw new OAuth2Exception( array('code' => BAD_CHALLENGE_ID, 'message' => "Not a challenge id" ) );
            }

            if( $challenge->isFinished() )
                throw new OAuth2Exception( array('code' => CHALLENGE_FINISHED, 'message' => "Challenge is finished" ) );

            $challenge->decline($pk);
            if( !$challenge->update() )
                throw new OAuth2Exception( array('code' => UNKNOWN_DB_ERROR, 'message' => "Cannot save challenge" ) );

            else if( !$user->declineChallenge( $challenge ) ) {
                    $challenge->delete();
                    throw new OAuth2Exception( array('code' => UNKNOWN_DB_ERROR, 'message' => "Cannot save challenge" ) );
                }

            $this->sendOk("User {$pk} declined the challenge {$challenge->_id}");

        } catch( Exception $e){ 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }

    /**git 
     *  The current user declines the challenge with identified by $id
     *  @param $id The declined challenge identificator
     */
    public function actionDetail($id) {
        try {
            
            $this->setJsonHeaders();
            $pk = Yii::app()->controller->getModule("oauth2")->getUid();
            $user = $this->findByPk('Oauth2User', $pk );
            if(!$user)
                throw new OAuth2Exception( array('code' => 401, 'message' => OAUTH2_ERROR_UNAUTHORIZED_CLIENT ) );

            $challenge = Challenge::model()->findByPk( $id );
            if( !$challenge ) { 
                throw new OAuth2Exception( array('code' => BAD_CHALLENGE_ID, 'message' => "Not a valid id") );
            }
            //if( $challenge->type == 'private' && ! $challenge->isEnrolled($pk) )
            if( $challenge->type == 'private' && !$challenge->isInvited($pk) && !$challenge->isOwner($pk) )
                throw new OAuth2Exception( array('code' => 401, 'message' => "Not invited to challenge") );   

            $this->sendObj($challenge);


        } catch( Exception $e){ 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    } 

    /**
     *  The current user declines the challenge with identified by $id
     *  @param $id The declined challenge identificator
     */
    public function actionBystatus($id) {
        try {
            
            $this->setJsonHeaders();
            $pk = Yii::app()->controller->getModule("oauth2")->getUid();
            $user = $this->findByPk('Oauth2User', $pk );
            if(!$user)
                throw new OAuth2Exception( array('code' => 401, 'message' => OAUTH2_ERROR_UNAUTHORIZED_CLIENT ) );

            $offset=0;
            $pageSize=20;

            $status = filter_var( $id, FILTER_VALIDATE_REGEXP, array( "options"=> array("regexp"=>"/(accepted)|(invited)|(passed)/i")) );
            $challenges = array();
            if( !$status ) {
                throw new OAuth2Exception( array('code' => BAD_CHALLENGE_STATUS, 'message' => "Not a valid status" ) );
            }

            // Check if the actual post contains extra parameters
            $params = $this->validatePagination();
            if( $params ) {
                $offset = $params['offset'];
                $pageSize = $params['pageSize'];
            }

            //echo $pageSize; die;

            $c = new EMongoCriteria;
            if( isset( $_POST['type']) ) {
                $type = filter_var( $_POST['type'], FILTER_VALIDATE_REGEXP, array( "options"=> array("regexp"=>"/(public)|(private)/i")) );
                if( !empty($type) )
                    $c->addCondition( 'type', $type );
            }

            $loopSize = 100;
            $loopoffset = $offset;
            $itemCount = 0;
            $exit=false;

            $c->setLimit($loopSize)->setSort( array('created'=>'desc'))->setSkip($loopoffset);  

            while( !$exit ) {
                $it = Challenge::model()->find( $c );

                if( $it->count(true) < $loopSize )
                    $exit = true;

                switch ( $status ) {
                    case 'accepted': {
                        while( $it->hasNext() ) {
                            $challenge = $it->getNext();
                            if( $challenge->isEnrolled( $pk ) ) {
                                $challenges[] = Challenge::publish( $challenge );
                                $itemCount++;

                                if( $itemCount == $pageSize ) {
                                    $exit = true;
                                    break;
                                }
                            }
                        }
                    } break;
                    case 'invited': {
                        while( $it->hasNext() ) {
                            $challenge = $it->getNext();
                            if( $challenge->isInvited( $pk ) ) {
                                $challenges[] = Challenge::publish( $challenge );
                                $itemCount++;

                                if( $itemCount == $pageSize ) {
                                    $exit = true;
                                    break;
                                }
                            }
                        }
                    } break;
                    case 'passed': {
                        while( $it->hasNext() ) {
                            $challenge = $it->getNext();
                            if( $challenge->isEnrolled( $pk ) && $challenge->end < time() ) {
                                $challenges[] = Challenge::publish( $challenge );
                                $itemCount++;

                                if( $itemCount == $pageSize ) {
                                    $exit = true;
                                    break;
                                }
                            }
                        }

                    } break;
                }

                $loopoffset = $loopoffset + $loopSize;
                $c->setLimit($loopSize)->setSort( array('created'=>'desc'))->setSkip($loopoffset);  
            }

            echo json_encode( $this->setPaginatedOutput( $challenges, count($challenges), $offset), JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES );

        } catch( Exception $e){ 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }

    /**
     *  Send notifications to the current Challenge invitors
     *  Current version sends one request per user to the Push server
     *
     *  @param $challenge The current challenge object
     *  @param $user The current oauth2user object
     */
    protected function notifyNewChallenge( $challenge, $user ) {

        $it = Oauth2User::model()->findAllByPk( $challenge->invited(), array( 'enable_notifications','device_id','_id' ) );
        while( $it->hasNext() ) {
        //foreach ($challenge->invited() as $value) {
            $value = $it->getNext();
            // $obj = new Array();
            // $obj['_id'] = new MongoId( $value );
            $this->manageNotification('newchallenge', $user, $value, $challenge );
        }
    }

    /**
     * Wrapper function for search by status attribute
     * @param  String $status The status string identifier
     */
    private function searchByStatus( $status ) {
        try {
            $this->setJsonHeaders();
            //$pk = Yii::app()->controller->getModule("oauth2")->getUid();
            //$user = $this->findByPk('Oauth2User', $pk );

            $offset=0;
            $pageSize=20;

            // Check if the actual post contains extra parameters
            $params = $this->validatePagination();
            if( $params ) {
                $offset = $params['offset'];
                $pageSize = $params['pageSize'];
            }

            $c = new EMongoCriteria();
            //$c->compare('status', '<>closed');
            $c->addCondition('status', $status );
            $c->setLimit($pageSize)->setSkip($offset);

            $items = Challenge::model()->findAll( $c, Challenge::fields() );
            $array = iterator_to_array( $items , false );

            if( $array->count() > 0 ) { 
                $response = array(
                    "total" => $items->count(),
                    "items" => $array,
                );
            } else {
                $response = array(
                    "total" => 0,
                    "items" => array(),
                );
            }

            echo json_encode($response, JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES) ;


        } catch( Exception $e){ 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }
}