<?php

/**
 * Kinematiq API
 * SpotController
 * 
 * Library implementing all Spot functionallity
 * 
 * @author      Karlos Álvarez <karlos.alvan@gmail.com>
 * @uses        OAuth2Exception
 * @uses        MongoException
 * @uses        Exception
 * 
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "lib" . DIRECTORY_SEPARATOR . "OAuth2Exception.inc";


class SpotController extends KController {

    private $safe_ts = 1426190400;

    /**
     *  Check Spots health
     *  Parses the spot data and compares it with real stored data
     *  Every spot marked must be updated with real data
     *  
     *  @version 1.3.5
     *  
     */
    public function actionHealth(){
        try {            
            $start = microtime(true);
            ob_start();
            $this->setJsonHeaders();
            $ill = 0;
            $loopSize = 100;
            $loopoffset = 0;
            $exit = false;
            
            // Only spots with sessions stored haaaa
            $spotIds = Yii::app()->mongodb->session->distinct('_spot');
            echo "done spots " . "\n";
            ob_flush();
            flush();
            //$spotCollection = Spot::model()->findAllByPk( $spots );

            $c = new EMongoCriteria();
            $c->compare("_id", $spotIds );
            //$c->addCondition("_id", $rank->_session );
            $c->setLimit( $loopSize );
            $c->setSkip( $loopoffset );
            $it = Spot::model()->find($c);
            
            echo "Loaded {$it->count()} spots\n";

            while ( !$exit ) {
                // Nothing else to get
                if( $it->count() < $loopSize )
                    $exit=true;

                while( $it->hasNext() ) {
                    $spot = $it->getNext();
                    
                    $c = new EMongoCriteria();
                    $c->addCondition("_spot", $spot->id );
                    $session = Session::model()->find($c);
                    
                    $kiters = array();
                    if( $session->count() == $spot->totalSessions ) {
                        foreach ($session as $s ) {
                            if( ! in_array($s->_user, $kiters) )
                                $kiters[] = $s->_user;
                        }
                        if( count($kiters) == $spot->totalKiters )
                            echo "Spot {$spot->name} is consistent\n";
                        else {
                            echo "Warning: Spot {$spot->name} has to be updated\n";
                            $ill++;
                        }
                    } else {
                        echo "Warning: Spot {$spot->name} has to be updated\n";
                        $ill++;
                    }

                    ob_flush();
                    flush();
                        
                }

                // Otherwise we have nothing else to do
                if( !$exit ) {
                    $loopoffset += $loopSize; 

                    $c = new EMongoCriteria();
                    $c->compare("_spot", $spotIds );
                    //$c->addCondition("_id", $rank->_session );
                    $c->setLimit( $loopSize );
                    $c->setSkip( $loopoffset );
                    $it = Spot::model()->find($c);
                }

            }

            $end = microtime(true) - $start;

            echo "total ". $ill . " unstable spots\n";
            echo "total {$end} seconds\n";
            ob_flush();
            flush();

            ob_clean();


        } catch (Exception $e) { 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        } 
    }
    
    /**
     * Creates a Spot
     * It receives spot data into http $_POST and stores them upon validation inside db
     * The operation is ATOMIC
     *
     * @return 201 http code Created 
     *         404 http code Not Found
     *         401 http code Unauthorized | Token Expired
     */
    public function actionCreate(){
        try {
            $this->setJsonHeaders();
            $pk = Yii::app()->controller->getModule("oauth2")->getUid();
            $user = $this->findByPk('Oauth2User', $pk );    

            if(!$user)
                throw new OAuth2Exception( array('code' => 401, 'message' => OAUTH2_ERROR_UNAUTHORIZED_CLIENT ) );

            $role = ( isset($user->role) && $user->role !== null ) ? $user->role : 'user';
            if( $role !== 'admin' && $role !== 'spotmanager') {
                throw new OAuth2Exception( array('code' => 401, 'message' => OAUTH2_ERROR_INVALID_SCOPE) );
            }

            // check post data 
            if( isset( $_POST ) ) {

                $spot = new Spot();
                $spot->validateParams( $_POST );

                $spot->modified = 0;
                $spot->created = time();

                $c = new EMongoCriteria();
                $c->addCondition('name', new MongoRegex('/(' . $spot->continent . ')/i') );
                $cont = Continent::model()->findOne($c);

                if( !$cont ){
                    throw new OAuth2Exception( array('code' => BAD_POST_DATA, 'message' => "No valid continent string found." ));
                }

                if( $spot->save() ) {

                    $cont->addSpot( $spot->_id );
                    $this->sendCreated("Spot successfully created", (string) $spot->_id );

                } else {
                    throw new OAuth2Exception( array('code' => UNKNOWN_DB_ERROR, 'message' => "Cannot create spot" ));
                    // Fallback for ACID
                    // $cont->removespot( $spot->_id );
                }
            }

        } catch (OAuth2Exception $e){
            if( $e->getCode() === 401 )
                $this->sendExceptionInfo($e, $this->id, $this->action->id, OAUTH2_HTTP_UNAUTHORIZED);
            else
                $this->sendExceptionInfo($e, $this->id, $this->action->id);

        } catch (Exception $e) { 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        } 
    }

    /**
     * Removes a Spot
     * Receives spot id within the request uri
     * The operation is ATOMIC
     *
     * @return 200 http code OK
     *         404 http code Not Found
     *         401 http code Unauthorized | Token Expired
     */
    public function actionRemove($id=null){
        try {

            /**
             * Chechk the authorization of the user
             */
            
            $this->setJsonHeaders();
            $pk = Yii::app()->controller->getModule("oauth2")->getUid();
            $user = $this->findByPk('Oauth2User', $pk );    

            if(!$user)
                throw new OAuth2Exception( array('code' => 401, 'message' => OAUTH2_ERROR_UNAUTHORIZED_CLIENT ) );

            $role = ( isset($user->role) && $user->role !== null ) ? $user->role : 'user';
            if( $role !== 'admin' && $role !== 'spotmanager') {
                throw new OAuth2Exception( array('code' => 401, 'message' => OAUTH2_ERROR_INVALID_SCOPE) );
            }

            //  check post data 
            //  check name and geolocation for validation
            
            switch( $_SERVER['REQUEST_METHOD'] ){ 
                case 'DELETE': { 
                    if( $id == null )
                        throw new OAuth2Exception( array('code' => BAD_SPOT_ID, 'message' => "Not a valid spot" ));

                    $spot = Spot::model()->findByPk( $id );

                    if( !$spot )
                        throw new OAuth2Exception( array('code' => BAD_SPOT_ID, 'message' => "Not spot found" ));
                    else 
                        if ( $spot->totalSessions > 0)
                            throw new OAuth2Exception( array('code' => UNKNOWN_DB_ERROR, 'message' => "Cannot remove spot with sessions" ));

                    $c = new EMongoCriteria();
                    $c->addCondition('name', new MongoRegex('/(' . $spot->continent . ')/i') );
                    $cont = Continent::model()->findOne($c);

                    if( !$cont )
                        throw new OAuth2Exception( array('code' => UNKNOWN_DB_ERROR, 'message' => "Cannot remove spot" ));

                    if( Spot::model()->deleteByPk( new MongoId($id) ) ) {
                        $cont->removespot( $spot->_id );
                        $this->sendOk("Spot successfully removed");
                    } else 
                        throw new OAuth2Exception( array('code' => UNKNOWN_DB_ERROR, 'message' => "Cannot remove spot" ));
                }
                break;
                default:
                    throw new OAuth2Exception( array('code' => INVALID_HTTP_METHOD, 'message' => "Not valid" ));
            }

        } catch (OAuth2Exception $e){
            if( $e->getCode() === 401 )
                $this->sendExceptionInfo($e, $this->id, $this->action->id, OAUTH2_HTTP_UNAUTHORIZED);
            else
                $this->sendExceptionInfo($e, $this->id, $this->action->id);

        } catch (Exception $e) { 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }  
    }

    /**
     * Updates a Spot
     * Receives spot data in POST request
     * The operation is ATOMIC
     *
     * @return 200 http code OK
     *         404 http code Not Found
     *         401 http code Unauthorized | Token Expired
     */
    public function actionUpdate(){
        try {

            /**
             * Chechk the authorization of the user
             */
            $this->setJsonHeaders();
            $pk = Yii::app()->controller->getModule("oauth2")->getUid();
            $user = $this->findByPk('Oauth2User', $pk );    

            if(!$user)
                throw new OAuth2Exception( array('code' => 401, 'message' => OAUTH2_ERROR_UNAUTHORIZED_CLIENT ) );

            $role = ( isset($user->role) && $user->role !== null ) ? $user->role : 'user';
            if( $role !== 'admin' && $role !== 'spotmanager') {
                throw new OAuth2Exception( array('code' => 401, 'message' => OAUTH2_ERROR_INVALID_SCOPE) );
            }

            //  check post data 
            //  check name and geolocation for validation
            
            switch( $_SERVER['REQUEST_METHOD'] ) { 
                case 'POST': { 
                    $filters = array(
                        "spotId" =>         array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/[a-z0-9\s]{2,24}/") ),  
                        "name" =>           array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/[\s\S]{1,128}/") ),  
                        "lat" =>            array("filter" => FILTER_VALIDATE_FLOAT,  "options"=> array("min_range"=> -90.0, 
                                                                                                        "max_range"=> 90.0 ) ),
                        "lng" =>            array("filter" => FILTER_VALIDATE_FLOAT,  "options"=> array("min_range"=> -180.0, 
                                                                                                        "max_range"=> 180.0 ) ),  
                        "country" =>        array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/[\s\S]{1,128}/") ),  
                        "area" =>           array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/[\s\S]{1,128}/") ),  
                        "area_level_1" =>   array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/[\s\S]{1,128}/") ),  
                        "countryCode" =>    array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/[\s\S]{2,3}/") ),
                    );  
        
                    $input = filter_input_array(INPUT_POST, $filters); 

                    if( empty( $input['spotId'] ) )
                        throw new OAuth2Exception( array('code' => BAD_SPOT_ID, 'message' => "Not a valid spot." ));
                    //if( empty( $input['lat'] ) )
                    //    throw new OAuth2Exception( array('code' => INCORRECT_COORDINATE, 'message' => "Invalid latitude coordinate" ));
                    //if( empty( $input['lng'] ) )
                    //    throw new OAuth2Exception( array('code' => INCORRECT_COORDINATE, 'message' => "Invalid longitude coordinate" ));

                    $spot = Spot::model()->findBy_id( $input['spotId'] );
                    if( !$spot )
                        throw new OAuth2Exception( array('code' => BAD_SPOT_ID, 'message' => "Not spot found." ));

                    $fields = array();
                    foreach ($input as $key => $value) {
                        if( !empty( $value ) && $key !== "spotId" ) {
                            $fields[] = $key;
                            $spot->{$key} = $value;
                        }
                    }

                    /**
                     *  @hack
                     *  This is to avoid 1.6 spot updates 
                     *  
                     *  $spot->modified=1426190466;
                     * 
                     *  removed 1.4.3
                     */
                    
                    $spot->modified= time();
                    $fields[] = 'modified';

                    if( $spot->update( $fields ) )
                        // $this->sendOk("Spot successfully updated.");
                        echo json_encode( $spot );
                    else
                        throw new OAuth2Exception( array('code' => UNKNOWN_DB_ERROR, 'message' => "Cannot update spot" ));
                }
                break;
                default:
                    throw new OAuth2Exception( array('code' => INVALID_HTTP_METHOD, 'message' => "Not valid" ));
            }
        } catch (OAuth2Exception $e){
            if( $e->getCode() === 401 )
                $this->sendExceptionInfo($e, $this->id, $this->action->id, OAUTH2_HTTP_UNAUTHORIZED);
            else
                $this->sendExceptionInfo($e, $this->id, $this->action->id);

        } catch (Exception $e) { 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        } 
    }
    
    /**
     * Retrieves a list of Spots 
     * The pagination has been limited to 100 elements for performance reasons.
     *
     * @updated Sep 9th 2014
     * 
     * @param Integer $id
     * @return Mixed Object | Array If $id is specified will return the object belonging to that spot
     * 
     */
    public function actionIndex( $id = null ){
        try {
            
            $this->setJsonHeaders();
            
            if( $id !== null ) { 
                $res = Spot::model()->findByPk( new MongoId($id) );
                if( !$res )
                    throw new OAuth2Exception( array('code' => BAD_SPOT_ID, 'message' => "Cannot find spot with the id provided.") );

               echo json_encode( $res  );
               return;
            } 

            // default session parameters
            $offset = 0;
            $pageSize = 20;
            
            // Check if the actual post contains extra parameters
            $params = $this->validatePagination();
            if( $params ) {
                $offset = $params['offset'];
                $pageSize = ( $params['pageSize'] > 1000 ) ? 1000 : $params['pageSize'];
            }
            
            $c = new EMongoCriteria;
            $c->setLimit($pageSize)->setSort(array('name'=>'asc'))->setSkip($offset);     
            $result = $this->findAll( 'Spot', $c);

            echo json_encode( $this->setPaginatedOutput( $result, count($result), $offset)  , JSON_NUMERIC_CHECK );
            
        } catch (Exception $e) { 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        } 
    } 
    

    /**
     *  Spot search by spot name
     *  @return pagination Object
     */
    public function actionSearch() {
        try {

            $this->setJsonHeaders(); 

            $start = microtime(true);

            $offset = 0;
            $pageSize = 20;
            
            // Check if the actual post contains extra parameters
            $params = $this->validatePagination();
            if( $params ) {
                $offset = $params['offset'];
                $pageSize = $params['pageSize'];
            }          

            if( 
                isset( $_POST['rad'] ) && !empty( $_POST['rad'] )
            ) {

                if( isset( $_POST['lat'] ) && isset( $_POST['lng'] )  ) {

                    $lat = filter_var( $_POST['lat'], FILTER_VALIDATE_REGEXP, array( "options"=> array("regexp"=>"/^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,6}$/")) );
                    $lng = filter_var( $_POST['lng'], FILTER_VALIDATE_REGEXP, array( "options"=> array("regexp"=>"/^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,6}$/")) );

                    if( ! $lat )
                        throw new OAuth2Exception( array('code' => INCORRECT_COORDINATE, 'message' => "The lat coordinate seems to be incorrect.") );
                    if( ! $lng )
                        throw new OAuth2Exception( array('code' => INCORRECT_COORDINATE, 'message' => "The lng coordinate seems to be incorrect.") );

                } else {
                    $lat = 41.38;
                    $lng = 2.18;
                }

                $earthRadiousInKm = 6371;
                $earthRadiusInMiles = 3963;

                $radiusValueInRadians = $_POST['rad'] / $earthRadiousInKm;
                $q = array(
                    'loc' => array(
                        '$geoWithin' => array(
                            '$centerSphere' => array( array( $lng, $lat), $radiusValueInRadians )
                        )
                    )
                ); 

                /**
                 * This is a workaround to solve a phpmongo driver bug 
                 * The driver passes the float values of coordinates as strings.
                 * This causes error within the remote mongo, so the phpmongodriver sends the incorrect values
                 */
                $json = json_encode($q, JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);   
                $formattedQuery = json_decode($json);     
                
                $it = Yii::app()->mongodb->spot->find( $formattedQuery );
                $spotsInSphere = $it->count();

                $spots = iterator_to_array( $it->limit($pageSize), false );    

                $elapsed = microtime(true) - $start;

                $response = array(
                    "elapsed" => $elapsed,
                    "total" => $spotsInSphere,
                    "items" => $spots
                );

                echo json_encode( $response, JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK );
                exit(1);

            }

            if( isset( $_POST['query']) ) {

                $c = new EMongoCriteria;
                $c->addCondition('name', new MongoRegex('/(' . $_POST['query'] . ')/i') ); 
                $c->setLimit($pageSize)->setSort( array('name' => 'asc'))->setSkip($offset);           
                $search = iterator_to_array( Spot::model()->findAll($c), false);

                if( !$search ) {
                    $c = new EMongoCriteria;
                    $c->addCondition('name', new MongoRegex('/.*' . $_POST['query'] . '.*/i') ); 
                    $c->setLimit($pageSize)->setSort( array('name' => 'asc'))->setSkip($offset);           
                    $search = iterator_to_array( Spot::model()->findAll($c), false);
                }

                echo json_encode( $this->setPaginatedOutput( $search, count($search), $offset) );
            }

            

        } catch( Exception $e){ 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }  

    /**
     *  Receives a csv file with spot information to be added to the db
     *  The format of the csv file is fixed columns:
     * 
     *  continent,country,countryCode,area_level_1,area,name,lat,lng,[unused_fields, ...]
     * 
     *  @version 1.3.3
     */
    public function actionUpload(){
        try {

            $oauth = YiiOAuth2::instance();
            $token = $oauth->verifyToken();
            
            if( !$_FILES )
                throw new Exception(NO_IMAGE, 'No image found');

            $added = 0;
            $file = $_FILES['spotsfile'];
            $saveDir = '/tmp/';
            $fname = sha1( time() . substr($file['name'], 0, -4) );  
            $ext = 'csv';

            if (!move_uploaded_file(
                    $file['tmp_name'],
                    sprintf('%s%s.%s',
                        $saveDir,
                        $fname,
                        $ext
                    )
                )) //{
                    throw new RuntimeException('Failed to move uploaded file.');

            $filename = $saveDir . $fname . "." . $ext;
            $fd = fopen($filename, "r");
            if ($fd)
                while (($line = fgets($fd)) !== false) {
                    $lineArr = explode(",", $line);
                    
                    if( !in_array("continent", $lineArr) ) {

                        $spot = new Spot();

                        //print_r( $lineArr ); 

                        $obj['continent'] = isset($lineArr[0]) ? $lineArr[0] : "" ;
                        $obj['country'] = isset($lineArr[1]) ? $lineArr[1] : "" ;
                        $obj['countryCode'] = isset($lineArr[2]) ? $lineArr[2] : "" ;
                        $obj['area_level_1'] = isset($lineArr[3]) ? $lineArr[3] : "" ;
                        $obj['area'] = isset($lineArr[4]) ? $lineArr[4] : "" ;
                        $obj['name'] = isset($lineArr[5]) ? $lineArr[5] : "" ;
                        $obj['lat'] = isset($lineArr[6]) ? $lineArr[6] : "" ;
                        $obj['lng'] = isset($lineArr[7]) ? $lineArr[7] : "" ;
                        try { 
                            $spot->validateParams( $obj );

                            $spot->modified = 0;
                            $spot->created = time();

                            if( $spot->save() ) {
                                //echo json_encode( $spot ) . "\n";
                                $added++;
                            }
                        } catch( Exception $e ) { }
                    }
                }
            else
                throw new RuntimeException('No file descriptor available.');

            unlink($filename);
            echo json_encode( array(

                "status" => "ok",
                "message" => "Successfully added {$added} spots."
            
            ), JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES );
            
        } catch (Exception $e){
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }

    /**
     * Parses the changes within the spots db
     * Retuns paginated spot objects which were added or updated since the timestamp received in post params
     * Accepts pagination, max 100 items
     * 
     */
    public function actionChanges(){ 
        try {
            $this->setJsonHeaders();  

            $min_created = 1412950381;
            $min_modified = 1412950381;

            if( !isset( $_POST['timestamp']) || $_POST['timestamp'] > time() || $_POST['timestamp'] < $min_created )
                throw new OAuth2Exception( array('code' => BAD_POST_DATA, 'message' => "Not a valid timestamp") );  

            $ts = intval( $_POST['timestamp'] );        
            
            $offset = 0;
            $pageSize = 100;
            
            // Check if the actual post contains extra parameters
            $params = $this->validatePagination();
            if( $params ) {
                $offset = $params['offset'];
                $pageSize = $params['pageSize'];
            }

            $c = new EMongoCriteria;
            $c->compare('created', '>' . $ts ); 
            $c->setLimit($pageSize)->setSort( array('created' => 'desc'))->setSkip($offset);

            // Get last added spot
            $spots = $this->findAll( 'Spot', $c);


            $IOS_1_6 = false;
            foreach (getallheaders() as $key => $value) {
                if( $key == 'AppVersion' )
                    $IOS_1_6 = true;
            }

            /**
             * Hack to avoid errors in IOS versions lower than 1.6
             *
             * Will be removed when we don't support IOS < 1.6
             */
            if($IOS_1_6) {

                $c = new EMongoCriteria;
                $c->compare('modified', '>' . $ts ); 
                $c->setLimit($pageSize)->setSort( array('created' => 'desc'))->setSkip($offset);

                if( !$spots )
                    $spots = array();

                // Get last added spot
                $modSpots = $this->findAll( 'Spot', $c);

                if( !$modSpots )
                    $modSpots = array();

                 echo json_encode( $this->setSpotUpdates( $spots, $modSpots, array(), count($spots), count($modSpots), 0 ) );

            } else {
                if( !$spots )
                    $spots = array();

                echo json_encode( $this->setSpotUpdates( $spots, array(), array(), count($spots), 0 , 0 ) );

            }
            

        } catch( Exception $e){ 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }  
    }

    /**
     * Counts the number of added and updated spots since the timestamp received in post params
     * Returns the number of added, updated or removed spots
     * No pagination here
     *
     * @since  1.0.7 
     */
    public function actionChangesInfo(){ 
        try {
            $this->setJsonHeaders();  

            $min_created = 1412950381;

            if( !isset( $_POST['timestamp']) || $_POST['timestamp'] > time() || $_POST['timestamp'] < $min_created )
                throw new OAuth2Exception( array('code' => BAD_POST_DATA, 'message' => "Not a valid timestamp") );      

            $ts = intval( $_POST['timestamp'] );
            
            $c = new EMongoCriteria;
            $c->compare('created', '>' . $ts );

            // Get last added spot
            $spots = $this->findAll( 'Spot', $c);


            $IOS_1_6 = false;
            foreach ( getallheaders() as $key => $value ) {
                if( $key == 'AppVersion' )
                    $IOS_1_6 = true;
            }

            /**
             *  Hack to avoid errors in IOS versions lower than 1.6
             *  Will be removed when we don't support IOS < 1.6
             */
            if( $IOS_1_6 ) {

                $c = new EMongoCriteria;
                $c->compare('modified', '>' . $ts );

                if( !$spots )
                    $spots = array();

                // Get last added spot
                $modSpots = $this->findAll( 'Spot', $c);

                if( !$modSpots )
                    $modSpots = array();

                 echo json_encode( $this->setSpotUpdatesInfo( count($spots), count($modSpots), 0 ) );

            } else {
                if( !$spots )
                    $spots = array();

                echo json_encode( $this->setSpotUpdatesInfo( count($spots), 0, 0 ) );

            }
            

        } catch( Exception $e){ 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }  
    }    

    /**
     * Given the spot id as $id
     * Returns information about the spot contents
     * This information is extensible
     *
     * GET http method
     * @since 1.0.7
     *
     */
    public function actionInfo($id){ 
        try {
            $this->setJsonHeaders();

            switch( $_SERVER['REQUEST_METHOD'] ) { 
                case 'GET': { 
                    if( !$id )
                        throw new OAuth2Exception( array('code' => BAD_SPOT_ID, 'message' => "Not spot id found") );
                    $spot = $this->findByPk('Spot', $id );
                    if( !$spot )
                        throw new OAuth2Exception( array('code' => BAD_SPOT_ID, 'message' => "Not a valid spot id") );

                    echo json_encode( 
                        array( 
                            "totalKiters" => $spot->totalKiters, 
                            "totalSessions" => $spot->totalSessions
                        ), JSON_NUMERIC_CHECK );

                }  break;
                default: {
                    throw new OAuth2Exception( array('code' => BAD_REST_METHOD, 'message' => "No actions for ".$_SERVER['REQUEST_METHOD']) );
                } 
            }

        } catch( Exception $e){ 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }  
    }

    /**
     * Parses the Spot db by chunks of $limit spots
     * It applies the location updates in the spots table
     * 
     */
    public function actionParse(){ 
        try {

            $pk = Yii::app()->controller->getModule("oauth2")->getUid();
            $user = $this->findByPk('Oauth2User', $pk );    

            if(!$user)
                throw new OAuth2Exception( array('code' => 401, 'message' => OAUTH2_ERROR_UNAUTHORIZED_CLIENT ) );

            $role = ( isset($user->role) && $user->role !== null ) ? $user->role : 'user';
            if( $role !== 'admin' ) {
                throw new OAuth2Exception( array('code' => 401, 'message' => OAUTH2_ERROR_INVALID_SCOPE) );
            }

            $start = microtime(true);
            $q = array();

            $offset=0;
            $limit = 100;
            $exit= false;
            $updated = 0;
            $doUpdate = true;
            
            while( ! $exit ) {
                $c = new EMongoCriteria();
                $c->setLimit( $limit )->setSkip($offset);

                $spots = iterator_to_array(  Spot::model()->findAll($c), false );
                $offset += count($spots);

                foreach ($spots as $key => $spot) {
                    //print_r($spot);
                    if($doUpdate && !isset( $spot->loc )) { 
                        $spot = (object) $spot;
                        //print_r($spot); die;
                        $lat = $spot->lat;
                        $lng = $spot->lng;

                        $spot->loc = array ( 
                            "type" => "Point",
                            "coordinates" => array($lng, $lat)
                        );

                        if( $spot->update() ) {
                            echo "Spot {$spot->name} updated\n";
                            $updated++;
                        }
                        else
                            echo "Error updating {$spot->name}\n";
                        
                    } else {
                        echo "Spot {$spot->name} already has a loc field\n";
                    }

                    ob_flush();
                    flush();
                }

                if( count($spots) < $limit )
                    $exit = true;
            }

            $elapsed = microtime(true) - $start;
            echo "Total Execution time: {$elapsed} seconds.\n";
            echo "Total Updated {$updated} spots.\n";


        } catch( Exception $e){ 
            $elapsed = microtime(true) - $start;
            echo "Total Execution time: {$elapsed} seconds.\n";
            // $this->sendExceptionInfo($e, $this->id, $this->action->id);
            echo $e->getMessage(); die;
        }  
    }

    /**
     *  Read local json file and update remote spots
     */
    public function actionFile2db(){
        try { 

            $start = microtime(true);
            $userAttr = array(
                'area',
                'area_level_1',
                'name',
                'country',
                'countryCode'
            );

            $pk = Yii::app()->controller->getModule("oauth2")->getUid();
            $user = $this->findByPk('Oauth2User', $pk );    

            if(!$user)
                throw new OAuth2Exception( array('code' => 401, 'message' => OAUTH2_ERROR_UNAUTHORIZED_CLIENT ) );

            $role = ( isset($user->role) && $user->role !== null ) ? $user->role : 'user';
            if( $role !== 'admin' ) {
                throw new OAuth2Exception( array('code' => 401, 'message' => OAUTH2_ERROR_INVALID_SCOPE) );
            }

            $filedir = Yii::app()->basePath .DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR;
            $file = 'spots_prod.json';
            $update = true;
            $equalspots = 0;
            $updated=0;
            $updatethis = false;

            $spots_not_in_prod = array();
            $spots_to_be_updated = array();

            $fd = fopen($filedir . $file, "r");
            if ($fd) {
                while (($line = fgets($fd)) !== false) {
                    $spotObj = json_decode( $line );
                    $id = (array) $spotObj->_id;

                    if( $spotObj ) { 
                        $prod_spot = Spot::model()->findByPk( $id['$id'] );
                        if( ! $prod_spot ) {
                            $spots_not_in_prod[] = $spotObj;
                        } else {                             
                            $attr = array();
                            foreach ( $prod_spot as $key => $value ) {
                                if(in_array($key, $userAttr) && ( $prod_spot->$key !== $spotObj->$key ) ) { 
                                    $attr[] = $key;
                                    if( $key == 'modified') {
                                        $prod_spot->$key = $this->safe_ts;
                                    } else { 
                                        $prod_spot->$key = $spotObj->$key;
                                    } 
                                    $updatethis = true;
                                } 
                            }
                            if( $update && $updatethis ) {
                                if( $prod_spot->update( $attr ) ) {
                                    echo "Spot {$prod_spot->name} updated\n";
                                    $updated++;
                                }
                                else
                                    echo "Error Updating {$prod_spot->name}\n";

                                $updatethis = false;

                            } else {
                                $equalspots++;
                            }
                        }
                    }
                }

            fclose($fd);
            $elapsed = microtime(true) - $start;

            echo "Execution time " . $elapsed . " seconds\n";
            echo "Updated spots {$updated}\n";
            echo "Equal (stable) spots {$equalspots}\n";

            } else {
                echo "Error opening spots file: {$file}\n";
                die;
            }

        } catch(Exception $e) {

            echo $e->getMessage(); die;

        }
    }




    public function actionDb2File(){
        try { 

            $start = microtime(true);
           
            $pk = Yii::app()->controller->getModule("oauth2")->getUid();
            $user = $this->findByPk('Oauth2User', $pk );    

            if(!$user)
                throw new OAuth2Exception( array('code' => 401, 'message' => OAUTH2_ERROR_UNAUTHORIZED_CLIENT ) );

            $role = ( isset($user->role) && $user->role !== null ) ? $user->role : 'user';
            if( $role !== 'admin' ) {
                throw new OAuth2Exception( array('code' => 401, 'message' => OAUTH2_ERROR_INVALID_SCOPE) );
            }

            $filedir = Yii::app()->basePath .DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR;

            $file = 'spots_devjohnny.json';
            $update = true;
            $equalspots = 0;
            $updated=0;
            $updatethis = false;

            $offset=0;
            $limit = 100;
            $exit= false;

            $updated = 0;
            $doUpdate = true;
            $fd = fopen($filedir . $file, "w");
            
            while( ! $exit ) {
                $c = new EMongoCriteria();
                $c->setLimit( $limit )->setSkip($offset);

                $spots = iterator_to_array( Spot::model()->findAll($c), false );
                $offset += count($spots);

                if( count($spots) < $limit )
                    $exit = true;

                    foreach ($spots as $key => $spot) { 

                        unset($spot->created);
                        unset($spot->modified);
                        unset($spot->totalSessions);
                        unset($spot->_kiters);
                        unset($spot->totalKiters);
                        unset($spot->lastSessionTime);

                        $spotJson = json_encode($spot);
                        if( isset( $spotJson ) ) {
                            fwrite($fd, $spotJson.PHP_EOL );
                            $updated++;
                        }
                    }
                    
                echo "Written {$updated} Spots\n";
                ob_flush();
                flush();
            }

            fclose($fd);
            $elapsed = microtime(true) - $start;
            echo "Execution time " . $elapsed . " seconds\nWritten {$updated} Spots";

        } catch(Exception $e) {
            echo $e->getMessage(); 
            die;
        }
    }

}
?>
