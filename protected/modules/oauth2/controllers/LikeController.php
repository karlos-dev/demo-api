<?php
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . "..".DIRECTORY_SEPARATOR. "lib".DIRECTORY_SEPARATOR."OAuth2Exception.inc";

/**
 * Woo API LikeController
 * 
 * Library implementing all like related actions 
 * 
 * @author      Karlos Álvarez <karlos.alvan@gmail.com>
 * @uses        OAuth2Exception
 * @uses        MongoException
 * @uses        Exception
 * 
 */
class LikeController extends ComController {
    
    /**
     * Retrieves a list of elements with information about the users who liked the session belonging 
     * to the input id
     * 
     * @param String $id the session id
     */

    public function actionIndex( $id = null ) {
        try {

            $oauth = YiiOAuth2::instance();
            $token = $oauth->verifyToken();
            $this->setJsonHeaders();

            if( $id == null ){
                echo json_encode( $this->setPaginatedOutput( array(), 0, 0) );
                Yii::app()->end();
            }

            $c = new EMongoCriteria;            
            $c->addCondition('_session', new MongoId($id) );
            
            $offset = 0;
            $pageSize = 20;
            
            // Check if the actual post contains extra parameters
            $params = $this->validatePagination();
            if( $params ) {
                $offset = $params['offset'];
                $pageSize = $params['pageSize'];
            }

            $c->setLimit($pageSize)->setSort( array('created' => 'desc') )->setSkip($offset);
            $likes = iterator_to_array( Like::model()->findAll($c), false);

            $userIds = array();
            $feed = array();

            foreach ($likes as $like) {
                $userIds[] = $like->_user;
            }

            $users = Oauth2User::model()->findAllByPk( $userIds );
            foreach ($likes as $lk ) {
                $usr = $this->findInArray( $lk->_user, $users, '_id' );
                if ( $usr ) {
                    $obj = $lk->attributes;
                    unset( $obj['_session'] );
                    unset( $obj['_id'] );
                    $obj['user_name'] = $usr->details['name'];
                    $obj['user_lastname'] = $usr->details['lastname'];
                    $obj['user_pictures'] = $usr->details['_pictures'];
                    $feed[] = $obj;
                }
            }
            
            echo json_encode( $this->setPaginatedOutput( $feed, count($feed), $offset) );
            
        } catch( Exception $e ) {
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    } 
    
    private function removeFromArray( $array, $elem ){
        if( $array && $elem ) {
            $reducedArray = array();
            foreach ($array as $value) {
                if( $value !== $elem ) 
                    $reducedArray[] = $value;
            }
            return $reducedArray;
        } return null;
    }
    
}

?>
