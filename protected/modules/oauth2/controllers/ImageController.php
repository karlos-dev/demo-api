<?php

/**
 * Kinematiq API
 * ImageController
 * 
 * @author      Karlos Álvarez <karlos.alvan@gmail.com>
 * @abstract    Library implementing all Images functionallity
 *              A great Controller indeed
 * 
 * @uses        OAuth2Exception
 * @uses        MongoException
 * @uses        Exception
 * 
 */


require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . "..".DIRECTORY_SEPARATOR. "lib".DIRECTORY_SEPARATOR."OAuth2Exception.inc";


class ImageController extends RedisController {

    public function filters() {
        return array(
            'accessControl',
        );
    }


    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     *
     *  ORDER Deny, Allow
     * 
     * @return array access control rules
     */
    public function accessRules() {

        return array(
            
            
            array('deny', // deny all users
                'actions' => array('create','delete','index'),
                'users' => array('*'),
            ),

            array('allow', // allow all users to perform 'index' and 'view' actions
                'users' => array('*'),
                //'expression' => '',
            )

        );
    }
    
    /**
     * Creates a Image
     * It receives image information from $_POST params and stores them upon validation inside db
     * 
     * @throws OAuth2Exception
     * @throws MongoException
     */
    public function actionCreate(){
        try {
            
            //  check post data 
            //  check name and geolocation for validation
        
        } catch (Exception $e) {
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }
    
    /**
     * Creates a mongodb document for the uploaded Image and consistently 
     * and updates the related Documents (@link Session or @link Oauth2User) affected by the image insertion.
     * 
     * @param type $data The actuial information about the user
     * @return mixed Image|boolean  The image document if successfully saved or FALSE in any other case
     */
    private function packImage( $data ){
        $img = new Image();
        $img->filename = $data['filename'];
        $img->url = $data['uri'];
        $img->size = $data['size'];
        $img->type = $data['type'];
        $img->mimetype = $data['mimetype'];
        $img->_user = $data['uid'];
        //echo $data['uid']; die;
        $img->_session = isset($data['sid']) ? $data['sid'] : null;
        $img->created = time();
        if ($img->save()) {
            switch ($img->type){
                case 'cover': {
                    $c = new EMongoCriteria;
                    $c->addCondition( 'hash' , $data['uid'] );
                    $user = Oauth2User::model()->findOne($c);
                    if( !$user )
                        throw new OAuth2Exception( array('code' => BAD_USER_IDENT, 'message' => "No user found for this action") );
                    $user->addImage( $img, $img->type );
                    
                    return $img;
                } break;
                case 'user': {
                    $c = new EMongoCriteria;
                    $c->addCondition( 'hash' , $data['uid'] );
                    $user = Oauth2User::model()->findOne($c);
                    if( !$user )
                        throw new OAuth2Exception( array('code' => BAD_USER_IDENT, 'message' => "No user found for this action") );
                    $user->addImage( $img, $img->type );
                    
                    return $img;
                } break;
                case 'challenge': {
                    //$c = new EMongoCriteria;
                    //$c->_id = new MongoId( $data['sid'] );
                    if( !$img->_session )
                        throw new Exception('Failed saving challenge image with no session id');

                    $session = Challenge::model()->findByPk( new MongoId( $img->_session ) );
                    if( !$session )
                        throw new OAuth2Exception( array('code' => BAD_SESSION_ID, 'message' => "No session found for this action") );
                    
                    // FIX mongodbsuite BUG
                    $session->setIsNewRecord(false);
                    
                    $session->addImage( $img );
                    return $img;
                } break;
                case 'session': {
                    //$c = new EMongoCriteria;
                    //$c->_id = new MongoId( $data['sid'] );
                    if( !$img->_session )
                        throw new Exception('Failed saving  image with no session id');

                    $session = Session::model()->findByPk( new MongoId( $img->_session ) );
                    if( !$session )
                        throw new OAuth2Exception( array('code' => BAD_SESSION_ID, 'message' => "No session found for this action") );

                    /** 
                     * @abstract 
                     *     We need to update the redis element in activity
                     */
                    $oldSession = clone $session;

                    // FIX mongodbsuite BUG
                    $session->setIsNewRecord(false);
                    if( $session->addImage( $img ) ) {
                        if( $this->remove( "activity", $oldSession ) ) {
                            $this->add( "activity", $session );
                        }
                    }
                    
                    return $img;
                } break;
                case 'fbshare': {
                    /**
                     * In case its fbshare, the image just needs to be linked to img collection, not to the session document
                     * Session document holds image data for session section, not for 3rd party apps like FB
                     */
                    if( !$img->_session )
                        throw new Exception('Failed saving session image with no session id');

                    return $img;
                } break;
            }
        } else return FALSE;
    }
    
    /**
     * Uploads an image into filesystem, validates it, creates the mongodb ODM to handle its information and finally 
     * 
     */
    public function actionUpload(){
        try {

            $id = Yii::app()->controller->getModule("oauth2")->getUid();
            $user = $this->findByPk('Oauth2User', $id );
            
            if( !$user )
                throw new OAuth2Exception( array('code' => UNKNOWN_OAUTH_TOKEN, 'message' => "Cannot find user for this token.") );
                      
            if( !$_FILES )
                throw new Exception(NO_IMAGE, 'No image found');
            
            /**
             * @var $imageType  Holds the AWS bucket folder name where the img will be saved
             *                  Valid folders are user, session, cover or share
             *                  user has user profile images
             *                  session has session images
             *                  cover has user cover images
             *                  fbshare contains rendered images for FB session shares
             */
            
            $imageType = filter_var($_POST['type'], FILTER_VALIDATE_REGEXP, array( "options"=> array("regexp"=>"/(user)|(cover)|(session)|(fbshare)|(challenge)/")) ) ;
            if( !$imageType )
                throw new Oauth2Exception( array('code' => UNKNOWN_IMAGE_TYPE, 'message' => "No valid type field found. Review the type attribute."));

            $savedImage = $this->generateImageFromFile($_FILES['imgUpload'],  $imageType, $user );
           
            $this->sendImageCreated('Image successfully saved', $savedImage->url);  
            
        } catch (Exception $e){
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }


    public function generateImageFromFile( $file, $type, $user ) {

        $fileUploaded = $this->parseUploadedFile( $file ); 
        if( !$fileUploaded ) 
            throw new Exception('Invalid file format.');

        $data = $file;

        if( $user->hash )
            $userHash = $user->hash;
        else {
            $userHash = sha1( time() . $user->email );
            $user->hash = $userHash;
            $user->update();
        }

        if( isset($_POST['id'] ) && $_POST['id'] !== '' ) {
            //$session = Session::model()->findByPk( new MongoId( $_POST['id'] ) );
            //$sessionHash = $this->createFolderSessionName( $session );
            $data['sid'] = new MongoId($_POST['id']);
        }

        // Here we save the image into S3  
        $sc = new StorageController(0);
        // Create the bucket file path
        $awsUri = $type . '/' . $userHash . '/' . $fileUploaded;

        $data['uri'] = 'https://s3.amazonaws.com/' . $sc->bucketName . '/' . $awsUri;
        $data['filename'] = $fileUploaded;
        $data['type'] = $type;
        $data['mimetype'] = $file['type'];            
        $data['uid'] = $userHash;

        unset( $data['name'] );
        unset( $data['tmp_name'] );
                                
        // Saving Image Structure into db  
        $savedImage = $this->packImage($data);

        if ( !$savedImage )
            throw new Exception('Cannot save uploaded image');     

        if( !$sc->addImageToBucket( '/tmp/' . $fileUploaded, $awsUri ) )
            throw new Exception('ERROR saving into bucket' );

        unlink( '/tmp/' . $fileUploaded );   

        return $savedImage;
    }
    
    /**
     * Retrieves a list of Images for a recorded session or for the current user
     * 
     * @param Integer $id   Item id, the session id. If not provided, the images listed will belong to the user owning the access token
     * @throws OAuth2Exception
     */
    public function actionUrl( $id = null ){
        try {
            
            $oauth = YiiOAuth2::instance();
            $token = $oauth->verifyToken();
            $this->setJsonHeaders();

            if( $id ) {

                //$c = new EMongoCriteria;
                //$c->hash = $id;
                //$user = Oauth2User::model()->find( $c );
                
                $userHash = $id;
                
            } else {
                $user = $oauth->loadUser($token->oauth_token);

                if( !$user )
                    throw new Oauth2Exception( array('code' => BAD_USER_IDENT, 'message' => "Unknown user"));
                $userHash = $user->hash;
            }            
            
            if( !Yii::app()->request->getParam('_type') )
                throw new Oauth2Exception( array('code' => UNKNOWN_IMAGE_TYPE, 'message' => "No valid image type"));

            $type = filter_var(Yii::app()->request->getParam('_type'), FILTER_VALIDATE_REGEXP, array( "options"=> array("regexp"=>"/(user)|(cover)|(session)/")) );
            
            if( !Yii::app()->request->getParam('_id') && $type == 'session' )
                throw new Oauth2Exception( array('code' => UNKNOWN_IMAGE_TYPE, 'message' => "No valid image type"));
            else if( $type == 'session')
                $sessionId = Yii::app()->request->getParam('_id');

            $c = new EMongoCriteria;
            $c->addCondition('_user',$userHash);
            $c->addCondition('type',$type);

            if( isset( $sessionId ))
                $c->addCondition( '_session',new MongoId( $sessionId ) );

            $img = Image::model()->findOne( $c );
            if( !$img )
                throw new Oauth2Exception( array('code' => NO_IMAGE, 'message' => "No image"));                

            echo json_encode( array( "url" => $img->url ));

            
        } catch( Exception $e ) {
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    } 
    
    /**
     * Deletes an image document from mongodb instance
     * 
     * @param String $id    String of the MongoId associated to current image
     * @throws Exception
     */
    public function actionDelete( $id ){
        try {             
            $oauth = YiiOAuth2::instance();
            $token = $oauth->verifyToken();
            $user = $oauth->loadUser($token->oauth_token);

            $criteria = new EMongoCriteria;
            $criteria->addCondition(  '_id', new MongoId( $id ) );
            $criteria->addCondition(  '_user', $user->_id );

            $this->setJsonHeaders();            
            $result = Image::model()->find($criteria);
            
            if(!$result)
                throw new Exception("The requested image does not exist.");
            
            $where = strtoupper($result['type']);
            $pathId =  ( $result['type'] == 'user' ) ? (string)$result->_user : (string)$result->_session;
            $absfilename = substr(Yii::app()->basePath, 0, -3) . Yii::app()->params["local_{$where}_IMG_DIR"] . $pathId . DIRECTORY_SEPARATOR . $result['filename'];
            
            if( is_file( $absfilename )) 
                unlink($absfilename);
                
            if( Image::model()->deleteAll($criteria) )
                $res = array( 'status'=>'ok',   'message'=>'Image succesfully deleted' );
            else
                $res = array( 'status'=>'error','message'=>'Unknown error while deleting image' );
            
            echo json_encode($res);
            
        } catch(Exception $e){
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }


    /**
     * Validates the uploaded file as a valid image with valid MIME type
     * 
     * @param  $_FILE $file The uploaded file structure  
     * @return mixed String The temp filename if the file is valid and it's been sucessfully uploaded to temp folder, boolean False in any other case.
     */
    private function parseUploadedFile( $file ){
        try {
            if ( isset( $file ) ) {
                switch ( $file['error'] ) {
                    case UPLOAD_ERR_OK:
                        break;
                    case UPLOAD_ERR_NO_FILE:
                        throw new RuntimeException('No file sent.');
                    case UPLOAD_ERR_INI_SIZE:
                    case UPLOAD_ERR_FORM_SIZE:
                        throw new RuntimeException('Exceeded filesize limit.');
                    default:
                        throw new RuntimeException('Unknown errors.');
                }

                if ($file['size'] > 5000000) {
                    throw new RuntimeException('Exceeded filesize limit.');
                }
                
                //echo exif_imagetype($_FILES['tmp_name']); die;
                //$finfo = new finfo(FILEINFO_MIME_TYPE, null);
                //echo "HELLO"; 
                //echo $finfo->file($_FILES['tmp_name']);  die;
                //echo is_file( $_FILES['tmp_name'] ) ? " is file " : " no is file ";
                
                $validmime = array( 'jpg' => 'image/jpeg', 'png' => 'image/png', 'gif' => 'image/gif');
                $ext = array_search($file['type'], $validmime , true);
                if(!$ext)
                    throw new RuntimeException('Invalid file format.');

                $saveDir = '/tmp/';
                $fname = sha1( time() . substr($file['name'], 0, -4) );                
                
                if (!move_uploaded_file(
                    $file['tmp_name'],
                    sprintf('%s%s.%s',
                        $saveDir,
                        //sha1( substr($_FILES['name'], 0, -4) ),
                        $fname,
                        $ext
                    )
                )) //{
                    throw new RuntimeException('Failed to move uploaded file.');

                return sha1( time() . substr($file['name'], 0, -4) ) . ".{$ext}";
            }
            return false;

        } catch( RuntimeException $e ) {
            // $this->sendExceptionInfo($e, $this->id, $this->action->id);
            return false;
        }
    }

    /**
     * Generates a valid and consistent folder name for session related data
     * @param  Session $session The session ODM data from db
     * @return String The folder name based oin hashed user data
     *
     * @deprecated since 
     */
    private function createFolderSessionName( $session ) {
        return md5( $session->name );
    }

    /**
     * Generates a valid and consistent folder name for session related data
     * @param  Session $session The session ODM data from db
     * @return String The folder name based oin hashed user data
     *
     * @deprecated since 
     */
    private function createFolderChallengeName( $challenge ) {
        return md5( $challenge->name . $challenge->created );
    }

    /**
     * Generates a valid and consisten folder name for user related data
     * @param  oauth2User $user The oauth2user ODM data from db
     * @return String The folder name based oin hashed user data
     *
     * @deprecated since 
     */
    private function createFolderUserName( $user ) {
        return md5( $user->email );

    }

    
}
?>
