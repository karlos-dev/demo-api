<?php

/**
 * Kinematiq API
 * RankController
 * 
 * Library implementing ranking functionallity
 * 
 * @author      Karlos Álvarez <karlos.alvan@gmail.com>
 * @uses        OAuth2Exception
 * @uses        MongoException
 * @uses        Exception
 * 
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . "..".DIRECTORY_SEPARATOR. "lib".DIRECTORY_SEPARATOR."OAuth2Exception.inc";


class RankController extends KController {
     
    /**
     * Inserts a document into Ranking collection
     * It receives session information from $_POST params and stores them upon validation inside db
     *
     * @throws OAuth2Exception
     * @throws MongoException
     */

    public function Insert( $session, $user, $feature, $score ){
        try {
            
            $rank = new Rank();
            $feature =  ($feature == 'airtime' || $feature == 'maxAirtime' ) ? 'maxAirtime' : 
                        ($feature == 'height' || $feature == 'highestAir' ) ? 'highestAir' : 
                        ($feature == 'gforce' || $feature == 'maxGforce' ) ? 'maxGforce' : 'maxAirtime';

            $rank->score = $score;
            $rank->feature = $feature;
            $rank->_session = $session->_id;
            $rank->_user = $user->_id;
            $rank->_spot = $session->_spot;
            $rank->created = time();

            if(!$rank->save())
                throw new Oauth2Exception(array('code' => UNKNOWN_DB_ERROR, 'message' => "Error saving rank"));
                
            //  check post data 
            //  check name and geolocation for validation
        
        } catch (Exception $e) {
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }
    
    
    /**
     * Retrieves a list of Continents
     * 
     */

    public function actionIndex() {

        try {
            $oauth = YiiOAuth2::instance();
            $token = $oauth->verifyToken();
            $this->setJsonHeaders();
            
            // Check if the actual post contains extra parameters
            $params = $this->validatePagination();
            $offset = isset($params['offset']) ? $params['offset'] : 0;
            $pageSize = isset($params['pageSize']) ? $params['pageSize'] : 20;

            $c = new EMongoCriteria;
            $c->limit($pageSize)->sort('created', EMongoCriteria::SORT_DESC)->offset($offset);

            $continent = Continent::model()->findAll($c);

            echo json_encode( $this->setPaginatedOutput( $continent, count($continent), 0) );
            
        } catch( Exception $e ) {
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }
    
    
    public function actionDelete( $id ){
        try {
            $oauth = YiiOAuth2::instance();
            $token = $oauth->verifyToken();
            $this->setJsonHeaders();

        } catch( Exception $e ) {
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }
   
}
?>
