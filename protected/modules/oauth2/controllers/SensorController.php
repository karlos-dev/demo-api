<?php

/**
 * Kinematiq API
 * SensorController
 * 
 * Library implementing actions on Sensors
 * 
 * @author      Karlos Álvarez <karlos.alvan@gmail.com>
 * @uses        OAuth2Exception
 * @uses        MongoException
 * @uses        Exception
 * 
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "lib" . DIRECTORY_SEPARATOR . "OAuth2Exception.inc";


class SensorController extends KController {


    // need sensor wooid 
    // finds sensor, if not present creates new one (first time assignment)
    // assigns to current token owner
    
    public function actionAssign(){
        try {
            $oauth = YiiOAuth2::instance();
            $token = $oauth->verifyToken();
            $this->setJsonHeaders();

            
             $filters = array(
                    "fwv" =>    array("filter" => FILTER_VALIDATE_REGEXP, "options"=>array("regexp"=>"/[0-9]{1,4}\.[0-9]{1,4}\.[0-9]{1,4}/i")  ),
                    "wooid" =>  array("filter" => FILTER_VALIDATE_REGEXP, "options"=>array("regexp"=>"/^(woo)-[a-z0-9\s]{4}-[a-z0-9\s]{4}$/i") ),
                );

            $input = filter_input_array(INPUT_POST, $filters);


            if( !isset($input['wooid']) || empty($input['wooid']) || !isset($input['fwv']) || empty($input['fwv']) )
                throw new OAuth2Exception( array('code' => BAD_POST_DATA, 'message' => "No valid sensor data found") );

            $safeId = strtolower( $input['wooid'] );
            $c = new EMongoCriteria;
            $c->addCondition( 'wooid', $safeId );
            $sensor = Sensor::model()->findOne($c);

            if( !$sensor ) {
                $sensor = new Sensor();
                //$sensor->save();
                if (! $sensor->addNewWoo( array( 'wooid'=>$safeId,'fwv'=>$input['fwv'], 'owner'=>$token['user_id'] ), false ) )
                    throw new OAuth2Exception( array('code' => UNKNOWN_DB_ERROR, 'message' => "Cannot create sensor") );
            }

                $user= Oauth2User::model()->findByPk($token['user_id']);

                // sensor check within model
                $sensor->addToUser( $token['user_id'] );

                // then update the user
                $user->addSensor( (string) $sensor->_id );

            $this->sendOk("sensor assigned");

        } catch (Exception $e) {            
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }            
    }

    // need sensor wooid 
    // find sensor, if not present launches exception
    // unassigns from current token owner
    
    public function actionUnassign(){
        try {
            $oauth = YiiOAuth2::instance();
            $token = $oauth->verifyToken();
            $this->setJsonHeaders();

             $filters = array(
                    "wooid" =>  array("filter" => FILTER_VALIDATE_REGEXP, "options"=>array("regexp"=>"/^(woo)-[a-z0-9\s]{4}-[a-z0-9\s]{4}$/i") ),
                );

            $input = filter_input_array(INPUT_POST, $filters);

            if( ! isset($input['wooid']) )
                throw new OAuth2Exception( array('code' => BAD_POST_DATA, 'message' => "No valid wooid found") );

            $safeId = strtolower( $input['wooid'] );
            $c = new EMongoCriteria;
            $c->addCondition( 'wooid', $safeId );
            $sensor = Sensor::model()->findOne($c);

            if( !$sensor ){
                throw new OAuth2Exception( array('code' => HACKING_ACTIVITY, 'message' => "Not a sensor") );
                    
            } else {
                $user= Oauth2User::model()->findByPk($token['user_id']);
                $sensor->removeFromUser( $token['user_id'] );

                //the user the last thing
                $user->removeSensor( (string) $sensor->_id );
            }
            
            $this->sendOk("sensor unassigned");
        } catch (Exception $e) {            
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        } 

    }    
    
    /**
     * Retrieves a list of registerd Devices 
     * @param String $id    The user id 
     * @return Mixed Object | Array If $id is specified will return the object belonging to that User
     * 
     */
    
    public function actionIndex($id=null){
        try {
            
            $oauth = YiiOAuth2::instance();
            $token = $oauth->verifyToken();
            $this->setJsonHeaders();
            
            // default session parameters
            $offset = 0;
            $pageSize = 20;
            
            // Check if the actual post contains extra parameters
            $params = $this->validatePagination();
            if( $params ) {
                $offset = $params['offset'];
                $pageSize = ( $params['pageSize'] > 100 ) ? 100 : $params['pageSize'];
            }
            
            $c = new EMongoCriteria;
            if( $id !== null)
               //$c->addCondition('_owner', new MongoId($id) );
               $c->addCondition('_owner', $id );
                
            // $c->setSelect( Sensor::data() );
            // $c->addCondition('wooid', '<>' . null);
            $c->setLimit($pageSize)->setSkip($offset);     

            $result = $this->findAll('Sensor', $c, Sensor::data() );
            //print_r( array_keys( $result[0]->attributes ) ); die;
            //$result = iterator_to_array( Spot::model()->findAll($c), false );
            echo json_encode( $this->setPaginatedOutput( $result, count($result), $offset)  , JSON_NUMERIC_CHECK );
            
        } catch (Exception $e) { 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        } 
    } 

     /**
     * Receives a Json array of strings by POST data
     * Returns those ids which are not associated registered to any user
     * @return JSONObject 
     */
    public function actionReport(){
        try {
            $oauth = YiiOAuth2::instance();
            $token = $oauth->verifyToken();
            $this->setJsonHeaders();            

            if( !$_SERVER['REQUEST_METHOD'] == 'POST' || !isset( $_POST['set'] ))
                throw new OAuth2Exception( array('code' => BAD_POST_DATA, 'message' => "Cannot find valid deviceid set") );

            $ids = json_decode( $_POST['set'] );
            
            if( !isset($ids) || !is_array($ids)  )
                throw new OAuth2Exception( array('code' => BAD_POST_DATA, 'message' => "Not a valid json string") );   

            $loweredArray=array();
            foreach ($ids as $id)
                $loweredArray[]=strtolower($id);

            $ids = $loweredArray;
                 
            $offset = 0;
            $pageSize = 20;
            $params = $this->validatePagination();
            if( $params ) {
                $offset = $params['offset'];
                $pageSize = $params['pageSize'];
            }

            $result = array();
            $c = new EMongoCriteria;
            $c->compare( 'wooid', $ids );
            $c->addCondition('isRegistered', true);

            $sensors = Sensor::model()->findAll($c);

            foreach ($sensors as $s)
                if( ( $key=array_search( (string) $s->wooid, $ids) ) !== false  )
                    unset( $key );

            $free = array_filter( $ids );
            //print_r( $free ); die;

            echo json_encode( $this->setPaginatedOutput( $free, count($free), $offset) );

        } catch( Exception $e){ 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }

    /**
     * Adds a firware update to a sensor document
     * It receives the wooid and the new fw version by post params
     */
    public function actionFwUpdate(){
        try {
            $this->validateCompanionAction();
            $this->setJsonHeaders();  

            /**
             * Authentication
             * 
             * There's no user auth with companion.
             * API 1.0.7 requires special cid and csecret to it
             * We need to request a token based on user credentials to know who's updating OR
             * provide registration on download time.
             * 
             */            
            $compId="0000cocobaba1b22c33d4455";
            // $pk = Yii::app()->controller->getModule("oauth2")->getUid();

            $filters = array(
                "fwv" =>    array("filter" => FILTER_VALIDATE_REGEXP, "options"=>array("regexp"=>"/[0-9]{1,4}\.[0-9]{1,4}\.[0-9]{1,4}/i")  ),
                "wooid" =>  array("filter" => FILTER_VALIDATE_REGEXP, "options"=>array("regexp"=>"/^(woo)-[a-z0-9\s]{4}-[a-z0-9\s]{4}$/i") ),
            );

            $input = filter_input_array(INPUT_POST, $filters);

            if( !isset($input['wooid']) || empty($input['wooid']) || !isset($input['fwv']) || empty($input['fwv']) )
                throw new OAuth2Exception( array('code' => BAD_POST_DATA, 'message' => "No valid sensor data found") );

            $safeId = strtolower( $input['wooid'] );
            $c = new EMongoCriteria;
            $c->addCondition( 'wooid', $safeId );
            $sensor = Sensor::model()->findOne($c);

            if( !$sensor ) {
                $sensor = new Sensor();
                //$sensor->save();
                if (! $sensor->addNewWoo( array(
                    'wooid'=>$safeId,
                    'fwv'=>$input['fwv'],
                    'owner'=>$compId,
                ), true ) )
                    throw new OAuth2Exception( array('code' => UNKNOWN_DB_ERROR, 'message' => "Cannot create sensor") );
            } else {
                $sensor->updateFirmware( $compId , $input['fwv']);                
            }

            $this->sendOk("Firmware successfully updated");

        } catch( Exception $e){ 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }

    

    /**
     * Accepts requests POST and GET
     * GET needs the $id url param to be set thus the user may have many registered sensors
     * POST accepts an error object as param:
     */
    /*
    {
        watchdog : [
            { 
                memory-addr: string ,
                time: timestamp
            },
        ],
        board-level : { 
            uart : { 
                tx-overflow : integer,
                rx-buffer-full : integer,
                rx-overflow : integer,   
                uart-error : integer,   
                }, 
            ble : {
                    tx-overflow : integer,
                    rx-buffer-full : integer,
                    ble-stack : integer,   
                },
            other: {
                nvm: integer,
                spi: integer,
                bmp183: integer,
                mpu: integer,
                flash-memory: integer,
            }
        },
        rssi : { 
            intensity: integer,
            time: timestamp,        
        }, 

        self-test : { 
            gyros : boolean,
            accept: boolean,
            compass: boolean
        }   
    }        
    */
    public function actionErrors( $id=null ){ 
        try{
            $oauth = YiiOAuth2::instance();
            $token = $oauth->verifyToken();
            $this->setJsonHeaders();        
            $currentUser = $oauth->loadUser($token->oauth_token);

            switch( $_SERVER['REQUEST_METHOD'] ){ 
                case 'GET':
                        $filters = array(
                            "wooid" =>  array("filter" => FILTER_VALIDATE_REGEXP, "options"=>array("regexp"=>"/(woo)-[a-z0-9\s]{4}-[a-z0-9\s]{4}/i") ),
                        );

                        $input = filter_var($id, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/(woo)-[a-z0-9\s]{4}-[a-z0-9\s]{4}/i") ) );
                        
                        if( isset( $input ) ) {
                            $c = new EMongoCriteria();
                            $c->addCondition('wooid', $input);
                            $s = Sensor::model()->findOne($c);    

                        } else {
                            $s =  Sensor::model()->findBy_id($id);    
                        }
                        
                        if( !$s )
                            throw new OAuth2Exception( array('code' => BAD_POST_DATA, 'message' => "No sensor found for the id provided") );
                        $errors = iterator_to_array( SensorError::model()->findAllByPk( $s->errors ), false  );

                        echo json_encode( array(
                            "status"=>"ok",
                            "errors"=>$errors,
                        ));

                    break;
                case 'POST':
                    $filters = array(
                        //"errors" =>  array("filter" => FILTER_SANITIZE_STRING, ),  
                        "errors" =>  array(),  
                        "sensorId" =>  array("filter" => FILTER_VALIDATE_REGEXP, "options"=>array( "regexp"=>"/[a-z0-9\s]{24}/") ),
                    );
                    $input = filter_input_array(INPUT_POST, $filters);
                    if( empty($input['errors']) )
                        throw new OAuth2Exception( array('code' => BAD_POST_DATA, 'message' => "No valid error data found") );

                    if( empty($input['sensorId'])){
                        throw new OAuth2Exception( array('code' => BAD_POST_DATA, 'message' => "No valid sensor id found") );
                    }

                    $s =  Sensor::model()->findBy_id( $input['sensorId'] );
                    if( !$s )
                        throw new OAuth2Exception( array('code' => BAD_POST_DATA, 'message' => "No sensor found for the id provided") );

                    $errors = json_decode( $input['errors'] );
                    if( !$errors )
                        throw new OAuth2Exception( array('code' => BAD_POST_DATA, 'message' => "Not a valid json error definition") );

                    if( is_object($errors) ){

                        $se = new SensorError();
                        if( !$se->fill( $s, $errors ) )
                            throw new OAuth2Exception( array('code' => UNKNOWN_DB_ERROR, 'message' => "Couldn't save error data") );
                        $s->addError( (string) $se->_id );

                    } else if(is_array($errors)) {

                        foreach($errors as $err ) {
                            $se = new SensorError();
                            if( !$se->fill( $s, $err ) )
                                throw new OAuth2Exception( array('code' => UNKNOWN_DB_ERROR, 'message' => "Couldn't save error data") );
                            $s->addError( (string) $se->_id );
                        }
                    }                  

                    $this->sendOk("Error data succesfully stored");

            }
        } catch (Exception $e) {
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }
    
}
?>
