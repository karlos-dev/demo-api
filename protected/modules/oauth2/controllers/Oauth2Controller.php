<?php

class OAuth2Controller extends Controller {
    
    /**
     * Endpoint for access_token retrieval
     * This endpoint sends back JSON formated token upon an authorized user has been created
     */
    public function actionAccess_token() {
        $oauth = YiiOAuth2::instance();

        header("Content-Type: application/json");
        header("Cache-Control: no-store");

        echo json_encode( $oauth->grantAccessToken() );
    }
    
    public function actionAuthorize() {
        $oauth = YiiOAuth2::instance();
        
        if($_POST){            
            
            $oauth->setVariable("user_id", $_POST['user_id']);
            $oauth->finishClientAuthorization(TRUE, $_POST);
            
        } else if( $_GET ) {
            
            $auth_params = $oauth->getAuthorizeParams();
            $oauth->setVariable("user_id", $_GET['user_id']);
            
            echo $oauth->redirectToTokenEndpoint(TRUE, $auth_params);
            
        }
    }

}
?>
