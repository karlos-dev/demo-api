<?php

/**
 * Kinematiq API
 * ContinentController
 * 
 * @author      Karlos Álvarez <karlos.alvan@gmail.com>
 * @abstract    Library implementing Continent functionallity
 * @uses        OAuth2Exception
 * @uses        MongoException
 * @uses        Exception
 * 
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . "..".DIRECTORY_SEPARATOR. "lib".DIRECTORY_SEPARATOR."OAuth2Exception.inc";


class ContinentController extends RedisController {

    public function filters() {
        return array(
            'accessControl',
        );
    }

     /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {

        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'users' => array('*'),
                //'expression' => '',
            ),
            /*
            array('deny', // deny all users
                'actions' => array('create'),
                'users' => array('*'),
            )
            */
        );
    }

    /**
     *  Builds the Global Leaderboards
     *  Gets the lb from cache
     *  
     *  @since  1.4.1
     *
     *  @param string $feature  The Leaderboard topic
     *  @param string $target   The Leaderboard target user set
     *  @param string $user     Current token owner
     *  
     *  @version Added with Redis Cache feature
     */
    public function actionLeaderboard( $name ){

        //$start = microtime(true);
        //$debug = false;

        try {            
            $this->setJsonHeaders();

            $id = Yii::app()->controller->getModule("oauth2")->getUid();
            $user = $this->findByPk('Oauth2User', $id );
            if(!$user)
                throw new OAuth2Exception( array('code' => OAUTH2_ERROR_UNAUTHORIZED_CLIENT, 'message' => "Unauthorized token.") );
            /*
            if( $debug ) {
                $end = microtime(true) - $start;
                echo "$end seconds for authenticate user...\n";
                ob_flush();
                flush();
            }
            */

            $spotIterator = $this->getSpotList( $name );
            /*
            if( $debug ) {
                $end = microtime(true) - $start;
                echo "$end seconds for spot query...\n";
                ob_flush();
                flush();
            }
            */
            if( $spotIterator->count() == 0 ) {
                echo json_encode( $this->setLeaderboardFeed(array(), array("total"=>0, "position"=> 0 ), 0, 0), JSON_NUMERIC_CHECK );
                Yii::app()->end();
            }

            $filters = array(
                "target" =>     array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/(following)|(community)/") ),  
                "feature" =>    array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/(airtime)|(height)|(gforce)/") ),  
                "offset" =>     array("filter" => FILTER_REQUIRE_SCALAR),
                "pageSize" =>   array("filter" => FILTER_REQUIRE_SCALAR),                
            );
            
            $input = filter_input_array(INPUT_POST, $filters);  

            $target =   isset($input['target']) ? $input['target'] : 'following';
            $offset =   isset($input['offset']) ? $input['offset'] : 0;
            $pageSize = isset($input['pageSize']) ? $input['pageSize'] : 20;
            $feature = 'maxAirTime';
            
            if( isset($input['feature']) )
                switch ($input['feature']) {
                    case 'airtime':
                        $feature = 'maxAirTime';
                        break;
                    case 'height':
                        $feature = 'highestAir';
                        break;
                    case 'gforce':
                        $feature = 'maxGforce';
                        break;                    
                }

            /*
            if( $debug ) {
                $end = microtime(true) - $start;
                echo "$end seconds for spots ready...\n";
                ob_flush();
                flush();
            }
            */
           
            $ids = array();
            $totalUsers = array();
            $position = -1;

            $lb = $this->getLb( $feature );
            if($lb) { 

            } else {
                $lb = $this->load($feature, false);
            }

            // Range not working due to dupplicate user results
            // $loads = $lb->getRange($offset, $pageSize, -1);
            
            // Loads ALL the LB
            // Execution 2 s (in local)
            $loads = $lb->getRange(0, -1, -1);

            /*
            if( $debug ) {
                $end = microtime(true) - $start;
                echo "$end seconds for load lb ready...\n";
                ob_flush();
                flush();
            }
            */

            /*
            if( $debug ) {
                $end = microtime(true) - $start;
                echo "$end seconds for totalUsers ready...\n";
                ob_flush();
                flush();
            }
            */
            
            // Need to parse all the array to fill $totalUsers and properly set $position
            // Need to fill $totalusers because there are repeated user ids from local spot ranking
            // Execution 0.41 s
            
            $followedIds = array_merge( $user->_followed, array($id) );
            $complete = true;
            $spotArr = array();

            /**
             * @abstract
             *      Solving continents bug
             * 
             *  @since 1.4.4
             */
            
            if( $name !== 'all') {
                // Building the spot id array
                $complete=false;
                while( $spotIterator->hasNext() ) {
                    $spotArr[] = (string) $spotIterator->getNext()->_id;
                }
            }

            /**
             *  @abstract
             *     Divided code in terms of efficiency, not about beauty of code
             */
            
            if( $target == 'following') { 
                foreach( $loads as $key => $value ) {
                    
                    $obj = json_decode($key);
                    if( $obj ) {
                        // Filter target
                        if( in_array( $obj->_user->{'$id'}, $followedIds  ) ) {
                            // Filter place, always if complete
                            if( $complete || in_array( $obj->_spot->{'$id'}, $spotArr ) ) {

                                $totalUsers[ $obj->_user->{'$id'} ] = 1;
                                $ids[ $obj->_user->{'$id'} ][] = $obj->score;

                                // Set position on first occurence because it's sorted
                                if( $position == -1 && $obj->_user->{'$id'} == $id ) {
                                    $position = count( $totalUsers );
                                }
                            }
                        }
                    }
                }
            } else {
                foreach( $loads as $key => $value ) {
                    $obj = json_decode($key);
                    if( $obj ) {
                        // Filter continent, always if complete
                        if( $complete || in_array( $obj->_spot->{'$id'}, $spotArr  ) ) {

                            $totalUsers[ $obj->_user->{'$id'} ] = 1;
                            $ids[ $obj->_user->{'$id'} ][] = $obj->score;

                            // Set position on first occurence because it's sorted
                            if( $position == -1 && $obj->_user->{'$id'} == $id ) {
                                $position = count( $totalUsers );
                            }
                        }
                    }
                }
            }

            // Minimize mongodb transation to only needed content
            $fields = array( 
                "_id",
                "email", 
                "details.name", 
                "details.lastname", 
                "details._pictures"
            );

            // Limit the process with the part of the LB we want to process
            $short = array_slice($ids, $offset, $pageSize);

            // Fill the -sorted- $ids Array with user profile data
            $users = Oauth2User::model()->findAllByPk( array_keys($short), $fields );
            foreach ($users as $user) {
                if( isset( $short[ (string) $user->_id ] ) ) {

                    $place = $short[ (string) $user->_id ];
                    $score = $place[0];
                    $obj = array_filter( (array) $user )['details'];

                    $obj['_id'] =   $user->_id;
                    $obj['email'] = $user->email;
                    $obj['score'] = $score;

                    $short[ (string) $user->_id ] = (object) $obj;
                }
            }

            $feedArr = array_values($short);

            /*
            if( $debug ) {
                $end = microtime(true) - $start;
                echo "$end seconds for total task ready...\n";
                ob_flush();
                flush();
            }
            */

            echo json_encode( $this->setLeaderboardFeed($feedArr, array("total"=>count($totalUsers), "position"=>$position ), count($feedArr), $offset), JSON_NUMERIC_CHECK );

        } catch (Exception $e) { 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        } 
    }

    /**
     * Retrieves activity from all the spots within the selected continent
     * 
     * @param  String $name The name of the continent
     */
    public function actionActivity( $name ){
        try {            
            //$oauth = YiiOAuth2::instance();
            //$token = $oauth->verifyToken();
            
            $id = Yii::app()->controller->getModule("oauth2")->getUid();
            $user = $this->findByPk('Oauth2User', $id );
            if(!$user)
                throw new OAuth2Exception( array('code' => OAUTH2_ERROR_UNAUTHORIZED_CLIENT, 'message' => "Unauthorized token.") );

            $role = $user->getRole();

            $this->setJsonHeaders();

            $spotIterator = $this->getSpotList( $name );

            if( $spotIterator->count() == 0 ) {
                echo json_encode( $this->setLeaderboardFeed(array(), array("total"=>0, "position"=> 0 ), 0, 0), JSON_NUMERIC_CHECK );
                Yii::app()->end();
            }

            $filters = array(
                "offset" =>     array("filter" => FILTER_REQUIRE_SCALAR),
                "pageSize" =>   array("filter" => FILTER_REQUIRE_SCALAR),                
            );
            
            $input = filter_input_array(INPUT_POST, $filters);  
            $offset = isset($input['offset']) ? $input['offset'] : 0;
            $pageSize = isset($input['pageSize']) ? $input['pageSize'] : 20;
            $spotIds = array();
            $continentKiters = array();
            $kitersMongoIds = array();

            while( $spotIterator->hasNext() ) {
                $spot = $spotIterator->getNext();
                // foreach ($spotlist as $spot) {
                $spotIds[] = $spot->_id;

                if( is_array( $spot->_kiters ) )
                    $continentKiters = array_merge($continentKiters, $spot->_kiters );
            }

            foreach ($continentKiters as $kiterId ) {
                $kitersMongoIds[] = new MongoId( $kiterId );
            }
            
            $users = Oauth2User::model()->findAllByPk( $kitersMongoIds );

            $c = new EMongoCriteria;
            $c->compare('_user', $kitersMongoIds);
            $c->compare('_spot', $spotIds);
            $c->setLimit($pageSize)->setSort( array('created' => 'desc') )->setSkip($offset);   

            $feed = array();
            $result = iterator_to_array( Session::model()->findAll($c), false);

            foreach ($result as $session ) {
                $jumps = $this->filter( $session->_airs, $role );
                $usr = $this->findInArray( $session->_user, $users, '_id' );

                if ( $usr ) {
                    //$jumps = $session->_airs;
                    $obj = $session->attributes;
                    $usr = $this->findInArray( $session->_user, $users, '_id' );
                    $details = (Object) $usr->details;
                    $obj['_airs'] = $jumps;
                    $obj['user_name'] = $details->name;
                    $obj['user_lastname'] = $details->lastname;
                    $obj['user_pictures'] = $details->_pictures;
                    $feed[] = $obj;
                }
            }
            echo json_encode( $this->setPaginatedOutput($feed, count($feed), $offset), JSON_NUMERIC_CHECK );

        } catch (Exception $e) { 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        } 
    }
    
    /**
     * Creates a Continent
     * It receives session information from $_POST params and stores them upon validation inside db
     *
     * @deprecated
     * 
     * @todo Future development
     * @throws OAuth2Exception
     * @throws MongoException
     */

    public function actionCreate(){
        try {
            
            //  check post data 
            //  check name and geolocation for validation
        
        } catch (Exception $e) {
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }
    
    /**
     * Retrieves the list of Continents
     * POST contains pagination and filter info
     * 
     * @param spots 
     *        Send spots info in continents feed
     *        values 0,1 
     *        default 0
     * @param kiters 
     *        send kiters info in continents feed
     *        values 0,1
     *        default 0
     * @param offset 
     *        The starting paginated item
     *        values 0,n 
     *        default 0 
     * @param pageSize 
     *        The feed size
     *        values 0,n 
     *        default 20
     */
    public function actionIndex() {
        try {
            $this->setJsonHeaders();
            
            // Check if the actual post contains extra parameters
            $params = $this->validatePagination();
            $offset = isset($params['offset']) ? $params['offset'] : 0;
            $pageSize = isset($params['pageSize']) ? $params['pageSize'] : 20;

            $filters = array(
                "spots" =>  array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/[0-1]{1}/") ),  
                "kiters" => array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/[0-1]{1}/") ),  
            );

            $input = filter_input_array(INPUT_POST, $filters); 
            $input['spots'] = isset( $input['spots'] ) ? $input['spots'] : 0;
            $input['kiters'] = isset( $input['kiters'] ) ? $input['kiters'] : 0;

            $c = new EMongoCriteria;
            $c->setLimit($pageSize)->setSort( array('created' => 'desc') )->setSkip($offset);   

            $cursor = Continent::model()->findAll($c);
            $feed = array();
            foreach ($cursor as $continent) {
                $feed[] = $continent->filter($input);
            }

            echo json_encode( $this->setPaginatedOutput( $feed, count($feed), $offset) );
            
        } catch( Exception $e ) {
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }
    
    
    public function actionDelete( $id ){
        try {
            $oauth = YiiOAuth2::instance();
            $token = $oauth->verifyToken();
            $this->setJsonHeaders();

        } catch( Exception $e ) {
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }

    /**
     * @abstract Get all spots placed in any country regarding the continent which have at least one session
     * 
     *           0.12461996078491 seconds for authenticate user...
     *           3.5548989772797 seconds for spot query...
     *           3.5550129413605 seconds for spots ready...
     * 
     * @since 0.8.7
     * @version 3/09/2014
     * 
     * @abstract The previous version does not sorten the query by removing spots with no sessions on them
     *           Now it filters the spot collection
     *           
     *           0.103511095047 seconds for authenticate user...
     *           0.10524201393127 seconds for spot query...
     *           0.21976494789124 seconds for spots ready...
     *           
     * @since 1.4.1
     * 
     * @param String $cname The name of the continent to query about
     * @return iterator The filtered spot collection iterator
     *     
     */
    public function getSpotList( $cname ){
        $c = new EMongoCriteria;
        $c->compare( 'totalSessions', '>0' );

        if( $cname == 'all') {
            // $spots = iterator_to_array( Spot::model()->findAll(), false);
            $it = Spot::model()->findAll( $c );
            // echo "(".$it->count() ."/". $it->count(true).")\n"; die;

        } else {
            $trueName = str_replace( '-', ' ', $cname );
            $c->addCondition('continent', new MongoRegex('/(' . $trueName . ')/i') );
            // $spots = iterator_to_array( Spot::model()->findAll($c), false);
            
            $it = Spot::model()->findAll( $c );
        } 
        
        //if(!$spots)
            //throw new Exception( "No spots found in continent {$cname}" );
        //    return array();

        return $it;
    }

    /**
     * Retrieve the paginated list of spots hold in a given continent
     * @param  String $name The continent name with - instead of blank spaces
     * @return jsonObject Tha pagination info with a items array
     *
     * @since 0.8.11
     */
    public function actionSpots( $name=null ){
        try {
            $this->setJsonHeaders();
            
            // Check if the actual post contains extra parameters
            $params = $this->validatePagination();
            $offset = isset($params['offset']) ? $params['offset'] : 0;
            $pageSize = isset($params['pageSize']) ? $params['pageSize'] : 20;

            $c = new EMongoCriteria;
            $trueName = str_replace( '-', ' ', $name );

            //echo $trueName; die;

            $c->addCondition('continent', new MongoRegex('/(' . $trueName . ')/i') );
            $c->setLimit($pageSize)->setSort( array('name' => 'asc') )->setSkip($offset);   

            $spots = iterator_to_array( Spot::model()->findAll($c), false);

            echo json_encode( $this->setPaginatedOutput( $spots, count($spots), $offset) );
            
        } catch( Exception $e ) {
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }



    /**
     * Tests execution time and validates the endpoints for session controller
     * 
     * @return Array Report with every endpoint tested with total time, status and received response
     */
    
    public function actionTest(){
        try {

            $oauth = YiiOAuth2::instance();
            $token = $oauth->verifyToken();
            $user = $oauth->loadUser($token->oauth_token);
            $this->setJsonHeaders();

            $serverPath = $this->getServerPath();

            $spot = '53440727632095c30d8b456d';
            $session = '53860beb63209500168b4567'; // 53860c2e632095b2158b4567, 53860ca663209546148b4567
            $controller = 'continent';

            $sessionMethods = array( 
                array( 
                    'endpoint' => 'activity/all',
                    'method' => 'POST',
                    'params' => array( 
                            'offset'=>0,
                            'pageSize'=>100,),
                    ) , 
                array( 
                    'endpoint' => 'leaderboard/all',
                    'method' => 'POST',
                    'params' => array(
                            'target'=>'community',
                            'offset'=>0,
                            'pageSize'=>100,
                            'feature' => 'airtime',
                        ),
                    ) ,
                array( 
                    'endpoint' => 'leaderboard/europe',
                    'method' => 'POST',
                    'params' => array(
                            'target'=>'following',
                            'offset'=>0,
                            'pageSize'=>100,
                            'feature' => 'airtime',
                        ),
                    ) ,
                array( 
                    'endpoint' => 'leaderboard/north-america',
                    'method' => 'POST',
                    'params' => array(
                            'target'=>'me',
                            'offset'=>0,
                            'pageSize'=>100,
                            'feature' => 'airtime',
                        ),
                    ) ,
                 array( 
                    'endpoint' => 'leaderboard/africa',
                    'method' => 'POST',
                    'params' => array(
                            'target'=>'community',
                            'offset'=>0,
                            'pageSize'=>100,
                            'feature' => 'height',
                        ),
                    ) ,
                array( 
                    'endpoint' => 'leaderboard/oceania',
                    'method' => 'POST',
                    'params' => array(
                            'target'=>'following',
                            'offset'=>0,
                            'pageSize'=>100,
                            'feature' => 'height',
                        ),
                    ) ,
            );

            $methodArray = array();
            foreach( $sessionMethods as $method ){
                $obj['postFields'] = $method['params'];
                $obj['url'] = $serverPath . $controller . '/' . $method['endpoint'] . '?token=' . (string) $token->oauth_token;
                $obj['type'] = $method['method'];

                $urlArray[$obj['url']] = $obj; 
            }

            $results = curlMultiRequest($urlArray, null);
            echo $results ;

        } catch (Exception $e){
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }

   
}
?>
