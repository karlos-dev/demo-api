<?php



require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . "..".DIRECTORY_SEPARATOR. "lib".DIRECTORY_SEPARATOR."OAuth2Exception.inc";


define("REPORT", '["_id", "details.name", "details.lastname", "details._pictures"]');

/**
 *  Callback function used in search method
 *  @version 1.3.3
 */

function faux( &$in ){
    $in = array_filter( (array) $in );
}

/**
 *  WOO API AppuserController
 *  Implements the logic to represent the mobile app user data and every method working with its data
 * 
 *  - Updated Controller  
 *  - Removed token check from single actions  
 *  - Updated search method with projection data protection  
 * 
 * @author      Karlos Álvarez <karlos.alvan@gmail.com>
 */

class AppuserController extends SensorController {


    /**
     *  Checks the User profile stability according the fields stored from the Air object version
     *  There's a need of passing througt the application the information about current Air fields and unified acceptance of data.
     * 
     */
    public function actionHealth( $id=null ){
        
        $pk = Yii::app()->controller->getModule("oauth2")->getUid();
        $user = $this->findByPk('Oauth2User', $pk );
        $this->setJsonHeaders();
        
        if( !$user )
            throw new OAuth2Exception( array('code' => UNKNOWN_OAUTH_TOKEN, 'message' => "Cannot find user for this token.") );
        if( isset( $user->role ) &&  $user->role !== 'admin' )
            throw new OAuth2Exception( array('code' => UNKNOWN_OAUTH_TOKEN, 'message' => "You cannot do this") );

        $n=0;
        $fix=false;
        $start = round(microtime(true) * 1000);  

        if( isset( $_POST["fix"]) ) {
            if( $_POST["fix"] == true ){
                $fix=true;
            }
        }
        
        if( isset($id) ) {
            $users = iterator_to_array( Oauth2User::model()->findAllByPk( array($id) ), false );
            $fix=true;

        } else {
            $users = iterator_to_array( Oauth2User::model()->findAll(), false);
        }

        $info = ( $fix ) ? 'Enabled' : 'Disabled';
        $good = 0;
        $fail = 0;

        foreach ( $users as $user ) {
            $details = (object) $user->details;

                $newDetails = new UserDetails();
                foreach ( get_object_vars($details) as $key => $value ) {
                    $newDetails->$key = $value;
                }

            $details = $newDetails;

            $c= new EMongoCriteria();
            $c->addCondition('_user', $user->_id );
            $any = iterator_to_array(  Session::model()->findAll($c), false) ;
            $n = count($any);

                $maxHeight=0;
                $highestAir=0;
                $maxAirTime=0;

                $maxPop=0;
                $maxHpower=0;
                $maxCrashPower=0;
                $maxCrashVelocity=0;
                $maxrotations=0;
                $numberofjumnps = 0;
                $totalHeight = 0;
                $totalAirTime = 0;
                $timeInWater = 0;

                foreach ($any as $key => $session ) {
                    $maxHeight = ($maxHeight > $session->highestAir) ? $maxHeight : $session->highestAir;
                    $highestAir = ($highestAir > $session->highestAir) ? $highestAir : $session->highestAir;
                    $maxAirTime = ($maxAirTime > $session->maxAirTime) ? $maxAirTime : $session->maxAirTime;
                    
                    $maxPop = ($maxPop > $session->maxPop) ? $maxPop : $session->maxPop;
                    $maxHpower = ($maxHpower > $session->maxHpower) ? $maxHpower : $session->maxHpower;
                    $maxCrashPower = ($maxCrashPower > $session->maxCrashPower) ? $maxCrashPower : $session->maxCrashPower;                    
                    $maxCrashVelocity = ($maxCrashVelocity > $session->maxCrashVelocity) ? $maxCrashVelocity : $session->maxCrashVelocity;

                    $maxrotations = ($maxrotations > $session->rotations) ? $maxrotations : $session->rotations;

                    $numberofjumnps += $session->numberOfAirs;
                    $totalHeight += $session->totalHeight;
                    $totalAirTime += $session->totalAirTime;
                    $timeInWater += $session->duration;
                }

                if ( 
                    $details->highestAir == $highestAir &&  
                    $details->maxAirTime == $maxAirTime &&  
                    $details->maxPop == $maxPop &&  
                    $details->maxHpower == $maxHpower &&  
                    $details->maxCrashPower == $maxCrashPower &&  
                    $details->maxCrashVelocity == $maxCrashVelocity &&

                    $details->totalHeight == $totalHeight &&
                    $details->totalAirTime == $totalAirTime &&
                    $details->totalTimeInWater == $timeInWater &&

                    $details->numberOfSessions == $n

                ) { 
                    
                    echo "User ".$details->name." is consistent\n";
                    $good++;

                } else {

                    $details->maxHeight = $maxHeight;
                    $details->highestAir = $highestAir;
                    $details->maxAirTime = $maxAirTime;
                    $details->maxPop = $maxPop;
                    $details->maxHpower = $maxHpower;
                    $details->maxCrashPower = $maxCrashPower;
                    $details->maxCrashVelocity = $maxCrashVelocity;

                    $details->rotations = $maxrotations;
                    $details->numberOfAirs = $numberofjumnps;
                    $details->numberOfSessions = count($any);

                    $details->totalHeight = $totalHeight;
                    $details->totalAirTime = $totalAirTime;
                    $details->totalTimeInWater = $timeInWater;

                    $user->details = $details;

                    echo "User ".$details->name." has inconsistent profile\n";
                    $fail++;

                    if( $fix )
                        if( $user->update() ) {
                            echo "{$details->name} {$details->lastname} updated\n";
                        }
                        else {
                            echo "ERROR updating {$details->name} {$details->lastname}\n";
                        }
                    
                    ob_flush();
                    flush();
                }

            // echo json_encode($details);
            // ob_flush();
            // flush();
        }
        $end = round(microtime(true) * 1000);  

        echo "Completed:\nConsistent: {$good}\nUnconsistent: {$fail}\n";
        echo $end-$start . " seconds\n";
        // $this->sendOk();
    }
           
    /**
     * Updates the information about a concrete user profile
     * 
     * @version 0.8.8
     * 
     * @updated to MongoClient 
     * @updated language, email, fullname
     * 
     * @param mongoId $id   The user id of the profile being edited
     */
    public function actionUpdate(){
        try {
            $this->setJsonHeaders();

            $id = Yii::app()->controller->getModule("oauth2")->getUid();
            $user = $this->findbyPk( "Oauth2User", new MongoId($id) );

            if( !$user )
                throw new OAuth2Exception( array('code' => UNKNOWN_OAUTH_TOKEN, 'message' => "Cannot find user for this token.") );
            
            if( isset($_POST) && !empty($_POST) ) {  
                $lines = array_map('trim', file(__DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "data" . DIRECTORY_SEPARATOR . "badwords.txt") );              
                
                /**
                 *  Extending the UserDetails to the last UserDetails object
                 */
                
                $newDetails = new UserDetails();
                foreach ( get_object_vars( (object)$user->details) as $key => $value ) {
                    $newDetails->$key = $value;
                }
                $details = $newDetails;
                //$details = (object) $user->details;

                foreach ( array('name', 'lastname', 'gender', 'country', 'birthday', 'weight', '_homespot') as $value ) {                    
                    if( isset( $_POST[ $value ] ) ) {  
                        if( $value == 'name')
                            $this->checkbadwords($lines, $_POST['name'], BAD_WORDS_NAME_ERROR, "Are you sure your name is {$_POST['name']}?" );
                        else if( $value == 'lastname')
                            $this->checkbadwords($lines, $_POST['lastname'], BAD_WORDS_LASTNAME_ERROR, "Are you sure your lastname is {$_POST['lastname']}?" );
                        else if( $value == '_homespot') {
                            // Check id from spot
                            $spot = $this->findbyPk("Spot", $_POST['_homespot']);
                            if( !$spot )
                                throw new OAuth2Exception( array('code' => BAD_SPOT_ID, 'message' => "Not a valid spot" ));

                            $_POST[$value] = new MongoId($_POST['_homespot']);
                        }

                        $details->{$value} = $_POST[$value];
                    }
                }

                $details->fullname = strtolower( $details->name . "-" . $details->lastname ); 
                foreach ( array('language', 'email') as $value ) {  
                    if( isset( $_POST[ $value ] ) )
                        $user->{$value} = $_POST[ $value ];
                }

                $details->modified = time();
                $user->details = $details;
                
                if( !$user->update( ) )
                     throw new Exception("Cannot update user profile");

                // $this->sendOk("User successfully updated");
                
                echo json_encode( $user->compress() );
                
            }
        } catch( Exception $e){ 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);

        } catch( EMongoException $e){ 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }   

    /**
     * POST Modifies the role of the current user
     * GET returns the role of the current user
     * 
     * @version 1.0.6
     */
    public function actionScope($id=null){
        try {
            $this->setJsonHeaders();
            $error = null;

            if( $id !== null )
                $destuser = Oauth2User::model()->findByPk( new MongoId($id) );
            
            $id = Yii::app()->controller->getModule("oauth2")->getUid();
            $actionuser = $this->findbyPk( "Oauth2User", new MongoId($id) );

            //$actionuser = $oauth->loadUser($token->oauth_token);

            if( !$actionuser )
                throw new OAuth2Exception( array('code' => UNKNOWN_OAUTH_TOKEN, 'message' => "Cannot find user") );
            
            if( isset($_POST) && !empty($_POST) ) {  
                $filters = array( "scope" => array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/(admin)|(dev)|(tester)|(user)/i") ) );
                $input = filter_input_array(INPUT_POST, $filters );
                
                if( empty($input['scope']) )
                    throw new OAuth2Exception( array('code' => UNKNOWN_OAUTH_TOKEN, 'message' => "Not a valid scope found") );        
                if( isset( $actionuser->role ) && $actionuser->role !== 'admin' ) {
                    $error = OAUTH2_HTTP_UNAUTHORIZED;
                    throw new OAuth2Exception( array('code' => UNKNOWN_OAUTH_TOKEN, 'message' => "Not a valid scope found") );        
                }

                if( !isset($destuser) ){
                    $actionuser->role=$input['scope'];
                    $actionuser->update(array("role"));                    
                } else {
                    $destuser->role=$input['scope'];
                    $destuser->update(array("role"));
                    $actionuser=$destuser;
                }
            }

            echo json_encode( array(
                "status"=>"ok",
                "role"=> $actionuser->role
            ) ); 

        } catch( EMongoException $e){ 
            $this->sendExceptionInfo($e, $this->id, $this->action->id, $error);
        }
    }       

    /**
     * Provides reduced information about the stored user's document
     * @since 0.8.5
     * @updated to MongoClient 
     * 
     * @param $id mongoId $id
     * 
     */
    public function actionProfile($id=null) {
        try {
            // $oauth = YiiOAuth2::instance();
            // $token = $oauth->verifyToken();
            $this->setJsonHeaders();  
            $pk = Yii::app()->controller->getModule("oauth2")->getUid();          
            
            if( $id !== null ) {

                $result = $this->findbyPk( "Oauth2User", new MongoId($id) );
                if( !$result )    
                    throw new OAuth2Exception( array('code' => BAD_USER_ID, 'message' => "Cannot find user. Check user identifier.") );

                /**
                 *  @abstract
                 *  Some Apps do not implement the get my profile using no user $id
                 *  The API must check if the $id provided belongs to the current token owner
                 *
                 *  @version 1.3.3
                 */
                if( $id == $pk )
                    echo json_encode( $result->compress(), JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES);
                else
                    echo json_encode( $result->publish(), JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES );
                
            } else {
                
                $result = $this->findbyPk( "Oauth2User", new MongoId($pk) );

                if( !$result )    
                    throw new OAuth2Exception( array('code' => UNKNOWN_OAUTH_TOKEN, 'message' => "Cannot find user for this token.") );     

                echo json_encode( $result->compress(), JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES);
            }

        } catch( Exception $e){ 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }

    /**
     * Returns information array with user profiles 
     * Receives the content within the http request POST body: { set:["user_id_0", "user_id_1", "user_id_2"] }
     *
     * @overrides DeviceController Method
     * @since 0.8.5
     * @updated to MongoClient 
     * 
     */
    public function actionReport() {
        try {
            $this->setJsonHeaders();            

            if( !$_SERVER['REQUEST_METHOD'] == 'POST' || !isset( $_POST['set'] ))
                throw new OAuth2Exception( array('code' => BAD_POST_DATA, 'message' => "Cannot find valid user set") );

            $ids = json_decode( $_POST['set'], true );
            if( !$ids || !is_array($ids) )
                throw new OAuth2Exception( array('code' => BAD_POST_DATA, 'message' => "Not a valid json string") );     
                       
            $mongoids = array();
            $userprofiles = array();

            /**
             *  HACK
             *  @abstract
             *
             *  Oauth2User::$publicAttributes is always getting _followed and _followedBy from the db
             *  It should be removed and changed for: 
             *  Oauth2User::publicAttributes() 
             *  
             *  Which dynamically deal with data projection
             *
             *  @version 1.3.3
             */
            //$result = iterator_to_array( Oauth2User::model()->findAllByPk( $mongoids, Oauth2User::$publicAttributes ), false);
            $it = Oauth2User::model()->findAllByPk( $ids, Oauth2User::$publicAttributes );
            
            /*if( !$result ) {  
                echo json_encode( array() );
                Yii::app()->end();
            }*/
            
            $offset = 0;
            $pageSize = 100;
            
            // Check if the actual post contains extra parameters
            $params = $this->validatePagination();
            if( $params ) {
                $offset = $params['offset'];
                $pageSize = $params['pageSize'];
            }
            $parsed = array();
            $current = 0;
            while ($it->hasNext()) {
                if($current >= $offset) {
                    if( count($parsed) >= $pageSize) { 
                        break;
                    }
                    $value = $it->getNext();
                    $parsed[] = $value->compress();
                }
                $current+=1;
            }

            echo json_encode( $this->setPaginatedOutput( $parsed, count($parsed), $offset), JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES );

        } catch( Exception $e){ 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }

    /**
     *  @abstract
     *  
     *  T&C are requested to the API by APPs >1.8.1
     *  We do not perform authentication and send directly the T&C version
     *
     * @version 1.3.3
     */
    public function actionTc() {
        try {
            $this->setJsonHeaders(); 

            echo json_encode( array('version' => Yii::app()->params['tcversion'] ) );


        } catch( Exception $e){ 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }

    /**
     * Endpoint used to add or remove social accounts from the WOO user profile
     * There exists a relationship between the account name and the oauth2user object attribute, in order to work both account and user attribute must be
     * valid beforehand
     * 
     * @since 0.8.10
     * @updated remove token check
     *
     */
    public function actionAccount( $account = null){
        try{
            $this->setJsonHeaders();

            $id = Yii::app()->controller->getModule("oauth2")->getUid();
            $currentUser = $this->findbyPk( "Oauth2User", new MongoId($id) );

            switch( $_SERVER['REQUEST_METHOD'] ){ 
                case 'GET':
                    $input = filter_var( $account, FILTER_VALIDATE_REGEXP, array( 'options' => array("regexp"=>"/(facebook)|(twitter)|(play)/i") ) );
                    if( !$input )
                        throw new OAuth2Exception( array('code' => BAD_POST_DATA, 'message' => "Dont know what to do") );

                    $attr = constant( strtoupper($input) );
                    $id = $currentUser->{ $attr };
                    echo json_encode( array( 'account' =>  $id) );
                    break;

                case 'POST':
                    $filters = array( "account" => array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/(facebook)|(twitter)|(play)/") ) );
                    $input = filter_input_array(INPUT_POST, $filters);
                    if( !$input['account'] )
                        throw new OAuth2Exception( array('code' => BAD_POST_DATA, 'message' => "Dont know what to do") );

                    $accountAttr = constant( strtoupper( $input['account'] ) );

                    //print_r($currentUser); die;
                    //echo $accountAttr; die;
                    
                    if( !property_exists( 'Oauth2User', $accountAttr ) )
                        throw new OAuth2Exception( array('code' => UNCONFIGURED_SOCIAL, 'message' => "No support for {$input['account']} social") );

                    $filters = array( "action" => array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/(add)|(remove)/") ) );
                    $input = filter_input_array(INPUT_POST, $filters);
                    if( !$input['action'] )
                        throw new OAuth2Exception( array('code' => BAD_POST_DATA, 'message' => "Dont know what to do") );

                    switch ($input['action']) {
                        case 'add':
                            $accountId = isset($_POST['id']) ? $_POST['id'] : null;
                            if( !$accountId )
                                throw new OAuth2Exception( array('code' => BAD_POST_DATA, 'message' => "No account id found") );
                            $currentUser->{$accountAttr} = $accountId;
                            break;
                        case 'remove':
                            $currentUser->{$accountAttr} = null;
                            break;
                    }
                    $currentUser->update(array($accountAttr));
                    $this->sendOk("acount succesfully {$input['action']}ed");
            }
        } catch (Exception $e) {
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        } 
    }

    /**
     * Search for users by names
     * @since 0.8.8
     * @updated to MongoClient 
     * @removed token
     * 
     * @version 1.3.3
     *  - Removed extra data from search response (array filter)
     *  - Added projection for results object
     * 
     */
    public function actionSearch() {
        try {
            $this->setJsonHeaders();            

            if( !isset( $_POST['query']) )
                throw new OAuth2Exception( array('code' => BAD_POST_DATA, 'message' => "Not a search string") );     
                       
            $keywords = explode(' ', $_POST['query'] );  

            $myquery = implode('-', $keywords );
            

            $offset = 0;
            $pageSize = 20;
            
            // Check if the actual post contains extra parameters
            $params = $this->validatePagination();
            if( $params ) {
                $offset = $params['offset'];
                $pageSize = $params['pageSize'];
            }

            $projection = Oauth2User::publicAttributes();

            $c = new EMongoCriteria;
            $c->addCondition('details.fullname', new MongoRegex('/(' . $myquery . ')/i') ); 
            $c->setLimit($pageSize)->setSort( array('name' => 'asc'))->setSkip($offset);       
            $it = Oauth2User::model()->findAll($c, $projection);    

            $position = 0;
            if( $it->count() <= 0 ){
                foreach ($keywords as $key) {
                    if( $position == 0){
                        $c = new EMongoCriteria;
                        $c->addCondition('details.name', new MongoRegex('/(' . $key . ')/i') ); 
                        $c->setLimit($pageSize)->setSort( array('name'=>'asc'))->setSkip($offset);   

                        $it = Oauth2User::model()->findAll($c, $projection);   
                    }
                    if( $position == 1 && count($keywords) > 2 ){
                        $c = new EMongoCriteria;
                        $c->addCondition('details.name', new MongoRegex('/(' . $key . ')/i') ); 
                        $c->setLimit($pageSize)->setSort( array('name'=>'asc'))->setSkip($offset);   

                        if( $it->count() == 0 )
                            $it = Oauth2User::model()->findAll($c, $projection);
                        else {
                            $at = Oauth2User::model()->findAll($c, $projection);   
                            $arr = array_unique( 
                                    array_merge( 
                                        iterator_to_array( $it, false ), 
                                        iterator_to_array( $at, false ) 
                                    ),  
                                    SORT_REGULAR );
                            $it = new ArrayIterator( $arr );
                        }
                    } 

                    if( $position == 1 && count($keywords) <= 2  ){
                        $c = new EMongoCriteria;
                        $c->addCondition('details.lastname', new MongoRegex('/(' . $key . ')/i') ); 
                        $c->setLimit($pageSize)->setSort( array('lastname'=>'asc'))->setSkip($offset);  

                        if( $it->count() == 0 )
                            $it = Oauth2User::model()->findAll($c, $projection);
                        else {
                            $at = Oauth2User::model()->findAll($c, $projection);   
                            $arr = array_unique( 
                                    array_merge( 
                                        iterator_to_array( $it, false ), 
                                        iterator_to_array( $at, false ) 
                                    ),  
                                    SORT_REGULAR );
                            $it = new ArrayIterator( $arr );
                        }
                    } 

                    if( $position > 1 ){
                        $c = new EMongoCriteria;
                        $c->addCondition('details.lastname', new MongoRegex('/(' . $key . ')/i') ); 
                        $c->setLimit($pageSize)->setSort( array('name'=>'asc'))->setSkip($offset);  
                        if( $it->count() == 0 )
                            $it = Oauth2User::model()->findAll($c, $projection);
                        else {
                            $at = Oauth2User::model()->findAll($c, $projection);   
                            $arr = array_unique( 
                                    array_merge( 
                                        iterator_to_array( $it, false ), 
                                        iterator_to_array( $at, false ) 
                                    ),  
                                    SORT_REGULAR );
                            $it = new ArrayIterator( $arr );
                        }
                    }
                    $position++;
                }
            }

            $search = json_decode( json_encode( iterator_to_array( $it, false) ) );

            array_walk( $search, 'faux' );

            echo json_encode( $this->setPaginatedOutput( $search, count($search), $offset), JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES );

        } catch( Exception $e){ 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }


    private function mergeNoDuplicates($arr1, $arr2 ){
        if( count($arr1) > count($arr2) )
            $big = $arr1;

        $add2big = array();
        foreach ($small as $a) {          
            
            foreach ($big as $s) {
                if( $s->_id == $a->_id ) {         
                    $present = true;
                }
            }
            if( !$present ){
                $add2big[]=$a;
                $present=false;
            }
        }

        return array_merge($big, $add2big);
    }

    
    /**
     * Signs in an API consumer as a Kinematiq user
     * @uses $_POST urlencoded data
     */
    public function actionLogin() {

        try {
            if( isset($_POST) && !empty($_POST) ) {
                
                $signature = isset($_POST['sig']) ? $_POST['sig'] : '';
                $userEmail = isset($_POST['email']) ? $_POST['email'] : '';
                $userPass = isset($_POST['pass']) ? $_POST['pass'] : '';
                $deviceId = isset($_POST['deviceId']) ? $_POST['deviceId'] : null;
                
                $md5Pass= $userPass;
                $oauth = YiiOAuth2::instance();

                $authProcess = $oauth->checkUserCredentials( md5($userEmail), $userEmail, $md5Pass);
                $this->setJsonHeaders();

                /**
                 * Extended seq login
                 * @version 1.0.3
                 */
                $client_id = isset($_POST['client_id']) ? $_POST['client_id'] : '';
                $client_secret = isset($_POST['client_secret']) ? $_POST['client_secret'] : '';
                //print_r( $oauth->getVariable('user_id') ); die;
                $oauth->validateUser( $userEmail, $client_id, $client_secret );

                if( $authProcess == UserIdentity::ERROR_NONE ) {

                    $c = new EMongoCriteria;        
                    $c->addCondition( 'email', $userEmail ) ;
                    $data = Oauth2User::model()->findOne($c);

                    /**
                     *  @abstract
                     *  From 1.7.10 we allow users to use the app during some time.
                     *  After this time the user must activate their account using the sent email
                     *  
                     *  @version 1.3.2
                     */
                    
                    if( !$data->active ) 
                        if ( $data->trialEnd !== 0 && $data->trialEnd < time() ) {

                            /**
                             *  Invalidates the token the App might have stored.
                             *  Removes all tokens for the current user 
                             *  (Only once per user account)
                             * 
                             *  @version 1.3.3
                             */
                            $data->deleteAllTokens();
                            throw new OAuth2Exception( array('code' => UNCONFIRMED_ACCOUNT, 'message' => "This user account needs to be activated.") );
                        }

                    // Check deviceId modification
                    $detect = new Mobile_Detect;

                    if ( $detect->isMobile() || $detect->isTablet() )
                        if( !$deviceId  )
                            $data->enable_notifications= false;                        
                    
                    if( $data->userAgent !== $detect->getUserAgent() ){
                        $data->userAgent = $detect->getUserAgent();                        
                    }

                    if( $deviceId  )
                        $data->device_id = $deviceId;
                    else                        
                        throw new OAuth2Exception( array('code' => BAD_POST_DATA, 'message' => "No valid client data received.") );

                    $version = Yii::app()->controller->getModule("oauth2")->getClientVersion();
                    

                    $data->appVersion = $version;

                    /**
                     *  @abstract
                     *  Check each login if we have a new version of T&C
                     *  Check version in order to send or not the details
                     *
                     *  @version 1.3.3
                     */
                    $compatibility = Yii::app()->params['version_check'];

                    if( version_compare( $version, $compatibility ) >= 0 ) {
                        if( isset( $_POST['tc'] ) && version_compare($_POST['tc'], Yii::app()->params['tcversion']) >= 0 )
                            $data->acceptTC( $_POST['tc'] );

                        elseif( version_compare($data->tc_version, Yii::app()->params['tcversion']) < 0 ) {
                            $this->sendAcceptVersion("Need to accept new terms and conditions", Yii::app()->params['tcversion'], OUTDATED_TC  );
                            return;
                        } 
                    }

                    $c = new EMongoCriteria;
                    $c->addCondition( 'device_id', $deviceId );         
                    Oauth2User::model()->UpdateAll($c, array( '$set' => array( 'device_id' => "") ) );

                    $data->update();
                    $redirect_uri = isset($_POST['redirect_uri']) ? $_POST['redirect_uri'] : $data['redirect_uri'];
                    $response_type = isset($_POST['response_type']) ? $_POST['response_type'] : 'code';
                    
                    $_id = (string) $data->getPrimaryKey();
                    $oauth->setVariable("user_id", $data['client_id']);
                    
                    $authorizeUrl = '/authorize?client_id='.$data['client_id']."&redirect_uri=".$redirect_uri."&response_type=".$response_type."&user_id=".$_id;       
                    
                    $this->woopost($authorizeUrl, null, true, false );
                    
                } elseif( $authProcess == UserIdentity::ERROR_PASSWORD_INVALID ) {
                    throw new OAuth2Exception( array('code' => BAD_CREDENTIALS, 'message' => "User email or password is not valid") );
                } elseif( $authProcess == UserIdentity::ERROR_USERNAME_INVALID ) {
                    throw new OAuth2Exception( array('code' => BAD_CREDENTIALS, 'message' => "User email or password is not valid") );
                }
                
            } else { 
                throw new OAuth2Exception( array('code' => BAD_POST_DATA, 'message' => "No valid post data received.") );
            }
            
        } catch( Exception $e){ 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }
    
    /**
     * Registers a new user into Woo API
     * @throws OAuth2Exception
     */
    public function actionRegister() {
        try {
            if( isset($_POST) && !empty($_POST) ) {

                $signature = isset($_POST['sig']) ? $_POST['sig'] : '';
                $userEmail = trim( isset($_POST['email']) ? $_POST['email'] : '' );
                $userPass = isset($_POST['pass']) ? $_POST['pass'] : '';
                
                $client_id = isset($_POST['client_id']) ? $_POST['client_id'] : '';
                $redirect_uri = isset($_POST['redirect_uri']) ? $_POST['redirect_uri'] : '';       
                $client_secret = isset($_POST['client_secret']) ? $_POST['client_secret'] : '';
                $deviceId = isset($_POST['deviceId']) ? $_POST['deviceId'] : '';

                $profileImage = isset($_FILES['userImgUpload']) ? $_FILES['userImgUpload'] : null;
                $coverImage = isset($_FILES['coverImgUpload']) ? $_FILES['coverImgUpload'] : null;
                
                $this->setJsonHeaders();

                $data = $this->checkRegistrationData();
                $version = Yii::app()->controller->getModule("oauth2")->getClientVersion();
                
                if( !$data )
                    throw new OAuth2Exception( array('code' => INCOMPLETE_PROFILE_DATA, 'message' => "Invalid Post data, please double check the provided credentials") );
                
                $oauth = YiiOAuth2::instance();
                $oauth->validateUser( $userEmail, $client_id, $client_secret );

                $tcObj = null;

                if( version_compare( $version, Yii::app()->params['version_check'] ) >= 0 ) { 

                    if( !isset($_POST['tc']) )
                        throw new OAuth2Exception(array('code' => ACCEPT_TC, 'message' => "You must accept terms and conditions."));

                    elseif ( version_compare($_POST['tc'], Yii::app()->params['tcversion']) >= 0 ) { 
                        $tcObj = $_POST['tc'];
                    }
                }
                
                // Prepare validated params to be inserted in the db
                $response_type = 'code';

                /**
                 * @abstract The T&C are set within the addClient oauth function
                 */
                $user = $oauth->addClient($_POST, $tcObj);
                
                $oauth->setVariable("user_id", $user->_id );

                $ic = new ImageController(0);
                if($profileImage)
                    $ic->generateImageFromFile($_FILES['userImgUpload'], 'user' , $user );
                if($coverImage)
                    $ic->generateImageFromFile($_FILES['coverImgUpload'],  'cover', $user );
                
                if( version_compare( $version, Yii::app()->params['version_check'] ) >= 0 ) { 

                        /**
                         * @abstract
                         * @hack
                         * 
                         * Here we authenticate the user and generate authorization token
                         * It allows direct access to any registered user with no account confirmation
                         * The email is sent to be confirmed, but not confirmation needed till expiration of trial
                         *
                         * @version 1.3.2 
                         * Starting on IOS 1.8.1 we allow one week to use the app for 7 days
                         */
                        
                        $this->sendConfirmationMail( (Object) $user );
                        $authorizeUrl = '/authorize?client_id='.$data['client_id']."&redirect_uri=".$redirect_uri."&response_type=".$response_type."&user_id=".$user->_id;       
                        $this->woopost($authorizeUrl, null, true, false );

                } else {
                    $this->sendConfirmationMail( (Object) $user );
                    $this->sendOk("account confirmation email sent");
                }
            } else {
                throw new OAuth2Exception(array('code' => BAD_POST_DATA, 'message' => "No valid post data received."));
            }
                
        } catch( Exception $e ){ 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }

    /**
     * Recover passowrd
     * The application sends an email with a temporal reset link. This link is valid during 24 hours.
     * The user is redirected to the Woo API home page, where he/she can sedt the new password and password confirmation.
     */
    public function actionRecoverPassword() {
        try {

            $filter = array( "email" => array( "filter"=> FILTER_SANITIZE_EMAIL ) );
            $input = filter_input_array(INPUT_POST, $filter);
            
            if( !$input )
                throw new OAuth2Exception( array('code' => BAD_MAIL_ERR, 'message' => "Not a valid email address") );

            $c = new EMongoCriteria();            
            $c->addCondition( 'email', $input['email'] );
            $any = Oauth2User::model()->findOne($c);
            if(!$any)
                throw new OAuth2Exception( array('code' => BAD_MAIL_ERR, 'message' => "Cannot find user for that email") );

            $code = new PassCode();
            $code->code = md5( $input['email'] . rand(999,9999) );
            $code->user = $input['email'];
            $code->created = time();
            $code->expires = time() + 24*60*60;            
            
            if( !$code->save() )
                throw new Exception( "Could not send the recovery email" );
            $uimage='';
            $code = $code->attributes;
            //print_r($any->details['_pictures']); die;
            if($any->details['_pictures'])
            foreach ( $any->details['_pictures'] as $img) {
                if( $img['type'] == "user")
                    $uimage = $img['url'];
            }
            $code['user_image'] = $uimage;
            $code['user_name'] = $any->details['name'];
            $code = ( Object ) $code;

            $this->sendRecoverMail( $code );
            $this->sendOk("Email successfully sent");
            
        } catch (Exception $e) {
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }        
    }
    
    /**
     * Log out a client device from Woo Oauth2 system
     *
     * @since 0.8.7
     * @version  1.3.0
     *
     * @updated 1.0.6
     * @updated to MongoClient 
     * @updated with device Id reset on logout
     * 
     */
    public function actionLogout(){
        try { 
            
            $this->setJsonHeaders();
            $id = Yii::app()->controller->getModule("oauth2")->getUid();
                       
            $user = $this->findbyPk( "Oauth2User", new MongoId($id) );
            if( !$user )
                throw new OAuth2Exception( array('code' => UNKNOWN_OAUTH_TOKEN, 'message' => "Cannot find user for this token.") );

            // Remove all device ids matching the current user deviceId
            $c = new EMongoCriteria;
            $c->addCondition( 'device_id', $user->device_id );         
            Oauth2User::model()->UpdateAll($c, array( '$set' => array( 'device_id' => "") ) );


            if( $user->deleteAllTokens() ) { 
                $res = array("status"=>"ok", "message"=>"The user {$id} has been succesfully logged out.");
                echo json_encode($res, JSON_NUMERIC_CHECK);
            }
                
        } catch (Exception $e) {
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }            
    }    

    /**
     * Receives data and updates the notifications status for the current user
     *
     * POST { status: 'enable' | 'disable' }
     * 
     * @return http 200 on success 
     *         http 400 on request error
     *         http 401 on unauthorized action                               
     */
    public function actionNotifications($command=null){
        try {
            $this->setJsonHeaders();  
            $id = Yii::app()->controller->getModule("oauth2")->getUid();

            $status = ( isset($_POST['status']) &&  $_POST['status'] == 'enable') ?  true : false;   

            if( isset( $command ) && $command == 'readall' ){ 
                $c = new EMongoCriteria;
                $c->addCondition('_receiver', new MongoId( $id ) );


                $result = Yii::app()->mongodb->notification->update(
                    array( '_receiver' => new MongoId( $id ) ),
                    array('$set' => array( "read" => time() ) ),
                    array("multiple" => true)
                );//->count();

                if( $result )
                    $this->sendOk("{$result['n']} notifications read");
                else 
                    throw new OAuth2Exception( array('code' => UNKNOWN_DB_ERROR, 'message' => "Error updating notifications") );

            } else if( !isset($_POST['status'] ) && !isset($_POST['notificationId']) ){
                /**
                 * Retrieve notifications with pagination control
                 */
                $offset = 0;
                $pageSize = 20;

                // Check if the actual post contains extra parameters
                $params = $this->validatePagination();
                if( $params ) {
                    $offset = $params['offset'];
                    $pageSize = $params['pageSize'];
                }

                $c = new EMongoCriteria;
                
                $c->addCondition('_receiver', new MongoId( $id ) );                
                $c->addCondition('read', 0 );                

                $c->setLimit( $pageSize )->setSort( array('created' => 'desc') )->setSkip($offset);                

                $array = iterator_to_array( Notification::model()->findAll( $c ), false) ;
                
                echo json_encode( $this->setPaginatedOutput( $array, count($array), $offset), JSON_NUMERIC_CHECK ); 

            } else if( isset($_POST['notificationId']) && isset($_POST['read']) ) {

                Notification::model()->updateByPk( new MongoId($_POST['notificationId']), array('$set' => array('read' => (int) $_POST['read'] ) ) );
                $this->sendOk("Notification {$_POST['notificationId']} read");
                
            } else {

                $user = $this->findbyPk( "Oauth2User", new MongoId($id) );
                $user->updateNotifications($status);
                $this->sendOk("Notifications are {$_POST['status']}");
            }

        } catch( Exception $e){ 
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }

    /**
     * Actions to provide sensor registration to user profile
     * GET retrieves the current wooid string
     * POST adds or removes wooid based on the parameter action received
     * @return String GET method returns the wooid, the POST operation returns the status of the operation
     * @deprecated
     */
    
    public function actionWooDevice(){
        $oauth = YiiOAuth2::instance();
        $token = $oauth->verifyToken();

        $this->noActive();
        return;

        $this->setJsonHeaders();
        try{
            $currentUser = $oauth->loadUser($token->oauth_token);
            switch( $_SERVER['REQUEST_METHOD'] ){ 
                case 'GET':
                    $id = isset($currentUser->woo_id) ? $currentUser->woo_id : array();
                    echo json_encode( array( 'woodevice' =>  $id) );
                    break;
                case 'POST':
                    $filters = array(
                        "action" =>  array("filter" => FILTER_VALIDATE_REGEXP, "options"=> array("regexp"=>"/(add)|(remove)/") ),  
                        "wooid" =>  array("filter" => FILTER_VALIDATE_REGEXP, "options"=>array("regexp"=>"/^(woo)-[a-z0-9\s]{4}-[a-z0-9\s]{4}$/i") ),
                    );
                    $input = filter_input_array(INPUT_POST, $filters);
                    if( !$input['action'] )
                        throw new OAuth2Exception( array('code' => BAD_POST_DATA, 'message' => "Dont know what to do") );
                    if( !$input['wooid'] )
                        throw new OAuth2Exception( array('code' => BAD_POST_DATA, 'message' => "No valid wooid found") );
                    
                    $newWooId = strtolower( $input['wooid'] );

                    switch ($input['action']) {
                        case 'add':
                            // The device is not registered by any other user
                            if( $this->checkDevice($newWooId) )
                                throw new OAuth2Exception( array('code' => HACKING_ACTIVITY, 'message' => "This device is already registered") );
                            if($currentUser->addWooDevice($newWooId))
                                $this->sendOk("woo id added to user");
                            break;
                        case 'remove':
                            if( $currentUser->removeWooDevice($newWooId) )
                                $this->sendOk("woo id removed from user");
                            break;
                    }
            }
        } catch (Exception $e) {
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        } 
    }

    /**
     * [actionFollowing description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     * 
     * @version 1.3.0
     */
    public function actionFollowing( $id=null ) {
        try{
            $this->setJsonHeaders();

            if( !$id ) {
                $pk = Yii::app()->controller->getModule("oauth2")->getUid();
                $user = $this->findbyPk( "Oauth2User", new MongoId($pk) );
            } else 
                $user = $this->findbyPk( "Oauth2User", new MongoId($id) );

            if( !$user )
                throw new OAuth2Exception( array('code' => BAD_USER_ID, 'message' => "Cannot find user. Check user identifier.") );

            //print_r($user); die;
            
            // default session parameters
            $offset = 0;
            $pageSize = 20;

            // Check if the actual post contains extra parameters
            $params = $this->validatePagination();
            if( $params ) {
                $offset = $params['offset'];
                $pageSize = $params['pageSize'];
            }

            $projection = array( '_id'=>1, 'details.name'=>1, 'details.lastname'=>1, 'details._pictures'=>1 );
            $users = array();

            if( !empty($user->_followed ) ) {
                // $it = Oauth2User::model()->setProjectedFields($projection)->findAllByPk( $user->_followed );

                $c = new EMongoCriteria;
                $arr = Oauth2User::model()->getPrimaryKeyArray($user->_followed);
                $c->compare( '_id', $arr );
                $c->setLimit($pageSize)->setSort( array('details.name'=>'asc'))->setSkip($offset);  
                
                $it = Oauth2User::model()->find( $c, $projection );

                // $arr = iterator_to_array( $it, false);

                while( $it->hasNext() ) {
                //foreach ($arr as $value) {
                    $value = $it->getNext();
                    $pic = $value->getProfileImage(true);
                    $det = (object) $value->details;

                    //print_r( $pic ); die;
                    unset( $det->_pictures );
                    $det->_pictures = array( (object) $pic );
                    $value->details = $det;

                    $value = (object) array_filter( (array) $value );

                    # code...
                    $users[] = $value;
                }
            }

            echo json_encode( $this->setPaginatedOutput( $users, $it->count(), $offset), JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES );
            //echo json_encode( $users, JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES );


        } catch( Exception $e ) {
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }        
    }

    /**
     * [actionFollowers description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     * 
     * @version 1.3.0
     */
    public function actionFollowers($id=null) {
        try{
            $this->setJsonHeaders();

            if( !$id ) {
                $pk = Yii::app()->controller->getModule("oauth2")->getUid();
                $user = $this->findbyPk( "Oauth2User", new MongoId($pk) );
            } else 
                $user = $this->findbyPk( "Oauth2User", new MongoId($id) );

            if( !$user )
                throw new OAuth2Exception( array('code' => BAD_USER_ID, 'message' => "Cannot find user. Check user identifier.") );

            // default session parameters
            $offset = 0;
            $pageSize = 20;

            // Check if the actual post contains extra parameters
            $params = $this->validatePagination();
            if( $params ) {
                $offset = $params['offset'];
                $pageSize = $params['pageSize'];
            }

            $projection = array( '_id'=>1, 'details.name'=>1, 'details.lastname'=>1, 'details._pictures'=>1 );
            $users = array();

            if( !empty($user->_followedBy ) ) {
                // $it = Oauth2User::model()->setProjectedFields($projection)->findAllByPk( $user->_followed );
                $c = new EMongoCriteria;
                $arr = Oauth2User::model()->getPrimaryKeyArray($user->_followedBy);
                $c->compare( '_id', $arr );
                $c->setLimit($pageSize)->setSort( array('details.name'=>'asc'))->setSkip($offset);  
                
                $it = Oauth2User::model()->find( $c, $projection );

                // $arr = iterator_to_array( $it, false);

                while( $it->hasNext() ) {
                //foreach ($arr as $value) {
                    $value = $it->getNext();
                    $pic = $value->getProfileImage(true);
                    $det = (object) $value->details;

                    //print_r( $pic ); die;
                    unset( $det->_pictures );
                    $det->_pictures = array( (object) $pic );
                    $value->details = $det;

                    $value = (object) array_filter( (array) $value );

                    # code...
                    $users[] = $value;
                }
            }

            echo json_encode( $this->setPaginatedOutput( $users, ( isset($it) ) ? $it->count() : 0, $offset), JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES );


        } catch( Exception $e ) {
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }

    /**
     *  If the token owner follows the user with the $id provided
     *  @param  String $id The id of the requested user
     * 
     *  @version 1.3.3
     */
    public function actionIsFollowed($id) {
        try{
            $this->setJsonHeaders();

            $pk = Yii::app()->controller->getModule("oauth2")->getUid();
            $projection = array( '_followed'=>1  );
            $currentUser = $this->findbyPk( "Oauth2User", new MongoId($pk), $projection );

            if( !$currentUser )
                throw new OAuth2Exception( array('code' => UNKNOWN_OAUTH_TOKEN, 'message' => "Cannot find user for this token.") );     

            echo json_encode( array( "isFollowed" => in_array( $id, $currentUser->_followed ) ) );

        } catch( Exception $e ) {
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }


    

    /**
     * Resend activation mail in case not received
     */
    public function actionResendActivationMail(){

        if( !$_SERVER['REQUEST_METHOD'] == 'POST' || !isset( $_POST['email'] ))
            throw new OAuth2Exception( array('code' => BAD_POST_DATA, 'message' => "Cannot find valid user set") );

        //$data = Oauth2User::model()->findByAttributes( array('email'=>$_POST['email']) );
        //$oldCode = PassCode::model()->findByAttributes(array('user'=>$email));
        //if($oldCode)
        
            $c = new EMongoCriteria;
            $c->addCondition( 'user', $_POST['email'] );

            $resetCode = PassCode::model()->deleteAll($c);
            $user['email']=$_POST['email'];

        $this->sendConfirmationMail((Object)$user);
    }
        
    /**
     * Registration data validator
     * Validates the presence and the format of mandatory registration parameters
     * 
     * @return boolean
     */
    private function checkRegistrationData(){

        $filters = array(
            "deviceId" => array(                "filter"=> FILTER_VALIDATE_REGEXP,                "options"=>array("regexp"=>"/[a-z0-9\s]{16,128}/") ),
            "client_id" => array(               "filter"=> FILTER_VALIDATE_REGEXP,                "options"=>array("regexp"=>"/[a-z0-9\s]{128}/") ),
            "client_secret" => array(           "filter"=> FILTER_VALIDATE_REGEXP,                "options"=>array("regexp"=>"/[a-z0-9\s]{128}/") ),
            "redirect_uri" => array(            "filter"=> FILTER_SANITIZE_URL, ),                
            "email" => array(                   "filter"=> FILTER_SANITIZE_EMAIL, ), 
            "pass" => array(                    "filter"=> FILTER_VALIDATE_REGEXP,                "options"=>array("regexp"=>"/[\w]{6,128}/") ),    
            "name" => array(                    "filter"=> FILTER_VALIDATE_REGEXP,                "options"=>array("regexp"=>"/[\w]{2,128}/") ),
            "lastname" => array(                "filter"=> FILTER_VALIDATE_REGEXP,                "options"=>array("regexp"=>"/[\w]{2,128}/") ),
        );
        
        $input = filter_input_array(INPUT_POST, $filters);
        $lines = array_map('trim', file(__DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "data" . DIRECTORY_SEPARATOR . "badwords.txt") );

        if( isset($input['client_id']) && !empty( $input['client_id'] ) &&
            isset($input['client_secret']) && !empty( $input['client_secret'] ) &&
            isset($input['redirect_uri']) && !empty( $input['redirect_uri'] ) &&
            isset($input['pass']) && !empty( $input['pass'] ) &&
            isset($input['deviceId']) &&
            isset($input['email']) && !empty( $input['email'] ) &&
            isset($input['name']) && !empty( $input['name'] )
            
        ) {
            $c = new EMongoCriteria;
            $c->addCondition( 'email', $input['email'] ) ;
            $result = Oauth2User::model()->findOne($c);
            if ($result)  {
                    throw new OAuth2Exception(array('code' => BAD_MAIL_ERR, 'message' => "The email you entered already exist. Please try another one."));
            }

            //already checking this 
            //$this->checkbadwords($lines, $input['name'], BAD_WORDS_NAME_ERROR, "Are you sure your name is {$input['name']}?" );
            //$this->checkbadwords($lines, $input['lastname'], BAD_WORDS_LASTNAME_ERROR, "Are you sure your lastname is {$input['lastname']}?" );
            return $input; 
        
        } else return FALSE;
    }

    
    private function checkbadwords( $lines, $name, $code=null, $message='') {
        if( in_array( $name, $lines) )                
            throw new OAuth2Exception(array('code' => $code, 'message' => $message));    
    }

    /**
     * Returns the url belonging to the current user profile
     * @internal
     */
    private function getProfileImage( $email ){
        $c = new EMongoCriteria();
        $c->addCondition( 'email', $email );
        $result = Oauth2User::model()->findOne($c);

        if(!$result)
            throw new OAuth2Exception( array('code' => BAD_MAIL_ERR, 'message' => "Cannot find user for the email") );

        $details = (Object) $result->details;

        foreach ($details->_pictures as $img) {
            if( $img['type'] == "user")
                return $img['url'];
        }
        return "";
    }
    
    
    private function checkAppCredentials(){
        $filters = array(
            "client_id" => array(
                "filter"=> FILTER_VALIDATE_REGEXP,
                "options"=>array("regexp"=>"/[a-z0-9\s]{128}/") ),
            "client_secret" => array(
                "filter"=> FILTER_VALIDATE_REGEXP,
                "options"=>array("regexp"=>"/[a-z0-9\s]{128}/") ),
            "redirect_uri" => array(
                "filter"=> FILTER_SANITIZE_URL, ), 
            "email" => array(
                "filter"=> FILTER_SANITIZE_EMAIL, ),  
        );
        
        $input = filter_input_array(INPUT_POST, $filters);
        
        if( isset($input['client_id']) && !empty( $input['client_id'] ) &&
            isset($input['client_secret']) && !empty( $input['client_secret'] ) !== '' &&
            isset($input['redirect_uri']) && !empty( $input['redirect_uri'] ) !== '' &&            
            isset($input['email']) && !empty( $input['email'] ) !== ''
        ) return $input; 
        
        else return FALSE;
    }

    /**
     * @internal 
     */
    private function sendRecoverMail( $options ){
        try {
            $resetCode = $options->code;
            $subject = "WOO Password Recovery";           
            $serverName = $this->getServerPath(true);
            $message ='<table border="0" cellpadding="0" cellspacing="0" width="100%"">' . 
            '<tr><td colspan="2" align="center" style="font-size: 39px; font-family: Serif; font-weight: normal; color: #ffffff; letter-spacing:-2px;" bgcolor="#dddddd"></td></tr>'.
            '<tr><td bgcolor="#dddddd">' . 
            '<div align="center">'.
                '<table border="0" cellpadding="0" cellspacing="0" width="640" align="center" style="padding-top: 40px; padding-left:20px; padding-right:20px;">'.
                    '<tr><td bgcolor="#29aae0" style="border-radius: 10px 10px 0px 0px; -moz-border-radius: 0px;-webkit-border-radius: 10px 10px 0px 0px;">'. 
                        '<a href="http://www.woosports.com/" style="color: #999999; text-decoration: none;"><img alt="WOO" src="https://s3.amazonaws.com/wooimg/assets/mail/logo_white.png" title="WOO Logo" width="100" style="padding: 15px 20px; display: block; font-family: Arial; color: #ffffff; font-size: 14px; margin-right: 20px;" border="0"/></a>' .
                    '</td></tr>'.
                    '<tr><td bgcolor="#fff" height="30" style="">'. '</td></tr>'.
                    // Email white header
                    '<tr>'.
                        '<td style="padding: 15px 0px 10px 0px;" bgcolor="#fff">'.
                            '<table border="0" cellpadding="0" cellspacing="0" align="center">'.
                              '<tr><td width="350" align="center"><a href="http://litmus.com" style="color: #000; text-decoration: none; font-size: 25px; font-weight: bold;">'.
                                'RESET PASSWORD' . 
                              '</td></tr>'.  
                              /*
                                '<tr><td bgcolor="#fff" height="40" style="">'. '</td></tr>'. 
                                '<tr><td width="200" height="1em" align="center" style="font-size: 12px; font-family: Arial; color: #333333; text-decoration: none;"></td></tr>' .                 

                                '<tr><td width="200" height="1em" align="center" style="font-size: 12px; font-family: Arial; color: #333333; text-decoration: none;">' . 
                                    '<img alt="WOO" src="' . $options->user_image .'" title="Your profile image" style="width: 120px; border-radius: 50%; -moz-border-radius: 50%;-webkit-border-radius: 50%; display: block;" border="0"/>' . 
                                '</td></tr>' .                 

                                '<tr><td width="200" height="30" align="center" style="font-size: 12px; font-family: Arial; color: #333333; text-decoration: none;"></td></tr>' .                 

                                '<tr><td width="200" align="center" style="font-size: 24px; font-family: Arial; color: #333333; text-decoration: none;">' .

                                'Hi ' . $options->user_name . ':' .

                                '</td></tr>'. 
                                */
                                '<tr><td bgcolor="#fff" height="30" style="">'. '</td></tr>'.
                                '<tr><td width="200" height="1em" align="center" style="font-size: 12px; font-family: Arial; color: #333333; text-decoration: none;"></td></tr>' .
                                '<tr><td align="center" style="font-size: 12px; font-family: Arial; color: #333333; text-decoration: none;">' . 
                                '<a href="' . $serverName  . 'site/resetpassword/' . $resetCode . '" style="text-transform:capitalize; text-decoration: none;font-weight: bold;display: block;padding: 18px 5px;font-size: 15px; width:120px; color:#fff; text-transform: capitalize; background-color:#818181; border-radius: 10px; -moz-border-radius: 10px;-webkit-border-radius: 10px;">'.
                                'RESET'.'</a>'.
                                '</td></tr>' .   

                                '<tr><td bgcolor="#fff" height="50" style="">'. '</td></tr>'. 
                                '<tr><td width="200" height="1em" align="center" style="font-size: 12px; font-family: Arial; color: #333333; text-decoration: none;"></td></tr>' .

                                '<tr><td width="200" align="center"  style="font-size: 12px; background-color:#fff; font-family: Arial, sans-serif; color: #333333; text-decoration: none;">' . 
                                    'If you don’t see ‘Reset’ button click the link below:'.
                                '</td></tr>' .
                                '<tr><td bgcolor="#fff" height="15" style="">'. '</td></tr>'.
                                '<tr><td height="1em" align="center" style="font-size: 12px; font-family: Arial, sans-serif; color: #333333; text-decoration: none;">' .
                                '<a href="' . $serverName  . 'site/resetpassword/' . $resetCode . '" />Reset Password</a>' .
                                '</td></tr>' .                                

                                '<tr><td bgcolor="#fff" height="30" style="">'. '</td></tr>'.
                                '<tr><td width="200" align="center"  style="font-size: 12px; background-color:#fff; font-family: Arial, sans-serif; color: #333333; text-decoration: none;">' . 
                                'The link will be valid for 24 hours.' .    
                                '</td></tr>' .
                               
                            '</table>' . 
                        '</td>' . 
                    '</tr>'.
                    '<tr>' . 
                        '<td style="padding: 0px 0px 0px 0px" bgcolor="#fff">'.
                        '<table width="100%" border="0" cellspacing="0" cellpadding="0">'.
                            '<tr><td colspan="2" align="left" style="padding: 5px 10px;">' . 
                                '<img alt="WOO" src="https://s3.amazonaws.com/wooimg/assets/mail/logo_grey.png" title="WOO Sports" width="80" style="padding: 15px 20px 0px 20px; display: block; font-family: Arial; color: #ffffff; font-size: 14px; margin-right: 20px;" border="0"/>'.
                                '<div style="letter-spacing: -0.8px;padding-left: 10px;color: #dddddd;">' . '&copy;2014 WOO Sports' . '</div>'.
                            '</td>'.
                            '<td valign="bottom" colspan="2" align="right" style="vertical-align:bottom; padding: 0px 0px 5px 0px; font-size: 12px; font-family: sans-serif; font-weight: normal; color: #333333;">' . 
                                '<div style="vertical-align:bottom; display:inline-block; color:#dddddd;">'. 'follow us: ' .'</div>'.
                                '<img alt="WOO" src="https://s3.amazonaws.com/wooimg/assets/mail/social_facebook.png" title="WOO on Facebook" width="28" style="display: inline-block; font-family: Arial; color: #ffffff; font-size: 14px; margin-right: 20px;" border="0"/>'.
                                '<img alt="WOO" src="https://s3.amazonaws.com/wooimg/assets/mail/social_twitter.png" title="WOO on Twitter" width="28" style="display: inline-block; font-family: Arial; color: #ffffff; font-size: 14px; margin-right: 20px;" border="0"/>'.
                                '<img alt="WOO" src="https://s3.amazonaws.com/wooimg/assets/mail/social_instagram.png" title="WOO on Instagram" width="28" style="display: inline-block; font-family: Arial; color: #ffffff; font-size: 14px; margin-right: 20px;" border="0"/>'.
                                '<img alt="WOO" src="https://s3.amazonaws.com/wooimg/assets/mail/social_vimeo.png" title="WOO on Vimeo" width="28" style="display: inline-block; font-family: Arial; color: #ffffff; font-size: 14px; margin-right: 20px;" border="0"/>'.
                                '<img alt="WOO" src="https://s3.amazonaws.com/wooimg/assets/mail/social_youtube.png" title="WOO on Youtube" width="28" style="display: inline-block; font-family: Arial; color: #ffffff; font-size: 14px; margin-right: 20px;" border="0"/>'.
                            '</td></tr>' .        
                        '</table>' . 
                        '</td>' . 
                    '</tr>' . 
                '</table>' . 
                '</div>' . 
            '</td></tr>'.
            '<tr>' . 
                '<td align="center" style="padding: 20px 20px 30px 30px; font-size: 10px; font-family: Arial; font-weight: normal; color: #333333;" bgcolor="#dddddd">' . 
                    "You're receiving this email because you signed up for WOO. We hate spam and make an effort to keep notifications to a minimum." .
                '</td>' . 
            '</tr>'. 
            '</table>';

        
            $this->sendMail($options, $subject, $message);

        } catch (Excption $e){
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }

    
   
    /**
     * @internal 
     */
    private function sendConfirmationMail( $user ){
        
        $code = new PassCode();
        $code->code = openssl_digest($user->email , 'sha512');
        $code->user = $user->email;
        $code->created = time();
        $code->expires = time() + 24*60*60;    
        if( !$code->save() )
            throw new Exception( "Could not send the confirmation email" );  

        $resetCode = $code->code;

        $subject = "WOO Account Confirmation";       
      
        $serverName = $this->getServerPath(true);
        $message = '<table border="0" cellpadding="0" cellspacing="0" width="100%"">' . 
            '<tr><td colspan="2" align="center" style="font-size: 39px; font-family: Serif; font-weight: normal; color: #ffffff; letter-spacing:-2px;" bgcolor="#dddddd"></td></tr>'.
            '<tr><td bgcolor="#dddddd">' . 
            '<div align="center">'.
                '<table border="0" cellpadding="0" cellspacing="0" width="640" align="center" style="padding-top: 40px; padding-left:20px; padding-right:20px;">'.
                    '<tr><td bgcolor="#29aae0" style="border-radius: 10px 10px 0px 0px; -moz-border-radius: 0px;-webkit-border-radius: 10px 10px 0px 0px;">'. 
                        '<a href="http://www.woosports.com/" style="color: #999999; text-decoration: none;"><img alt="WOO" src="https://s3.amazonaws.com/wooimg/assets/mail/logo_white.png" title="WOO Logo" width="100" style="padding: 15px 20px; display: block; font-family: Arial; color: #ffffff; font-size: 14px; margin-right: 20px;" border="0"/></a>' .
                    '</td></tr>'.
                    '<tr><td bgcolor="#fff" height="30" style="">'. '</td></tr>'.
                    // Email white header
                    '<tr>'.
                        '<td style="padding: 15px 0px 10px 0px;" bgcolor="#fff">'.
                            '<table border="0" cellpadding="0" cellspacing="0" align="center">'.
                              '<tr><td width="350" align="center"><a href="http://litmus.com" style="color: #000; text-decoration: none;font-size: 25px; font-weight: bold;">'.
                                'WELCOME TO WOO' . 
                              '</td></tr>'.  
                                '<tr><td bgcolor="#fff" height="40" style="">'. '</td></tr>'. 
                                '<tr><td width="200" height="1em" align="center" style="font-size: 12px; font-family: Arial; color: #333333; text-decoration: none;"></td></tr>' .                 
                                '<tr><td width="200" align="center" style="font-size: 14px; font-family: Arial; color: #333333; text-decoration: none;">' .

                                'Thank you for registering!' .

                                '</td></tr>'.

                                '<tr><td bgcolor="#fff" height="40" style="">'. '</td></tr>'.
                                '<tr><td width="200" height="1em" align="center" style="font-size: 12px; font-family: Arial; color: #333333; text-decoration: none;"></td></tr>' .
                                '<tr><td width="200" align="center" style="font-size: 12px; font-family: Arial, sans-serif; color: #333333; text-decoration: none;">' . 
                                    'To activate your account and start using WOO,<br>please click the \'Activate\' button below:' .
                                '</td></tr>' .

                                '<tr><td bgcolor="#fff" height="30" style="">'. '</td></tr>'.
                                '<tr><td align="center" style="font-size: 12px; font-family: Arial; background-color:#fff; text-decoration: none;">' . 
                                '<a href="' . $serverName  . 'site/activate/' . $resetCode . '" style="text-decoration: none;display: block;padding: 18px 5px;font-size: 15px; width:120px; color:#fff; text-transform: capitalize; background-color:#39b44a; border-radius: 10px; -moz-border-radius: 10px;-webkit-border-radius: 10px;">'.
                                'Activate'.'</a>'.
                                '</td></tr>' .

                                //Link

                                '<tr><td bgcolor="#fff" height="30" style="">'. '</td></tr>'.

                                '<tr><td bgcolor="#fff" height="30" style="">'. '</td></tr>'.
                                '<tr><td width="200" align="center"  style="font-size: 12px; background-color:#fff; font-family: Arial, sans-serif; color: #333333; text-decoration: none;">' . 
                                    'If you don\'t see   \'Activate\' button click the link below:'.
                                '</td></tr>' .
                                '<tr><td width="200" height="1em" align="center" style="font-size: 12px; font-family: Arial, sans-serif; color: #333333; text-decoration: none;">' .
                                '<a href="' . $serverName  . 'site/activate/' . $resetCode . '" />Activate account</a></td></tr>' .
                                '</td></tr>' .
                                
                            '</table>' . 
                        '</td>' . 
                    '</tr>'.
                    '<tr>' . 
                        '<td style="padding: 0; background-color:#dddddd;">'.
                        '<table border="0" cellspacing="0" cellpadding="0" width="640" align="center" style="padding: 0px 20px;">'.
                            '<tr><td colspan="2" align="left" style="padding: 5px 10px; background-color:#fff">' . 
                                '<img alt="WOO" src="https://s3.amazonaws.com/wooimg/assets/mail/logo_grey.png" title="WOO Logo" width="80" style="padding: 15px 20px 0px 20px; display: block; font-family: Arial; color: #ffffff; font-size: 14px; margin-right: 20px;" border="0"/>'.
                                '<div style="letter-spacing: -0.8px;padding-left: 10px;color: #dddddd;">' . '&copy;2014 WOO Sports' . '</div>'.
                            '</td>'.
                            '<td valign="bottom" colspan="2" align="right" style="vertical-align:bottom; padding: 0px 0px 5px 0px; font-size: 12px; font-family: sans-serif; font-weight: normal; color: #333333; background-color:#fff">' . 
                                '<div style="vertical-align:bottom; display:inline-block; color:#dddddd;">'. 'follow us: ' .'</div>'.
                                '<img alt="WOO" src="https://s3.amazonaws.com/wooimg/assets/mail/social_facebook.png" title="WOO on Facebook" width="28" style="display: inline-block; font-family: Arial; color: #ffffff; font-size: 14px; margin-right: 20px;" border="0"/>'.
                                '<img alt="WOO" src="https://s3.amazonaws.com/wooimg/assets/mail/social_twitter.png" title="WOO on Twitter" width="28" style="display: inline-block; font-family: Arial; color: #ffffff; font-size: 14px; margin-right: 20px;" border="0"/>'.
                                '<img alt="WOO" src="https://s3.amazonaws.com/wooimg/assets/mail/social_instagram.png" title="WOO on Instagram" width="28" style="display: inline-block; font-family: Arial; color: #ffffff; font-size: 14px; margin-right: 20px;" border="0"/>'.
                                '<img alt="WOO" src="https://s3.amazonaws.com/wooimg/assets/mail/social_vimeo.png" title="WOO on Vimeo" width="28" style="display: inline-block; font-family: Arial; color: #ffffff; font-size: 14px; margin-right: 20px;" border="0"/>'.
                                '<img alt="WOO" src="https://s3.amazonaws.com/wooimg/assets/mail/social_youtube.png" title="WOO on Youtube" width="28" style="display: inline-block; font-family: Arial; color: #ffffff; font-size: 14px; margin-right: 20px;" border="0"/>'.
                            '</td></tr>' .        
                        '</table>' . 
                        '</td>' . 
                    '</tr>' . 
                    '<tr>' . 
                        '<td align="center" style="padding: 20px 20px 30px 30px; font-size: 10px; font-family: Arial; font-weight: normal; color: #333333;" bgcolor="#dddddd">' . 
                            "You're receiving this email because you signed up for WOO. We hate spam and make an effort to keep notifications to a minimum." .
                        '</td>' . 
                    '</tr>'. 
                '</table>' . 
                '</div>' . 
            '</td></tr>'.
        '</table>';

        try {
            $options['user']=$user->email;
            $this->sendMail( (Object)$options, $subject, $message);

        } catch (Excption $e){
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }

    /**
     * @internal 
     */
    public function sendAccountactiveMail( $user ){

        /********************************

        METHOD VARS

        *********************************/

        $shopAddr = 'http://shop.woosports.com';
        $devicePicture = 'https://s3.amazonaws.com/wooimg/assets/mail/woo_picture2.png';

        
        $subject = "Your WOO account is now Active ";
        $serverName = $this->getServerPath(true);
        $message = '<table border="0" cellpadding="0" cellspacing="0" width="100%"">' . 
            '<tr><td colspan="2" align="center" style="font-size: 39px; font-family: Serif; font-weight: normal; color: #ffffff; letter-spacing:-2px;" bgcolor="#dddddd"></td></tr>'.
            '<tr><td bgcolor="#dddddd">' . 
            '<div align="center">'.
                '<table border="0" cellpadding="0" cellspacing="0" width="640" align="center" style="padding-top: 40px; padding-left:20px; padding-right:20px;">'.
                    '<tr><td bgcolor="#29aae0" style="border-radius: 10px 10px 0px 0px; -moz-border-radius: 0px;-webkit-border-radius: 10px 10px 0px 0px;">'. 
                        '<a href="http://www.woosports.com/" style="color: #999999; text-decoration: none;"><img alt="WOO" src="https://s3.amazonaws.com/wooimg/assets/mail/logo_white.png" title="WOO Logo" width="100" style="padding: 15px 20px; display: block; font-family: Arial; color: #ffffff; font-size: 14px; margin-right: 20px;" border="0"/></a>' .
                    '</td></tr>'.
                    '<tr><td bgcolor="#fff" height="30" style="">'. '</td></tr>'.
                    // Email white header
                    '<tr>'.
                        '<td style="padding: 15px 0px 10px 0px;" bgcolor="#fff">'.
                            '<table border="0" cellpadding="0" cellspacing="0" align="center">'.
                              '<tr><td width="350" align="center"><a href="http://litmus.com" style="color: #000; text-decoration: none;font-size: 25px; font-weight: bold;">'.
                                'WELCOME TO WOO!' . 
                              '</td></tr>'.  
                                '<tr><td bgcolor="#fff" height="40" style="">'. '</td></tr>'. 
                                '<tr><td width="200" height="1em" align="center" style="font-size: 12px; font-family: Arial; color: #333333; text-decoration: none;"></td></tr>' .                 
                                '<tr><td width="200" align="center" style="text-transform:lowercase;font-size: 15px; font-family: Arial; color: #39b44a; text-decoration: none;">' .

                                'Your account has been activated!' .

                                '</td></tr>'.
                                '<tr><td bgcolor="#fff" height="40" style="">'. '</td></tr>'.
                     
                                '<tr><td width="200" align="center" style="font-weight: bold;text-transform:uppercase;font-size: 15px; font-family: Arial; color: #333333; text-decoration: none;">' .

                                'Have you got your WOO?' .

                                '</td></tr>'.
                                '<tr><td bgcolor="#fff" height="20" style="">'. '</td></tr>'.
                                '<tr><td width="200" height="1em" align="center" style="font-size: 12px; font-family: Arial; color: #333333; text-decoration: none;">' . 
                                    '<img alt="WOO" src="' . $devicePicture . '" title="The WOO" style="width: 350px; border-radius: 50%; -moz-border-radius: 50%;-webkit-border-radius: 50%; display: block;" border="0"/>' . 
                                '</td></tr>' . 
                                '<tr><td bgcolor="#fff" height="40" style="">'. '</td></tr>'.

                                '<tr><td align="center" style="font-size: 12px; font-family: Arial; background-color:#fff; text-decoration: none;">' . 

                                '<a href="' . $shopAddr . '" style="font-weight:bold;text-decoration: none;display: block;padding: 18px 5px;font-size: 15px; width:120px; color:#fff; text-transform: uppercase; background-color:#29aae0; border-radius: 10px; -moz-border-radius: 10px;-webkit-border-radius: 10px;">'.
                                'BUY IT!'.'</a>'.
                                '</td></tr>' .
                            '</table>' . 
                        '</td>' . 
                    '</tr>'.
                    '<tr>' . 
                        '<td style="padding: 0; background-color:#dddddd;">'.
                        '<table border="0" cellspacing="0" cellpadding="0" width="640" align="center"">'.
                            '<tr><td colspan="2" align="left" style="padding: 5px 10px; background-color:#fff">' . 
                                '<img alt="WOO" src="https://s3.amazonaws.com/wooimg/assets/mail/logo_grey.png" title="WOO Logo" width="80" style="padding: 15px 20px 0px 20px; display: block; font-family: Arial; color: #ffffff; font-size: 14px; margin-right: 20px;" border="0"/>'.
                                '<div style="letter-spacing: -0.8px;padding-left: 10px;color: #dddddd;">' . '&copy;2014 WOO Sports' . '</div>'.
                            '</td>'.
                            '<td valign="bottom" colspan="2" align="right" style="vertical-align:bottom; padding: 0px 0px 5px 0px; font-size: 12px; font-family: sans-serif; font-weight: normal; color: #333333; background-color:#fff">' . 
                                '<div style="vertical-align:bottom; display:inline-block; color:#dddddd;">'. 'follow us: ' .'</div>'.
                                '<img alt="WOO" src="https://s3.amazonaws.com/wooimg/assets/mail/social_facebook.png" title="WOO on Facebook" width="28" style="display: inline-block; font-family: Arial; color: #ffffff; font-size: 14px; margin-right: 20px;" border="0"/>'.
                                '<img alt="WOO" src="https://s3.amazonaws.com/wooimg/assets/mail/social_twitter.png" title="WOO on Twitter" width="28" style="display: inline-block; font-family: Arial; color: #ffffff; font-size: 14px; margin-right: 20px;" border="0"/>'.
                                '<img alt="WOO" src="https://s3.amazonaws.com/wooimg/assets/mail/social_instagram.png" title="WOO on Instagram" width="28" style="display: inline-block; font-family: Arial; color: #ffffff; font-size: 14px; margin-right: 20px;" border="0"/>'.
                                '<img alt="WOO" src="https://s3.amazonaws.com/wooimg/assets/mail/social_vimeo.png" title="WOO on Vimeo" width="28" style="display: inline-block; font-family: Arial; color: #ffffff; font-size: 14px; margin-right: 20px;" border="0"/>'.
                                '<img alt="WOO" src="https://s3.amazonaws.com/wooimg/assets/mail/social_youtube.png" title="WOO on Youtube" width="28" style="display: inline-block; font-family: Arial; color: #ffffff; font-size: 14px; margin-right: 20px;" border="0"/>'.
                            '</td></tr>' .        
                        '</table>' . 
                        '</td>' . 
                    '</tr>' . 
                    '<tr>' . 
                        '<td align="center" style="padding: 20px; font-size: 10px; font-family: Arial; font-weight: normal; color: #333333;" bgcolor="#dddddd">' . 
                            "You're receiving this email because you signed up for WOO. We hate spam and make an effort to keep notifications to a minimum." .
                        '</td>' . 
                    '</tr>'. 
                '</table>' . 
                '</div>' . 
            '</td></tr>'.
        '</table>';

        try {
            $options['user']=$user->email;
            $this->sendMail( (Object)$options, $subject, $message);

        } catch (Excption $e){
            $this->sendExceptionInfo($e, $this->id, $this->action->id);
        }
    }
    
    /**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Centro the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id) {
        $model = OAuth2User::loadUser($id);
		//$model= AppUser::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested OAuth2User does not exist.');
		return $model;
	}
}

?>
