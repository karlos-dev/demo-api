<?php
/* @var $this AppUserController */
/* @var $model AppUser */
/* @var $form CActiveForm */
?>

<div class="form">

<?php 
    $form=$this->beginWidget('CActiveForm', array(
        'id'=>'userdetails-form',
        'action' => '/user/profile',
        'enableAjaxValidation'=>true,
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
         ),
    )); 
    
    $baseUrl = Yii::app()->baseUrl; 
?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>
    
    <?php echo $form->errorSummary($model); ?>

    <div class="row">
	<?php echo '<label for="userdetails_name">Name <span class="required">*</span></label>'; ?>
    <p class="hint"><?php echo "<i>email will be used as your unique username.</i>"; ?></p>
	<?php echo $form->textField($model,'name'); ?>
	<?php echo $form->error($model,'name'); ?>
	</div>
    
	<div class="row">
	<?php echo '<label for="userdetails_lastname">Lastname <span class="required">*</span></label>'; ?>
	<?php echo $form->textField($model,'lastname'); ?>
	<?php echo $form->error($model,'lastname'); ?>
	<p class="hint"></p>
	</div>
    
    <div class="row">
		<?php echo $form->labelEx($model,'gender'); ?>
		<?php echo $form->dropDownList($model,'gender',array('male'=>'Male','female'=>'Female')); ?>
		<?php echo $form->error($model,'gender'); ?>
	</div>
    
    <?php 
        $this->widget('zii.widgets.jui.CJuiDatePicker',array(
            'name'=>'birthday',
            // additional javascript options for the date picker plugin
            'options'=>array(
                'showAnim'=>'fold',
            ),
            'htmlOptions'=>array(
                'style'=>'height:20px;'
            ),
        ));
    ?>
	
	<div class="row">	
        <?php echo "<label for=\"userdetails_images_wrap\">Profile Image Upload</label>"; ?>
        <p class="hint">Puedes añadir <strong>una imagen</strong> para la Especialidad</p>
        <?php $this->widget('CMultiFileUpload',array(
            'name'=>'product_images',
            //'attribute'=>'product_images',
            'accept'=>'jpg|png',
            'max'=>1,
            'remove'=>Yii::t('ui','Remove'),
            'denied'=>'type is not allowed', //message that is displayed when a file type is not allowed
            'duplicate'=>'file appears twice', //message that is displayed when a file appears twice
            'options'=>array(
                'onFileSelect'=>'function(e, v, m){ console.log("onFileSelect - "+v) }',
                'afterFileSelect'=>'function(e, v, m){ console.log("afterFileSelect - "+v) }',
                'onFileAppend'=>'function(e, v, m){ console.log("onFileAppend - "+v) }',
                'afterFileAppend'=>'function(e, v, m){ 
                    $(window).trigger("load-image");
                    console.log("afterFileAppend - "+v);
                    console.dir(m);
                    }',
                'onFileRemove'=>'function(e, v, m){ console.log("onFileRemove - "+v) }',
                'afterFileRemove'=>'function(e, v, m){ console.log("afterFileRemove - "+v) }',
             ),
            'htmlOptions'=>array('size'=>25),
        )); 
        ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->