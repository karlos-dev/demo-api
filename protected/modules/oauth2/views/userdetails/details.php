<?php
/* @var $this AppUserController */
/* @var $model AppUser */

$this->breadcrumbs=array(
	'Kinematiq users'=>array('index'),
	'Details',
);

?>

<h1>Update details of Kinematiq user</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>