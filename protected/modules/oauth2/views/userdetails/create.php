<?php
/* @var $this AppUserController */
/* @var $model AppUser */

$this->breadcrumbs=array(
	'Kinematiq users'=>array('index'),
	'Create',
);

?>

<h1>Crear Usuario Kinematiq</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>