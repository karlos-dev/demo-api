<?php
/* @var $this AppUserController */
/* @var $model AppUser */
/* @var $form CActiveForm */
?>

<div class="form">

<?php 
    $form=$this->beginWidget('CActiveForm', array(
        'id'=>'appuser-form',
        'action' => '/user/create',
        'enableAjaxValidation'=>true,
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
         ),
    )); 
    
    $baseUrl = Yii::app()->baseUrl; 
?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>
    
    <?php echo $form->errorSummary($model); ?>

    <div class="row">
	<?php echo '<label for="AppUser_password">email <span class="required">*</span></label>'; ?>
    <p class="hint"><?php echo "<i>email will be used as your unique username.</i>"; ?></p>
	<?php echo $form->textField($model,'email'); ?>
	<?php echo $form->error($model,'email'); ?>
	</div>
    
	<div class="row">
	<?php echo '<label for="AppUser_password">password <span class="required">*</span></label>'; //$form->labelEx($model,'password'); ?>
	<?php echo $form->passwordField($model,'password'); ?>
	<?php echo $form->error($model,'password'); ?>
	<p class="hint"></p>
	</div>
	
	<div class="row">
	<?php echo '<label for="AppUser_password">confirm password <span class="required">*</span></label>'; ?>
	<?php echo $form->passwordField($model,'verifyPassword'); ?>
	<?php echo $form->error($model,'verifyPassword'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->