<?php
/* @var $this TextoController */
/* @var $model Texto */

$this->breadcrumbs=array(
	'Textos'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Texto', 'url'=>array('index')),
	array('label'=>'Create Texto', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#texto-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Textos</h1>

<p>
Puedes utilizar opcionalmente (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
o <b>=</b>) al principio de cada valor buscado para modificar el tipo de comparación.
</p>

<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
    
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'texto-grid',
	'dataProvider'=>$model->search(),
    'filter'=>$model,
	//'filter'=>CHtml::activeTextField($model, 't_name', array('placeholder'=>'Introduce el nombre')),
	'columns'=>array(
        /*
		array(
            'name'=>'t_id',
            'type'=>'raw',
            'value'=>'CHtml::encode($data->t_id)',
            'filter'=>CHtml::activeTextField($model, 't_name', array('placeholder'=>'E')),
        ),
         * */
         
		//'t_name',
        array(
            'name'=>'t_name',
            'type'=>'raw',
            'value'=>'CHtml::encode($data->t_name)',
            'filter'=>CHtml::activeTextField($model, 't_name', array('placeholder'=>'Nombre que buscas...')),
        ),
        array(
            'name'=>'t_titulo',
            'type'=>'raw',
            'value'=>'CHtml::encode($data->t_titulo)',
            'filter'=>CHtml::activeTextField($model, 't_titulo', array('placeholder'=>'Titulo que buscas...')),
        ),
        array( 'name'=>'t_text', 'type'=>'raw', 
            'value'=>'substr($data->t_text,0,79) . "..."',
            'filter'=>CHtml::activeTextField($model, 't_text', array('placeholder'=>'Primeras letras...')),
            ),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
