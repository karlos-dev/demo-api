<?php
/* @var $this TextoController */
/* @var $data Texto */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('t_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->t_id), array('view', 'id'=>$data->t_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('t_name')); ?>:</b>
	<?php echo CHtml::encode($data->t_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('t_titulo')); ?>:</b>
	<?php echo CHtml::encode($data->t_titulo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('t_text')); ?>:</b>
	<?php echo $data->t_text; ?>
	<br />


</div>