<?php
/* @var $this SiteController */
/* @var $client Object */

$_id = (string) $client->getPrimaryKey();


?>

<h1>Congratulations</h1>
<h2>Welcome to K's Oauth2 system</h2>
<h3>Here it comes the <b><?php echo $client->email; ?></b> user profile.</h3>


<div>
    <p>Find below the client secret associated to your profile:</p>
    <code>Secret: <?php //echo $client->client_secret; ?></code><br>
    <code>Access token: <?php //echo $secret->access_token; ?> expires in <?php //echo $expiration; ?> min.</code>
    <?php if( isset($client->details) ) { ?>
        <p>There are also available details...</p>
    <?php } else { ?>
        <p>There are no Extra details still. Please add them <a href="/user/updateprofile/<?php echo $_id; ?>">here</a></p>
    <?php } ?>    
</div>
<div>    
    <a href="/user" class="">View users</a>
    <a href="/action/user/" class="">View Sample User</a>    
</div>

<?php
    $end = round(microtime(true) * 1000);
    $stringEnd = date("M d Y H:i:s", time());
    Yii::log("**********> RENDER END [site/index.php] [{$stringEnd}] **** Ms: {$end}");
?>
