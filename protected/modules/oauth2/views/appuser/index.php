<?php
/* @var $this TextoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Textos',
);

$this->menu=array(
	array('label'=>'Create Texto', 'url'=>array('create')),
	array('label'=>'Manage Texto', 'url'=>array('admin')),
);
if( UserModule::isAdmin() ) {
?>
<h1>Textos</h1>
<?php 

    $this->widget('zii.widgets.CListView', array(
        'dataProvider'=>$dataProvider,
        'itemView'=>'_view',
         'emptyText' => 'No hay Textos que mostrar',
    )); 

} else { ?>

<h1>Consejos de Salud</h1>

<?php  
    $this->pageTitle=Yii::app()->name . ' - Consejos de salud';    
    $this->widget('zii.widgets.CListView', array(
        'dataProvider'=>$dataProvider,
        'itemView'=>'_cview',
         'emptyText' => 'No hay Textos que mostrar',
    )); 
    }
    ?>
