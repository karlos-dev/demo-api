<?php
/* @var $this AppUserController */
/* @var $model AppUser */

$this->breadcrumbs=array(
	'Textos'=>array('index'),
	$model->t_id=>array('view','id'=>$model->t_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Texto', 'url'=>array('index')),
	array('label'=>'Create Texto', 'url'=>array('create')),
	array('label'=>'View Texto', 'url'=>array('view', 'id'=>$model->t_id)),
	array('label'=>'Manage Texto', 'url'=>array('admin')),
);
?>

<h1>Editar <?php echo $model->t_titulo; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>