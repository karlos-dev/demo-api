<?php

class OAuth2Module extends CWebModule {

    static private $_version;
    static private $_uid;
    static private $_oauth;
    
    private $_debug = false;
    private $_freeAction = array('register','login','recoverpassword','fwupdate', 'resendactivationmail','tc' );
    
    
    public function init() { }

    
    public function oauth2_init() {
        YiiOAuth2::instance();
    }
    
    /**
     * Checks whether an action incoming to module can be executed or not.
     * Oauth2 Authorization valid token is needed 
     * 
     * @param type $controller
     * @param type $action
     * @return boolean
     */
    public function beforeControllerAction($controller, $action) {
        
        if( Yii::app()->params['maintenance']) {

            header("HTTP/1.1 503 Server Maintenance");
            header("Content-Type: application/json");
            echo json_encode ( array(
                "status"=> "Unavailable", 
            ));
            
            return;
        }

        if( !isset(self::$_version) && isset( getallheaders()['AppVersion'] ) ) {

            $version = getallheaders()['AppVersion'];    
            $dirtyArray = explode(".", $version );

            if( isset($dirtyArray[2] ) )
                $appVersion = $dirtyArray[0] . "." . $dirtyArray[1] . "." . $dirtyArray[2];
            else
                $appVersion = $dirtyArray[0] . "." . $dirtyArray[1];

            self::setClientVersion( $appVersion );
        }
        
        /**
         * @author Karlos Alvan <karlos.alvan@gmail.com>
         * @abstract Custom sequrity model. Enables some actions to be executed within oauth2 module
         */
        
        if( in_array($action->id, $this->_freeAction) ) {
            return true;
        }
        
        if ( parent::beforeControllerAction($controller, $action) ) {
            $array = array("default",'statistics', 'wooadm','com');
            if (!in_array($controller->id, $array)) {
                
                $this->oauth2_init();
                if ($controller->id !== 'oauth2' ) {
                    $this->authorization();
                }
            }
            return true;
        } else
            return false;
    }
    
    public function authorization() {
        $token = YiiOAuth2::instance()->verifyToken();
        
        // If we have an user_id, then login as that user (for this request)
        if ($token && isset($token['user_id'])) {
            self::setUid($token['user_id']);
            self::$_oauth = true;
        } else {
            $msg = "Can't verify request, missing oauth_consumer_key or oauth_token";
            throw new CHttpException(401, $msg);
            exit();
        }
    }
    
    public static function setUid($uid) {
        if (empty($uid)) {
            $msg = "authorization failed, missing login user id.";
            throw new CHttpException(401, $msg);
            exit();
        }
        //登录为yii user
        self::$_uid = $uid;
    }
    
    public static function getUid() {
        //return "test";

        if (empty(self::$_uid)) {
            $msg = "Not found";
            throw new CHttpException(403, $msg);
            exit();
        }

        return self::$_uid;
    }

    public static function setClientVersion( $v ) {
        if (empty($v)) {
            $msg = "No version to set";
            throw new CHttpException(401, $msg);
            exit();
        }
        //登录为yii user
        self::$_version = (string) $v;
    }
    
    public static function getClientVersion() {

        if (empty(self::$_version)) {
            //$msg = "Not found";
            //throw new CHttpException(403, $msg);
            //exit();
            return "1.5";
        }

        return self::$_version;
    }

}

?>
