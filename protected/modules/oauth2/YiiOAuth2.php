<?php

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'lib/OAuth2.inc';
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'controllers/ComController.php';

/**
 * OAuth2 Library PDO DB Implementation.
 */

class YiiOAuth2 extends OAuth2 {

    const TOKEN_TYPE_CODE = 'code';
    const TOKEN_TYPE_ACCESS_TOKEN = 'access';
    const TOKEN_TYPE_REFRESH_TOKEN = 'refresh';

    static private $instance = false;
    private $_identity = false;

    
    public static function instance() {
        if (YiiOAuth2::$instance === false) {
            YiiOAuth2::$instance = new YiiOAuth2;
        }
        return YiiOAuth2::$instance;
    }

    
    public function verifyToken() {
        if (YiiOAuth2::$instance->verifyAccessToken())
            return YiiOAuth2::$instance->getVariable('token');
        return FALSE;
    }

    /**
     * Release DB connection during destruct.
     */
    function __destruct() {
        
    }

    /**
     * Handle PDO exceptional cases.
     */
    private function handleException($e) {
        echo "Database error: " . $e->getMessage();
        exit;
    }

    /**
     * Add a new client to the database
     *
     *  @param $post Already validated User data to be inserted as Woo User profile
     *
     *  @version 1.3.2 Added trialEnd set to one week when creating account
     *  @version 1.3.3 Added T&C version and approval date
     *  
     */
    public function addClient( $post, $tc = null ) {
        try {

            $oneWeek = 7 * 24 * 60 * 60;

            $client = new Oauth2User();
            $client->client_id = $post['client_id'];
            $client->client_secret = $post['client_secret'];
            $client->redirect_uri = $post['redirect_uri'];
            $client->email = strtolower( $post['email'] );
            $client->hash = sha1( time() . $post['email']);
            $client->_friends = array();
            $client->_followed = array();
            $client->_followedBy = array();
            $client->device_id = $post['deviceId'];
            $client->enable_notifications = true;
            $client->active = false;
            $client->userAgent = $this->getUserAgent( $client->client_id );
            $client->language = 'en';

            $client->password = $post['pass'];
            $client->fbid = '';  
            $client->role = 'user';
            # $client->hasWooProfile = true;
            
            $client->woo_id=null;
            $client->sensors=array();
            $client->created = time();
            $client->trialEnd = $client->created + $oneWeek;

            if( $tc ) {
                $client->tc_version = $tc;
                $client->tc_acceptdate = time();
            } else {
                $client->tc_version = "";
                $client->tc_acceptdate = 0;
            }
            
            $details = new UserDetails();
            
            
            $details->modified = $client->created;
            $details->name = isset( $post['name']) ? $post['name'] : '';
            $details->lastname = isset( $post['lastname']) ? $post['lastname'] : '';
            $details->fullname = $client->generateFullname($details->name, $details->lastname );

            // Inserted already name lastname
            $client->username = $client->generateUsername($details->name, $details->lastname );
            
            /**
             *  We need to take Air features from API config based on IOS version
             */

            $details->highestAir = 0;
            $details->numberOfAirs = 0;
            $details->maxAirTime = 0;
            $details->maxGforce = 0;

            $details->maxPop = 0;     
            $details->maxHpower = 0;  
            $details->maxCrashPower = 0;
            $details->maxCrashVelocity = 0;
            $details->rotations = array();

            $details->numberOfSessions = 0;
            $details->numberOfAirs=0;

            $details->totalTimeInWater = 0;
            $details->totalHeight = 0;
            $details->totalAirTime = 0;
            $details->_homespot = null;

            $details->_pictures = array();
            $details->country = "";
            $details->gender = "";
            $details->birthday = "";
            $details->weight = "";

            $client->details = $details;

            if( $client->save(false) )
                return $client;
            
            throw new Exception( "Cannot create client, please retry.");
            
        } catch (Exception $e) {
            $this->handleException($e);
        }
    }


    private function getUserAgent( $client ){
        switch( $client ){
            case CLIENT_ID_IOS : 
                return "IOS";
            break;
            case CLIENT_ID_ANDROID : 
                return "Android"; 
            break;
            case CLIENT_ID_WPHONE : 
                return "WindowsPhone";
            break;
        }
    }
    
    /**
     * 
     * @param type $client_id
     * @return type
     */
    public function getClients($client_id) {
        try {
            /*
            $sql = "SELECT * FROM oauth2_clients WHERE client_id = :client_id";
            $connection = Yii::app()->db;
            $stmt = $connection->createCommand($sql);
            $stmt->bindParam(":client_id", $client_id, PDO::PARAM_STR);

            return $stmt->queryRow();
             */
            $c = new EMongoCriteria;
            $c->addCondition( 'client_id', $client_id );
            $results = Oauth2User::model()->findOne($c);
            return $results;
            
        } catch (Exception $e) {
            $this->handleException($e);
        }
    }

    /**
     * Implements OAuth2::checkClientCredentials()
     * 
     */
    protected function checkClientCredentials($client_id, $client_secret = NULL) {
        try {
            $c = new EMongoCriteria;
            $c->addCondition( 'client_id', $client_id ) ;
            $result = Oauth2User::model()->findOne($c);

            if ($client_secret === NULL)
                return $result !== FALSE;

            return $result["client_secret"] == $client_secret;
        } catch (Exception $e) {
            $this->handleException($e);
        }
    }
    
    /**
     * 
     * @param String $userEmail     Actual email to be checked in the db
     * @param String $client_id     Client identification number. It also gives access to API layer.
     * @param String $client_secret Client secret to provide API access
     * @return boolean              If the user can be stored within databse and therefore successfully signed up
     * @throws OAuth2Exception
     */
    public function validateUser(){
        
        $filters = array(
            "client_id" => array("filter" => FILTER_VALIDATE_REGEXP, "options" => array("regexp" => OAUTH2_CLIENT_ID_REGEXP), "flags" => FILTER_REQUIRE_SCALAR),
            "client_secret" => array("filter" => FILTER_VALIDATE_REGEXP, "options" => array("regexp" => OAUTH2_CLIENT_ID_REGEXP), "flags" => FILTER_REQUIRE_SCALAR),
            "email" => array("filter" => FILTER_VALIDATE_EMAIL),
            "name" => array("flags" => FILTER_REQUIRE_SCALAR),
            "lastname" => array("flags" => FILTER_REQUIRE_SCALAR),
            "country" => array("flags" => FILTER_REQUIRE_SCALAR),
            //"birthday" => array("filter" => FILTER_VALIDATE_REGEXP, "options" => array("regexp" => '~^\d{2}/\d{2}/\d{4}$~')),
            //"gender" => array("filter" => FILTER_VALIDATE_REGEXP, "options" => array("regexp" => '~^(male|female)$~')),
        );
        
        $input = filter_input_array(INPUT_POST, $filters);

        if ($input['client_id'] !== CLIENT_ID_IOS && $input['client_id'] == CLIENT_ID_ANDROID && $input['client_id'] == CLIENT_ID_WPHONE)
            throw new OAuth2Exception(array('code' => OUTDATED_APP_VERSION, 'message' => "There is a new version available for your APP. You can already download it."));

        if ($input['client_secret'] !== CLIENT_SECRET)
            throw new OAuth2Exception(array('code' => UNSOPPORTED_APP_VERSION, 'message' => "The client Mobile APP you use is not allowed for API access. Consult your APP provider for further details." ));
        
        $this->validateUserDetails( $input );
        
        return true;
    }
    
    /**
     * Checks if the selected name and passwords are known bad words 
     * 
     * @param String $name
     * @param String $lastname
     * @return boolean
     * @throws OAuth2Exception
     */
    protected function validateUserDetails( $input ){
        $file = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'badwords.txt';
        $badWordsFile = file($file,  FILE_IGNORE_NEW_LINES);
        
        if( in_array( $input['name'] , $badWordsFile ) ) 
            throw new OAuth2Exception( array('code' => BAD_WORDS_NAME_ERROR, 'message' => "You shoudl't use bad words as childs do. Please be polite and provide proper name."));    
        else if(in_array( $input['lastname'] , $badWordsFile ) ) 
            throw new OAuth2Exception( array('code' => BAD_WORDS_LASTNAME_ERROR, 'message' => "You shoudl't use bad words as childs do. Please be polite and provide proper lastname."));    
        else if( in_array( $input['country'] , $badWordsFile )  )
            throw new OAuth2Exception( array('code' => BAD_WORDS_COUNTRY_ERROR, 'message' => "You shoudl't use bad words as childs do. Please be polite and provide proper country name."));
        
        return true;     
    }
    
    /**
     * Checks if the selected name and passwords are known bad words 
     * 
     * @param String $name
     * @param String $lastname
     * @return boolean
     * @throws OAuth2Exception
     */
    protected function validateBirthday( $birthday ){
        $file = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'badwords.txt';
        $badWordsFile = file($file,  FILE_IGNORE_NEW_LINES);
        if( in_array(array( $name, $surname, $country), $badWordsFile ) )
            throw new OAuth2Exception( array('code' => BAD_WORDS_ERROR, 'message' => "You shoudl't use bad words as childs do. Please be polite."));
        return true;     
    }

    /**
     * Implements OAuth2::getRedirectUri().
     */
    protected function getRedirectUri($client_id) {
        try {
            $c = new EMongoCriteria;
            $c->addCondition( 'client_id', $client_id ) ;
            $result=Oauth2User::model()->findOne($c);

            if ($result === FALSE)
                return FALSE;
            
            return isset($result->redirect_uri) && $result->redirect_uri ? $result->redirect_uri : NULL;
        } catch (Exception $e) {
            $this->handleException($e);
        }
    }

    /**
     * Implements OAuth2::getAccessToken().
     */
    protected function getAccessToken($oauth_token) {
        return $this->getToken($oauth_token, YiiOAuth2::TOKEN_TYPE_ACCESS_TOKEN);
    }

    /**
     * Implements OAuth2::setAccessToken().
     */
    protected function setAccessToken($oauth_token, $client_id, $expires, $scope = NULL) {
        $this->setToken($oauth_token, YiiOAuth2::TOKEN_TYPE_ACCESS_TOKEN, $client_id, $expires, '', $scope);
    }
    
    /**
     * Overrides OAuth2::getRefreshToken().
     * 
     * @param type $refresh_token
     * @return null
     */
    protected function getRefreshToken($refresh_token) {
        return $this->getToken($refresh_token, YiiOAuth2::TOKEN_TYPE_REFRESH_TOKEN);
        //$this->setToken($refresh_token, YiiOAuth2::TOKEN_TYPE_REFRESH_TOKEN, $client_id, $expires, '', $scope);
    }
    
    /**
     * Overrides OAuth2::getRefreshToken().
     * 
     * @param type $refresh_token
     * @return null
     */
    protected function setRefreshToken($refresh_token, $client_id, $expires, $scope = NULL) {
        $this->setToken($refresh_token, YiiOAuth2::TOKEN_TYPE_REFRESH_TOKEN, $client_id, $expires, '', $scope);
    }

    /**
     * Overrides OAuth2::getSupportedGrantTypes().
     */
    protected function getSupportedGrantTypes() {
        return array(
            OAUTH2_GRANT_TYPE_AUTH_CODE,
            OAUTH2_GRANT_TYPE_USER_CREDENTIALS,
            OAUTH2_GRANT_TYPE_REFRESH_TOKEN,
        );
    }

    /**
     * Overrides OAuth2::getAuthCode().
     */
    protected function getAuthCode($code) {

        return $this->getToken($code, YiiOAuth2::TOKEN_TYPE_CODE);
    }

    /**
     * Overrides OAuth2::setAuthCode().
     */
    protected function setAuthCode($code, $client_id, $redirect_uri, $expires, $scope = NULL) {
        $this->setToken($code, YiiOAuth2::TOKEN_TYPE_CODE, $client_id, $expires, $redirect_uri, $scope);
    }

    /**
     * Customized function. It checks out whether param credentials are valid or not.
     * 
     * @param string $client_id THe client id associated to email and password
     * @param string $username The email of the current user trying to log in
     * @param string $password The credentials password
     * @return boolean If the current params are actually valid
     */
    public function checkUserCredentials($client_id, $username, $password) { 

        // username is an email address now
        $this->_identity = new UserIdentity($username, $password);
        $this->_identity->authenticate($username, true);
        
        if ( $this->_identity->errorCode === UserIdentity::ERROR_NONE ) {
            // deleted from default version
            Yii::app()->user->login($this->_identity, 0);
            $user_id = Yii::app()->user->id;
            $this->setVariable("user_id", $user_id);
            return UserIdentity::ERROR_NONE;
        } elseif ( $this->_identity->errorCode === UserIdentity::ERROR_PASSWORD_INVALID ) {
            return UserIdentity::ERROR_PASSWORD_INVALID;
        } elseif ( $this->_identity->errorCode === UserIdentity::ERROR_USERNAME_INVALID ) {
            return UserIdentity::ERROR_USERNAME_INVALID;
        }
        
    }

    
    public function redirectToTokenEndpoint($is_authorized, $params = array()) {
        
        $params += array(
          'scope' => NULL,
          'state' => NULL,
        );

        extract($params);

        if ($state !== NULL)
          $result["query"]["state"] = $state;

        if ($is_authorized === FALSE) {
          $result["query"]["error"] = OAUTH2_ERROR_USER_DENIED;
        }
        else {
          if ($response_type == OAUTH2_AUTH_RESPONSE_TYPE_AUTH_CODE || $response_type == OAUTH2_AUTH_RESPONSE_TYPE_CODE_AND_TOKEN)
            $result["query"]["code"] = $this->createAuthCode($client_id, $redirect_uri, $scope);

          if ($response_type == OAUTH2_AUTH_RESPONSE_TYPE_ACCESS_TOKEN || $response_type == OAUTH2_AUTH_RESPONSE_TYPE_CODE_AND_TOKEN) {
              //echo "not generating token"; die;
            $result["fragment"] = $this->createAccessToken($client_id, $scope);
          }
        }
        
        if ($response_type == OAUTH2_AUTH_RESPONSE_TYPE_AUTH_CODE) {

            $serverName = $_SERVER['SERVER_NAME'];

            if( $serverName == 'li674-168.members.linode.com' )
                $ssl = 'https://';
            else if( !preg_match( '(ec2-([0-9]{2,3})-([0-9]{2,3})-([0-9]{2,3})-([0-9]{2,3})\.(.*)*)', $_SERVER['SERVER_NAME'], $matches) ) {

                $ssl = 'http://';
            } else {
                $ssl = 'http://';
                $serverName = $matches[0].':8080' ;
            }

			//$curl = curl_init($ssl . $serverName . "/token");
            //curl_setopt($curl, CURLOPT_POST, true);
            
            $user_id = $this->getVariable('user_id');
            $tokenParams = array(
                'client_id' => $params['client_id'],
                'redirect_uri' => $params['redirect_uri'],
                'user_id' => $user_id,
                'code' => $result["query"]["code"], // The code from the previous request
                'grant_type' => "authorization_code",
            );        

            $cc = new ComController(0);
            $response = $cc->woopost("/token", $tokenParams, false, true );

            header("Content-Type: application/json");
            header("Cache-Control: no-store");

            print_r( $response );
        }
    }
    

    private function doRedirectUriCallback($redirect_uri, $params) {
        header("HTTP/1.1 ". OAUTH2_HTTP_FOUND);
        header("Location: " . $this->buildUri($redirect_uri, $params));
        exit;
    }
    
    
    private function buildUri($uri, $params) {
        $parse_url = parse_url($uri);

        // Add our params to the parsed uri
        foreach ($params as $k => $v) {
          if (isset($parse_url[$k]))
            $parse_url[$k] .= "&" . http_build_query($v);
          else
            $parse_url[$k] = http_build_query($v);
        }

        // Put humpty dumpty back together
        $url =
          ((isset($parse_url["scheme"])) ? $parse_url["scheme"] . "://" : "")
          . ((isset($parse_url["user"])) ? $parse_url["user"] . ((isset($parse_url["pass"])) ? ":" . $parse_url["pass"] : "") . "@" : "")
          . ((isset($parse_url["host"])) ? $parse_url["host"] : "")
          . ((isset($parse_url["port"])) ? ":" . $parse_url["port"] : "")
          . ((isset($parse_url["path"])) ? $parse_url["path"] : "")
          . ((isset($parse_url["query"])) ? "?" . $parse_url["query"] : "");
          //. ((isset($parse_url["fragment"])) ? "#" . $parse_url["fragment"] : "");
        // echo $url; die;
        return $url;
    }
    
    
    private function getToken($oauth_token, $token_type = YiiOAuth2::TOKEN_TYPE_ACCESS_TOKEN) {
        try {
            //echo $oauth_token; 
            //echo ' ' . $token_type;
            
            $c = new EMongoCriteria;
            $c->addCondition( 'oauth_token', $oauth_token ) ;
            $c->addCondition( 'token_type', $token_type ) ;

            //$c->oauth_token = $oauth_token;
            //$c->token_type = $token_type;

            $result = Oauth2Token::model()->findOne($c);

            //print_r( $result );

            return ($result !== FALSE) ? $result : NULL;
        } catch (Exception $e) {
            $this->handleException($e);
        }
    }
    
    /**
     * Creates a new token or updates the existing one in case it has the same auth-type inside authorization token's table
     * 
     * @param string    $oauth_token
     * @param string    $token_type [code] [token] [code & token]
     * @param string    $client_id The id of the current user
     * @param integer   $expires The TTL for the created access token
     * @param string    $redirect_url The uri for the redirect
     * @param string    $scope 
     * 
     */
    private function setToken($oauth_token, $token_type, $client_id, $expires, $redirect_url = 'oob', $scope = '') {
        $user_id = $this->getVariable('user_id');
        
        /**
         * We have an access token
         */
        try {
            $c = new EMongoCriteria;
            $c->addCondition( 'user_id', $user_id ) ;
            $c->addCondition( 'token_type', $token_type ) ;
            //$c->user_id = $user_id;
            //$c->token_type = $token_type;
            $result = Oauth2Token::model()->findOne($c);
            // Check whether the access token was already for current user
            if( $result ){
                $result->oauth_token=$oauth_token;
                $result->expires = $expires;
                $result->token_type = $token_type;
                $result->scope = $scope;
                $result->redirect_uri = $redirect_url;
                $result->update( array('oauth_token', 'expires', 'token_type', 'scope', 'redirect_uri'), true );
            } else {
                $client = new Oauth2Token();
                $client->oauth_token = $oauth_token;
                $client->client_id = $client_id;
                $client->token_type = $token_type;
                $client->expires = $expires;
                $client->user_id = $user_id;
                $client->redirect_uri = $redirect_url;
                $client->scope = $scope;

                //$client->client_secret = md5($client->client_id);
                $client->created_at = time();
                $valid = $client->validate() ? 'yes':'no';
                //echo $user_id; die;
                if($valid == 'yes')
                    $client->save();
            }
        
        } catch (Exception $e) {
            $this->handleException($e);
        }
    }

    /**
     * Loads all the user related data with the oauth2 token provided
     * 
     * @param type $oauth_token
     * @return type
     * @throws OAuth2Exception
     */
    public function loadUser($oauth_token) {
        try {
            $c = new EMongoCriteria;   
            $c->addCondition( 'oauth_token', $oauth_token );     
            $result = Oauth2Token::model()->findOne($c);
            
            if( !$result ) {
                $exceptionParams = array(
                        'code'=>422,
                        'error'=>'Not authorized user.',
                    );
                throw new OAuth2Exception($exceptionParams);
            }

            $user_id = $result->user_id;
            $model = Oauth2User::model()->findByPk( new MongoId($user_id) );
            
            if(!$model){
                $exceptionParams = array(
                        'code'=>422,
                        'error'=>'Not user found.',
                        //'message'=>,
                    );
                
                throw new OAuth2Exception($exceptionParams);
            }

            return $model;
            
        } catch (OAuth2Exception $e) {
            $res = array("error"=> "OAuth2Exception", "message" => $e->getMessage(), "file" => $e->getFile(), "method" => "AppuserController.loadUser()", "line" => $e->getLine());
            echo json_encode($res);
        }
    }
}

?>