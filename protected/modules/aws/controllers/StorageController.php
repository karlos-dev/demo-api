<?php

/**
 * WOO API
 * StorageController
 *
 * Storage is performed by using AWS S3 buckets
 * Each user has its own bucket. This way it's quite easy to maintain all user related data.
 * Bucket identifiers are not related to any persistent db storage id. Identifiers for buckets are obtained from information provided by the user in registration time.
 *
 * StorageController works using an S3Wrapper implementation which will be loaded only once into server memory as a unique static instance of itself.
 * 
 * @author      Karlos Álvarez <karlos.alvan@gmail.com>
 * @abstract    Library implementing AWS S3 Bucket storage system
 * @uses        S3Wrapper
 * @uses        S3
 * 
 */


class StorageController extends S3Wrapper {

    static private $bucketInstance = false;
    public $bucketName = 'wooimg';
    public $prodFirmwareBucketName = 'prod-firmware-builds';
    public $testFirmwareBucketName = 'test-firmware-builds';
    public $firmwareBucketName = 'firmware-builds';


    public function init() {
        self::instance();
        echo "StorageController was launched!";
    }

    /**
     * Checks if the unique static instance of the S3Wrapper has been created, and returs it for its usage
     * @return S3Wrapper Wrapper with necessary actions to work with S3 buckets
     */
    public static function instance() {
        if (StorageController::$bucketInstance === false) {
            StorageController::$bucketInstance = new S3Wrapper();
        }
        return StorageController::$bucketInstance;
    }

    /**
     * [checkBucketIntegrity description]
     * @param  [type] $name [description]
     * @return [type]       [description]
     */
    public function checkBucketIntegrity( $bucketName ) {  
        $s3 = $this->instance();
        $bucketArray = $s3->listBuckets();
        //$bucketIsPresent = false;
        foreach ($bucketArray as $name) {
            if( $name == $bucketName ) {
                //$bucketIsPresent = true;
                //break;
                return true;
            }
        }

        return false;

        //if( !$bucketIsPresent )
        //    throw new Exception( "No AWS bucket present" );
    }

    /**
     * This method uploads an image file into its belonging bucket.
     * In case no bucket is found with $bucketName, it will be created 
     * 
     * @param String $bucketName Hashed name for this bucket
     * @param String $uri Unique route for the $file
     * @param String $file The current system path to the file to be uploaded
     */
    
    //public function addImageToBucket( $bucketName, $uri, $file  ) {
    public function addImageToBucket( $file, $uri ) {
        $s3 = $this->instance();

        if( !$this->checkBucketIntegrity( $this->bucketName ) )  
            mkdir("s3://$this->bucketName");

        if ($s3->putObjectFile($file, $this->bucketName, $uri, S3::ACL_PUBLIC_READ)) 
            return true;

        return false;

            //echo "S3::putObjectFile(): File copied to {$bucketName}/".baseName($uploadFile).PHP_EOL;

            //$object = $s3->getObject($bucketName, baseName($uploadFile));

            // Get the contents of our bucket
            //$contents = $s3->getBucket($bucketName);
        //    echo "S3::getBucket(): Files in bucket {$bucketName}: ".print_r($contents, 1);


            // Get object info
        //    $info = $s3->getObjectInfo($bucketName, baseName($uploadFile));
        //    echo "S3::getObjectInfo(): Info for {$bucketName}/".baseName($uploadFile).': '.print_r($info, 1);

        //    $object = $s3->getObject($bucketName, baseName($uploadFile));
            //var_dump($object);
            //
            //header('Content-Type: image/' . $ext); print_r($object);
    }

    /**
     * This method uploads a firmware tarball file into its belonging bucket.
     * In case no bucket is found in S3, it will be created 
     * 
     * @param String $bucketName Hashed name for this bucket
     * @param String $uri Unique route for the $file
     * @param String $file The current system path to the file to be uploaded
     */
    
    //public function addImageToBucket( $bucketName, $uri, $file  ) {
    public function addFwToBucket( $file, $uri ) {
        $s3 = $this->instance();

        if( !$this->checkBucketIntegrity( $this->firmwareBucketName ) )  
            mkdir("s3://$this->firmwareBucketName");

        if ($s3->putObjectFile($file, $this->firmwareBucketName, $uri, S3::ACL_AUTHENTICATED_READ)) 
            return true;

        return false;
    }    

    /**
     * Creates a temporary lint to a protected resource in Amazon S3
     * Bucketname is the firmware bucket name
     * 
     * @param  String $hwversion The harrware version
     * @param  String $fwversion The firmware version
     */
    public function getLastFirmwareDownload($hwversion=null, $fwversion=null) {

        $timeInSeconds = 200;
        $useSSL=false;

        $this->dir_opendir($hwversion."/".$fwversion, array( "host" => $this->firmwareBucketName ));
        $objArray = array();

        // fill a local array with file object data
        while( false !== ($objName = $this->dir_readdir()) ) {
            $objArray[$objName] = self::getObjectInfo( $this->firmwareBucketName, $this->url['path'] . $objName );
        }

        //parse the latest one uploaded for this hw/fw
        $youngest = 0;
        $yougestObject=null;
        foreach ($objArray as $filename => $obj) {
            if( $obj['time'] > $youngest ) { 
                $yougestObject = $filename;
                $youngest = $obj['time'];
            }
        }

        // get response picture
        $exp = time()+$timeInSeconds;
        $link = $this->getAuthenticatedURL($this->firmwareBucketName, $hwversion."/".$fwversion. $yougestObject, $timeInSeconds, false, $useSSL );
        // echo "The yougest file is " . $yougestObject;

        echo json_encode( array(
            "link"=> $link,
            "expires"=> $exp
        ), JSON_UNESCAPED_SLASHES );

    }

    /**
     * [renameBucket description]
     * @param  [type] $oldNAme [description]
     * @param  [type] $newName [description]
     * @return [type]          [description]
     */
    public function renameBucket($oldName, $newName){
        $s3 = $this->instance();

        $contents = $s3->getBucket($oldName);
        echo "S3::getBucket(): Files in bucket {$bucketName}: ".print_r($contents, 1);
        
    }



}