<?php

/**
 *  @author Karlos Álvarez
 *
 *  Modified Curl parallel request laucher to meet Woo system needs
 * 
 *  Thanks to Josh Fraser 
 *  @link http://www.onlineaspect.com/
 *
 * ADMIN FEATURES
 *    
 */

function rolling_curl($urls, $callback, $custom_options = null) {

    // make sure the rolling window isn't greater than the # of urls
    $rolling_window = 10;
    $rolling_window = (sizeof($urls) < $rolling_window) ? sizeof($urls) : $rolling_window;

    $master = curl_multi_init();
    $curl_arr = array();

    // add additional curl options here
    $std_options = array(CURLOPT_RETURNTRANSFER => true,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_MAXREDIRS => 5);
    $options = ($custom_options) ? ($std_options + $custom_options) : $std_options;

    // start the first batch of requests
    for ($i = 0; $i < $rolling_window; $i++) {
        $ch = curl_init();
        $options[CURLOPT_URL] = $urls[$i]['url'];

        switch ($urls[$i]['type']) {
            case 'GET':
                $options[CURLOPT_HTTPGET] = true;
                //curl_setopt($ch[$key], CURLOPT_HTTPGET, true);
            break;

            case 'POST':
                $options[CURLOPT_POST] = true;
                $options[CURLOPT_POSTFIELDS] = $urls[$i]['postFields'];
                //curl_setopt($ch[$key], CURLOPT_POST, true);
                //curl_setopt($ch[$key], CURLOPT_POSTFIELDS, $val['postFields']);
            break;   

            case 'DELETE':    
                $options[CURLOPT_CUSTOMREQUEST] = "DELETE";
                //curl_setopt($ch[$key], CURLOPT_CUSTOMREQUEST, "DELETE");
            break;     
        }

        curl_setopt_array($ch,$options);
        curl_multi_add_handle($master, $ch);
    }

    $i=0;

    do {
        while(($execrun = curl_multi_exec($master, $running)) == CURLM_CALL_MULTI_PERFORM);
        if($execrun != CURLM_OK)
            break;
        // a request was just completed -- find out which one
        while($done = curl_multi_info_read($master)) {

            $info = curl_getinfo($done['handle']);
            if ($info['http_code'] == 200 || $info['http_code'] == 201 )  {
                $output = curl_multi_getcontent($done['handle']);

                // request successful.  process output using the callback function.
                $callback($output, $info);
                if( $i < sizeof($urls)-1) {
                    // start a new request (it's important to do this before removing the old one)
                    $ch = curl_init();
                    $options[CURLOPT_URL] = $urls[$i++]['url'];  // increment i
                    curl_setopt_array($ch,$options);
                    curl_multi_add_handle($master, $ch);
                }

                // remove the curl handle that just completed
                curl_multi_remove_handle($master, $done['handle']);
            } else {
                // request failed.  add error handling.
                $output = curl_multi_getcontent($done['handle']);
                $callback($output, $info);
            }
        }
    } while ($running > 0);
    
    curl_multi_close($master);
    return true;
}

?>