<?php 

/**
* Perform parallel cURL request.
*
* @param array $urls Array of URLs to make request.
* @param array $options (Optional) Array of additional cURL options.
* @return mixed Results from the request (if any).
*/

function curlMultiRequest($urls, $options = array()) {
    $ch = array();
    $results = array();
    $obj= array();
    $mh = curl_multi_init();
    
    foreach($urls as $key => $val) {

        $ch[$key] = curl_init();
        if ($options) {
            curl_setopt_array($ch[$key], $options);
        }
        curl_setopt($ch[$key], CURLOPT_URL, $val['url']);
        curl_setopt($ch[$key], CURLOPT_RETURNTRANSFER, true);

        $obj[$key]['url'] = $val['url'];

        switch ($val['type']) {
            case 'GET':
                curl_setopt($ch[$key], CURLOPT_HTTPGET, true);
            break;

            case 'POST':
                curl_setopt($ch[$key], CURLOPT_POST, true);
                curl_setopt($ch[$key], CURLOPT_POSTFIELDS, $val['postFields']);
            break;   

            case 'DELETE':    
                curl_setopt($ch[$key], CURLOPT_CUSTOMREQUEST, "DELETE");
            break;     
        }

        curl_multi_add_handle($mh, $ch[$key]);
    }

    $running = null;
    do {
        curl_multi_exec($mh, $running);
        curl_multi_select($mh);
    }
    while ($running > 0);
    // Get content and remove handles.
    foreach ($ch as $key => $val) {

        $obj[$key]['total_time'] = curl_getinfo( $val, CURLINFO_TOTAL_TIME);
        $obj[$key]['response_code'] = curl_getinfo( $val, CURLINFO_HTTP_CODE); 
        $obj[$key]['connection_time'] = curl_getinfo( $val, CURLINFO_CONNECT_TIME);
        $obj[$key]['bytes_transfered'] = curl_getinfo( $val, CURLINFO_SIZE_DOWNLOAD);
        
        $obj[$key]['result'] = curl_multi_getcontent($val);
        $results[$key] = $obj[$key];
        
        //$results[$key] = curl_multi_getcontent($val);

        curl_multi_remove_handle($mh, $val);
    }
    curl_multi_close($mh);

    return json_encode( $results );
}

?>