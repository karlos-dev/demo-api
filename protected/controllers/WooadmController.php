<?php
/**
 * Kinematiq API
 * WooadmController
 * 
 * @author      Karlos Álvarez <karlos.alvan@gmail.com>
 * @abstract    Library implementing all API admin related actions 
 * 
 * @uses        OAuth2Exception
 * @uses        MongoException
 * @uses        Exception
 * 
 * @deprecated
 * 
 */


class WooadmController extends ComController { 

    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout='//layouts/column2';

    /**
     * Why not to avoid unregistered users to come in.
     */
    public function init(){

        if( Yii::app()->user->isGuest ) {
            $this->render('index');
            Yii::app()->end();
        }
    }

    public function actionIndex() {
        $this->render('index');
    }

    /**
     * Shows the API server information
     */
    public function actionInfo() {
        
        if( Yii::app()->user->isGuest ) { 
            $this->render('index');
            Yii::app()->end();      
        }
        try {
            $file = '/var/log/deploy/deployment.csv';
            if( is_file($file) )
                $fd = fopen($file, 'r+');
            if( $fd ) {
                $mylastline = '';
                while( !feof($fd) ) {
                    $line = fgets($fd);
                    if( $line ){
                        $mylastline = $line;
                    }
                }   

                $linePieces = explode(' ', $mylastline);
            }

            

            /** 
             *   [0] deployment date
             *   [1] deployment branch
             *   [2] deployment commit id
             *   [3] deployment db name
             *   [4] deployment server(0)
             */

            $v = Yii::app()->params['version'];
            $db = Yii::app()->mongodb->db;
            $dbc = Yii::app()->mongodb->server;
            $redis_ip = Yii::app()->redis->hostname;

            if( isset($linePieces) ) {
                $date = $linePieces[0];
                $commitBranch = $linePieces[1];
                $commitUrl = "https://bitbucket.org/karlos-dev/oauth2/commits/" . $linePieces[2];
            }

            //echo $commitBranch;
            //print_r( $linePieces ); die;


            $pars = explode('@', $dbc );
            $purename = explode('/', $pars[1] );

            $data = array( 
                'v' => $v,
                'dbn' => $db,
                'dbc' => $purename[0],
                'rdip' => $redis_ip,
                'cbranch' => $commitBranch,
                'curl' => $commitUrl,
                'cdate' => $date
            );

        } catch( Exception $e ) {
            echo "<pre>Error: " . $e->getMessage() . "\n</pre>";
            $data = array( 
                'v' => $v,
                'dbn' => $db,
                'dbc' => $purename[0],
                'rdip' => $redis_ip,
                'cbranch' => "?",
                'curl' => "?",
                'cdate' => "?"
            );
        }

        $this->render('info', $data);
    }

    
    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUserupdate($id) {

        $model = Oauth2User::model()->findByPk( new MongoId($id) );
        $model->IsNewRecord = false; 
           
        $this->render('userupdate',array(
            'model'=>$model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionSessionupdate($id) {

        $model = Session::model()->findByPk( new MongoId($id) );
        $model->IsNewRecord = false; 
           
        $this->render('sessionupdate',array(
            'model'=>$model,
        ));
    }

    /**
     * Deletes a session 
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionSessiondelete($id) {

        echo "Deleting session..."; 

        $result = Session::model()->findByPk( new MongoId($id) );

        echo $result->name; die;

        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));


        if( !$result )
            throw new OAuth2Exception( array('code' => BAD_SESSION_ID, 'message' => "Session unexistent or already deleted") );

        $user = Oauth2User::model()->findByPk( $result->_user );
        if( !$user )
            throw new OAuth2Exception( array('code' => UNKNOWN_OAUTH_TOKEN, 'message' => "No valid user id provided.") );

        // The user is an admin user
        //if( (string) $user->_id !== (string) $result->_user )
        //    throw new OAuth2Exception( array('code' => BAD_USER_ID, 'message' => "This user cannot delete this session") );
        $sc = new SessionController(0);

        $spot = Spot::model()->findByPk( $result->_spot );
        $n = $sc->numberOfSessionsInSpot($spot->_id, $user->_id);

        $spot->updateSpot($n, $user, $result );                    
        $user->removeSessionData($result);

        // insert parse the user sessions to redo the index
        // get best session for airtime, height y gforce
        // add info to rank structure

        $result->IsNewRecord = false;
        if( !$result->delete() )
            throw new OAuth2Exception( array('code' => UNKNOWN_DB_ERROR, 'message' => "Cannot delete session") );
    }

    /**
     * Deletes a user 
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUserdelete($id) {

        echo "Deleting user..."; die;

        $user = Oauth2User::model()->findByPk( new MongoId($id) );
        if( !$user )
            throw new OAuth2Exception( array('code' => UNKNOWN_OAUTH_TOKEN, 'message' => "No valid user id provided.") );

        $c = new EMongoCriteria();
        $c->_user = $user->_id;
        $sessions = Session::model()->findAll( $c );

        //if( !$sessions )
        //    throw new OAuth2Exception( array('code' => BAD_SESSION_ID, 'message' => "Session unexistent or already deleted") );

        $c = new EMongoCriteria();
        $c->_user = $user->_id;
        $ranks = Rank::model()->findAll( $c );
        // if( !$ranks )
            

        /*
        // The user is an admin user
        //if( (string) $user->_id !== (string) $result->_user )
        //    throw new OAuth2Exception( array('code' => BAD_USER_ID, 'message' => "This user cannot delete this session") );
        $sc = new SessionController(0);

        $spot = Spot::model()->findByPk( $result->_spot );
        $n = $sc->numberOfSessionsInSpot($spot->_id, $user->_id);

        $spot->updateSpot($n, $user, $result );                    
        $user->removeSessionData( $result );

        // insert parse the user sessions to redo the index
        // get best session for airtime, height y gforce
        // add info to rank structure

        $result->IsNewRecord = false;
        if( !$result->delete() )
            throw new OAuth2Exception( array('code' => UNKNOWN_DB_ERROR, 'message' => "Cannot delete session") );
        */
    }

    /**
     * Display information about a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionSessionview($id) { 

        $model = Session::model()->findByPk( new MongoId($id) );
        $this->render('sessionview',array(
            'model'=>$model,
        ));
    }

    /**
     * Display information about a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionUserview($id) { 

        $model = Oauth2User::model()->findByPk( new MongoId($id) );

        $c = new EMongoCriteria();
        $c->_user = new MongoId($id);
        $n = Session::model()->count($c);


        $this->render('userview',array(
            'model'=>$model,
            'total'=>$n,
        ));
    }    
};