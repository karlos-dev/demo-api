<?php

class SiteController extends ComController
{
    /**
	 * Declares class-based actions.
	 */
    public function init(){
        parent::init();
    }
    
	/**
	 * Declares class-based actions.
	 */
	public function actions() {

        $controller = Yii::app()->controller->id;
        $action = isset(Yii::app()->controller->action->id) ? Yii::app()->controller->action->id : 'init';
        
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex() {
		$this->render('index');
        
        $start = round(microtime(true) * 1000);        
        $controller = Yii::app()->controller->id;
        $action = isset(Yii::app()->controller->action->id) ? Yii::app()->controller->action->id : 'init';
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError() {
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact() {
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
        
        $start = round(microtime(true) * 1000);        
        $controller = Yii::app()->controller->id;
        $action = isset(Yii::app()->controller->action->id) ? Yii::app()->controller->action->id : 'init';
	}
    
	/**
	 * Displays the login page
	 */
	public function actionLogin() {

		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if( isset($_POST['LoginForm']) ) {
            
            $user = isset($_POST['LoginForm']['email']) ? $_POST['LoginForm']['email'] : '';
            $pass = isset($_POST['LoginForm']['password']) ? $_POST['LoginForm']['password'] : '';
            $rememberMe = isset($_POST['LoginForm']['rememberMe']) ? $_POST['LoginForm']['rememberMe'] : false;

            $model->attributes=$_POST['LoginForm'];
            $model->password=openssl_digest($pass, 'sha512');
            $identity=new UserIdentity($user, $model->password);
			
			// validate user input and redirect to the previous page if valid
            // Login method
            //  - authenticates
            //  - adds identity to model
            
            if( $model->login() ) {
                $this->redirect("/site/index");
            }
		} 

		// display the login form
		$this->render('login',array('model'=>$model));
	}
    
	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout() {
		Yii::app()->user->logout();
        $start = round(microtime(true) * 1000);        
        $controller = Yii::app()->controller->id;
        $action = isset(Yii::app()->controller->action->id) ? Yii::app()->controller->action->id : 'init';
		$this->redirect(Yii::app()->homeUrl);
	}
    
    
    public function actionResetPassword( $id ){
        $form  = new ResetForm();
        /**
         * @abstract When the website would be placed within another differenct domain or server
         * the communication cannot use models within oauth module which is the REST API itself.
         * 
         * We should modify all oauth2 methods to run using different server
         */
        $c = new EMongoCriteria;        
        $c->addCondition( 'code', $id );
        $resetCode = PassCode::model()->findOne($c);

        if( !$resetCode )
            $mode = 'alreadyused';
        
        else {    
            if( time() < $resetCode->expires )
                $mode = 'active';
            else
                $mode = 'expired';
            
            $form->email = $resetCode->user;
        }
        
        if( isset( $_POST['ResetForm'] ) ){
            
            $c = new EMongoCriteria;
            $c->addCondition( 'email', $_POST['ResetForm']['email'] );
            $user = Oauth2User::model()->findOne($c);

            if( !$user ) 
                $mode = 'error';
            
            $user->password = openssl_digest($_POST['ResetForm']['password'], 'sha512');
            if ( $user->update(array( 'password', true )) ){
                
                $c = new EMongoCriteria;
                $c->addCondition( 'user', $_POST['ResetForm']['email'] );

                $resetCode = PassCode::model()->deleteAll($c);
                $mode = 'updated';
            }
        }
        
        $this->render('reset', array(
            'mode'=>$mode,
            'model'=>$form, 
            'resetCode'=>$id,
        ));
    }

    /**
     * @updated MongoClient
     * @since 0.8.5
     *
     * @version  1.3.2 Added trialEnd set to 0 when activating account.
     * 
     */
    public function actionActivate( $id ){

        $c = new EMongoCriteria;        
        $c->addCondition( 'code', $id );
        $resetCode = PassCode::model()->findOne($c);

        if( !$resetCode )
            $mode = 'alreadyused';
        else {    
            if( time() < $resetCode->expires )
                $mode = 'active';
            else
                $mode = 'expired';

            $c = new EMongoCriteria;
            $c->addCondition( 'email', $resetCode->user );
            $user = Oauth2User::model()->findOne($c);

            if( !$user ) 
                $mode = 'error';
            
            $user->active = true;
            $user->trialEnd = 0;

            if ( $user->update() ){
                
                $c = new EMongoCriteria;
                $c->addCondition( 'user', $resetCode->user );
                $c->addCondition( 'code', $id );

                $resetCode = PassCode::model()->deleteAll($c);
                $mode = 'updated';

                $auc = new AppuserController(0);
                $auc->sendAccountactiveMail($user);
            }
        }

        
        $this->render('activate', array(
            'mode'=>$mode,
        ));
    }
    

    /**
     *  Requires Login to Show the documentation fo the API
     *  Documentation is written using raml language in doc/api-doc/api-definition/woo.raml
     * 
     * @version 1.3.2   Set as Password protected 
     * 
     * @deprecated
     */
    
    public function actionDoc() {

        $model=new LoginForm;

        if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        $docFile = dirname(__FILE__) . 
            DIRECTORY_SEPARATOR .'..'. DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'doc' . DIRECTORY_SEPARATOR . 'api-doc' . 
            DIRECTORY_SEPARATOR . 'index.html';

        if( isset($_POST['LoginForm']) ) {
            $user = isset($_POST['LoginForm']['email']) ? $_POST['LoginForm']['email'] : '';
            $pass = isset($_POST['LoginForm']['password']) ? $_POST['LoginForm']['password'] : '';
            $rememberMe = isset($_POST['LoginForm']['rememberMe']) ? $_POST['LoginForm']['rememberMe'] : false;
            
            $model->attributes=$_POST['LoginForm'];
            $model->password=openssl_digest($pass, 'sha512');

            if( $model->validate() && $model->login() ) {

                if ( file_exists($docFile) ) {
                    $content = file_get_contents($docFile);
                    echo $content; 
                    Yii::app()->end();
                }
            }
            
        } else if ( !Yii::app()->user->isGuest ) {
                if ( file_exists($docFile) ) {
                    $content = file_get_contents($docFile);
                    echo $content; 
                    Yii::app()->end();
                }
        }

        $this->render('login',array('model'=>$model));
    }
    
}