<?php

/**
 *  WOO Sports  KITE REST API
 *  January 2016
 *  
 *  @author     Karlos Alvarez <karlos.alvan@gmail.com>
 *  @abstract   Redis Cache management 
 *              This controller provides utils to other controllers which need Redis operations in their logic.
 *              Initially thought to manage Leaderboards and Activity feed 
 *
 *          // Php Redis Lib
 *          https://github.com/phpredis/phpredis#sorted-sets
 *
 *          // Server install
 *          http://blog.railsupgrade.com/2011/08/install-redis-server-on-ubuntu-maunally.html
 *
 *          // Secure redis
 *          https://www.stunnel.org/index.html
 *
 *          // Set data folder for redis
 *          http://stackoverflow.com/questions/19581059/misconf-redis-is-configured-to-save-rdb-snapshots
 *          - CONFIG SET dir /etc/redis/data
 *          - CONFIG SET dbfilename woo.rdb
 *
 *          // Solve Redis Problems with Memory
 *          https://pydelion.com/2013/05/27/redis-cant-save-in-background-fork-cannot-allocate-memory/
 *
 *          // Redis Stop working
 *          // Server MODIFICATIONS
 *          http://stackoverflow.com/questions/14460528/redis-periodicly-stops-responding-on-high-load
 *
 *          // AWS Instance set up
 *          https://redislabs.com/blog/5-tips-for-running-redis-over-aws#.VryOe1QrJhE
 *          
 *                      
 */

class RedisController extends KController { 

    private static $LBFEATURES;
    private static $SESSIONFEATURES;
    private static $managerActions = array( "index", "test", "get", "see"); // "drop"); //drop

    /**
     *  $REDIS If the cache is enabled
     *  
     *  Redis Yii extension: 
     *  @link https://github.com/phpnode/YiiRedis
     *
     *  @since 1.4.1
     *  
     *  @var boolean
     */
    public static $REDIS;
    public static $rediscli;

    /**
     * Execution time helper $start
     * @var float
     */
    private $start;


    /**
     *  Init Base Controller
     *  Check the REDIS cache configuration
     * 
     *  @since 1.4.1
     *  
     */
    public function init() {
        self::$LBFEATURES =         array( "highestAir", "maxAirTime" );
        self::$SESSIONFEATURES =    array( "activity" );
        self::$REDIS = Yii::app()->params['redis_enabled'];

        if( self::$REDIS )
            if( !Yii::app()->redis->getClient(true) )  {
                echo "Redis cache not found\n";
                Yii::app()->end();
            } else {
                if( !self::$rediscli )
                    self::$rediscli = Yii::app()->redis->getClient(true);
                    
            }
        // else echo "Redis cache disabled\n";

        // self::$rediscli = Yii::app()->redis->getClient(true);
        parent::init();
    }


    public function filters() {
        return array(
            'accessControl',
            // 'postOnly + edit, create, upload',
            /*
            array(
                'application.filters.PerformanceFilter - edit, create',
                'unit'=>'second',
            ),
            */
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('load'),
                'users' => array('*'),
                //'expression'=> 'Yii::app()->user->isGuest',
            ),
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => self::$managerActions,
                'users' => array('@'),
                //'expression'=> 'Yii::app()->user->isGuest',
            ),
            array('deny', // deny all users
                'users' => array('*'),
            )
        );
    }


    public function getLb( $lbname ) { 
        $feature = filter_var( $lbname, FILTER_VALIDATE_REGEXP, array( 'options' => array("regexp"=>"/(maxAirTime)|(highestAir)|(activity)/i") ) );

        if( !$feature ){
            Yii::log("Redis::Not valid $feature","info");
            return;
        }

        if( in_array($feature, self::$LBFEATURES ) ) {
            $word = "global_lb_";
        } elseif( in_array($feature, self::$SESSIONFEATURES ) ) {
            $word = "session_";
        }

        $sortedSet = new ARedisSortedSet("{$word}{$feature}");

        if( $sortedSet ) {
            if( $sortedSet->getCount() == 0 ) {
                Yii::log("Redis::{$word}{$feature} empty", 'warning');
                return null;
            }

            return $sortedSet;

        } else 
            return null;
    }

    /**
     * Loads the cache content
     * @version 1.4.1
     * 
     * @param  string  $id    The cache identifier
     * @param  boolean $debug If the request is performed from console it provides output
     * 
     * @return ARedisSortedSet  The cached content
     */
    public function load( $id, $debug=false) {
        ini_set('max_execution_time', 240);

        $start = microtime(true);
        $feature = filter_var( $id, FILTER_VALIDATE_REGEXP, array( 'options' => array("regexp"=>"/(maxAirTime)|(highestAir)|(activity)/i") ) );

        if( !$feature ){
            Yii::log("Redis::Not valid $feature","info");
            return;
        }

        $modeLB=false;
        if( in_array($feature, self::$LBFEATURES ) ) {
            $word = "global_lb_";
            $modeLB = true;
        } elseif( in_array($feature, self::$SESSIONFEATURES ) ) {
            $word = "session_";
            $modeLB = false;
        }

        $feed = 50;
        $sortedSet = new ARedisSortedSet("{$word}{$feature}");

        if( $debug )
            echo "count " . $sortedSet->getCount() . "\n"; // = self::$rediscli->get( "global_lb" );

        $exit = false;
        $loopSize = 100;
        $offset = 0;
        $minHeight = 5;

        $c = new EMongoCriteria();
        $c->setLimit( $loopSize );

        if( $modeLB ) {
            $c->addCondition( "feature", $feature );
            $it = Rank::model()->find( $c );
        } else {
            // $c->addCondition( "feature", $feature );
            $c->compare('highestAir', '>' . $minHeight );
            $it = Session::model()->find( $c );
        }

        $count = $it->count();

        while( !$exit ) {
            if( $it->count(true) < $loopSize )
                $exit = true;

            while( $it->hasNext() ) {
                $a   =  $it->getNext();     
                if( $modeLB )           
                    $obj = (object) $a;
                else
                    $obj = (object) $a->attributes;

                // echo json_encode( $obj ); 
                // print_r( $obj ); die;

                $score = ( $modeLB ) ? $obj->score : $obj->time;
                $sortedSet->add( json_encode($obj), $score );
                
                if( $feed <= 0 && $debug ) {
                    $feed = 50;
                    $n = $sortedSet->getCount();
                    echo "added ($n/$count)\n";
                    ob_flush();
                    flush();
                }
                $feed-=1;
            }

            if( !$exit ) {
                $c = new EMongoCriteria();
                $c->setLimit( $loopSize );
                $c->setSkip( $offset + $loopSize );

                if( $modeLB ) {
                    $c->addCondition( "feature", $feature );
                    $it = Rank::model()->find( $c );
                } else {
                    $c->compare('highestAir', '>' . $minHeight );
                    $it = Session::model()->find( $c );
                }
                $offset += $loopSize;
            }
        }
        
        if( YII_DEBUG ) {
            $end = microtime(true) - $this->start;
            echo "loaded " . $count . " elements in $end seconds\n"; // = self::$rediscli->get( "global_lb" );
        }
        return $sortedSet;
    }


    public function add( $id, $item ){
        $start = microtime(true);

        $feature = filter_var( $id, FILTER_VALIDATE_REGEXP, array( 'options' => array("regexp"=>"/(maxAirTime)|(highestAir)|(activity)/i") ) );
        if( !$feature ) {
            Yii::log("Redis::Not valid $feature","info");
            return;
        }

        $modeLB=false;

        if( in_array($feature, self::$LBFEATURES ) ) {
            $word = "global_lb_";
            $modeLB = true;
            $obj =  $item;
        } elseif( in_array($feature, self::$SESSIONFEATURES ) ) {
            $word = "session_";
            $modeLB = false;
            $obj =  $item->attributes;
        }

        $sortedSet = new ARedisSortedSet("{$word}{$feature}");

        $n_before = $sortedSet->getCount();

        // $score = $item->score;
        $score = ( $modeLB ) ? $item->score : $item->time;
        
        if( !$sortedSet->add( json_encode($obj), $score ) ) {
            Yii::log("Redis::Cannot save in sorted Set $feature", "info");
        }

        $n_after = $sortedSet->getCount();
        
        return ($n_before < $n_after);
    }


    public function remove( $id, $item ){
        $start = microtime(true);

        // $feature='highestAir';
    
        $feature = filter_var( $id, FILTER_VALIDATE_REGEXP, array( 'options' => array("regexp"=>"/(maxAirTime)|(highestAir)|(activity)/i") ) );
        if( !$feature ) {
            Yii::log("Redis::Not valid $feature","info");
            return;
        }

        $modeLB=false;

        if( in_array($feature, self::$LBFEATURES ) ) {
            $word = "global_lb_";
            $modeLB = true;
            $obj = $item;
        } elseif( in_array($feature, self::$SESSIONFEATURES ) ) {
            $word = "session_";
            $modeLB = false;
            $obj = $item->attributes;
        }

        $sortedSet = new ARedisSortedSet("{$word}{$feature}");

        $n_before = $sortedSet->getCount();
        $score = ( $modeLB ) ? $item->score : $item->time;

        if( !$sortedSet->remove( json_encode($obj) ) ) {
            Yii::log("Redis::Cannot remove from sorted Set $feature", "info");
        }

        $n_after = $sortedSet->getCount();
        
        return ($n_before > $n_after);
    }


    /**
     * Returns the position of the key inside the sortedSet
     * 
     * @param  String $id   The name of the sorted set
     * @param  String $item The key we want to know the index of
     * @return Integer      The position
     */
    public function getIndex($id, $item) {
        $feature = filter_var( $id, FILTER_VALIDATE_REGEXP, array( 'options' => array("regexp"=>"/(maxAirTime)|(highestAir)|(activity)/i") ) );

        if( !$feature ) {
            Yii::log("Redis::Not valid $feature","info");
            return;
        }

        if( in_array($feature, self::$LBFEATURES ) ) {
            $word = "global_lb_";
            $obj = $item;

        } elseif( in_array($feature, self::$SESSIONFEATURES ) ) {
            $word = "session_";
            $obj = (object) $item->attributes;
        }

        $key = json_encode( $obj );
        $sortedSet = new ARedisSortedSet("{$word}{$feature}");

        return $sortedSet->getRank( $key );
    }


    public function update( $id, $position, $item ) { 
        $feature = filter_var( $id, FILTER_VALIDATE_REGEXP, array( 'options' => array("regexp"=>"/(maxAirTime)|(highestAir)|(activity)/i") ) );

        if( !$feature ) {
            Yii::log("Redis::Not valid $feature","info");
            return;
        }

        if( in_array($feature, self::$LBFEATURES ) ) {
            $word = "global_lb_";
            $obj = $item;

        } elseif( in_array($feature, self::$SESSIONFEATURES ) ) {
            $word = "session_";
            $obj = (object) $item->attributes;
        }

        /*
            $key = json_encode( $obj );
            $sortedSet = new ARedisSortedSet("{$word}{$feature}");
            $sortedSet->offsetSet( $position, $key );
        */

    }

    /**********************************************************************************************************************************************************************************
     *
     *  ACTIONS
     *     
     **********************************************************************************************************************************************************************************/
        
    public function beforeAction( $action ){

        if( !in_array( $action->id, self::$managerActions ) )
            return true;
        else {
            echo "<pre>";
            echo "<div>";
            echo "<a href='/redis/see/activity'>See</a>" . " | <a href='/redis/test'>Test</a>" . " | <a href='/redis/get/111'>Get</a>" . " | <a href='/redis/drop'>Drop</a>"; 
            if( YII_DEBUG )
                $this->start = microtime(true);
            echo "</div>";
            echo "<hr>";
        }

        return true;
    }
   

    public function actionIndex() {

        echo "HELLO RedisController::index()\n";

    }

    /**
     *  Get temporal sortedset
     *     
     */
    public function actionGet($id, $pos) {
        $debug=true;
        $feature = filter_var( $id, FILTER_VALIDATE_REGEXP, array( 'options' => array("regexp"=>"/(maxAirTime)|(highestAir)|(activity)/i") ) );

        if( !$feature ){
            echo "No feature $feature\n";
            return;
        }

        if( $debug ) {
            echo "<pre>";
            echo "HELLO RedisController::get($id)\n";
        }

        if( in_array($feature, self::$LBFEATURES ) ) {
            $word = "global_lb_";
            $modeLB = true;
        } elseif( in_array($feature, self::$SESSIONFEATURES ) ) {
            $word = "session_";
            $modeLB = false;
        }

        //$sortedSet = new ARedisSortedSet("global_lb");
        //$sortedSet->remove("gloabl_lb");
        $sortedSet = new ARedisSortedSet("{$word}{$feature}");
        //$sortedSet->getData();

        if( $sortedSet->offsetExists($pos) ) {
            
            $range = $sortedSet->getRange($pos, $pos+1, -1);
            $i = $pos;
            foreach($range as $key => $score) {
                echo "<a href='/redis/get/$i'>$i</a> " . "<a href='/redis/del/$i'>Delete</a> " . $key.": ".$score."<br />";
                $i+=1;
            }
                // $elem = $sortedSet->offsetGet($id);
            
            //print_r( $elem );

        } else {
            echo "Offset doesn't exist...\n";
        }

        if( YII_DEBUG ) {
            $end = microtime(true) - $this->start;
            echo "loaded " . count($range) . " elements in $end seconds\n"; // = self::$rediscli->get( "global_lb" );
        }
    }

    /**
     *  Check $id cached content
     *     
     */
    public function actionSee($id, $ini=null, $end=null) {
        $debug=true;

        $feature = filter_var( $id, FILTER_VALIDATE_REGEXP, array( 'options' => array("regexp"=>"/(maxAirTime)|(highestAir)|(activity)/i") ) );

        if( !$feature ){
            echo "No feature $feature\n";
            return;
        }

        if( $debug ) {
            echo "<pre>";
            echo "HELLO RedisController::see($feature)\n";
        }

        if( in_array($feature, self::$LBFEATURES ) ) {
            $word = "global_lb_";
            $modeLB = true;
        } elseif( in_array($feature, self::$SESSIONFEATURES ) ) {
            $word = "session_";
            $modeLB = false;
        }


        $sortedSet = new ARedisSortedSet("{$word}{$feature}");

        echo "count " . $sortedSet->getCount() . "\n"; // = self::$rediscli->get( "global_lb" );

        /*
        //$r = new Rank();
        $r->score = 4.63;
        $r->feature = 'airtime';
        $sortedSet->add( json_encode($r), $r->score );

        $r->score = 1.03;
        $r->feature = 'airtime';
        $sortedSet->add( json_encode($r), $r->score );

        $r->score = 0.09;
        $r->feature = 'airtime';
        $sortedSet->add( json_encode($r), $r->score );
        */
        $start = 0;
        $finish=30;

        if( $ini )
            $start = $ini;
        if( $end )
            $finish = $end;
        
        $range = $sortedSet->getRange($start, $finish, -1);
        $i = $start;
        foreach($range as $key => $score) {
            echo "<a href='/redis/get/$feature/$i'>$i</a> " . "<a href='/redis/del/$i'>Delete</a> " . $key.": ".$score."<br />";
            $i+=1;
        }

        if( YII_DEBUG ) {
            $end = microtime(true) - $this->start;
            echo "loaded " . count($range) . " elements in $end seconds\n"; // = self::$rediscli->get( "global_lb" );
        }
    }

    /**
     *  Welcome action
     *  Test
     *     
     */
    public function actionLoad($id) {
        //if( in_array($id, self::$LBFEATURES) )
        $this->load( $id, true );
    }

    /**
     *  Delete redis cache key at pos $id from sortedSet
     *  @abstract
     *
     *  offsetUnset does not work on redis deletion
     *  In order to delete a cached offset, we need to recover the $key of that offset and call 
     *  ARedisSortedSet::remove() method
     *  
     *  Test
     *     
     */
    public function actionDel($id, $offset) {
        $debug=true;

        $feature = filter_var( $id, FILTER_VALIDATE_REGEXP, array( 'options' => array("regexp"=>"/(maxAirTime)|(highestAir)|(activity)/i") ) );

        if( !$feature ){
            echo "No feature $feature\n";
            return;
        }

        if( $debug ) {
            echo "<pre>";
            echo "HELLO RedisController::see($feature)\n";
        }

        if( in_array($feature, self::$LBFEATURES ) ) {
            $word = "global_lb_";
            $modeLB = true;
        } elseif( in_array($feature, self::$SESSIONFEATURES ) ) {
            $word = "session_";
            $modeLB = false;
        }

        $sortedSet = new ARedisSortedSet("{$word}{$feature}");
        
        if( $sortedSet->offsetExists($offset) ) {

            $total = $sortedSet->getCount();
            echo "Before delete $total\n";
            $sortedSet->offsetUnset($offset);

            $range = $sortedSet->getRange($offset, $offset+1, -1);
            foreach($range as $key => $score) { }
            $sortedSet->remove( $key );

            $totalAfter = $sortedSet->getCount();
            echo "After delete $totalAfter\n";
            if( $totalAfter < $total )
                echo "Succesfully deleted $id at $offset\n";

        } else {
            echo "Offset doesn't exist...\n";
        }
    }

    /**
     * Drop the Redis cache
     * 
     */
    public function actionDrop() { 
        //self::$rediscli->del("global_lb_highestAir");
        //self::$rediscli->del("WOO.global_lb_highestAir");
        //self::$rediscli->del("session_activity");
        //self::$rediscli->del("WOO.session_activity");
        //self::$rediscli->del("activity");
        //self::$rediscli->del("global_lb");
        //self::$rediscli->del("key");
        
        // deletes all this db
        self::$rediscli->flushDb();
    }

    /**
     *  See available redis keys
     */
    public function actionTest() {
        echo "HELLO RedisController::test()\n";

        //$sortedSet = new ARedisSortedSet("global_lb");
        //$sortedSet->remove("gloabl_lb");
        //$sortedSet = 
        //$data = $sortedSet->getData();
        // IMPORTANT
        // print_r( array_keys( $data ) );
        
        self::$rediscli = Yii::app()->redis->getClient(true);
         
        // SLOWLOG
        // print_r( self::$rediscli->slowLog('get') );
        
        $keys = self::$rediscli->keys('*');
        foreach ($keys as $key => $value) {
            //echo $value;
            $parts = explode(".", $value);
            //print_r($parts); die;

            $keyparts = explode("_", $parts[1] );
            $name = $keyparts[ count($keyparts)-1 ];

            echo "<a href='/redis/see/$name'>$key</a> $value<br />";
        }

        if( YII_DEBUG ) {
            $end = microtime(true) - $this->start;
            echo "loaded " . count($keys) . " keys in $end seconds\n"; // = self::$rediscli->get( "global_lb" );
        }
    }
}

?>
