<?php

class KController extends ComController {

    private $collections = array();
    private $proyection;

    public function init() {
        parent::init();
    }

    public function findOne( $what, $criteria=array(), $fields = null ) {
        //if( !$this->validateWhat( $what ) )
        //    throw new Exception("No a valid collection", 1);
            
        $object = $what::model()->findOne( $criteria );
        return $object;
    }

    public function findbyPk( $what, $pk, $fields = null ) {
        //if( !$this->validateWhat( $what ) )
        //    throw new Exception("No a valid collection", 1);
        if( $fields )
            $object = $what::model()->findByPk( $pk, $fields );
        else
            $object = $what::model()->findByPk( $pk );
        return $object;
    }

    public function findAll( $what, $criteria=array(), $proyection=null ) {

        //    if( !$this->validateWhat( $what ) )
        //        throw new Exception("No a valid collection", 1);
        if( !$proyection )
            return iterator_to_array( $what::model()->findAll( $criteria ), false);
        
        $criteria->setSelect( $proyection );
        $send=array();
        $items = $what::model()->findAll( $criteria );
        
        if( $proyection )
            foreach ($items as $item) {
                $send[] = (object) array_filter( (array) $item );
            }
        else
            $send = $items;

        return $send;
    }

    public function findAllbyPk( $what, $pkArray=array(), $fields = array() ) {

        //    if( !$this->validateWhat( $what ) )
        //        throw new Exception("No a valid collection", 1);
        $arr = iterator_to_array( $what::model()->findAllByPk( $pkArray, $fields ), false);

        return $arr;
    }

    private function validateWhat( $what ){
        if( in_array( strtolower( $what ), $this->collections ))
            return true;
        return false;
    }
}

?>