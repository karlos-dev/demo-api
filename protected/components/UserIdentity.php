<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
    private $_id;
    const ERROR_INSUFFICIENT_ROLE = 3;
	/**
	 * Authenticates a user.
	 * @return boolean whether authentication succeeds.
	 */
	//public function authenticate()
    public function authenticate() { 
        try {
            // @modified NOW IT WORKS WITH NO USERNAME AT ALL 
            $c = new EMongoCriteria;        
            $c->addCondition( 'email', $this->username );

            //$c->email = urldecode( $this->username );    
            // $dbUserList = Oauth2User::model()->findOne($c, array('email', 'password', 'client_id', 'client_secret', 'redirect_uri') );
            
            $dbUserList = Oauth2User::model()->findOne($c);
            /*
            //$class = new ReflectionClass("Oauth2User");
            $obj = new Oauth2User();
            $refObject   = new ReflectionObject( $obj );
            $refProperty = $refObject->getProperty( 'password' );
            $refProperty->setAccessible( true );
            echo $refProperty->getValue( Oauth2User::model()->findOne($c) ); die;

            $property = $class->getProperty("password");
            $property->setAccessible(true); 
            $property = $class->getProperty("client_id");
            $property->setAccessible(true); 
            $property = $class->getProperty("client_secret");
            $property->setAccessible(true); 
            $property = $class->getProperty("redirect_uri");
            $property->setAccessible(true); 

            $property = $class->getProperty("password");
            $property->setAccessible(true);

            //print_r( $class );
            echo $property->getValue( $obj ); die;
            
            
            print_r( $dbUserList ); die;
            */
            $users= array( "{$this->username}" => $dbUserList['password'] );
            if(!isset($users[$this->username])){
                $this->errorCode=self::ERROR_USERNAME_INVALID; 
                //echo "no user";
            }
            elseif($users[$this->username]!==$this->password){
                $this->errorCode=self::ERROR_PASSWORD_INVALID;
                //echo "no valid pass";
            }
            elseif($dbUserList->role !== 'admin'){
                $this->errorCode=self::ERROR_INSUFFICIENT_ROLE;
                //echo "no valid pass";
            }
            else {
                //print_r( $dbUserList ); die;
                // $record = (object) $users[$this->username];
                $this->_id= $dbUserList->_id;
                $this->setState('title', $dbUserList->role);
                $this->errorCode=self::ERROR_NONE;
                //echo "no error";
            }
            //print_r($this->errorCode); die;
            return !$this->errorCode;
        
        } catch (Exception $e) {
            // Silence
            
            //echo $e->getMessage(); echo $e->getTraceAsString();
        }
	}

    public function getId()
    {
        return $this->_id;
    }
}