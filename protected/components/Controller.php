<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column2';
	
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();

	

	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();


	/**
     * Filters the info provided about the user according the scope of current user
     * @todo Set action based in oauth2 current scope
     * 
     * @param type $result
     * @return type
     */

	public function filter( $data, $user ){
		$clean=array();
		foreach ($data as $key) {
			$key = (object) $key;
			if( $key->tag == 'valid' || $user === 'admin' || $user === 'dev' )
			{
				$clean[] = $key;
			}
		}
		return $clean ;
	}

}