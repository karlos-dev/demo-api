<?php
/* @var $this SiteController */
/* @var $model ResetForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Activate account';
$this->breadcrumbs=array(
    'Activate account',
);

//print_r( $model ); die;
?>



<?php if ( $mode == 'expired' ) { ?>
	<h1>Account activation</h1>
    <p>The activation link you try to use for account activation has expired. Please request a new one by login into your APP</p>
    
<?php } else if ( $mode == 'updated' ) { ?>
	<h1>Account activated</h1>
    <p><b>Congratulations!</b> your account is now activated.</p>
    <p>You will receive a confirmation email shortly.</p>

<?php } else if ( $mode == 'error' ) { ?>
	<h1>Account activation</h1>
    <p>We couldn't determine your account, please request a new link</p>
    
<?php } else if ( $mode == 'alreadyused' ) { ?>
	<h1>Account activation</h1>
    <p>Your account is already active. Get inside the Woo!</p>
    
<?php } else { ?>
    

<?php } ?>