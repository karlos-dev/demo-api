<?php
/* @var $this SiteController */
/* @var $model ResetForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Reset password';
$this->breadcrumbs=array(
	'Reset Password',
);

//print_r( $model ); die;
?>

<h1>Reset Password</h1>

<?php if ( $mode == 'expired' ) { ?>
    <p>The reset link you try to use for password reset has expired. Please request a new one throught your APP</p>
    
<?php } else if ( $mode == 'updated' ) { ?>
    <p>The password has been changed</p>

<?php } else if ( $mode == 'error' ) { ?>
    <p>We couldn't determine your user, please request a new link</p>
    
<?php } else if ( $mode == 'alreadyused' ) { ?>
    <p>You already used this link. Please request a new one throught your APP</p>
    
<?php } else { ?>
    
<p>Please fill out the following form with your new password:</p>
<div class="form">
    <?php 
        $form=$this->beginWidget('CActiveForm', array(
            'id'=>'reset-form',
            'clientOptions'=>array(
                'validateOnSubmit'=>true,
            ),
        )); 
        //$form->attributes['email'] = $model->email;
        echo $form->hiddenField($model,'email');
    ?>
	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password'); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>
    
	<div class="row">
		<?php echo $form->labelEx($model,'confirmpass'); ?>
		<?php echo $form->passwordField($model,'confirmpass'); ?>
		<?php echo $form->error($model,'confirmpass'); ?>
	</div>
    
	<div class="row buttons">
		<?php echo CHtml::submitButton('Reset'); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->

<?php } ?>
