<?php
/* @var $this SiteController */
$action = $this->action->id;

?>
<h1>Woo <?php echo $action; ?></h1>

<?php
if( Yii::app()->user->isGuest ) {
?>
<h3>Please, log in if you want to do anything...</h3>

<?php

    echo "You are connecting from: <br><br>"; 
    echo "IPv4 address <b>{$_SERVER['REMOTE_ADDR']}</b><br>";   
    echo "Using: <b>{$_SERVER['HTTP_USER_AGENT']}<br><br>";
    echo "This visit will not be logged";
   // echo "<a href='/site/createspots'>create spots</a>";
    

} else { 

  $spots = Spot::model()->findAll();
  $res = array( array('Spot', 'Number of sessions' ) );
  foreach ($spots as $spot) {
      $res[] = array( $spot->name, $spot->totalSessions );
  }
  $json = json_encode($res);

echo "<div>{$json}</div>"; 

?>

<div>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var array = JSON.parse( '<?php echo $json; ?>');
        console.dir(array);
        var data = google.visualization.arrayToDataTable( array );
        /*
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Work',     11],
          ['Eat',      2],
          ['Commute',  2],
          ['Watch TV', 2],
          ['Sleep',    7]
        ]);
        */
        var options = {
          title: 'Spot Activity'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
        chart.draw(data, options);
      }
    </script>
    <div id="piechart" style="width: 900px; height: 500px;"></div>
    
</div>

<?php   
}
    $end = round(microtime(true) * 1000);
    $stringEnd = date("M d Y H:i:s", time());
    Yii::log("**********> RENDER END [site/index.php] [{$stringEnd}] **** Ms: {$end}");
?>