<?php

/* @var $this WooadmController */
/* @var $model Oauth2User */

$this->breadcrumbs=array(
    'Especialistas'=>array('index'),
    'Manage',
);

$this->menu=array(
    array('label'=>'Manage Sessions', 'url'=>array('session')),
    array('label'=>'Manage Spots', 'url'=>array('spot')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('.search-form form').submit(function(){
    $('#especialista-grid').yiiGridView('update', {
        data: $(this).serialize()
    });
    return false;
});
");

$c = new EMongoCriteria();

$spots = Spot::model()->findAll();
$sArray= array();

foreach ($spots as $spot) {
    $sArray[] = array( '_id' => (string) $spot->_id, 'name' => $spot->name );
}

?>

<h1>Admin Users</h1>

<p>
Puedes utilizar opcionalmente (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
o <b>=</b>) al principio de cada valor buscado para modificar el tipo de comparación.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('u_search',array(
    'model'=>$model,
)); ?>
</div><!-- search-form -->
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'especialista-grid',
    'dataProvider'=> $data,
    'filter'=>$model,
    'columns'=>array(
        array(
            'name'=>'created',
            'type'=>'raw',
            'value'=>'CHtml::encode( date("d/m/Y h:i", $data->created))',
            //'filter'=>CHtml::activeTextField($model, 'e_name', array('placeholder'=>'Nombre del especialista..')),
        ),      
        array(
            'name'=>'Email',
            'type'=>'raw',
            'value'=>'CHtml::encode($data->email)',
            'filter'=>CHtml::activeTextField($model, 'email', array('placeholder'=>'User email')),
        ),
        array(
            'name'=>'User Agent',
            'type'=>'raw',
            'value'=> 'CHtml::encode($data->userAgent)',
            'filter'=>CHtml::activeTextField($model, 'userAgent', array('placeholder'=>'User agent')),
        ),
        array(
            'name' => 'Name',
            'type' => 'raw',
            'value' => 'CHtml::encode($data->details->name)',
            'filter' => CHtml::activeTextField($model->details, 'name', array('placeholder'=>'Name')),
            
        ),
        array(
            'name' => 'Lastname',
            'type' => 'raw',
            'value' => 'CHtml::encode($data->details->lastname)',
            'filter' => CHtml::activeTextField($model->details, 'lastname', array('placeholder'=>'Lastname')),
            
        ),
        array(
            'class'=>'CButtonColumn',
            'buttons'=> array(
                'view' => array(
                    'url'=>'CHtml::encode("/wooadm/userview/{$data->_id}")',
                ),
                'update' => array(
                    'url'=>'CHtml::encode("/wooadm/userupdate/{$data->_id}")',
                ),
                'delete' => array(
                    'url'=>'CHtml::encode("/wooadm/userdelete/{$data->_id}")',
                )
            ),
        ),
    ),
)); 
?>

