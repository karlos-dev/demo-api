<?php
/* @var $this EspecialistaController */
/* @var $model Especialista */



$this->breadcrumbs=array(
    'Session'=>array('session'),
    $model->_id,
);

$this->menu=array(
    array('label'=>'Manage Sessions', 'url'=>array('session')),
    //array('label'=>'Create Marker', 'url'=>array('create')),
    array('label'=>'Update Session', 'url'=>array('sessionupdate', 'id'=>$model->_id)),
    array('label'=>'Delete Session', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->_id),'confirm'=>'Are you sure you want to delete this item?')),
    //array('label'=>'Manage Marker', 'url'=>array('admin')),
);

$airs = $model->_airs ;
$str = '';
foreach ($airs as $air) {
    $str .= json_encode( $air ) . '<br>';
}


?>

<h1><?php echo $model->name; ?></h1>

<?php 
    
    $this->widget('zii.widgets.CDetailView', array(
    'data'=>$model,
    'attributes'=>array(
        '_id',
        'name',
        'numberOfAirs',
        'highestAir',
        'maxGforce',
        array(
            'label' => 'Airs',
            'type' => 'raw',
            'value' => $str,
        ), 
    ),
)); 

?>