<?php
/* @var $this WooadmController */
/* @var $model Session */


if( !$model ) {
?>

<p>This session does not exist any more</p>

<?php

} else {

    $this->breadcrumbs=array(
        'Session'=>array('session'),
        $model->_id,
    );

    $this->menu=array(
        array('label'=>'Manage Sessions', 'url'=>array('session')),
        //array('label'=>'Create Marker', 'url'=>array('create')),
        array('label'=>'Update Session', 'url'=>array('sessionupdate', 'id'=>$model->_id)),
        array('label'=>'Delete Session', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->_id),'confirm'=>'Are you sure you want to delete this item?')),
        //array('label'=>'Manage Marker', 'url'=>array('admin')),
    );

    $airs = $model->_airs ;
    $str = '';
    foreach ($airs as $air) {
        $str .= json_encode( $air ) . '<br>';
    }


    ?>

    <h1><?php echo $model->name; ?></h1>

    <?php echo $this->renderPartial('_sform', array('model'=>$model)); 

} // end else

?>