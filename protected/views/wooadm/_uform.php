<?php
/* @var $this TextoController */
/* @var $model Texto */
/* @var $form CActiveForm */

?> 

<style>
    .inline{ display: inline-block; }

    input.readonly{ background-color: rgb(223, 223, 223)!important; }
</style>

<div class="form">

<?php 
    $form=$this->beginWidget('CActiveForm', array(
        'id'=>'texto-form',
        'enableAjaxValidation'=>false,
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
         ),
    )); 
    
    //$baseUrl = Yii::app()->baseUrl; 
    //$cs = Yii::app()->getClientScript();
    //$cs->registerScriptFile($baseUrl.'/js/manager.helper.js');
    
    
?>

    <?php echo $form->errorSummary($model); ?>
    <div class="inline">
        <div class="row">
            <?php echo $form->labelEx($model->details,'name'); ?>
            <?php echo $form->textField($model->details,'name',array('size'=>32,'maxlength'=>512)); ?>
            <?php echo $form->error($model->details,'name'); ?>
        </div>
    </div>
    <div class="inline">
        <div class="row">
            <?php echo $form->labelEx($model->details,'lastname'); ?>
            <?php echo $form->textField($model->details,'lastname',array('size'=>32,'maxlength'=>512)); ?>
            <?php echo $form->error($model->details,'lastname'); ?>
        </div>
    </div>
    <div class="inline">
        <div class="row">
            <?php echo $form->labelEx($model,'email'); ?>
            <?php echo $form->textField($model,'email',array('size'=>32,'maxlength'=>512)); ?>
            <?php echo $form->error($model,'email'); ?>
        </div>
    </div>
    <div class="inline">
        <div class="row">
            <?php echo $form->labelEx($model,'userAgent'); ?>
            <?php echo $form->textField($model,'userAgent',array('size'=>32,'maxlength'=>512)); ?>
            <?php echo $form->error($model,'userAgent'); ?>
        </div>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model->details,'country'); ?>
        <?php echo $form->textField($model->details,'country',array('size'=>32,'maxlength'=>512)); ?>
        <?php echo $form->error($model->details,'country'); ?>
    </div>


    <hr />

    <div class="row buttons">
        <?php echo CHtml::submitButton($model->IsNewRecord ? 'Create' : 'Save'); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->