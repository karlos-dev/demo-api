<?php
/* @var $this WooadmController */
/* @var $model Session */


if( !$model ) {
?>

<p>This user does not exist any more</p>

<?php

} else {

    $this->breadcrumbs=array(
        'User'=>array('user'),
        $model->details->name . " " . $model->details->lastname,
    );

    $this->menu=array(
        array('label'=>'Back', 'url'=>array('index')),
        //array('label'=>'Manage Users', 'url'=>array('user')),
        //array('label'=>'Create Marker', 'url'=>array('create')),
        //array('label'=>'Update User', 'url'=>array("userupdate/{$model->_id}")),
        array('label'=>'Delete User', 'url'=>'#', 'linkOptions'=>array('submit'=>array('userdelete','id'=>$model->_id),'confirm'=>'Are you sure you want to delete this item?')),
        //array('label'=>'Manage Marker', 'url'=>array('admin')),
    );

    ?>

    <h1><?php echo $model->details->name . " " . $model->details->lastname; ?></h1>

    <?php echo $this->renderPartial('_uform', array('model'=>$model)); 

} // end else

?>