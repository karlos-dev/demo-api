<?php

/* @var $this WooadmController */
/* @var $model Session */

$this->breadcrumbs=array(
    'Woo Admin'=>array('index'),
    'Manage Sessions',
);

$this->menu=array(
    array('label'=>'Manage Users', 'url'=>array('user')),
    array('label'=>'Manage Spots', 'url'=>array('spot')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('.search-form form').submit(function(){
    $('#especialista-grid').yiiGridView('update', {
        data: $(this).serialize()
    });
    return false;
});
");

$c = new EMongoCriteria();

$spots = Spot::model()->findAll();
$sArray= array();

foreach ($spots as $spot) {
    $sArray[] = array( '_id' => (string) $spot->_id, 'name' => $spot->name );
}

?>

<h1>Admin Sessions</h1>
<!--
<p>
Puedes utilizar opcionalmente (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
o <b>=</b>) al principio de cada valor buscado para modificar el tipo de comparación.
</p>
-->
<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('s_search',array(
    'model'=>$model,
)); ?>
</div><!-- search-form -->
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'especialista-grid',
    'dataProvider'=> $data,
    'filter'=>$model,
    'columns'=>array(
        array(
            'name'=>'created',
            'type'=>'raw',
            'value'=>'CHtml::encode( date("d/m/Y h:i", $data->created))',
            //'filter'=>CHtml::activeTextField($model, 'e_name', array('placeholder'=>'Nombre del especialista..')),
        ),      

        array(
            'name'=>'name',
            'type'=>'raw',
            'value'=>'CHtml::encode($data->name)',
            'filter'=>CHtml::activeTextField($model, 'name', array('placeholder'=>'Session name')),
        ),
        
        array(
            'name'=>'_spot',
            'type'=>'raw',
            'value'=> 'CHtml::encode( Spot::model()->findByPk( $data->_spot )->name )',
            'filter'=>CHtml::listData( $sArray, '_id', 'name' ),
        ),
        /*
        array(
            'name' => 'especialidad',
            'type' => 'raw',
            'value' => 'isset($data->especialidad) ? $data->getEspecialidadNamesLinkFormat() : "null"',
            'filter' => CHtml::listData(Especialidad::model()->findAll(), 'es_id', 'es_nombre'),
            
        ),
        array(
            'name' => 'centro',
            'type' => 'raw',
            'value' => 'isset($data->centro) ? $data->getCentroNamesLinkFormat() : "null"',
            'filter' => CHtml::listData(Centro::model()->findAll(), 'c_id', 'c_name'),
            
        ),
        array(
            'name' => 'mutua',
            'type' => 'raw',
            'value' => 'isset($data->mutua) ? $data->getMutuaNamesLinkFormat() : "null"',
            'filter' => CHtml::listData(Mutua::model()->findAll(), 'm_id', 'm_name'),
            
        ),*/
        array(
            'class'=>'CButtonColumn',
            'buttons'=> array(
                'view' => array(
                    'url'=>'CHtml::encode("/wooadm/sessionview/{$data->_id}")',
                ),
                'update' => array(
                    'url'=>'CHtml::encode("/wooadm/sessionupdate/{$data->_id}")',
                ),
                'delete' => array(
                    'url'=>'CHtml::encode("/wooadm/sessiondelete/{$data->_id}")',
                )
            ),
        ),
    ),
)); ?>

