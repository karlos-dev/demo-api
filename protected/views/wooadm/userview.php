<?php

/* @var $this WooadmController */
/* @var $model Oauth2User */
/* @var $total Integer */

$this->breadcrumbs=array(
    'User'=>array('user'),
    $model->_id,
);

$this->menu=array(
    array('label'=>'Manage Users', 'url'=>array('user')),
    array('label'=>'Update User', 'url'=>array('userupdate', 'id'=>$model->_id)),
    array('label'=>'Delete User', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->_id),'confirm'=>'Are you sure you want to delete this item?')),
);



?>

<h1><?php echo $model->details->name . " " . $model->details->lastname ; ?></h1>

<?php 
    $this->widget('zii.widgets.CDetailView', array(
    'data'=>$model,
    'attributes'=>array(
        array(
            'label' => 'Since',
            'type' => 'raw',
            'value' => date('Y/m/d H:i', $model->created ),
        ), 
        '_id',
        'email',
        'userAgent',
        array(
            'label' => 'highestAir',
            'type' => 'raw',
            'value' => $model->details->highestAir,
        ), 
        array(
            'label' => 'maxAirTime',
            'type' => 'raw',
            'value' => $model->details->maxAirTime,
        ),
        array(
            'label' => 'maxGforce',
            'type' => 'raw',
            'value' => $model->details->maxGforce,
        ),
        array(
            'label' => 'Total recorded sessions',
            'type' => 'raw',
            'value' => $total,
        ), 
        array(
            'label' => 'Notifications',
            'type' => 'raw',
            'value' => (!$model->enable_notifications) ? "disabled" : "enabled",
        ), 
    ),
)); 
?>