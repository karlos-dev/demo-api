<?php
/* @var $this TextoController */
/* @var $model Texto */
/* @var $form CActiveForm */

?> 

<style>
    .inline{ display: inline-block; }

    input.readonly{ background-color: rgb(223, 223, 223)!important; }
</style>

<div class="form">

<?php 
    $form=$this->beginWidget('CActiveForm', array(
        'id'=>'texto-form',
        'enableAjaxValidation'=>false,
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
         ),
    )); 
    
    //$baseUrl = Yii::app()->baseUrl; 
    //$cs = Yii::app()->getClientScript();
    //$cs->registerScriptFile($baseUrl.'/js/manager.helper.js');
    
    
?>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'name'); ?>
        <?php echo $form->textField($model,'name',array('size'=>32,'maxlength'=>512)); ?>
        <?php echo $form->error($model,'name'); ?>
    </div>

    <hr />

<?php 
    $airs = json_encode($model->_airs);
    $n=0;
    foreach ($model->_airs as $air) {
        $n++;
?>
    <div class="row">
<?php
        echo $form->labelEx($model,"Air {$n}");
?>      
        <div class="inline">
        <label for="Session[_airs][<?php echo $n; ?>]">Height</label>
        <input size="8" type="text" name="Session[_airs][<?php echo $n; ?>]" value='<?php echo $air->height; ?>' readonly />
        </div>
        <div class="inline">
        <label for="Session[_airs][<?php echo $n; ?>]">Airtime</label>
        <input size="8" type="text" name="Session[_airs][<?php echo $n; ?>]" value='<?php echo $air->airtime; ?>' readonly />
        </div>
        <div class="inline">
        <label for="Session[_airs][<?php echo $n; ?>]">Gforce</label>
        <input size="8" type="text" name="Session[_airs][<?php echo $n; ?>]" value='<?php echo json_encode($air->gforce); ?>' readonly />
        </div>

    </div>
<?php 
    }
    $user = Oauth2User::model()->findByPk( $model->_user );
?>
    <hr />

    <div class="row">
        <label for="Session[_user][_id]">User Id</label>
        <input class='readonly' size="32" type="text" name="Session[_user][_id]" value='<?php echo $user->_id; ?>' readonly />

        <label for="Session[_user][name]">Name</label>
        <input size="32" type="text" name="Session[_user][name]" value='<?php echo $user->details->name . " " . $user->details->lastname; ?>' readonly />
        <label for="Session[_user][country]">Country</label>
        <input size="32" type="text" name="Session[_user][country]" value='<?php echo $user->details->country; ?>' readonly />
        <?php echo $form->error($model,'_user'); ?>
    </div>
    <div class="row buttons">
        <?php echo CHtml::submitButton($model->IsNewRecord ? 'Create' : 'Save'); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->