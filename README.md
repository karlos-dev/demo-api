# Rest API demo #
### Code Examples Invoice Sharing 
![InvoiceSharing](https://hollandfintech.com/wp-content/uploads/2015/07/invoice-sharing.png)

### Rest API for mobile apps, sensor clients and web clients ###

* This API code belongs to January 2016
* API version is 1.4.3

### Feeds data to: ###
* IOS Mobile app
* Android Mobile app
* Sensor Management Tools
* Session Management Tools

## Set up ##
It is mount of top of Apache Server 2.2, uses Php >5.3, Mongodb, Redis Cache, Amazon S3.

### Server ###
Set up an Apache Server with mod_rewrite enabled.  
Add a virtual host (_api.conf_) to /apache/sites-enabled/ with the following configuration,  
and restart Apache.

`

    Listen 80
    <VirtualHost *:80>
      ServerAdmin webmaster@gmail.com
      DocumentRoot "/path/to/project"
      ServerName localhost 
      SetEnv YII_HOME /var/lib/yii     
      <Directory "/path/to/project/">
        Options FollowSymLinks
        AllowOverride all
        Require all granted
      </Directory>
    
      ErrorLog "/var/log/apache2/error.apidemo.log"
      CustomLog "/var/log/apache2/access.apidemo.log" common
    
      LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{Authorization}i\" \"%{User-Agent}i\" %T" timed_combined
      CustomLog "/var/log/apache2/wood.log" timed_combined

    </VirtualHost>

### Php config ###
It requires Yii framework.  
Download [this](https://github.com/yiisoft/yii/releases/download/1.1.19/yii-1.1.19.5790cb.tar.gz) version and place it somewhere in your system.  
As defined in the virtual host `YII_HOME`, must rename the folder to: `/var/lib/yii`  


### Cache ###

**Redis** has been disabled for this demo.  
The API Infrastructure has a server instance exclusive for Redis usage.  
It is connected to the API using the AWS private networks attached to the internal 
server network interface [eth0, eth1, etc].

### Database ###

The **mongodb** Database configuration is not provided.  
We used mongoHQ.com, which was later transformed in Compose.io  
It is a costly mongodb provider (DAAS) but still a good service.
We use a master server which is replicated in other 2 slave servers.

The policy set is write op not deferred till confirmation.
This is unconfirmed writes in replica but confirmed writes in master.
The read operations take place in the whole replica set.

### Interesting files ###
* API configuration `/protected/config/main.php`
* Extensions are included in the `/protected/extensions` folder
* API code is in `/protected/modules/oauth2` folder
* AWS code is in `/protected/modules/aws` folder
* Statistics is deprecated module.  
  In its place, a separated server instance handles **API monitoring**.  
  It is connected as well using the private networking and 
  has set up certificates for message encoding.  
  The tech stack I used for API monitoring is ElasticSearch on top of Logstash and rsyslog.


### Tests ###
Tests are a separated suite built in python by myself.   
I use receipt oriented methodology [puppet, fabric] and create a request to every endpoint.
This suite runs:  
  
* Acceptance. Check the quality and correctness of endpoint responses   
* Stress. Try to break the server to check request throttle policies  
* Load. Tests on create (write) operations  

There exist a previous [Codeption](http://codeception.com/quickstart) test suite (which is great) 
that we used for early stages of API development.  
Later we decided to split the testing to a separated suite.

It is possible to run the tests (no real testing thus no db)  
`php codecept.phar run`

### Documentation ###

The documentation for Api **client developers** is accessible once the server is running, by visiting:  
`http://localhost/doc/api-doc/`  

The **code documentation** was generated using [apigen](https://github.com/ApiGen/ApiGen). There, one can find documentation about Php files (Models and Controllers Actions).  
Opening the file:  
`/path/to/project/doc/tree.html`  
or, if the server is running, visit:  
`http://localhost/doc/`  


### Contact ###

Suggestions, concerns:  
_Karlos Álvarez_  
karlos.alvan@gmail.com