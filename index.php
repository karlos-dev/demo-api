<?php

/**
 *   Mandatory Environment variables 
 */

$yii_home = getenv('YII_HOME');

if( !$yii_home ) {
  echo 'No YII_HOME set in user environment.';
  exit;
}

// change the following paths if necessary
$yii = $yii_home . '/framework/yii.php';
$config=dirname(__FILE__).'/protected/config/main.php';

// debug the application enabled
defined('YII_DEBUG') or define('YII_DEBUG', true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);


require_once($yii);
Yii::createWebApplication($config)->run();
