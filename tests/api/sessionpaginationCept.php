<?php
$I = new ApiGuy($scenario);
$I->wantTo('List my sessions in a concrete Spot using pagination');
$I->haveHttpHeader('Content-Type','application/x-www-form-urlencoded');

$I->sendPOST( $version . '/user/login', $user['karlos'] );
$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();
$I->seeResponseContains('access_token');

$tokenJSON = $I->grabResponse();
$phpTokenString = json_decode($tokenJSON, true);
$token = $phpTokenString['access_token'];

$I->sendGET($version . '/user/profile/', array( "token"=>$token ) );
$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();
$I->seeResponseContains($user['karlos']['email']);

$I[ 'This is the received token screen '.$tokenJSON ];
$I[ 'This is the parsed token '.$token ];

$I->wantTo('See my last session in Tarifa Spot');
$I->sendPOST($version . '/session/byspot/52a85581f6d35e481400002e?token=' . $token , array( "offset"=>0, "pageSize"=>1 ) );
$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();
$I->seeResponseContains('[');
$I->seeResponseContains(']');

?>
