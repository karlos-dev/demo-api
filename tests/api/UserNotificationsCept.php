<?php

$I = new ApiGuy($scenario);
$I->wantTo('Check user\'s notifications');

$I->haveHttpHeader('Content-Type','application/x-www-form-urlencoded');
$I->sendPOST( $version . '/user/login', $user['karlos'] );
$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();
$I->seeResponseContains('access_token');

$tokenJSON = $I->grabResponse();
$phpTokenString = json_decode($tokenJSON, true);
$token = $phpTokenString['access_token'];

$I[ 'This is the received token screen '.$tokenJSON ];
$I[ 'This is the parsed token '.$token ];

$I->wantTo('Get my profile');
// $I->sendPOST('/user/profile?token=' . $token , array('pageSize' => 50, 'offset' => 0) );
$I->sendGET($version . '/user/profile/', array( "token"=>$token ) );
$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();

$userProfile = $I->grabResponse();
$upCode = json_decode($userProfile);
$I['Have this user profile  '. $userProfile ];

if( isset($upCode->enable_notifications) && $upCode->enable_notifications ){
    $I->sendPOST('/user/notifications?token=' . $token , array('status' => 'disable') );
    $I->seeResponseCodeIs(200);
    $I->seeResponseIsJson();
    $response = $I->grabResponse();
    $I->seeResponseContains('disable');

} else{
    $I->sendPOST('/user/notifications?token=' . $token , array('status' => 'enable') );
    $I->seeResponseCodeIs(200);
    $I->seeResponseIsJson();
    $response = $I->grabResponse();
    $I->seeResponseContains('enable');
}

$I['Notifications work fine'];

?>