<?php
$I = new ApiGuy($scenario);
$I->wantTo('Ensure my tokens are invalid after logout');
$I->haveHttpHeader('Content-Type','application/x-www-form-urlencoded');


$I->sendPOST( $version . '/user/login', $user['karlos'] );
$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();
$I->seeResponseContains('access_token');

$tokenJSON = $I->grabResponse();
$phpTokenString = json_decode($tokenJSON, true);
$token = $phpTokenString['access_token'];

$I[ 'This is the received token screen '.$tokenJSON ];
$I[ 'This is the parsed token '.$token ];

$I->wantTo('See my user profile');
$I->sendGET($version .'/user/profile/', array( "token"=>$token ) );
$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();
$I->seeResponseContains('email');
$I->seeResponseContains($user['karlos']['email']);

$I->wantTo('make logout');
$I->sendGET($version .'/user/logout/', array( "token"=>$token ) );
$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();
$I->seeResponseContains('ok');
$I->seeResponseContains('logged out');
$I[ 'I\'m logged out ' ];

$I->wantTo('Ensure my token is not useful anymore');
$I->sendGET($version .'/user/logout/', array( "token"=>$token ) );
$I->seeResponseCodeIs(401);
//$I->seeResponseIsJson();
$I->seeResponseContains('invalid_token');

?>
