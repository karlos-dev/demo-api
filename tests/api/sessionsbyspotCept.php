<?php
$I = new ApiGuy($scenario);
$I->wantTo('List my sessions in a concrete Spot');
$I->haveHttpHeader('Content-Type','application/x-www-form-urlencoded');

$I->sendPOST( $version . '/user/login', $user['karlos'] );
$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();
$I->seeResponseContains('access_token');

$tokenJSON = $I->grabResponse();
$phpTokenString = json_decode($tokenJSON, true);
$token = $phpTokenString['access_token'];

$I[ 'This is the received token screen '.$tokenJSON ];
$I[ 'This is the parsed token '.$token ];

$I->wantTo('See all my sessions in Tarifa Spot');
$I->sendGET($version . '/session/byspot/52a85581f6d35e481400002e', array( "token"=>$token ) );
$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();
$I->seeResponseContains('[');
$I->seeResponseContains(']');

?>
