<?php

$I = new ApiGuy($scenario);
$I->wantTo('Assign a sensor');

$I->haveHttpHeader('Content-Type','application/x-www-form-urlencoded');
$I->sendPOST( $version . '/user/login', $user['karlos'] );
$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();
$I->seeResponseContains('access_token');

$tokenJSON = $I->grabResponse();
$phpTokenString = json_decode($tokenJSON, true);
$token = $phpTokenString['access_token'];

$I[ 'This is the received token screen '.$tokenJSON ];
$I[ 'This is the parsed token '.$token ];

$I->sendPOST($version . '/sensor/assign', 
    array( 
        "token"=>$token,
        "wooid"=>'woo-0000-ccc2',
        "fwv"=>'0.1.234',
));

$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();
$I->seeResponseContains('status');
$I->seeResponseContains('ok');


// acceptance
// the user profile must have one sensor id at least in the sensors array
// must know the id of the assigned sensor

$I->sendGET($version .  '/user/profile', array( "token"=>$token ) );
$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();

$profile = $I->grabResponse();
$profileObj = json_decode($profile, true);





?>