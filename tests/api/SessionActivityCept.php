<?php
$I = new ApiGuy($scenario);
$I->wantTo('Check session activity endpoint');
$I->haveHttpHeader('Content-Type','application/x-www-form-urlencoded');

$I->sendPOST( $version . '/user/login', $user['karlos'] );
$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();
$I->seeResponseContains('access_token');

$tokenJSON = $I->grabResponse();
$phpTokenString = json_decode($tokenJSON, true);
$token = $phpTokenString['access_token'];

$I[ 'This is the received token screen '.$tokenJSON ];
$I[ 'This is the parsed token '.$token ];

$I->wantTo('Get session activity for me');
$I->sendPOST('/session/activity?token=' . $token , array( 'target'=>'me', 'pageSize' => 50, 'offset' => 0) );
$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();
$I->seeResponseContains('[');
$I->seeResponseContains(']');

$I->wantTo('Get session activity for the followed users');
$I->sendPOST('/session/activity?token=' . $token , array( 'target'=>'following', 'pageSize' => 50, 'offset' => 0) );
$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();
$I->seeResponseContains('[');
$I->seeResponseContains(']');

$I->wantTo('Get session activity for the all the community');
$I->sendPOST('/session/activity?token=' . $token , array( 'target'=>'community', 'pageSize' => 50, 'offset' => 0) );
$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();
$I->seeResponseContains('[');
$I->seeResponseContains(']');

?>