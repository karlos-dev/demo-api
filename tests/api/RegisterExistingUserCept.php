<?php
$I = new ApiGuy($scenario);
$I->wantTo('Register Existing User');
$I->haveHttpHeader('Content-Type','application/x-www-form-urlencoded');

$name = 'Karlos';
$lastName = 'Álvarez';

$I->sendPOST($version.'/register', array(
    'email'=> 'milnomada@gmail.com', 
    'pass'=>'ae1ead841c8075515b2684b8e0fb710def5db2ca6e2925c65d9445070058764b93d384c901cee5b21cdf24d2e6ae94fa1af78dd34d06b29a4fbb5f2804e03398' , 
    'client_id' => '82886a948a1c63e861be8cc4855d9c515f3a352d096dd25fe015f85cd9123de5df36a11ec260b47d9500898e4cde5c19feac79c3e74683f1879f8560fd496d17',
    'client_secret' => '8119cd9da2faf75a5cdd0c0ae7ea3b41ae4c31ab54335029e03ff070c7d0fb7f2daac55bb32575e1b51a33480cd8ca10eb42b3d559801d9a8a7329d40395d5d6',
    'redirect_uri'=> '/user/profile',
    'name'=>$name,
    'lastname'=>$lastName,
    'country'=>'Spain',
    'deviceId'=>'',
    'birthday'=>'16/03/1983',
    'gender'=>'male',
));

$I->seeResponseCodeIs(400);
$I->seeResponseIsJson();

$I->seeResponseContains('error');
$I->seeResponseContains('already exist');

?>
