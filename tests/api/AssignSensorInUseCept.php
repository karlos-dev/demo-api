<?php

$I = new ApiGuy($scenario);
$I->wantTo('Assign a sensor already in use');

$I->haveHttpHeader('Content-Type','application/x-www-form-urlencoded');
$I->sendPOST( $version . '/user/login', $user['karlos'] );
$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();
$I->seeResponseContains('access_token');

$tokenJSON = $I->grabResponse();
$phpTokenString = json_decode($tokenJSON, true);
$token = $phpTokenString['access_token'];

$I[ 'This is the received token screen '.$tokenJSON ];
$I[ 'This is the parsed token '.$token ];

$I->sendPOST($version . '/sensor/assign', 
    array( 
        "token"=>$token,
        "wooid"=>'woo-0000-ccc2',
        "fwv"=>'0.1.234',
));

$I->seeResponseCodeIs(400);

$I->haveHttpHeader('Content-Type','application/x-www-form-urlencoded');
$I->sendPOST( $version . '/user/login', $user['ben'] );
$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();
$I->seeResponseContains('access_token');

$tokenJSON = $I->grabResponse();
$phpTokenString = json_decode($tokenJSON, true);
$token = $phpTokenString['access_token'];

$I[ 'This is the received token screen '.$tokenJSON ];
$I[ 'This is the parsed token '.$token ];

$I->sendPOST($version . '/sensor/assign', 
    array( 
        "token"=>$token,
        "wooid"=>'woo-0000-ccc2',
        "fwv"=>'0.1.234',
));

$I->seeResponseCodeIs(400);


?>