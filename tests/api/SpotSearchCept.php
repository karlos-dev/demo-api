<?php

$I = new ApiGuy($scenario);
$I->wantTo('Check the spot search');
$I->haveHttpHeader('Content-Type','application/x-www-form-urlencoded');

$I->sendPOST( $version . '/user/login', $user['karlos'] );
$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();
$I->seeResponseContains('access_token');

$tokenJSON = $I->grabResponse();
$phpTokenString = json_decode($tokenJSON, true);
$token = $phpTokenString['access_token'];

$I[ 'This is the received token screen '.$tokenJSON ];
$I[ 'This is the parsed token '.$token ];


$I->sendPOST($version . '/spot/search', array( 
    "query"=>"tari",
    "token"=>$token ) );

$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();

$I->seeResponseContains('tari');

?>