<?php

$I = new ApiGuy($scenario);
$I->wantTo('Unassign a sensor');

$I->haveHttpHeader('Content-Type','application/x-www-form-urlencoded');
$I->sendPOST( $version . '/user/login', $user['karlos'] );
$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();
$I->seeResponseContains('access_token');

$tokenJSON = $I->grabResponse();
$phpTokenString = json_decode($tokenJSON, true);
$token = $phpTokenString['access_token'];

$I[ 'This is the received token screen '.$tokenJSON ];
$I[ 'This is the parsed token '.$token ];

$I->sendPOST($version . '/sensor/unassign', 
    array( 
        "token"=>$token,
        "wooid"=>'woo-0000-ccc2',
));

$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();

$I->seeResponseContains('status');
$I->seeResponseContains('ok');



?>