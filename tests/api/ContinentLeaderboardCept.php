<?php

$I = new ApiGuy($scenario);
$I->wantTo('See any continent leaderboard');
$I->haveHttpHeader('Content-Type','application/x-www-form-urlencoded');

$I->sendPOST( $version . '/user/login', $user['karlos'] );
$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();
$I->seeResponseContains('access_token');

$tokenJSON = $I->grabResponse();
$phpTokenString = json_decode($tokenJSON, true);
$token = $phpTokenString['access_token'];

$I[ 'This is the received token screen '.$tokenJSON ];
$I[ 'This is the parsed token '.$token ];


foreach (array('north-america', 'africa', 'asia', 'south-america', 'europe', 'oceania', 'all' ) as $continentName ) {
    $I[''];
    $I[ 'Check leaderboard in continent '.$continentName ];

    $I->sendPOST('/continent/leaderboard/' . $continentName . '?token=' . $token , array( 'target'=>'community', 'pageSize' => 50, 'offset' => 0) );
    $I->seeResponseCodeIs(200);
    $I->seeResponseIsJson();
    

    $I->seeResponseContains('[');
    $I->seeResponseContains(']');
    /*
    $sessionArrayJson = $I->grabResponse();

    $I['Show response '. $sessionArrayJson ];
    $sessions = array();
    $sessionArray = json_decode($sessionArrayJson, true);
    $sessions = $sessionArray['items'];
    */
}

?>