<?php

$I = new ApiGuy($scenario);
$I->wantTo('Check continent activity endpoint');

$I->haveHttpHeader('Content-Type','application/x-www-form-urlencoded');
$I->sendPOST( $version . '/user/login', $user['karlos'] );
$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();
$I->seeResponseContains('access_token');

$tokenJSON = $I->grabResponse();
$phpTokenString = json_decode($tokenJSON, true);
$token = $phpTokenString['access_token'];

$I[ 'This is the received token screen '.$tokenJSON ];
$I[ 'This is the parsed token '.$token ];

foreach (array('north-america', 'africa', 'asia', 'south-america', 'europe', 'oceania' ) as $continentName ) {
    $I[''];
    $I[ 'Check activity in continent '.$continentName ];

    $I->wantTo('Get session activity for following users');
    $I->sendPOST('/continent/activity/' . $continentName . '?token=' . $token , array('target'=>'following', 'pageSize' => 50, 'offset' => 0) );
    $I->seeResponseCodeIs(200);
    $I->seeResponseIsJson();
    $I->seeResponseContains('[');
    $I->seeResponseContains(']');

    $I->wantTo('Get session activity for me');
    $I->sendPOST('/continent/activity/' . $continentName . '?token=' . $token , array('target'=>'me', 'pageSize' => 50, 'offset' => 0) );
    $I->seeResponseCodeIs(200);
    $I->seeResponseIsJson();
    $I->seeResponseContains('[');
    $I->seeResponseContains(']');

    $I->wantTo('Get session activity for me');
    $I->sendPOST('/continent/activity/' . $continentName . '?token=' . $token , array('target'=>'community', 'pageSize' => 50, 'offset' => 0) );
    $I->seeResponseCodeIs(200);
    $I->seeResponseIsJson();
    $I->seeResponseContains('[');
    $I->seeResponseContains(']');
}

?>