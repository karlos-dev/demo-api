<?php

$I = new ApiGuy($scenario);
$I->wantTo('List my sessions in a concrete Spot');
$I->haveHttpHeader('Content-Type','application/x-www-form-urlencoded');

$I->sendPOST( $version . '/user/login', $user['karlos'] );
$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();
$I->seeResponseContains('access_token');

$tokenJSON = $I->grabResponse();
$phpTokenString = json_decode($tokenJSON, true);
$token = $phpTokenString['access_token'];

$I[ 'This is the received token screen '.$tokenJSON ];
$I[ 'This is the parsed token '.$token ];

$otherUser = '52e2bbc0f6d35e8810000041';

$I->wantTo('See all sessions in for user id: ' + $otherUser);

//$I->sendGET($version . '/session/' . $otherUser , array( "token"=>$token ) );
$I->sendPOST('/session?token=' . $token , array( 'target'=>'community', 'pageSize' => 50, 'offset' => 0) );

// $I->sendGET('/session/' , array( "token"=>$token ) );
 
$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();

$sessionArrayJson = $I->grabResponse();

$I['Show response '. $sessionArrayJson ];
$sessions = array();
$sessionArray = json_decode($sessionArrayJson, true);
$sessions = $sessionArray['items'];

//$I['Check session list: ' . json_encode($sessions)];

$I[''];	
if( is_array($sessions) )
	foreach($sessions as $session) {	
		$I['Check single session'];	

		$I['Check session spot: '. (string) $session['_spot']['$id'] ];
		$I->sendGET($version . '/spot/' . (string) $session['_spot']['$id'] , array( "token"=>$token ) );
		$I->seeResponseCodeIs(200);

		$I['Check session user: '. (string) $session['_user']['$id'] ];
		$I->sendGET($version . '/user/profile/' . (string) $session['_user']['$id'] , array( "token"=>$token ) );
		$I->seeResponseCodeIs(200);

		$I['Check number of comments of session: '. (string) $session['_id']['$id'] ];
		$I->sendGET($version . '/comment/' . (string) $session['_id']['$id'] , array( "token"=>$token ) );
		$I->seeResponseCodeIs(200);
		$commentJson = $I->grabResponse();

		$commentArray = json_decode($commentJson, true);
		$comments = $commentArray['items'];

		if( count( $comments ) == $session['totalcomments'] )
			$I[ 'Session ' . $session['_id']['$id'] .  ' is consistent in [Spot] [User] [Comment]'];	

		$I[''];	

	}

?>