<?php

$I = new ApiGuy($scenario);
$I->wantTo('Check Integrity of: ');
$I->haveHttpHeader('Content-Type','application/x-www-form-urlencoded');

$I->sendPOST( $version . '/user/login', $user['ben'] );
$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();
$I->seeResponseContains('access_token');

$tokenJSON = $I->grabResponse();
$phpTokenString = json_decode($tokenJSON, true);
$token = $phpTokenString['access_token'];

$I['This is the received token screen '.$tokenJSON ];
$I['This is the parsed token '.$token ];

$I->wantTo('Check the USER database consistency');
$I->sendGET($version .  '/user/profile', array( "token"=>$token ) );
$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();
$I->seeResponseContains('details');

$profile = $I->grabResponse();
$profileJSON = json_decode($profile, true);

// deprecated since friends feature is not active any more
// $userFriends = $profileJSON['_friends'];
$userFollow = $profileJSON['_followed'];
$userFollowedBy = $profileJSON['_followedBy'];

// deprecated since friends feature is not active any more
// $nFr = ($userFriends !== null) ? count($userFriends) : 0;
$nFollow = ($userFollow !== null) ? count($userFollow) : 0;
$nFollowBy = ($userFollowedBy !== null) ? count($userFollowedBy) : 0;

// $I['This user has '.$nFr. ' friends.'];
$I['This user follows '.$nFollow. ' users.'];
$I['This user is followed by '.$nFollowBy. ' users.'];

$I[''];
$I['Check followed users '. $userFollow ];

if( $userFollow )
	foreach($userFollow as $uf ) {
		$I[''];
		$I['Check user '. $uf ];
		
		$I->sendGET($version .  '/user/profile/' . $uf, array( "token"=>$token ) );	
		$I->seeResponseCodeIs(200);
		$I->seeResponseIsJson();
		$I->seeResponseContains('details');
		$prof = $I->grabResponse();
		$profJSON = json_decode($prof, true);

		$ufollowedBy = $profJSON['_followedBy'];
		foreach($ufollowedBy as $ufb ) { 		
			if( $ufb == $profileJSON['_id']['$id'] ) {
				$I['User '. $uf . ' actually follows ' . $ufb ];
				break;
			}
		}
	}

$I[''];
$I['Check users following '. $userFollow ];

if( $userFollow )
	foreach($userFollow as $uf ) {
		$I[''];
		$I['Check user '. $uf ];
		
		$I->sendGET($version .  '/user/profile/' . $uf, array( "token"=>$token ) );	
		$I->seeResponseCodeIs(200);
		$I->seeResponseIsJson();
		$I->seeResponseContains('details');
		$prof = $I->grabResponse();
		$profJSON = json_decode($prof, true);

		$ufollowedBy = $profJSON['_followed'];
		foreach($ufollowedBy as $ufb ) { 		
			if( $ufb == $profileJSON['_id']['$id'] ) {
				$I['User '. $uf . ' is followed by ' . $ufb ];
				break;
			}
		}
	}



?>
