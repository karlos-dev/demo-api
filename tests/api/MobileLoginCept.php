<?php

$I = new ApiGuy($scenario);
$I->wantTo('Login into API as a mobile device');
$I->haveHttpHeader('Content-Type','application/x-www-form-urlencoded');

$I->sendPOST( $version . '/user/login', $user['karlos'] );
$I->seeResponseCodeIs(200);
$I->seeResponseIsJson();
$I->seeResponseContains('access_token');

?>
